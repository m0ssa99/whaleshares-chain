#include <wls/plugins/acc_hist_nudb/acc_hist_nudb_objects.hpp>
#include <wls/plugins/acc_hist_nudb/nudbstore_database.hpp>
#include <appbase/application.hpp>

namespace wls {
namespace plugins {
namespace acc_hist_nudb {

auto const nudbstore_dat_path = "db.dat";
auto const nudbstore_key_path = "db.key";
auto const nudbstore_log_path = "db.log";

const string get_path(const string &filepath) {
  string data_dir = appbase::app().data_dir().string();
  if (data_dir == "") {
    data_dir = ".";
  }
  string result = data_dir + "/plugins/acc_hist_nudb/" + filepath;
  return result;
}

void acc_hist_nudb::nudbstore_database::open() {
  error_code ec;

  if (!fc::exists(get_path(nudbstore_dat_path))) {
    boost::filesystem::create_directories(get_path(""));
    auto key_size = acc_hist_object_nudb_key().data_size(); // fc::raw::pack_size(acc_hist_object_nudb_key());
    uint64_t appnum = 1;
    uint64_t salt = nudb::make_salt();
    auto blockSize = nudb::block_size(".");
    float load_factor = 0.5f;
    // clang-format off
    nudb::create<nudb::xxhasher>(
        get_path(nudbstore_dat_path),
        get_path(nudbstore_key_path),
        get_path(nudbstore_log_path),
        appnum,
        salt,
        key_size,
        blockSize,
        load_factor,
        ec);
    // clang-format on
    FC_ASSERT(!ec, "${msg}", ("msg", ec.message()));
  }

  nudbstore.open(get_path(nudbstore_dat_path), get_path(nudbstore_key_path), get_path(nudbstore_log_path), ec);
  FC_ASSERT(!ec, "${msg}", ("msg", ec.message()));
}

void nudbstore_database::close() {
  error_code ec;
  nudbstore.close(ec);
  FC_ASSERT(!ec, "${msg}", ("msg", ec.message()));
}

void nudbstore_database::clean() {
  nudb::erase_file(get_path(nudbstore_dat_path));
  nudb::erase_file(get_path(nudbstore_key_path));
  nudb::erase_file(get_path(nudbstore_log_path));
}

void nudbstore_database::insert(const acc_hist_object_nudb_key &key, const acc_hist_object_nudb& item, error_code& ec) {
  // pack the data
  size_t packed_data_size = fc::raw::pack_size(item);
  char packed_data[packed_data_size];
  fc::raw::pack<acc_hist_object_nudb>(packed_data, packed_data_size, item);

  //--
//  error_code ec;
  nudbstore.insert(key.data(), packed_data, packed_data_size, ec);
//  FC_ASSERT(!ec, "${msg}", ("msg", ec.message()));
}

const acc_hist_object_nudb nudbstore_database::fetch(const acc_hist_object_nudb_key& key, error_code& ec) {
//  error_code ec;
  acc_hist_object_nudb result;
  // clang-format off
  nudbstore.fetch(key.data(),
      [&result](void const* buffer, std::size_t size) {
          fc::raw::unpack<acc_hist_object_nudb>((const char*)buffer, size, result, 0);
      },
      ec);
  // clang-format on
  FC_ASSERT(!ec, "${msg}", ("msg", ec.message()));
  return result;
}

}  // namespace acc_hist_nudb
}  // namespace plugins
}  // namespace wls
