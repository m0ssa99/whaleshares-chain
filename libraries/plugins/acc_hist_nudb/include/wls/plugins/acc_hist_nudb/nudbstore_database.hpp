#pragma once

#include <wls/plugins/acc_hist_nudb/acc_hist_nudb_objects.hpp>

#include <nudb/nudb.hpp>

namespace wls {
namespace plugins {
namespace acc_hist_nudb {

using boost::system::error_code;

/**
 * nudbstore_database is the store to insert account activity log and to query
 */
class nudbstore_database {
 public:
  nudbstore_database() {}

  virtual ~nudbstore_database() {}

  void open();

  void close();

  // delete database
  void clean();

  void insert(const acc_hist_object_nudb_key &key, const acc_hist_object_nudb &item, error_code& ec);

  const acc_hist_object_nudb fetch(const acc_hist_object_nudb_key& key, error_code& ec);

  //
  nudb::store nudbstore;
};

}  // namespace acc_hist_nudb
}  // namespace plugins
}  // namespace wls