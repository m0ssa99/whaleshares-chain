#pragma once

#include <wls/chain/wls_object_types.hpp>
#include <wls/plugins/database_api/database_api_objects.hpp>

#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace acc_hist_nudb {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;
using wls::plugins::database_api::applied_operation;
using acc_hist_object_nudb = applied_operation;

class acc_hist_object_nudb_key {
 public:
  acc_hist_object_nudb_key(const account_name_type &_account, const uint32_t _sequence)
  : account(_account), sequence(_sequence) {}

  acc_hist_object_nudb_key() {}

  const char *data() const {
    wls::protocol::digest_type::encoder enc;
    fc::raw::pack(enc, *this);
    wls::protocol::digest_type digest_data = enc.result();
    std::memcpy((void *)_data, digest_data._hash, std::min(data_size(), sizeof(digest_data)));

//    std::memset((void *)_data, 0, 20);
//    std::memcpy((void *)_data, &account.data, account.length());
//    std::memcpy((void *)(_data+16), &sequence, 4);

    return _data;
  }
  size_t data_size() const { return 20; }

  account_name_type account;
  uint32_t sequence = 0;

// private:
  char _data[20]; // raw data holder, 16 bytes for account name and 4 bytes for sequence
};

//------------------------------------------------------------------------------

#ifndef ACCOUNT_HISTORY_NUDB_SPACE_ID
#define ACCOUNT_HISTORY_NUDB_SPACE_ID 15
#endif

enum acc_hist_nudb_object_types {
  acc_hist_nudb_state_object_type = (ACCOUNT_HISTORY_NUDB_SPACE_ID << 8),
  acc_hist_nudb_head_object_type = (ACCOUNT_HISTORY_NUDB_SPACE_ID << 8) + 1
};

/**
 * to keep state of the plugin so it knows where to continue after restarting
 */
class acc_hist_nudb_state_object : public object<acc_hist_nudb_state_object_type, acc_hist_nudb_state_object> {
 public:
  template <typename Constructor, typename Allocator>
  acc_hist_nudb_state_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  uint32_t head_block_number = 0;
  uint32_t last_irreversible_block_num = 0;
  uint32_t processed_block_number = 0;
};

// clang-format off
typedef multi_index_container<
    acc_hist_nudb_state_object,
    indexed_by<
        ordered_unique< tag< by_id >,
            member<
                acc_hist_nudb_state_object,
                acc_hist_nudb_state_object::id_type,
                &acc_hist_nudb_state_object::id
            >
        >
    >,
    allocator< acc_hist_nudb_state_object >
> acc_hist_nudb_state_index;
// clang-format on

/**
 * to track last sequence of an account ( the head of the snake )
 */
class acc_hist_nudb_head_object : public object<acc_hist_nudb_head_object_type, acc_hist_nudb_head_object> {
 public:
  template <typename Constructor, typename Allocator>
  acc_hist_nudb_head_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  account_name_type account;
  uint32_t sequence = 0;
};

typedef acc_hist_nudb_head_object::id_type acc_hist_head_id_type;

// clang-format off
struct by_account;
typedef multi_index_container<
    acc_hist_nudb_head_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< acc_hist_nudb_head_object, acc_hist_head_id_type, &acc_hist_nudb_head_object::id > >,
        ordered_unique< tag< by_account >, member< acc_hist_nudb_head_object, account_name_type, &acc_hist_nudb_head_object::account > >
    >,
    allocator< acc_hist_nudb_head_object >
> acc_hist_nudb_head_index;
// clang-format on

}  // namespace acc_hist_nudb
}  // namespace plugins
}  // namespace wls


FC_REFLECT(wls::plugins::acc_hist_nudb::acc_hist_object_nudb_key, (account)(sequence))

FC_REFLECT(wls::plugins::acc_hist_nudb::acc_hist_nudb_state_object,
           (id)(head_block_number)(last_irreversible_block_num)(processed_block_number))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::acc_hist_nudb::acc_hist_nudb_state_object,
                         wls::plugins::acc_hist_nudb::acc_hist_nudb_state_index)

FC_REFLECT(wls::plugins::acc_hist_nudb::acc_hist_nudb_head_object, (id)(account)(sequence))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::acc_hist_nudb::acc_hist_nudb_head_object,
                         wls::plugins::acc_hist_nudb::acc_hist_nudb_head_index)
