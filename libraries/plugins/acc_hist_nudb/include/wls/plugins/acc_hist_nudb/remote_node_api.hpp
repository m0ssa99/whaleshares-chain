#pragma once

#include <fc/api.hpp>

#include <wls/plugins/database_api/database_api_objects.hpp>
#include <wls/plugins/database_api/database_api_args.hpp>

namespace wls {
namespace plugins {
namespace acc_hist_nudb {

using fc::optional;
using fc::variant;
using std::vector;

using namespace plugins;
using namespace plugins::database_api;

/**
 * This is a dummy class exists only to provide method signature information to fc::api, not to execute calls.
 * Class is used by lightnode to send formatted API calls to database_api plugin on remote node.
 */
struct remote_database_api {
  dynamic_global_property_api_obj get_dynamic_global_properties();
  vector<applied_operation> get_ops_in_block(uint32_t block_num, bool only_virtual = true);
  vector<applied_operation> get_ops_in_blocks(uint32_t from, uint32_t limit, bool only_virtual = true);
};

}  // namespace acc_hist_nudb
}  // namespace plugins
}  // namespace wls

// clang-format off
FC_API(wls::plugins::acc_hist_nudb::remote_database_api, (get_dynamic_global_properties)(get_ops_in_block)(get_ops_in_blocks))
// clang-format on
