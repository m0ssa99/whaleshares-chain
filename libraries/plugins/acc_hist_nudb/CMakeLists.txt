file(GLOB HEADERS "include/wls/plugins/acc_hist_nudb/*.hpp")

add_library(acc_hist_nudb_plugin
        acc_hist_nudb_plugin.cpp
        nudbstore_database.cpp
        )

target_link_libraries(acc_hist_nudb_plugin database_api_plugin chain_plugin wls_chain wls_protocol wls_utilities appbase fc)
target_include_directories(acc_hist_nudb_plugin PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/include"
        "${CMAKE_CURRENT_SOURCE_DIR}/../../vendor/NuDB/include"
        )

install(TARGETS
        acc_hist_nudb_plugin

        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        )
