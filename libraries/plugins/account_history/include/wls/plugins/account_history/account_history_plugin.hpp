#pragma once

#include <appbase/application.hpp>

#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>
#include <wls/plugins/account_history/account_history_objects.hpp>
#include <wls/nudbstore/nudbstore.hpp>

#define ACCOUNT_HISTORY_PLUGIN_NAME "account_history"

namespace wls {
namespace plugins {
namespace account_history {

namespace detail {
class account_history_plugin_impl;
}

using namespace appbase;
using wls::protocol::account_name_type;
using nudbstore::nudbstore;

class account_history_plugin : public plugin<account_history_plugin> {
 public:
  account_history_plugin();

  virtual ~account_history_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin)(wls::plugins::block_history::block_history_plugin))

  static const std::string &name() {
    static std::string name = ACCOUNT_HISTORY_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  const account_history_state_head_object* get_account_history_state_head_object(const account_name_type &account);

  nudbstore<account_history_key, account_history_object>& get_nudbstore_database();

  // friend class detail::account_history_plugin_impl;

 private:
  std::unique_ptr<detail::account_history_plugin_impl> my;
};

}  // namespace account_history
}  // namespace plugins
}  // namespace wls
