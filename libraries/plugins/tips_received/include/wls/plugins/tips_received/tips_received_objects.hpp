#pragma once

#include <wls/chain/wls_object_types.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace tips_received {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;

class tips_received_key {
 public:
  tips_received_key(const account_name_type &_account, const uint32_t _sequence)
      : account(_account), sequence(_sequence) {}

  tips_received_key() {}

  account_name_type account;
  uint32_t sequence = 0;
};

typedef wls::plugins::block_history::block_history_applied_operation tips_received_object;

//------------------------------------------------------------------------------

#ifndef TIPS_RECEIVED_SPACE_ID
#define TIPS_RECEIVED_SPACE_ID 21
#endif

enum tips_received_object_types {
  tips_received_state_object_type = (TIPS_RECEIVED_SPACE_ID << 8)
};

/**
 * to track last sequence of an account ( the head of the snake )
 */
class tips_received_state_head_object : public object<tips_received_state_object_type, tips_received_state_head_object> {
 public:
  template <typename Constructor, typename Allocator>
  tips_received_state_head_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  account_name_type account;
  uint32_t sequence = 0;
};

//------------------------------------------------------------------------------

typedef tips_received_state_head_object::id_type tips_received_state_head_object_id_type;

// clang-format off
struct by_account;
typedef multi_index_container<
    tips_received_state_head_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< tips_received_state_head_object, tips_received_state_head_object_id_type, &tips_received_state_head_object::id > >,
        ordered_unique< tag< by_account >, member< tips_received_state_head_object, account_name_type, &tips_received_state_head_object::account > >
    >,
    allocator< tips_received_state_head_object >
> tips_received_state_head_object_index;
// clang-format on


}  // namespace tips_received
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::tips_received::tips_received_key, (account)(sequence))
FC_REFLECT(wls::plugins::tips_received::tips_received_state_head_object, (id)(account)(sequence))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::tips_received::tips_received_state_head_object, wls::plugins::tips_received::tips_received_state_head_object_index)
