# snapshot plugin

when enabled, this plugin creates a snapshot of accounts for every n blocks

- Every `1'000'000` blocks or ~35 days
- Format in text eg., `JSON` or `CSV`
- data is exported to `plugins/snapshot` folder