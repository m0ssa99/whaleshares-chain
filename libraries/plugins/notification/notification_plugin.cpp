#include <wls/plugins/notification/notification_plugin.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/protocol/config.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/history_object.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/chain/util/signal.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>
#include <fc/macros.hpp>
#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace notification {

using namespace wls::protocol;

using chain::database;
using chain::operation_notification;
using chain::operation_object;

namespace detail {

struct get_impacted_account_visitor {
  flat_set<account_name_type>& _impacted;
  get_impacted_account_visitor(flat_set<account_name_type>& impact) : _impacted(impact) {}
  typedef void result_type;

  template <typename T>
  void operator()(const T& op) {
//    op.get_required_posting_authorities(_impacted);
//    op.get_required_active_authorities(_impacted);
//    op.get_required_owner_authorities(_impacted);
  }

  // ops
//  void operator()(const account_create_operation& op) {
//    _impacted.insert(op.new_account_name);
//    _impacted.insert(op.creator);
//  }

//  void operator()(const comment_operation& op) {
//    _impacted.insert(op.author);
//    if (op.parent_author.size()) _impacted.insert(op.parent_author);
//  }

//  void operator()(const vote_operation& op) {
//    _impacted.insert(op.voter);
//    _impacted.insert(op.author);
//  }

  void operator()(const transfer_operation& op) {
    if ((op.to != WLS_ROOT_POST_PARENT) && (op.to != op.from)) _impacted.insert(op.to);
  }

  void operator()(const transfer_to_vesting_operation& op) {
    if ((op.to != WLS_ROOT_POST_PARENT) && (op.to != op.from)) _impacted.insert(op.to);
  }

  void operator()(const set_withdraw_vesting_route_operation& op) {
    if ((op.to_account != WLS_ROOT_POST_PARENT) && (op.to_account != op.from_account)) _impacted.insert(op.to_account);
  }

  void operator()(const account_witness_vote_operation& op) {
    if ((op.witness != WLS_ROOT_POST_PARENT) && (op.witness != op.account)) _impacted.insert(op.witness);
  }

  void operator()(const account_witness_proxy_operation& op) {
    if ((op.proxy != WLS_ROOT_POST_PARENT) && (op.proxy != op.account)) _impacted.insert(op.proxy);
  }

  void operator()(const account_action_operation& op) {
    switch (op.action.which()) {
//      case account_action::tag<account_action_create_pod>::value: {
//        auto action = op.action.get<account_action_create_pod>();
//      } break;
//      case account_action::tag<account_action_transfer_to_tip>::value: {
//        auto action = op.action.get<account_action_transfer_to_tip>();
//      } break;
      case account_action::tag<account_action_htlc_create>::value: {
        auto action = op.action.get<account_action_htlc_create>();
        if (action.to != op.account) _impacted.insert(action.to);
      } break;
//      case account_action::tag<account_action_htlc_update>::value: {
//        auto action = op.action.get<account_action_htlc_update>();
//      } break;
//      case account_action::tag<account_action_htlc_redeem>::value: {
//        auto action = op.action.get<account_action_htlc_redeem>();
//      } break;
    }
  }

  void operator()(const social_action_operation& op) {
    switch (op.action.which()) {
      case social_action::tag<social_action_comment_create>::value: {
        auto action = op.action.get<social_action_comment_create>();
        if ((op.account != WLS_ROOT_POST_PARENT) && (op.account != action.parent_author)) _impacted.insert(action.parent_author);
      } break;
//      case social_action::tag<social_action_comment_update>::value: {
//        auto action = op.action.get<social_action_comment_update>();
//      } break;
//      case social_action::tag<social_action_comment_delete>::value: {
//        auto action = op.action.get<social_action_comment_delete>();
//      } break;
      case social_action::tag<social_action_claim_vesting_reward>::value: {
        auto action = op.action.get<social_action_claim_vesting_reward>();
        if (action.to) {
          if ((*action.to != WLS_ROOT_POST_PARENT) && (*action.to != op.account)) {
            _impacted.insert(*action.to);
          }
        }
      } break;
//      case social_action::tag<social_action_claim_vesting_reward_tip>::value: {
//        auto action = op.action.get<social_action_claim_vesting_reward_tip>();
//      } break;
      case social_action::tag<social_action_user_tip>::value: {
        auto action = op.action.get<social_action_user_tip>();
        if (action.to) {
          if ((*action.to != WLS_ROOT_POST_PARENT) && (*action.to != op.account)) {
            _impacted.insert(*action.to);
          }
        }
      } break;
      case social_action::tag<social_action_comment_tip>::value: {
        auto action = op.action.get<social_action_comment_tip>();
        if (action.author != op.account) _impacted.insert(action.author);
      } break;
    }
  }

  void operator()(const pod_action_operation& op) {
    switch (op.action.which()) {
      case pod_action::tag<pod_action_join_request>::value: {
        auto action = op.action.get<pod_action_join_request>();
        _impacted.insert(op.pod);
      } break;
//      case pod_action::tag<pod_action_cancel_join_request>::value: {
//        auto action = op.action.get<pod_action_cancel_join_request>();
//      } break;
      case pod_action::tag<pod_action_accept_join_request>::value: {
        auto action = op.action.get<pod_action_accept_join_request>();
        _impacted.insert(action.account);
      } break;
      case pod_action::tag<pod_action_reject_join_request>::value: {
        auto action = op.action.get<pod_action_reject_join_request>();
        _impacted.insert(action.account);
      } break;
      case pod_action::tag<pod_action_leave>::value: {
        //auto action = op.action.get<pod_action_leave>();
        _impacted.insert(op.pod);
      } break;
      case pod_action::tag<pod_action_kick>::value: {
        auto action = op.action.get<pod_action_kick>();
        _impacted.insert(action.account);
      } break;
//      case pod_action::tag<pod_action_update>::value: {
//        auto action = op.action.get<pod_action_update>();
//      } break;
    }
  }

  void operator()(const friend_action_operation& op) {
    _impacted.insert(op.another);
  }

  // vops

//  void operator()(const author_reward_operation& op) { _impacted.insert(op.author); }

//  void operator()(const curation_reward_operation& op) { _impacted.insert(op.curator); }

//  void operator()(const fill_vesting_withdraw_operation& op) {
//    _impacted.insert(op.from_account);
//    _impacted.insert(op.to_account);
//  }

  void operator()(const shutdown_witness_operation& op) { _impacted.insert(op.owner); }

//  void operator()(const comment_benefactor_reward_operation& op) {
//    _impacted.insert(op.benefactor);
//    _impacted.insert(op.author);
//  }

//  void operator()(const producer_reward_operation& op) { _impacted.insert(op.producer); }

//  void operator()(const devfund_operation& op) { _impacted.insert(op.account); }

  void operator()(const pod_virtual_operation& op) {
    switch (op.action.which()) {
//      case pod_virtual_action::tag<pod_escrow_transfer_virtual_action>::value: {
//        auto action = op.action.get<pod_escrow_transfer_virtual_action>();
//        _impacted.insert(action.account);
//      } break;
//      case pod_virtual_action::tag<pod_escrow_release_virtual_action>::value: {
//        auto action = op.action.get<pod_escrow_release_virtual_action>();
//        _impacted.insert(action.account);
//      } break;
    }
  }

  void operator()(const htlc_virtual_operation& op) {
    switch (op.action.which()) {
      case htlc_virtual_action::tag<htlc_redeemed_virtual_action>::value: {
        auto action = op.action.get<htlc_redeemed_virtual_action>();
//        _impacted.insert(action.from);
        if (action.to != action.from) _impacted.insert(action.to);
        if ((action.redeemer != action.from) && (action.redeemer != action.to)) _impacted.insert(action.redeemer);
      } break;
//      case htlc_virtual_action::tag<htlc_refund_virtual_action>::value: {
//        auto action = op.action.get<htlc_refund_virtual_action>();
//        _impacted.insert(action.to);
//      } break;
    }
  }

  // void operator()( const operation& op ){}
};

void get_impacted_accounts(const operation& op, flat_set<account_name_type>& result) {
  get_impacted_account_visitor vtor = get_impacted_account_visitor(result);
  op.visit(vtor);
}


class notification_plugin_impl {
 public:
  notification_plugin_impl(notification_plugin &_plugin, const string &path) : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin), _nudbstore_database{path} {
    _block_history_plugin = appbase::app().find_plugin<wls::plugins::block_history::block_history_plugin>();
  }

  virtual ~notification_plugin_impl() {}

  void on_irreversible_block( uint32_t block_num );

  /**
   * high level insert which appends item to history and updates head state
   * @param account
   * @param item
   */
  void insert(const account_name_type &account, const notification_object &item);

  const notification_state_head_object* get_notification_state_head_object(const account_name_type &account);

  database &_db;
  notification_plugin &_self;

  nudbstore<notification_key, notification_object> _nudbstore_database;
  boost::signals2::connection _irreversible_block_con;

  wls::plugins::block_history::block_history_plugin *_block_history_plugin;
};

const notification_state_head_object *notification_plugin_impl::get_notification_state_head_object(const account_name_type &account) {
  try {
    return _db.find<notification_state_head_object, by_account>(account);
  }
  FC_CAPTURE_AND_RETHROW((account))
}

void notification_plugin_impl::insert(const account_name_type &account, const notification_object &item) {
  auto head = this->get_notification_state_head_object(account);
  if (head == nullptr) {
    head = &_db.create<notification_state_head_object>([&](notification_state_head_object &obj) { obj.account = account; });
  }

  // insert
  notification_key new_key{account, head->sequence};
//  ilog("new_key: ${new_key}, item: ${item}", ("new_key", new_key)("item", item));
  boost::system::error_code ec;
  _nudbstore_database.insert(new_key, item, ec);
  if (ec) {
    if ((ec == nudb::error::key_exists) && (ec.category() == nudb::nudb_category())) {
      // ignore key_exists
    } else {
      FC_ASSERT(true, "${msg}", ("msg", ec.message()));
    }
  }

  // update last sequence to account head state
  _db.modify(*head, [&](notification_state_head_object& obj) { obj.sequence++; });
}

void notification_plugin_impl::on_irreversible_block(uint32_t block_num) {
  FC_ASSERT(_block_history_plugin != nullptr, "block_history_plugin is required for notification_plugin");

  try {
    vector<wls::plugins::block_history::block_history_operation_object> ops = _block_history_plugin->get_ops_recent_block(block_num);
    for (const auto &item : ops) {
//      operation op = fc::raw::unpack<operation>(item.serialized_op);
      notification_object temp;
      {
        temp.trx_id = item.trx_id;
        temp.block = item.block;
        temp.trx_in_block = item.trx_in_block;
        temp.op_in_trx = item.op_in_trx;
        temp.virtual_op = item.virtual_op;
        temp.timestamp = item.timestamp;
        temp.op = fc::raw::unpack<operation>(item.serialized_op);
      }

      flat_set<account_name_type> impacted;
      get_impacted_accounts(temp.op, impacted);

      for (auto &impacted_acc : impacted) {
        this->insert(impacted_acc, temp);
      }
    }
  }
  FC_CAPTURE_AND_RETHROW()
}

}  // end namespace detail

notification_plugin::notification_plugin() {}

notification_plugin::~notification_plugin() {}

void notification_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
//  cli.add_options();
  // clang-format on
  cfg.add(cli);
}

void notification_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  try {
    ilog("Initializing notification plugin");

    string data_dir = appbase::app().data_dir().string();
    if (data_dir == "") data_dir = ".";
    string path = data_dir + "/plugins/notification/";
    my = std::make_unique<detail::notification_plugin_impl>(*this, path);

    {
      bool replay = options.at("replay-blockchain").as<bool>();
      bool replay_blockchain = options.at("replay-blockchain").as<bool>();
      bool resync_blockchain = options.at("resync-blockchain").as<bool>();

      if (replay || replay_blockchain || resync_blockchain) my->_nudbstore_database.clean();
    }

    my->_nudbstore_database.open();
    my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });

    wls::chain::add_plugin_index<notification_state_head_object_index>(my->_db);

  } FC_CAPTURE_AND_RETHROW()
}

void notification_plugin::plugin_startup() { ilog("notification plugin: plugin_startup()"); }

void notification_plugin::plugin_shutdown() {
  try {
    ilog("notification::plugin_shutdown");
    chain::util::disconnect_signal(my->_irreversible_block_con);

    my->_nudbstore_database.close();
  } FC_CAPTURE_AND_RETHROW()
}

nudbstore<notification_key, notification_object>& notification_plugin::get_nudbstore_database() {
  return my->_nudbstore_database;
}

const notification_state_head_object *notification_plugin::get_notification_state_head_object(const account_name_type &account) {
  return my->get_notification_state_head_object(account);
}

}  // namespace notification
}  // namespace plugins
}  // namespace wls
