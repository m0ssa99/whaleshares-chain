#include <wls/plugins/podfeed/podfeed_plugin.hpp>
#include <wls/plugins/podfeed/podfeed_objects.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/plugins/follow/follow_objects.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/history_object.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/chain/util/signal.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>

#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace podfeed {

using namespace wls::protocol;

using chain::database;
using chain::operation_notification;

namespace detail {

class podfeed_plugin_impl {
 public:
  podfeed_plugin_impl(podfeed_plugin &_plugin, const string &path) : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin), _nudbstore_database{path} {
    _block_history_plugin = appbase::app().find_plugin<wls::plugins::block_history::block_history_plugin>();
  }

  virtual ~podfeed_plugin_impl() {}

  void on_irreversible_block( uint32_t block_num );

  /**
   * high level insert which appends item to feed and updates head state
   * @param pod
   * @param item
   */
  void insert(const pod_name_type &pod, const podfeed_object &item);

  const podfeed_state_head_object* get_podfeed_state_head_object(const pod_name_type &pod);

  //--

  database &_db;
  podfeed_plugin &_self;

  nudbstore<podfeed_key, podfeed_object> _nudbstore_database;
  boost::signals2::connection _irreversible_block_con;

  wls::plugins::block_history::block_history_plugin *_block_history_plugin;
};

const podfeed_state_head_object *podfeed_plugin_impl::get_podfeed_state_head_object(const pod_name_type &pod) {
  try {
    return _db.find<podfeed_state_head_object, by_pod>(pod);
  }
  FC_CAPTURE_AND_RETHROW((pod))
}

void podfeed_plugin_impl::insert(const pod_name_type &pod, const podfeed_object &item) {
  auto head = this->get_podfeed_state_head_object(pod);
  if (head == nullptr) {
    head = &_db.create<podfeed_state_head_object>([&](podfeed_state_head_object &obj) { obj.pod = pod; });
  }

  // insert
  podfeed_key new_key{pod, head->sequence};
//  ilog("new_key: ${new_key}, item: ${item}", ("new_key", new_key)("item", item));
  boost::system::error_code ec;
  _nudbstore_database.insert(new_key, item, ec);
  if (ec) {
    if ((ec == nudb::error::key_exists) && (ec.category() == nudb::nudb_category())) {
      // ignore key_exists
    } else {
      FC_ASSERT(true, "${msg}", ("msg", ec.message()));
    }
  }

  // update last sequence to pod head state
  _db.modify(*head, [&](podfeed_state_head_object& obj) { obj.sequence++; });
}

struct operation_visitor {
  operation_visitor(podfeed_plugin_impl &plugin) : _plugin(plugin) {}

  typedef void result_type;

  podfeed_plugin_impl &_plugin;

  template <typename T>
  void operator()(const T &) const {}

  void operator()(const social_action_operation &op) const {
    switch (op.action.which()) {
      case social_action::tag<social_action_comment_create>::value: {
        auto action = op.action.get<social_action_comment_create>();
        if (action.parent_author != WLS_ROOT_POST_PARENT) return;
        if (!action.pod) return;
        if (*action.pod == WLS_ROOT_POST_PARENT) return;

//        ilog("found social_action_comment_create author=${a}, permlink=${p}", ("a", op.account)("p", action.permlink));
//        const comment_object &comment = _plugin._db.get_comment(op.account, action.permlink);

        const auto *comment = _plugin._db.find_comment(op.account, action.permlink);
        if (comment == nullptr) return;

        podfeed_object feed_item(comment->author, chain::to_string(comment->permlink));
        _plugin.insert(*action.pod, feed_item);
      } break;
    }
  }
};

void podfeed_plugin_impl::on_irreversible_block(uint32_t block_num) {
  /**
   * Work in this function:
   * - detect new root-pod-comment
   * - put new comment into the feed
   */

  FC_ASSERT(_block_history_plugin != nullptr, "block_history_plugin is required for podfeed_plugin");

  try {
    vector<wls::plugins::block_history::block_history_operation_object> ops = _block_history_plugin->get_ops_recent_block(block_num);
    for (const auto &item : ops) {
      operation op = fc::raw::unpack<operation>(item.serialized_op);
      op.visit(operation_visitor(*this));
    }
  }
  FC_CAPTURE_AND_RETHROW()
}

}  // end namespace detail

podfeed_plugin::podfeed_plugin() {}

podfeed_plugin::~podfeed_plugin() {}

void podfeed_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
//  cli.add_options()(
//  );
  // clang-format on
  cfg.add(cli);
}

void podfeed_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  try {
    ilog("Initializing podfeed plugin");

    string data_dir = appbase::app().data_dir().string();
    if (data_dir == "") data_dir = ".";
    string path = data_dir + "/plugins/podfeed/";
    my = std::make_unique<detail::podfeed_plugin_impl>(*this, path);

    {
      bool replay = options.at("replay-blockchain").as<bool>();
      bool replay_blockchain = options.at("replay-blockchain").as<bool>();
      bool resync_blockchain = options.at("resync-blockchain").as<bool>();

      if (replay || replay_blockchain || resync_blockchain) my->_nudbstore_database.clean();
    }

    my->_nudbstore_database.open();
    my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });

    wls::chain::add_plugin_index<podfeed_state_head_object_index>(my->_db);

  } FC_CAPTURE_AND_RETHROW()
}

void podfeed_plugin::plugin_startup() {
  try {
    ilog("podfeed plugin: plugin_startup()");
  } FC_CAPTURE_AND_RETHROW()
}

void podfeed_plugin::plugin_shutdown() {
  try {
    ilog("podfeed::plugin_shutdown");
    chain::util::disconnect_signal(my->_irreversible_block_con);

    my->_nudbstore_database.close();
  } FC_CAPTURE_AND_RETHROW()
}

nudbstore<podfeed_key, podfeed_object>& podfeed_plugin::get_nudbstore_database() {
  return my->_nudbstore_database;
}

const podfeed_state_head_object *podfeed_plugin::get_podfeed_state_head_object(const pod_name_type &pod) {
  return my->get_podfeed_state_head_object(pod);
}

}  // namespace podfeed
}  // namespace plugins
}  // namespace wls
