#pragma once

#include <appbase/application.hpp>

#include <wls/plugins/chain/chain_plugin.hpp>

#include <fc/thread/future.hpp>

#define FOLLOW_PLUGIN_NAME "follow"

namespace wls {
namespace plugins {
namespace follow {

using namespace appbase;

namespace detail {
class follow_plugin_impl;
}

class follow_plugin : public appbase::plugin<follow_plugin> {
 public:
  follow_plugin();

  virtual ~follow_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin))

  static const std::string &name() {
    static std::string name = FOLLOW_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const boost::program_options::variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  // friend class detail::follow_plugin_impl;

  //
  uint32_t max_feed_size = 500;

 private:
  std::unique_ptr<detail::follow_plugin_impl> my;
};

}  // namespace follow
}  // namespace plugins
}  // namespace wls
