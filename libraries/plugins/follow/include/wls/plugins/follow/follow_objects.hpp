#pragma once

#include <wls/plugins/follow/follow_plugin.hpp>
#include <wls/chain/wls_object_types.hpp>

#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace follow {

using namespace std;
using namespace wls::chain;
using chainbase::shared_vector;

#ifndef FOLLOW_SPACE_ID
#define FOLLOW_SPACE_ID 8
#endif

enum follow_plugin_object_type {
  follow_object_type = (FOLLOW_SPACE_ID << 8),
  follow_count_object_type = (FOLLOW_SPACE_ID << 8) + 1,
  feed_object_type = (FOLLOW_SPACE_ID << 8) + 2,
  blog_object_type = (FOLLOW_SPACE_ID << 8) + 3
};

enum follow_type { undefined, blog, ignore };

class follow_object : public object<follow_object_type, follow_object> {
 public:
  template <typename Constructor, typename Allocator>
  follow_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  follow_object() {}

  id_type id;

  account_name_type follower;
  account_name_type following;
  uint16_t what = 0;
};


class follow_count_object : public object<follow_count_object_type, follow_count_object> {
 public:
  template <typename Constructor, typename Allocator>
  follow_count_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  follow_count_object() {}

  id_type id;

  account_name_type account;
  uint32_t follower_count = 0;
  uint32_t following_count = 0;
};

class feed_object : public object<feed_object_type, feed_object> {
 public:
  feed_object() = delete;

  template <typename Constructor, typename Allocator>
  feed_object(Constructor &&c, allocator<Allocator> a) : reblogged_by(a.get_segment_manager()) {
    c(*this);
  }

  id_type id;

  account_name_type account;
  shared_vector<account_name_type> reblogged_by;
  account_name_type first_reblogged_by;
  time_point_sec first_reblogged_on;
  comment_id_type comment;
  uint32_t reblogs;
  uint32_t account_feed_id = 0;
};


class blog_object : public object<blog_object_type, blog_object> {
 public:
  template <typename Constructor, typename Allocator>
  blog_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  blog_object() {}

  id_type id;

  account_name_type account;
  comment_id_type comment;
  time_point_sec reblogged_on;
  uint32_t blog_feed_id = 0;
};


//------------------------------------------------------------------------------

typedef oid<follow_object> follow_id_type;
typedef oid<feed_object> feed_id_type;
typedef oid<blog_object> blog_id_type;
typedef oid<follow_count_object> follow_count_id_type;

struct by_following_follower;
struct by_follower_following;

using namespace boost::multi_index;
// clang-format off
typedef multi_index_container<
  follow_object,
  indexed_by<
     ordered_unique<tag<by_id>, member<follow_object, follow_id_type, &follow_object::id> >,
     ordered_unique<tag<by_following_follower>,
        composite_key < follow_object,
           member<follow_object, account_name_type, &follow_object::following>,
           member<follow_object, account_name_type, &follow_object::follower>
        >,
        composite_key_compare < std::less<account_name_type>, std::less<account_name_type> >
     >,
     ordered_unique<tag<by_follower_following>,
        composite_key < follow_object,
           member<follow_object, account_name_type, &follow_object::follower>,
           member<follow_object, account_name_type, &follow_object::following>
        >,
        composite_key_compare <std::less<account_name_type>, std::less<account_name_type>>
     >
  >,
  allocator<follow_object>
> follow_index;


struct by_feed;
struct by_old_feed;
struct by_account;
struct by_comment;

typedef multi_index_container<
  feed_object,
  indexed_by<
     ordered_unique<tag<by_id>, member<feed_object, feed_id_type, &feed_object::id> >,
     ordered_unique<tag<by_feed>,
        composite_key < feed_object,
           member<feed_object, account_name_type, &feed_object::account>,
           member<feed_object, uint32_t, &feed_object::account_feed_id>
        >,
        composite_key_compare < std::less<account_name_type>, std::greater<uint32_t> >
     >,
     ordered_unique<tag<by_old_feed>,
        composite_key < feed_object,
           member<feed_object, account_name_type, &feed_object::account>,
           member<feed_object, uint32_t, &feed_object::account_feed_id>
        >,
        composite_key_compare <std::less<account_name_type>, std::less<uint32_t>>
     >,
     ordered_unique<tag<by_account>,
        composite_key < feed_object,
           member<feed_object, account_name_type, &feed_object::account>,
           member<feed_object, feed_id_type, &feed_object::id>
        >,
        composite_key_compare <std::less<account_name_type>, std::less<feed_id_type>>
     >,
     ordered_unique<tag<by_comment>,
        composite_key < feed_object,
           member<feed_object, comment_id_type, &feed_object::comment>,
           member<feed_object, account_name_type, &feed_object::account>
        >,
        composite_key_compare <std::less<comment_id_type>, std::less<account_name_type>>
     >
  >,
  allocator<feed_object>
> feed_index;

struct by_blog;
struct by_old_blog;

typedef multi_index_container<
  blog_object,
  indexed_by<
     ordered_unique<tag<by_id>, member<blog_object, blog_id_type, &blog_object::id> >,
     ordered_unique<tag<by_blog>,
        composite_key < blog_object,
           member<blog_object, account_name_type, &blog_object::account>,
           member<blog_object, uint32_t, &blog_object::blog_feed_id>
        >,
        composite_key_compare < std::less<account_name_type>, std::greater<uint32_t> >
     >,
     ordered_unique<tag<by_old_blog>,
        composite_key < blog_object,
           member<blog_object, account_name_type, &blog_object::account>,
           member<blog_object, uint32_t, &blog_object::blog_feed_id>
        >,
        composite_key_compare <std::less<account_name_type>, std::less<uint32_t>>
     >,
     ordered_unique<tag<by_comment>,
        composite_key < blog_object,
           member<blog_object, comment_id_type, &blog_object::comment>,
           member<blog_object, account_name_type, &blog_object::account>
        >,
        composite_key_compare <std::less<comment_id_type>, std::less<account_name_type>>
     >
  >,
  allocator<blog_object>
> blog_index;


typedef multi_index_container<
  follow_count_object,
  indexed_by<
     ordered_unique<tag<by_id>, member<follow_count_object, follow_count_id_type, &follow_count_object::id> >,
     ordered_unique<tag<by_account>, member<follow_count_object, account_name_type, &follow_count_object::account> >
  >,
  allocator<follow_count_object>
> follow_count_index;
// clang-format on
}  // namespace follow
}  // namespace plugins
}  // namespace wls

FC_REFLECT_ENUM(wls::plugins::follow::follow_type, (undefined)(blog)(ignore))

FC_REFLECT(wls::plugins::follow::follow_object, (id)(follower)(following)(what))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::follow::follow_object, wls::plugins::follow::follow_index)

FC_REFLECT(wls::plugins::follow::feed_object, (id)(account)(first_reblogged_by)(first_reblogged_on)(reblogged_by)(comment)(reblogs)(account_feed_id))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::follow::feed_object, wls::plugins::follow::feed_index)

FC_REFLECT(wls::plugins::follow::blog_object, (id)(account)(comment)(reblogged_on)(blog_feed_id))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::follow::blog_object, wls::plugins::follow::blog_index)

FC_REFLECT(wls::plugins::follow::follow_count_object, (id)(account)(follower_count)(following_count))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::follow::follow_count_object, wls::plugins::follow::follow_count_index)
