#include <wls/plugins/follow/follow_operations.hpp>
#include <wls/protocol/operation_util_impl.hpp>

namespace wls {
namespace plugins {
namespace follow {

void follow_operation::validate() const { FC_ASSERT(follower != following, "You cannot follow yourself"); }

void reblog_operation::validate() const { FC_ASSERT(account != author, "You cannot reblog your own content"); }

}  // namespace follow
}  // namespace plugins
}  // namespace wls

DEFINE_OPERATION_TYPE(wls::plugins::follow::follow_plugin_operation)
