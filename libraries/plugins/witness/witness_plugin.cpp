#include <wls/plugins/witness/witness_plugin.hpp>
#include <wls/plugins/witness/witness_objects.hpp>
#include <wls/plugins/witness/simple_daily_bandwidth_limit.hpp>
#include <wls/chain/account_object.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/database_exceptions.hpp>
#include <wls/chain/generic_custom_operation_interpreter.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/utilities/key_conversion.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/time.hpp>
#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>

#include <iostream>
#include <memory>

#define DISTANCE_CALC_PRECISION (10000)

namespace wls {
namespace plugins {
namespace witness {

namespace bpo = boost::program_options;

using std::string;
using std::vector;

using chain::account_object;
using protocol::signed_transaction;

void new_chain_banner(const wls::chain::database &db) {
  std::cerr << "\n"
               "********************************\n"
               "*                              *\n"
               "*   ------- NEW CHAIN ------   *\n"
               "*   Welcome to Whaleshares !   *\n"
               "*   ------------------------   *\n"
               "*                              *\n"
               "********************************\n"
               "\n";
  return;
}

namespace detail {
using namespace wls::chain;

class witness_plugin_impl {
 public:
  witness_plugin_impl(boost::asio::io_service &io)
      : _timer(io),
        _chain_plugin(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>()),
        _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()) {}

  void pre_transaction(const signed_transaction &trx);

//  void pre_operation(const operation_notification &note);

  void on_block(const signed_block &b);

  void update_account_bandwidth(const account_object &a, uint32_t trx_size, const bandwidth_type type);

  void update_account_daily_bandwidth(const account_object &a, uint32_t trx_size);

  boost::asio::deadline_timer _timer;
  plugins::chain::chain_plugin &_chain_plugin;
  chain::database &_db;
};

///**
// * TODO: move this to the wallet frontend instead !?
// */
//void check_memo(const string &memo, const account_object &account, const account_authority_object &auth) {
//  vector<public_key_type> keys;
//
//  try {
//    // Check if memo is a private key
//    keys.push_back(fc::ecc::extended_private_key::from_base58(memo).get_public_key());
//  } catch (fc::parse_error_exception &) {
//  } catch (fc::assert_exception &) {
//  }
//
//  // Get possible keys if memo was an account password
//  string owner_seed = account.name + "owner" + memo;
//  auto owner_secret = fc::sha256::hash(owner_seed.c_str(), owner_seed.size());
//  keys.push_back(fc::ecc::private_key::regenerate(owner_secret).get_public_key());
//
//  string active_seed = account.name + "active" + memo;
//  auto active_secret = fc::sha256::hash(active_seed.c_str(), active_seed.size());
//  keys.push_back(fc::ecc::private_key::regenerate(active_secret).get_public_key());
//
//  string posting_seed = account.name + "posting" + memo;
//  auto posting_secret = fc::sha256::hash(posting_seed.c_str(), posting_seed.size());
//  keys.push_back(fc::ecc::private_key::regenerate(posting_secret).get_public_key());
//
//  // Check keys against public keys in authorites
//  for (auto &key_weight_pair : auth.owner.key_auths) {
//    for (auto &key : keys)
//      WLS_ASSERT(key_weight_pair.first != key, chain::plugin_exception, "Detected private owner key in memo field. You should change your owner keys.");
//  }
//
//  for (auto &key_weight_pair : auth.active.key_auths) {
//    for (auto &key : keys)
//      WLS_ASSERT(key_weight_pair.first != key, chain::plugin_exception, "Detected private active key in memo field. You should change your active keys.");
//  }
//
//  for (auto &key_weight_pair : auth.posting.key_auths) {
//    for (auto &key : keys)
//      WLS_ASSERT(key_weight_pair.first != key, chain::plugin_exception, "Detected private posting key in memo field. You should change your posting keys.");
//  }
//
//  const auto &memo_key = account.memo_key;
//  for (auto &key : keys) WLS_ASSERT(memo_key != key, chain::plugin_exception, "Detected private memo key in memo field. You should change your memo key.");
//}

void witness_plugin_impl::pre_transaction(const signed_transaction &trx) {
  bool enable_daily_bandwidth = false;
#ifdef IS_TEST_NET
  enable_daily_bandwidth = true;
#else
  if (_db.has_hardfork(WLS_HARDFORK_0_2)) enable_daily_bandwidth = true;
#endif

  flat_set<account_name_type> required;
  vector<authority> other;
  trx.get_required_authorities(required, required, required, other);

  auto trx_size = fc::raw::pack_size(trx);

  for (const auto &auth : required) {
    const auto &acnt = _db.get_account(auth);

    // for the old bandwidth
    {
      update_account_bandwidth(acnt, trx_size, bandwidth_type::forum);

      for (const auto &op : trx.operations) {
        if (is_market_operation(op)) {
          update_account_bandwidth(acnt, trx_size * 10, bandwidth_type::market);
          break;
        }
      }
    }

    // for the new daily bandwidth
    if (enable_daily_bandwidth) update_account_daily_bandwidth(acnt, trx_size);
  }
}

//void witness_plugin_impl::pre_operation(const operation_notification &note) {
//  if (_db.is_producing()) {
//    note.op.visit(operation_visitor(_db));
//  }
//}

void witness_plugin_impl::on_block(const signed_block &b) {
  int64_t max_block_size = _db.get_dynamic_global_properties().maximum_block_size;
  auto reserve_ratio_ptr = _db.find(reserve_ratio_id_type());

  if (BOOST_UNLIKELY(reserve_ratio_ptr == nullptr)) {
    _db.create<reserve_ratio_object>([&](reserve_ratio_object &r) {
      r.average_block_size = 0;
      r.current_reserve_ratio = WLS_MAX_RESERVE_RATIO * RESERVE_RATIO_PRECISION;
      r.max_virtual_bandwidth =
          (static_cast<uint128_t>(WLS_MAX_BLOCK_SIZE) * WLS_MAX_RESERVE_RATIO * WLS_BANDWIDTH_PRECISION * WLS_BANDWIDTH_AVERAGE_WINDOW_SECONDS) / WLS_BLOCK_INTERVAL;
    });
  } else {
    _db.modify(*reserve_ratio_ptr, [&](reserve_ratio_object &r) {
      r.average_block_size = (99 * r.average_block_size + fc::raw::pack_size(b)) / 100;

      /**
       * About once per minute the average network use is consulted and used to adjust the reserve ratio. Anything above 25% usage reduces the reserve
       * ratio proportional to the distance from 25%. If usage is at 50% then the reserve ratio will half. Likewise, if it is at 12% it will increase by 50%.
       * If the reserve ratio is consistently low, then it is probably time to increase the capacity of the network.
       * This algorithm is designed to react quickly to observations significantly different from past observed behavior and make small adjustments when
       * behavior is within expected norms.
       */
      if (_db.head_block_num() % 20 == 0) {
        int64_t distance = ((r.average_block_size - (max_block_size / 4)) * DISTANCE_CALC_PRECISION) / (max_block_size / 4);
        auto old_reserve_ratio = r.current_reserve_ratio;

        if (distance > 0) {
          r.current_reserve_ratio -= (r.current_reserve_ratio * distance) / (distance + DISTANCE_CALC_PRECISION);

          // We do not want the reserve ratio to drop below 1
          if (r.current_reserve_ratio < RESERVE_RATIO_PRECISION) r.current_reserve_ratio = RESERVE_RATIO_PRECISION;
        } else {
          // By default, we should always slowly increase the reserve ratio.
          r.current_reserve_ratio += std::max(RESERVE_RATIO_MIN_INCREMENT, (r.current_reserve_ratio * distance) / (distance - DISTANCE_CALC_PRECISION));

          if (r.current_reserve_ratio > WLS_MAX_RESERVE_RATIO * RESERVE_RATIO_PRECISION)
            r.current_reserve_ratio = WLS_MAX_RESERVE_RATIO * RESERVE_RATIO_PRECISION;
        }

        if (old_reserve_ratio != r.current_reserve_ratio) {
          ilog("Reserve ratio updated from ${old} to ${new}. Block: ${blocknum}",
               ("old", old_reserve_ratio)("new", r.current_reserve_ratio)("blocknum", _db.head_block_num()));
        }

        r.max_virtual_bandwidth =
            (uint128_t(max_block_size) * uint128_t(r.current_reserve_ratio) * uint128_t(WLS_BANDWIDTH_PRECISION * WLS_BANDWIDTH_AVERAGE_WINDOW_SECONDS)) /
            (WLS_BLOCK_INTERVAL * RESERVE_RATIO_PRECISION);
      }
    });
  }
}

void witness_plugin_impl::update_account_bandwidth(const account_object &a, uint32_t trx_size, const bandwidth_type type) {
  const auto &props = _db.get_dynamic_global_properties();
  bool has_bandwidth = true;

  if (props.total_vesting_shares.amount > 0) {
    auto band = _db.find<account_bandwidth_object, by_account_bandwidth_type>(boost::make_tuple(a.name, type));

    if (band == nullptr) {
      band = &_db.create<account_bandwidth_object>([&](account_bandwidth_object &b) {
        b.account = a.name;
        b.type = type;
      });
    }

    share_type new_bandwidth;
    share_type trx_bandwidth = trx_size * WLS_BANDWIDTH_PRECISION;
    auto delta_time = (_db.head_block_time() - band->last_bandwidth_update).to_seconds();

    if (delta_time > WLS_BANDWIDTH_AVERAGE_WINDOW_SECONDS)
      new_bandwidth = 0;
    else
      new_bandwidth =
          (((WLS_BANDWIDTH_AVERAGE_WINDOW_SECONDS - delta_time) * fc::uint128(band->average_bandwidth.value)) / WLS_BANDWIDTH_AVERAGE_WINDOW_SECONDS)
              .to_uint64();

    new_bandwidth += trx_bandwidth;

    _db.modify(*band, [&](account_bandwidth_object &b) {
      b.average_bandwidth = new_bandwidth;
      b.lifetime_bandwidth += trx_bandwidth;
      b.last_bandwidth_update = _db.head_block_time();
    });

    fc::uint128 account_vshares(a.vesting_shares.amount.value);
    fc::uint128 total_vshares(props.total_vesting_shares.amount.value);
    fc::uint128 account_average_bandwidth(band->average_bandwidth.value);
    fc::uint128 max_virtual_bandwidth(_db.get(reserve_ratio_id_type()).max_virtual_bandwidth);

    has_bandwidth = (account_vshares * max_virtual_bandwidth) > (account_average_bandwidth * total_vshares);

    if (_db.is_producing()) {
      // clang-format off
      WLS_ASSERT(has_bandwidth, chain::plugin_exception,
          "Account: ${account} bandwidth limit exceeded. Please wait to transact or power up WLS.",
          ("account", a.name)
          ("account_vshares", account_vshares)
          ("account_average_bandwidth", account_average_bandwidth)
          ("max_virtual_bandwidth", max_virtual_bandwidth)
          ("total_vesting_shares", total_vshares)
      );
      // clang-format on
    }
  }
}

void witness_plugin_impl::update_account_daily_bandwidth(const account_object &a, uint32_t trx_size) {
  const auto &props = _db.get_dynamic_global_properties();
  auto whalestake = a.vesting_shares * props.get_vesting_share_price();

#ifdef IS_TEST_NET
  bool testnet = true;
#else
  bool testnet = false;
#endif

  bool has_hf3 = false;
  if (testnet || _db.has_hardfork(WLS_HARDFORK_0_3)) {
    has_hf3 = true;
  }
  auto daily_bw_limit = simple_daily_bandwidth_limit(whalestake.amount.value, has_hf3);

  WLS_ASSERT(daily_bw_limit >= 0, chain::plugin_exception, "daily_bw_limit must >= 0");
  auto acc_daily_bw = _db.find<account_daily_bandwidth_object, by_account>(a.name);
  if (acc_daily_bw == nullptr) {
    WLS_ASSERT(trx_size < daily_bw_limit, chain::plugin_exception, "Daily bandwidth limit exceeded. Please wait or power up WLS.");
    acc_daily_bw = &_db.create<account_daily_bandwidth_object>([&](account_daily_bandwidth_object &b) {
      b.account = a.name;
      b.bandwidth = daily_bw_limit - trx_size;
      b.last_update = _db.head_block_time();
    });
  } else {
    int64_t elapsed_seconds = (_db.head_block_time() - acc_daily_bw->last_update).to_seconds();
    int64_t regenerated_bw = daily_bw_limit * elapsed_seconds / 86400;
    int64_t new_bw = std::min(acc_daily_bw->bandwidth + regenerated_bw, int64_t(daily_bw_limit));

    WLS_ASSERT(trx_size < new_bw, chain::plugin_exception,
        "Daily bandwidth limit exceeded. Please wait or power up WLS. (account=${a}, stake=${stake}, trx_size=${trx_size}, limit=${limit})",
        ("a", a.name)("trx_size", trx_size)("limit", new_bw));
    _db.modify(*acc_daily_bw, [&](account_daily_bandwidth_object &b) {
      b.bandwidth = new_bw - trx_size;
      b.last_update = _db.head_block_time();
    });
  }

//  dlog("update_account_daily_bandwidth: account=${a}, stake=${stake}, daily_bw=${daily_bw}, limit=${limit}",
//      ("a", a.name)("stake", whalestake)("daily_bw", acc_daily_bw->bandwidth)("limit", daily_bw_limit));
}

}  // namespace detail

witness_plugin::witness_plugin() {}

witness_plugin::~witness_plugin() {}

void witness_plugin::set_program_options(  // plugin_set_program_options
    boost::program_options::options_description &command_line_options, boost::program_options::options_description &config_file_options) {
  // clang-format off
  command_line_options.add_options()(
     "enable-stale-production", bpo::bool_switch()->notifier([this](bool e) { _production_enabled = e; }),
     "Enable block production, even if the chain is stale."
  )(
     "required-participation", bpo::bool_switch()->notifier([this](int e) { _required_witness_participation = uint32_t(e * WLS_1_PERCENT); }),
     "Percent of witnesses (0-99) that must be participating in order to produce blocks"
  )(
     "witness,w", bpo::value<vector<string>>()->composing()->multitoken(),
     "name of witness controlled by this node (e.g. initwitness )"
  )(
     "private-key", bpo::value<vector<string>>()->composing()->multitoken(),
     "WIF PRIVATE KEY to be used by one or more witnesses or miners"
  );
  // clang-format on
  config_file_options.add(command_line_options);
}

using std::pair;
using std::string;
using std::vector;

void witness_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  try {
    ilog("Initializing witness plugin");
    _my = std::make_unique<detail::witness_plugin_impl>(appbase::app().get_io_service());
    LOAD_VALUE_SET(options, "witness", _witnesses, string)

    if (options.count("private-key")) {
      const std::vector<std::string> keys = options["private-key"].as<std::vector<std::string>>();
      for (const std::string &wif_key : keys) {
        fc::optional<fc::ecc::private_key> private_key = wls::utilities::wif_to_key(wif_key);
        FC_ASSERT(private_key.valid(), "unable to parse private key");
        _private_keys[private_key->get_public_key()] = *private_key;
      }
    }

    _my->_db.on_pre_apply_transaction.connect([&](const signed_transaction &tx) { _my->pre_transaction(tx); });
    // _my->_db.pre_apply_operation.connect([&](const operation_notification &note) { _my->pre_operation(note); });
    _my->_db.applied_block.connect([&](const signed_block &b) { _my->on_block(b); });

    add_plugin_index<account_bandwidth_index>(_my->_db);
    add_plugin_index<reserve_ratio_index>(_my->_db);
    add_plugin_index<account_daily_bandwidth_index>(_my->_db);
  }
  FC_LOG_AND_RETHROW()
}

void witness_plugin::plugin_startup() {
  try {
    ilog("witness plugin:  plugin_startup() begin");

    if (!_witnesses.empty()) {
      ilog("Launching block production for ${n} witnesses.", ("n", _witnesses.size()));
      idump((_witnesses));
      //                  app().set_block_production(true);
      set_block_production(true);
      appbase::app().get_plugin<wls::plugins::p2p::p2p_plugin>().set_block_production(true);

      if (_production_enabled) {
        if (_my->_db.head_block_num() == 0) new_chain_banner(_my->_db);
        _production_skip_flags |= wls::chain::database::skip_undo_history_check;
      }
      schedule_production_loop();
    } else {
      elog("No witnesses configured! Please add witness names and private keys to configuration.");
    }
    ilog("witness plugin:  plugin_startup() end");
  }
  FC_CAPTURE_AND_RETHROW()
}

void witness_plugin::plugin_shutdown() {
  ilog("witness_plugin::plugin_shutdown");

  if (!_witnesses.empty()) {
    ilog("shutting downing production timer");
    _my->_timer.cancel();
  }
}

void witness_plugin::schedule_production_loop() {
  // Schedule for the next second's tick regardless of chain state. If we would wait less than 50ms, wait for the whole second.
  fc::time_point fc_now = fc::time_point::now();
  int64_t time_to_next_second = 1000000 - (fc_now.time_since_epoch().count() % 1000000);
  if (time_to_next_second < 50000) time_to_next_second += 1000000;  // we must sleep for at least 50ms

  _my->_timer.expires_from_now(boost::posix_time::microseconds(time_to_next_second));
  _my->_timer.async_wait(boost::bind(&witness_plugin::block_production_loop, this));
}

block_production_condition::block_production_condition_enum witness_plugin::block_production_loop() {
  if (fc::time_point::now() < fc::time_point(WLS_GENESIS_TIME)) {
    wlog("waiting until genesis time to produce block: ${t}", ("t", WLS_GENESIS_TIME));
    schedule_production_loop();
    return block_production_condition::wait_for_genesis;
  }

  block_production_condition::block_production_condition_enum result;
  fc::mutable_variant_object capture;
  try {
    result = maybe_produce_block(capture);
  } catch (const fc::canceled_exception &) {
    // We're trying to exit. Go ahead and let this one out.
    throw;
  } catch (const wls::chain::unknown_hardfork_exception &e) {
    // Hit a hardfork that the current node know nothing about, stop production and inform user
    elog("${e}\nNode may be out of date...", ("e", e.to_detail_string()));
    throw;
  } catch (const fc::exception &e) {
    elog("Got exception while generating block:\n${e}", ("e", e.to_detail_string()));
    result = block_production_condition::exception_producing_block;
  }

  switch (result) {
    case block_production_condition::produced:
      ilog("Generated block #${n} with timestamp ${t} at time ${c} by ${w}", (capture));
      break;
    case block_production_condition::not_synced:
      // ilog("Not producing block because production is disabled until we receive a recent block (see: --enable-stale-production)");
      break;
    case block_production_condition::not_my_turn:
      // ilog("Not producing block because it isn't my turn");
      break;
    case block_production_condition::not_time_yet:
      // ilog("Not producing block because slot has not yet arrived");
      break;
    case block_production_condition::no_private_key:
      ilog("Not producing block for ${scheduled_witness} because I don't have the private key for ${scheduled_key}", (capture));
      break;
    case block_production_condition::low_participation:
      elog("Not producing block because node appears to be on a minority fork with only ${pct}% witness participation", (capture));
      break;
    case block_production_condition::lag:
      elog("Not producing block because node didn't wake up within 500ms of the slot time.");
      break;
    case block_production_condition::consecutive:
      elog(
          "Not producing block because the last block was generated by the same witness.\nThis node is probably disconnected from the network so block "
          "production has been disabled.\nDisable this check with --allow-consecutive option.");
      break;
    case block_production_condition::exception_producing_block:
      elog("Failure when producing block with no transactions");
      break;
    case block_production_condition::wait_for_genesis:
      break;
  }

  schedule_production_loop();
  return result;
}

block_production_condition::block_production_condition_enum witness_plugin::maybe_produce_block(fc::mutable_variant_object &capture) {
  fc::time_point now_fine = fc::time_point::now();
  fc::time_point_sec now = now_fine + fc::microseconds(500000);

  // If the next block production opportunity is in the present or future, we're synced.
  if (!_production_enabled) {
    if (_my->_db.get_slot_time(1) >= now)
      _production_enabled = true;
    else
      return block_production_condition::not_synced;
  }

  // is anyone scheduled to produce now or one second in the future?
  uint32_t slot = _my->_db.get_slot_at_time(now);
  if (slot == 0) {
    capture("next_time", _my->_db.get_slot_time(1));
    return block_production_condition::not_time_yet;
  }

  /**
   * this assert should not fail, because now <= db.head_block_time() should have resulted in slot == 0.
   * if this assert triggers, there is a serious bug in get_slot_at_time() which would result in allowing a later block to have a timestamp less than or equal
   * to the previous block
   */
  assert(now > _my->_db.head_block_time());

  string scheduled_witness = _my->_db.get_scheduled_witness(slot);
  // we must control the witness scheduled to produce the next block.
  if (_witnesses.find(scheduled_witness) == _witnesses.end()) {
    capture("scheduled_witness", scheduled_witness);
    return block_production_condition::not_my_turn;
  }

  const auto &witness_by_name = _my->_db.get_index<chain::witness_index>().indices().get<chain::by_name>();
  auto itr = witness_by_name.find(scheduled_witness);

  fc::time_point_sec scheduled_time = _my->_db.get_slot_time(slot);
  wls::protocol::public_key_type scheduled_key = itr->signing_key;
  auto private_key_itr = _private_keys.find(scheduled_key);

  if (private_key_itr == _private_keys.end()) {
    capture("scheduled_witness", scheduled_witness);
    capture("scheduled_key", scheduled_key);
    return block_production_condition::no_private_key;
  }

  uint32_t prate = _my->_db.witness_participation_rate();
  if (prate < _required_witness_participation) {
    capture("pct", uint32_t(100 * uint64_t(prate) / WLS_1_PERCENT));
    return block_production_condition::low_participation;
  }

  if (llabs((scheduled_time - now).count()) > fc::milliseconds(500).count()) {
    capture("scheduled_time", scheduled_time)("now", now);
    return block_production_condition::lag;
  }

//  int retry = 0;
//  do {
    try {
      auto block = _my->_db.generate_block(scheduled_time, scheduled_witness, private_key_itr->second, _production_skip_flags);
      capture("n", block.block_num())("t", block.timestamp)("c", now)("w", scheduled_witness);
//      fc::async([this, block]() {
        // p2p_node().broadcast(graphene::net::block_message(block));
        appbase::app().get_plugin<wls::plugins::p2p::p2p_plugin>().broadcast_block(block);
//      });

      return block_production_condition::produced;
    } catch (fc::exception &e) {
      elog("${e}", ("e", e.to_detail_string()));
      elog("Clearing pending transactions and attempting again");
      _my->_db.clear_pending();
//      retry++;
    }
//  } while (retry < 2);

  return block_production_condition::exception_producing_block;
}

}  // namespace witness
}  // namespace plugins
}  // namespace wls
