#pragma once

#include <appbase/application.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/comment_object.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>

#include <fc/thread/future.hpp>

#include <boost/multi_index/composite_key.hpp>

#define ACCOUNT_TAGS_PLUGIN_NAME "account_tags"

namespace wls {
namespace plugins {
namespace account_tags {

using namespace appbase;
using namespace wls::chain;
using namespace boost::multi_index;
using chainbase::allocator;
using chainbase::object;
using chainbase::oid;

#ifndef ACCOUNT_TAG_SPACE_ID
#define ACCOUNT_TAG_SPACE_ID 24
#endif

typedef fc::fixed_string_32 tag_name_type;

enum {
  account_tag_object_type = (ACCOUNT_TAG_SPACE_ID << 8)
};

namespace detail {
class account_tags_plugin_impl;
}

/**
 *  The purpose of the tag object is to allow to index and filter all accounts by a string tag.
 *  Whenever an account is modified, all tag_objects for that account are updated to match.
 */
class account_tag_object : public object<account_tag_object_type, account_tag_object> {
 public:
  template <typename Constructor, typename Allocator>
  account_tag_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  account_tag_object() {}

  id_type id;

  tag_name_type tag;
  account_name_type account;
};

typedef oid<account_tag_object> account_tag_id_type;

// clang-format off
struct by_tag_account;
struct by_account_tag;

typedef multi_index_container<
  account_tag_object,
  indexed_by<
     ordered_unique<tag<by_id>, member<account_tag_object, account_tag_id_type, &account_tag_object::id> >,
     ordered_unique< tag< by_tag_account >,
       composite_key< account_tag_object,
         member< account_tag_object, tag_name_type, &account_tag_object::tag >,
         member< account_tag_object, account_name_type, &account_tag_object::account >
       >,
      composite_key_compare < std::less<tag_name_type>, std::less<account_name_type> >
     >,
     ordered_unique< tag< by_account_tag >,
      composite_key< account_tag_object,
        member< account_tag_object, account_name_type, &account_tag_object::account >,
        member< account_tag_object, tag_name_type, &account_tag_object::tag >
      >,
      composite_key_compare < std::less<account_name_type>, std::less<tag_name_type> >
    >
  >,
  allocator<account_tag_object>
> account_tag_index;
// clang-format on

/**
 * Used to parse the metadata from the account json_meta field.
 */

struct account_metadata_profile {
  set<string> tags;
};

struct account_metadata {
  account_metadata_profile profile;
};

class account_tags_plugin : public appbase::plugin<account_tags_plugin> {
 public:
  account_tags_plugin();

  virtual ~account_tags_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin)(wls::plugins::block_history::block_history_plugin))

  static const std::string &name() {
    static std::string name = ACCOUNT_TAGS_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const boost::program_options::variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  friend class detail::account_tags_plugin_impl;

  std::unique_ptr<detail::account_tags_plugin_impl> my;
};

}  // namespace account_tags
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::account_tags::account_metadata_profile, (tags));
FC_REFLECT(wls::plugins::account_tags::account_metadata, (profile));
FC_REFLECT(wls::plugins::account_tags::account_tag_object, (id)(tag)(account))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::account_tags::account_tag_object, wls::plugins::account_tags::account_tag_index)

