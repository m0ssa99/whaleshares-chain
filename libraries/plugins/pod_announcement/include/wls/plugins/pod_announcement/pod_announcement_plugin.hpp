#pragma once

#include <appbase/application.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/follow/follow_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>
#include <wls/plugins/pod_announcement/pod_announcement_objects.hpp>
#include <wls/nudbstore/nudbstore.hpp>

#define POD_ANNOUCEMENT_PLUGIN_NAME "pod_announcement"

namespace wls {
namespace plugins {
namespace pod_announcement {

namespace detail {
class pod_announcement_plugin_impl;
}

using namespace appbase;
using wls::protocol::account_name_type;
using nudbstore::nudbstore;

class pod_announcement_plugin : public plugin<pod_announcement_plugin> {
 public:
  pod_announcement_plugin();

  virtual ~pod_announcement_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin)(wls::plugins::block_history::block_history_plugin)(wls::plugins::follow::follow_plugin))

  static const std::string &name() {
    static std::string name = POD_ANNOUCEMENT_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  const pod_announcement_state_head_object* get_pod_announcement_state_head_object(const account_name_type &account);

  nudbstore<pod_announcement_key, pod_announcement_object>& get_nudbstore_database();

 private:
  std::unique_ptr<detail::pod_announcement_plugin_impl> my;
};

}  // namespace pod_announcement
}  // namespace plugins
}  // namespace wls
