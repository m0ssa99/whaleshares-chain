#pragma once

#include <appbase/application.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>
#include <wls/plugins/block_missed/block_missed_objects.hpp>
#include <wls/nudbstore/nudbstore.hpp>

#define BLOCK_MISSED_PLUGIN_NAME "block_missed"

namespace wls {
namespace plugins {
namespace block_missed {

namespace detail {
class block_missed_plugin_impl;
}

using namespace appbase;
using wls::protocol::account_name_type;
using nudbstore::nudbstore;

class block_missed_plugin : public plugin<block_missed_plugin> {
 public:
  block_missed_plugin();

  virtual ~block_missed_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin)(wls::plugins::block_history::block_history_plugin))

  static const std::string &name() {
    static std::string name = BLOCK_MISSED_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  const block_missed_state_head_object* get_block_missed_state_head_object();

  nudbstore<block_missed_key, block_missed_object>& get_nudbstore_database();

  // friend class detail::block_missed_plugin_impl;

 private:
  std::unique_ptr<detail::block_missed_plugin_impl> my;
};

}  // namespace block_missed
}  // namespace plugins
}  // namespace wls
