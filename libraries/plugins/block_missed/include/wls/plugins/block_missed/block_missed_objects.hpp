#pragma once

#include <wls/chain/wls_object_types.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace block_missed {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;

class block_missed_key {
 public:
  block_missed_key(const uint32_t _sequence)
      : sequence(_sequence) {}

  block_missed_key() {}

  uint32_t sequence = 0;
};

class block_missed_object {
 public:
  block_missed_object() {}

  account_name_type owner;
  uint32_t block_num;
  fc::time_point_sec timestamp;
};

//------------------------------------------------------------------------------

#ifndef BLOCK_MISSED_SPACE_ID
#define BLOCK_MISSED_SPACE_ID 25
#endif

enum block_missed_object_types {
  block_missed_state_object_type = (BLOCK_MISSED_SPACE_ID << 8)
};

/**
 * to track last sequence of an account ( the head of the snake )
 */
class block_missed_state_head_object : public object<block_missed_state_object_type, block_missed_state_head_object> {
 public:
  template <typename Constructor, typename Allocator>
  block_missed_state_head_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  uint32_t sequence = 0;
};

//------------------------------------------------------------------------------

typedef block_missed_state_head_object::id_type block_missed_state_head_object_id_type;

// clang-format off
struct by_account;
typedef multi_index_container<
    block_missed_state_head_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< block_missed_state_head_object, block_missed_state_head_object_id_type, &block_missed_state_head_object::id > >
    >,
    allocator< block_missed_state_head_object >
> block_missed_state_head_object_index;
// clang-format on


}  // namespace block_missed
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::block_missed::block_missed_key, (sequence))
FC_REFLECT(wls::plugins::block_missed::block_missed_object, (owner)(block_num)(timestamp))
FC_REFLECT(wls::plugins::block_missed::block_missed_state_head_object, (id)(sequence))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::block_missed::block_missed_state_head_object, wls::plugins::block_missed::block_missed_state_head_object_index)
