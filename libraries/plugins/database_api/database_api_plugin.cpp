#include <wls/plugins/database_api/database_api_plugin.hpp>
#include <wls/plugins/database_api/database_api.hpp>

namespace wls {
namespace plugins {
namespace database_api {

database_api_plugin::database_api_plugin() {}

database_api_plugin::~database_api_plugin() {}

void database_api_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
  cfg.add_options(
  )(
      "max-blocks", bpo::value<uint32_t>()->default_value(0), "max limit n blocks in get_ops_in_blocks, default value is 10, set to 0 to disable get_ops_in_blocks."
  );
  // clang-format on
}

void database_api_plugin::plugin_initialize(const variables_map &options) {
  ilog("Initializing database_api plugin");

  api = std::make_shared<database_api>();

  if (options.count("max-blocks")) {
    api->max_blocks = options.at("max-blocks").as<uint32_t>();
  }
}

void database_api_plugin::plugin_startup() {}

void database_api_plugin::plugin_shutdown() { ilog("database_api_plugin::plugin_shutdown"); }

}  // namespace database_api
}  // namespace plugins
}  // namespace wls
