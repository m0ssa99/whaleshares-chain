#include <wls/chain/wls_objects.hpp>
#include <wls/chain/util/uint256.hpp>
#include <wls/chain/util/reward.hpp>
#include <wls/chain/history_object.hpp>
#include <wls/protocol/get_config.hpp>

#include <wls/plugins/database_api/database_api.hpp>
#include <wls/plugins/database_api/database_api_plugin.hpp>
#include <wls/plugins/database_api/database_api_objects.hpp>
#include <wls/plugins/database_api/database_api_args.hpp>
#include <wls/plugins/myfeed/myfeed_plugin.hpp>
#include <wls/plugins/podfeed/podfeed_plugin.hpp>
#include <wls/plugins/pod_announcement/pod_announcement_plugin.hpp>
#include <wls/plugins/blogfeed/blogfeed_plugin.hpp>
#include <wls/plugins/sharesfeed/sharesfeed_plugin.hpp>
#include <wls/plugins/tips_sent/tips_sent_plugin.hpp>
#include <wls/plugins/tips_received/tips_received_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>
#include <wls/plugins/account_history/account_history_plugin.hpp>
#include <wls/plugins/notification/notification_plugin.hpp>
#include <wls/plugins/block_missed/block_missed_plugin.hpp>
#include <wls/plugins/tags/tags_plugin.hpp>
#include <wls/plugins/podtags/podtags_plugin.hpp>
#include <wls/plugins/account_tags/account_tags_plugin.hpp>
#include <wls/plugins/follow_api/follow_api_plugin.hpp>
#include <wls/plugins/follow_api/follow_api.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/range/iterator_range.hpp>

#include <fc/macros.hpp>

#include <cctype>
#include <cfenv>
#include <iostream>

#define CHECK_ARG_SIZE(s) FC_ASSERT(args.size() == s, "Expected #s argument(s), was ${n}", ("n", args.size()));

namespace wls {
namespace plugins {
namespace database_api {

namespace detail {

class database_api_impl {
 public:
  database_api_impl(database_api &_api)
      : _chain(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>()),
        _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()),
        _self(_api) {
    auto _follow_api_plugin = appbase::app().find_plugin<follow::follow_api_plugin>();
    if (_follow_api_plugin != nullptr) _follow_api = _follow_api_plugin->api;

    _block_history_plugin = appbase::app().find_plugin<wls::plugins::block_history::block_history_plugin>();
    _myfeed_plugin = appbase::app().find_plugin<wls::plugins::myfeed::myfeed_plugin>();
    _podfeed_plugin = appbase::app().find_plugin<wls::plugins::podfeed::podfeed_plugin>();
    _pod_announcement_plugin = appbase::app().find_plugin<wls::plugins::pod_announcement::pod_announcement_plugin>();
    _blogfeed_plugin = appbase::app().find_plugin<wls::plugins::blogfeed::blogfeed_plugin>();
    _sharesfeed_plugin = appbase::app().find_plugin<wls::plugins::sharesfeed::sharesfeed_plugin>();
    _account_history_plugin = appbase::app().find_plugin<wls::plugins::account_history::account_history_plugin>();
    _notification_plugin = appbase::app().find_plugin<wls::plugins::notification::notification_plugin>();
    _tips_sent_plugin = appbase::app().find_plugin<wls::plugins::tips_sent::tips_sent_plugin>();
    _tips_received_plugin = appbase::app().find_plugin<wls::plugins::tips_received::tips_received_plugin>();
    _podtags_plugin = appbase::app().find_plugin<wls::plugins::podtags::podtags_plugin>();
    _account_tags_plugin = appbase::app().find_plugin<wls::plugins::account_tags::account_tags_plugin>();
    _block_missed_plugin = appbase::app().find_plugin<wls::plugins::block_missed::block_missed_plugin>();
  }

  ///////////////////
  // Subscriptions //
  ///////////////////

  // void set_block_applied_callback( std::function<void(const variant& block_header)> cb );

  get_health_check_return get_health_check(const get_health_check_args &args) const;

  /**
   * vector<tag_api_obj> get_trending_tags( string after_tag, uint32_t limit )const;
   */
  get_trending_tags_return get_trending_tags(const get_trending_tags_args &args) const;

  /**
   *  This API is a short-cut for returning all of the state required for a particular URL with a single query.
   *  state get_state( string path )const;
   */
  get_state_return get_state(const get_state_args &args) const;

  /**
   * vector< account_name_type > get_active_witnesses()const;
   */
  get_active_witnesses_return get_active_witnesses(const get_active_witnesses_args &args) const;

  /////////////////////////////
  // Blocks and transactions //
  /////////////////////////////

  /**
   * @brief Retrieve a block header
   * @param block_num Height of the block whose header should be returned
   * @return header of the referenced block, or null if no matching block was found
   *
   * optional<block_header> get_block_header(uint32_t block_num)const;
   */
  get_block_header_return get_block_header(const get_block_header_args &args) const;

  /**
   * @brief Retrieve a full, signed block
   * @param block_num Height of the block to be returned
   * @return the referenced block, or null if no matching block was found
   *
   * optional<signed_block_api_obj> get_block(uint32_t block_num)const;
   */
  get_block_return get_block(const get_block_args &args) const;

  get_blocks_missed_return get_blocks_missed(const get_blocks_missed_args &args) const;

  /**
   *  @brief Get sequence of operations included/generated within a particular block
   *  @param block_num Height of the block whose generated virtual operations should be returned
   *  @param only_virtual Whether to only include virtual operations in returned results (default: true)
   *  @return sequence of operations included/generated within the block
   *
   *  vector<applied_operation> get_ops_in_block(uint32_t block_num, bool only_virtual = true)const;
   */
  get_ops_in_block_return get_ops_in_block(const get_ops_in_block_args &args) const;

  /**
   *  @brief same to get_ops_in_block but for multiple blocks for faster lightnode synching
   *  @param from from block_num
   *  @param limit how many blocks next, max 10 blocks
   *  @param only_virtual Whether to only include virtual operations in returned results (default: true)
   *  @return sequence of operations included/generated within the block
   *
   *  vector<applied_operation> get_ops_in_blocks(uint32_t from, uint32_t limit, bool only_virtual = true)const;
   */
  get_ops_in_blocks_return get_ops_in_blocks(const get_ops_in_blocks_args &args) const;

  /////////////
  // Globals //
  /////////////

  /**
   * @brief Retrieve compile-time constants
   *
   * fc::variant_object get_config()const;
   */
  get_config_return get_config(const get_config_args &args) const;

  //               /**
  //                * @brief Return a JSON description of object representations
  //                */
  //               get_schema_return get_schema(const get_schema_args &arg) const;

  /**
   * @brief Retrieve the current @ref dynamic_global_property_object
   * dynamic_global_property_api_obj  get_dynamic_global_properties()const;
   */
  get_dynamic_global_properties_return get_dynamic_global_properties(const get_dynamic_global_properties_args &args) const;

  /**
   * chain_properties                 get_chain_properties()const;
   */
  get_chain_properties_return get_chain_properties(const get_chain_properties_args &args) const;

  /**
   * witness_schedule_api_obj         get_witness_schedule()const;
   */
  witness_schedule_api_obj get_witness_schedule(const get_witness_schedule_args &args) const;

  /**
   * hardfork_version                 get_hardfork_version()const;
   */
  get_hardfork_version_return get_hardfork_version(const get_hardfork_version_args &args) const;

  /**
   * scheduled_hardfork               get_next_scheduled_hardfork()const;
   */
  get_next_scheduled_hardfork_return get_next_scheduled_hardfork(const get_next_scheduled_hardfork_args &args) const;

  /**
   * reward_fund_api_obj              get_reward_fund( string name )const;
   */
  get_reward_fund_return get_reward_fund(const get_reward_fund_args &args) const;

  get_vesting_payout_fund_return get_vesting_payout_fund(const get_vesting_payout_fund_args &args) const;

  //////////
  // Keys //
  //////////

  /**
   * vector<set<string>> get_key_references( vector<public_key_type> key )const;
   */
  get_key_references_return get_key_references(const get_key_references_args &args) const;

  //////////////
  // Accounts //
  //////////////

  /**
   * vector< extended_account > get_accounts( vector< string > names ) const;
   */
  get_accounts_return get_accounts(const get_accounts_args &args) const;

  /**
   * vector<account_api_obj> get_accounts_by_tag( string tag, string from, uint32_t limit )const;
   */
  get_accounts_by_tag_return get_accounts_by_tag(const get_accounts_by_tag_args &args) const;

//  /**
//   *  @return all accounts that referr to the key or account id in their owner or active authorities.
//   */
//  vector<account_id_type> get_account_references(account_id_type account_id) const;

  /**
   * @brief Get a list of accounts by name
   * @param account_names Names of the accounts to retrieve
   * @return The accounts holding the provided names
   *
   * This function has semantics identical to @ref get_objects
   *
   * vector<optional<account_api_obj>> lookup_account_names(const vector<string>& account_names)const;
   */
  lookup_account_names_return lookup_account_names(const lookup_account_names_args &args) const;

  /**
   * @brief Get names and IDs for registered accounts
   * @param lower_bound_name Lower bound of the first name to return
   * @param limit Maximum number of results to return -- must not exceed 1000
   * @return Map of account names to corresponding IDs
   *
   * set<string> lookup_accounts(const string& lower_bound_name, uint32_t limit)const;
   */
  lookup_accounts_return lookup_accounts(const lookup_accounts_args &args) const;

  /**
   * @brief Get the total number of accounts registered with the blockchain
   *
   * uint64_t get_account_count()const;
   */
  get_account_count_return get_account_count(const get_account_count_args &args) const;

  /**
   * vector< withdraw_route > get_withdraw_routes( string account, withdraw_route_type type = outgoing )const;
   */
  get_withdraw_routes_return get_withdraw_routes(const get_withdraw_routes_args &args) const;

  /**
   * optional< account_bandwidth_api_obj > get_account_bandwidth( string account, witness::bandwidth_type type )const;
   */
  get_account_bandwidth_return get_account_bandwidth(const get_account_bandwidth_args &args) const;

  get_account_daily_bandwidth_return get_account_daily_bandwidth(const get_account_daily_bandwidth_args &args) const;

  ///////////////
  // Witnesses //
  ///////////////

  /**
   * @brief Get a list of witnesses by ID
   * @param witness_ids IDs of the witnesses to retrieve
   * @return The witnesses corresponding to the provided IDs
   *
   * This function has semantics identical to @ref get_objects
   *
   * vector<optional<witness_api_obj>> get_witnesses(const vector<witness_id_type>& witness_ids)const;
   */
  get_witnesses_return get_witnesses(const get_witnesses_args &args) const;

  /**
   * @brief Get the witness owned by a given account
   * @param account The name of the account whose witness should be retrieved
   * @return The witness object, or null if the account does not have a witness
   *
   * fc::optional< witness_api_obj > get_witness_by_account( string account_name )const;
   */
  get_witness_by_account_return get_witness_by_account(const get_witness_by_account_args &args) const;

  /**
   *  This method is used to fetch witnesses with pagination.
   *
   *  @return an array of `count` witnesses sorted by total votes after witness `from` with at most `limit' results.
   *
   *  vector< witness_api_obj > get_witnesses_by_vote( string from, uint32_t limit )const;
   */
  get_witnesses_by_vote_return get_witnesses_by_vote(const get_witnesses_by_vote_args &args) const;

  /**
   * @brief Get names and IDs for registered witnesses
   * @param lower_bound_name Lower bound of the first name to return
   * @param limit Maximum number of results to return -- must not exceed 1000
   * @return Map of witness names to corresponding IDs
   *
   * set<account_name_type> lookup_witness_accounts(const string& lower_bound_name, uint32_t limit)const;
   */
  lookup_witness_accounts_return lookup_witness_accounts(const lookup_witness_accounts_args &args) const;

  /**
   * @brief Get the total number of witnesses registered with the blockchain
   *
   * uint64_t get_witness_count()const;
   */
  get_witness_count_return get_witness_count(const get_witness_count_args &args) const;

  ////////////////////////////
  // Authority / validation //
  ////////////////////////////

  /**
   * @brief Get a hexdump of the serialized binary form of a transaction
   * @param args
   * @return
   *
   * std::string                   get_transaction_hex(const signed_transaction& trx)const;
   */
  get_transaction_hex_return get_transaction_hex(const get_transaction_hex_args &args) const;
  //               annotated_signed_transaction  get_transaction( transaction_id_type trx_id )const;

  /**
   *  This API will take a partially signed transaction and a set of public keys that the owner has the ability to sign for
   *  and return the minimal subset of public keys that should add signatures to the transaction.
   *
   *  set<public_key_type> get_required_signatures( const signed_transaction& trx, const flat_set<public_key_type>& available_keys )const;
   */
  get_required_signatures_return get_required_signatures(const get_required_signatures_args &args) const;

  /**
   *  This method will return the set of all public keys that could possibly sign for a given transaction.  This call can
   *  be used by wallets to filter their set of public keys to just the relevant subset prior to calling @ref get_required_signatures
   *  to get the minimum subset.
   *
   *  set<public_key_type> get_potential_signatures( const signed_transaction& trx )const;
   */
  get_potential_signatures_return get_potential_signatures(const get_potential_signatures_args &args) const;

  /**
   * @return true of the @ref trx has all of the required signatures, otherwise throws an exception
   *
   * bool           verify_authority( const signed_transaction& trx )const;
   */
  verify_authority_return verify_authority(const verify_authority_args &args) const;

  /**
   * @return true if the signers have enough authority to authorize an account
   *
   * bool           verify_account_authority( const string& name_or_id, const flat_set<public_key_type>& signers )const;
   */
  verify_account_authority_return verify_account_authority(const verify_account_authority_args &args) const;

  /**
   *  if permlink is "" then it will return all votes for author
   *
   *  vector<vote_state> get_active_votes( string author, string permlink )const;
   */
  get_active_votes_return get_active_votes(const get_active_votes_args &args) const;

  /**
   *  if permlink is "" then it will return all comment tips for author
   *
   *  vector<comment_tip> get_comment_tips( string author, string permlink )const;
   */
  get_comment_tips_return get_comment_tips(const get_comment_tips_args &args) const;

  /**
   * vector<account_vote> get_account_votes( string voter )const;
   */
  get_account_votes_return get_account_votes(const get_account_votes_args &args) const;

  /**
   * discussion           get_content( string author, string permlink )const;
   */
  get_content_return get_content(const get_content_args &args) const;

  /**
   * vector<discussion>   get_content_replies( string parent, string parent_permlink )const;
   */
  get_content_replies_return get_content_replies(const get_content_replies_args &args) const;

  ///@{ tags API
  /** This API will return the top 1000 tags used by an author sorted by most frequently used */

  /**
   * vector<pair<string,uint32_t>> get_tags_used_by_author( const string& author )const;
   */
  get_tags_used_by_author_return get_tags_used_by_author(const get_tags_used_by_author_args &args) const;

  /**
   * vector<discussion> get_discussions_by_payout(const discussion_query& query )const;
   */
  get_discussions_by_payout_return get_discussions_by_payout(const get_discussions_by_payout_args &args) const;

  /**
   * vector<discussion> get_post_discussions_by_payout( const discussion_query& query )const;
   */
  get_post_discussions_by_payout_return get_post_discussions_by_payout(const get_post_discussions_by_payout_args &args) const;

  /**
   * vector<discussion> get_comment_discussions_by_payout( const discussion_query& query )const;
   */
  get_comment_discussions_by_payout_return get_comment_discussions_by_payout(const get_comment_discussions_by_payout_args &args) const;

  /**
   * vector<discussion> get_discussions_by_trending( const discussion_query& query )const;
   */
  get_discussions_by_trending_return get_discussions_by_trending(const get_discussions_by_trending_args &args) const;

  /**
   * vector<discussion> get_discussions_by_created( const discussion_query& query )const;
   */
  get_discussions_by_created_return get_discussions_by_created(const get_discussions_by_created_args &args) const;

  /**
   * vector<discussion> get_discussions_by_active( const discussion_query& query )const;
   */
  get_discussions_by_active_return get_discussions_by_active(const get_discussions_by_active_args &args) const;

  /**
   * vector<discussion> get_discussions_by_cashout( const discussion_query& query )const;
   */
  get_discussions_by_cashout_return get_discussions_by_cashout(const get_discussions_by_cashout_args &args) const;

  /**
   * vector<discussion> get_discussions_by_votes( const discussion_query& query )const;
   */
  get_discussions_by_votes_return get_discussions_by_votes(const get_discussions_by_votes_args &args) const;

  /**
   * vector<discussion> get_discussions_by_children( const discussion_query& query )const;
   */
  get_discussions_by_children_return get_discussions_by_children(const get_discussions_by_children_args &args) const;

  /**
   * vector<discussion> get_discussions_by_hot( const discussion_query& query )const;
   */
  get_discussions_by_hot_return get_discussions_by_hot(const get_discussions_by_hot_args &args) const;

  /**
   * vector<discussion> get_discussions_by_feed( const discussion_query& query )const;
   */
  get_discussions_by_feed_return get_discussions_by_feed(const get_discussions_by_feed_args &args) const;

  /**
   * vector<discussion> get_discussions_by_blog( const discussion_query& query )const;
   */
  get_discussions_by_blog_return get_discussions_by_blog(const get_discussions_by_blog_args &args) const;

  /**
   * vector<discussion> get_discussions_by_comments( const discussion_query& query )const;
   */
  get_discussions_by_comments_return get_discussions_by_comments(const get_discussions_by_comments_args &args) const;

  ///@}

  /**
   *  For each of these filters:
   *     Get root content...
   *     Get any content...
   *     Get root content in category..
   *     Get any content in category...
   *
   *  Return discussions
   *     Total Discussion Pending Payout
   *     Last Discussion Update (or reply)... think
   *     Top Discussions by Total Payout
   *
   *  Return content (comments)
   *     Pending Payout Amount
   *     Pending Payout Time
   *     Creation Date
   *
   */
  ///@{

  /**
   *  Return the active discussions with the highest cumulative pending payouts without respect to category, total
   *  pending payout means the pending payout of all children as well.
   *
   *  vector<discussion>   get_replies_by_last_update( account_name_type start_author, string start_permlink, uint32_t limit )const;
   */
  get_replies_by_last_update_return get_replies_by_last_update(const get_replies_by_last_update_args &args) const;

  /**
   *  This method is used to fetch all posts/comments by start_author that occur after before_date and start_permlink with up to limit being returned.
   *
   *  If start_permlink is empty then only before_date will be considered. If both are specified the eariler to the two metrics will be used. This
   *  should allow easy pagination.
   *
   *  vector<discussion>   get_discussions_by_author_before_date( string author, string start_permlink, time_point_sec before_date, uint32_t limit )const;
   */
  get_discussions_by_author_before_date_return get_discussions_by_author_before_date(get_discussions_by_author_before_date_args args) const;

  /**
   *  Account operations have sequence numbers from 0 to N where N is the most recent operation. This method returns operations in the range [from-limit, from]
   *
   *  @param from - the absolute sequence number, -1 means most recent, limit is the number of operations before from.
   *  @param limit - the maximum number of items that can be queried (0 to 1000], must be less than from
   *
   *  map<uint32_t, applied_operation> get_account_history( string account, uint32_t from, uint32_t limit )const;
   */
  get_account_history_return get_account_history(const get_account_history_args &args) const;
  get_account_notification_return get_account_notification(const get_account_notification_args &args) const;

  lookup_pods_return lookup_pods(const lookup_pods_args &args) const;
  get_pods_return get_pods(const get_pods_args &args) const;
  get_pods_by_tag_return get_pods_by_tag(const get_pods_by_tag_args &args) const;
  lookup_pod_members_return lookup_pod_members(const lookup_pod_members_args &args) const;
  get_pod_members_by_user_return get_pod_members_by_user(const get_pod_members_by_user_args &args) const;
  get_pod_join_requests_return get_pod_join_requests(const get_pod_join_requests_args &args) const;
  get_pod_join_requests_by_user_return get_pod_join_requests_by_user(const get_pod_join_requests_by_user_args &args) const;

  get_friends_return get_friends(const get_friends_args &args) const;
  get_friend_requests_from_return get_friend_requests_from(const get_friend_requests_from_args &args) const;
  get_friend_requests_to_return get_friend_requests_to(const get_friend_requests_to_args &args) const;

  /**
   * (string account, uint64_t from, uint32_t limit)
   */
  get_user_feed_return get_user_feed(const get_user_feed_args &args) const;
  get_pod_feed_return get_pod_feed(const get_pod_feed_args &args) const;
  get_pod_announcements_return get_pod_announcements(const get_pod_announcements_args &args) const;
  get_blog_feed_return get_blog_feed(const get_blog_feed_args &args) const;
  get_shares_feed_return get_shares_feed(const get_shares_feed_args &args) const;

  get_htlcs_from_return get_htlcs_from(const get_htlcs_from_args &args) const;
  get_htlcs_to_return get_htlcs_to(const get_htlcs_to_args &args) const;

  /**
   * (string account, uint64_t from, uint32_t limit)
   */
  get_tips_from_return get_tips_from(const get_tips_from_args &args) const;
  get_tips_to_return get_tips_to(const get_tips_to_args &args) const;

  //--
  chain::chain_plugin &_chain;
  chain::database &_db;
  database_api &_self;
  std::shared_ptr<wls::plugins::follow::follow_api> _follow_api;
  wls::plugins::block_history::block_history_plugin *_block_history_plugin;
  wls::plugins::myfeed::myfeed_plugin *_myfeed_plugin;
  wls::plugins::podfeed::podfeed_plugin *_podfeed_plugin;
  wls::plugins::pod_announcement::pod_announcement_plugin *_pod_announcement_plugin;
  wls::plugins::blogfeed::blogfeed_plugin *_blogfeed_plugin;
  wls::plugins::sharesfeed::sharesfeed_plugin *_sharesfeed_plugin;
  wls::plugins::account_history::account_history_plugin *_account_history_plugin;
  wls::plugins::notification::notification_plugin *_notification_plugin;
  wls::plugins::tips_sent::tips_sent_plugin *_tips_sent_plugin;
  wls::plugins::tips_received::tips_received_plugin *_tips_received_plugin;
  wls::plugins::podtags::podtags_plugin *_podtags_plugin;
  wls::plugins::account_tags::account_tags_plugin *_account_tags_plugin;
  wls::plugins::block_missed::block_missed_plugin *_block_missed_plugin;

 private:
  void set_pending_payout(discussion &d) const;

  void set_url(discussion &d) const;

  discussion get_discussion(comment_id_type id, uint32_t truncate_body = 0) const;

  static bool filter_default(const comment_api_obj &c) { return false; }
  static bool exit_default(const comment_api_obj &c) { return false; }
  static bool tag_exit_default(const tags::tag_object &c) { return false; }

  template <typename Index, typename StartItr>
  vector<discussion> get_discussions(const discussion_query &q, const string &tag, comment_id_type parent, const Index &tidx, StartItr tidx_itr,
                                     uint32_t truncate_body = 0, const std::function<bool(const comment_api_obj &)> &filter = &filter_default,
                                     const std::function<bool(const comment_api_obj &)> &exit = &exit_default,
                                     const std::function<bool(const tags::tag_object &)> &tag_exit = &tag_exit_default, bool ignore_parent = false) const;

  comment_id_type get_parent(const discussion_query &q) const;

  void recursively_fetch_content(state &_state, discussion &root, set<string> &referenced_accounts) const;
};

get_health_check_return database_api_impl::get_health_check(const get_health_check_args &args) const {
  CHECK_ARG_SIZE(0)

  health_check result;

  const auto &props = _db.get_dynamic_global_properties();
  const auto &now = fc::time_point::now();
  if (now.sec_since_epoch() - props.time.sec_since_epoch() > 60) {
    result.status = 503;
    result.message = "HEALTH_CHECK_ERROR";
  }

  return result;
}

get_trending_tags_return database_api_impl::get_trending_tags(const get_trending_tags_args &args) const {
  CHECK_ARG_SIZE(2)

  string after_tag = args[0].as<string>();
  uint32_t limit = args[1].as<uint32_t>();

  FC_ASSERT(limit <= 1000, "Cannot retrieve more than 1000 tags at a time.");
  vector<tag_api_obj> result;

  result.reserve(limit);

  const auto &nidx = _db.get_index<tags::tag_stats_index>().indices().get<tags::by_tag>();
  const auto &ridx = _db.get_index<tags::tag_stats_index>().indices().get<tags::by_trending>();
  auto itr = ridx.begin();
  if (after_tag != "" && nidx.size()) {
    auto nitr = nidx.lower_bound(after_tag);
    if (nitr == nidx.end())
      itr = ridx.end();
    else
      itr = ridx.iterator_to(*nitr);
  }

  while (itr != ridx.end() && result.size() < limit) {
    result.push_back(tag_api_obj(*itr));
    ++itr;
  }

  return result;
}

get_state_return database_api_impl::get_state(const get_state_args &args) const {
  CHECK_ARG_SIZE(1)
  string path = args[0].as<string>();

  state _state;
  _state.props = get_dynamic_global_properties(get_dynamic_global_properties_args{});
  _state.current_route = path;

  try {
    if (path.size() && path[0] == '/') path = path.substr(1);  /// remove '/' from front

    if (!path.size()) path = "trending";

    /// FETCH CATEGORY STATE
    auto trending_tags = get_trending_tags(get_trending_tags_args{std::string(), 50});
    for (const auto &t : trending_tags) {
      _state.tag_idx.trending.push_back(string(t.name));
    }
    /// END FETCH CATEGORY STATE

    set<string> accounts;

    vector<string> part;
    part.reserve(4);
    boost::split(part, path, boost::is_any_of("/"));
    part.resize(std::max(part.size(), size_t(4)));  // at least 4

    auto tag = fc::to_lower(part[1]);

    if (part[0].size() && part[0][0] == '@') {
      auto acnt = part[0].substr(1);
      _state.accounts[acnt] = extended_account(_db.get_account(acnt), _db);
      _state.accounts[acnt].tags_usage = get_tags_used_by_author(get_tags_used_by_author_args{acnt});
      if (_follow_api) {
//        _state.accounts[acnt].guest_bloggers = _follow_api->get_blog_authors(wls::plugins::follow::get_blog_authors_args{acnt});
        _state.accounts[acnt].reputation = 0;  //_follow_api_plugin->get_account_reputations(acnt, 1)[0].reputation;
      }
      auto &eacnt = _state.accounts[acnt];
      if (part[1] == "transfers") {
        auto history = get_account_history(get_account_history_args{acnt, uint32_t(-1), 100});
        for (auto &item : history) {
          switch (item.second.op.which()) {
            case operation::tag<transfer_to_vesting_operation>::value:
            case operation::tag<withdraw_vesting_operation>::value:
            case operation::tag<transfer_operation>::value:
            case operation::tag<author_reward_operation>::value:
            case operation::tag<curation_reward_operation>::value:
            case operation::tag<comment_benefactor_reward_operation>::value:
            case operation::tag<claim_reward_balance_operation>::value:
              eacnt.transfer_history[item.first] = item.second;
              break;
            case operation::tag<comment_operation>::value:
              //   eacnt.post_history[item.first] =  item.second;
              break;
            case operation::tag<vote_operation>::value:
            case operation::tag<account_witness_vote_operation>::value:
            case operation::tag<account_witness_proxy_operation>::value:
              //   eacnt.vote_history[item.first] =  item.second;
              break;
            case operation::tag<account_create_operation>::value:
            case operation::tag<account_update_operation>::value:
            case operation::tag<witness_update_operation>::value:
            case operation::tag<custom_operation>::value:
            case operation::tag<producer_reward_operation>::value:
            case operation::tag<devfund_operation>::value:
            default:
//              eacnt.other_history[item.first] = item.second;
              break;
          }
        }
      } else if (part[1] == "recent-replies") {
        auto replies = get_replies_by_last_update(get_replies_by_last_update_args{acnt, "", 50});
        eacnt.recent_replies = vector<string>();
        for (const auto &reply : replies) {
          auto reply_ref = reply.author + "/" + reply.permlink;
          _state.content[reply_ref] = reply;
          if (_follow_api) {
            _state.accounts[reply_ref].reputation = 0;  //_follow_api->get_account_reputations(reply.author, 1)[0].reputation;
          }
          eacnt.recent_replies->push_back(reply_ref);
        }
      } else if (part[1] == "posts" || part[1] == "comments") {
#ifndef IS_LOW_MEM
        int count = 0;
        const auto &pidx = _db.get_index<comment_index>().indices().get<by_author_last_update>();
        auto itr = pidx.lower_bound(acnt);
        eacnt.comments = vector<string>();

        while (itr != pidx.end() && itr->author == acnt && count < 20) {
          if (itr->parent_author.size()) {
            const auto link = acnt + "/" + to_string(itr->permlink);
            eacnt.comments->push_back(link);
            _state.content[link] = *itr;
            set_pending_payout(_state.content[link]);
            ++count;
          }

          ++itr;
        }
#endif
      } else if (part[1].size() == 0 || part[1] == "blog") {
        if (_follow_api) {
          auto blog = _follow_api->get_blog_entries(wls::plugins::follow::get_blog_entries_args{variant(eacnt.name), variant(0), variant(20)});
          eacnt.blog = vector<string>();

          for (auto b : blog) {
            const auto link = b.author + "/" + b.permlink;
            eacnt.blog->push_back(link);
            _state.content[link] = _db.get_comment(b.author, b.permlink);
            set_pending_payout(_state.content[link]);

            if (b.reblog_on > time_point_sec()) {
              _state.content[link].first_reblogged_on = b.reblog_on;
            }
          }
        }
      } else if (part[1].size() == 0 || part[1] == "feed") {
        if (_follow_api) {
          auto feed = _follow_api->get_feed_entries(wls::plugins::follow::get_blog_entries_args{variant(eacnt.name), variant(0), variant(20)});
          eacnt.feed = vector<string>();

          for (auto f : feed) {
            const auto link = f.author + "/" + f.permlink;
            eacnt.feed->push_back(link);
            _state.content[link] = _db.get_comment(f.author, f.permlink);
            set_pending_payout(_state.content[link]);
            if (f.reblog_by.size()) {
              if (f.reblog_by.size()) _state.content[link].first_reblogged_by = f.reblog_by[0];
              _state.content[link].reblogged_by = f.reblog_by;
              _state.content[link].first_reblogged_on = f.reblog_on;
            }
          }
        }
      }
    }
    /// pull a complete discussion
    else if (part[1].size() && part[1][0] == '@') {
      auto account = part[1].substr(1);
      auto slug = part[2];

      auto key = account + "/" + slug;
      auto dis = get_content(get_content_args{account, slug});

      recursively_fetch_content(_state, dis, accounts);
      _state.content[key] = std::move(dis);
    } else if (part[0] == "witnesses" || part[0] == "~witnesses") {
      auto wits = get_witnesses_by_vote(get_witnesses_by_vote_args{"", 50});
      for (const auto &w : wits) {
        _state.witnesses[w.owner] = w;
      }
    } else if (part[0] == "trending") {
      discussion_query q;
      q.tag = tag;
      q.limit = 20;
      q.truncate_body = 1024;
      auto trending_disc = get_discussions_by_trending({variant(q)});

      auto &didx = _state.discussion_idx[tag];
      for (const auto &d : trending_disc) {
        auto key = d.author + "/" + d.permlink;
        didx.trending.push_back(key);
        if (d.author.size()) accounts.insert(d.author);
        _state.content[key] = std::move(d);
      }
    } else if (part[0] == "payout") {
      discussion_query q;
      q.tag = tag;
      q.limit = 20;
      q.truncate_body = 1024;
      auto trending_disc = get_post_discussions_by_payout({variant(q)});

      auto &didx = _state.discussion_idx[tag];
      for (const auto &d : trending_disc) {
        auto key = d.author + "/" + d.permlink;
        didx.payout.push_back(key);
        if (d.author.size()) accounts.insert(d.author);
        _state.content[key] = std::move(d);
      }
    } else if (part[0] == "payout_comments") {
      discussion_query q;
      q.tag = tag;
      q.limit = 20;
      q.truncate_body = 1024;
      auto trending_disc = get_comment_discussions_by_payout({variant(q)});

      auto &didx = _state.discussion_idx[tag];
      for (const auto &d : trending_disc) {
        auto key = d.author + "/" + d.permlink;
        didx.payout_comments.push_back(key);
        if (d.author.size()) accounts.insert(d.author);
        _state.content[key] = std::move(d);
      }
    } else if (part[0] == "responses") {
      discussion_query q;
      q.tag = tag;
      q.limit = 20;
      q.truncate_body = 1024;
      auto trending_disc = get_discussions_by_children({variant(q)});

      auto &didx = _state.discussion_idx[tag];
      for (const auto &d : trending_disc) {
        auto key = d.author + "/" + d.permlink;
        didx.responses.push_back(key);
        if (d.author.size()) accounts.insert(d.author);
        _state.content[key] = std::move(d);
      }
    } else if (!part[0].size() || part[0] == "hot") {
      discussion_query q;
      q.tag = tag;
      q.limit = 20;
      q.truncate_body = 1024;
      auto trending_disc = get_discussions_by_hot({variant(q)});

      auto &didx = _state.discussion_idx[tag];
      for (const auto &d : trending_disc) {
        auto key = d.author + "/" + d.permlink;
        didx.hot.push_back(key);
        if (d.author.size()) accounts.insert(d.author);
        _state.content[key] = std::move(d);
      }
    } else if (part[0] == "votes") {
      discussion_query q;
      q.tag = tag;
      q.limit = 20;
      q.truncate_body = 1024;
      auto trending_disc = get_discussions_by_votes({variant(q)});

      auto &didx = _state.discussion_idx[tag];
      for (const auto &d : trending_disc) {
        auto key = d.author + "/" + d.permlink;
        didx.votes.push_back(key);
        if (d.author.size()) accounts.insert(d.author);
        _state.content[key] = std::move(d);
      }
    } else if (part[0] == "cashout") {
      discussion_query q;
      q.tag = tag;
      q.limit = 20;
      q.truncate_body = 1024;
      auto trending_disc = get_discussions_by_cashout({variant(q)});

      auto &didx = _state.discussion_idx[tag];
      for (const auto &d : trending_disc) {
        auto key = d.author + "/" + d.permlink;
        didx.cashout.push_back(key);
        if (d.author.size()) accounts.insert(d.author);
        _state.content[key] = std::move(d);
      }
    } else if (part[0] == "active") {
      discussion_query q;
      q.tag = tag;
      q.limit = 20;
      q.truncate_body = 1024;
      auto trending_disc = get_discussions_by_active({variant(q)});

      auto &didx = _state.discussion_idx[tag];
      for (const auto &d : trending_disc) {
        auto key = d.author + "/" + d.permlink;
        didx.active.push_back(key);
        if (d.author.size()) accounts.insert(d.author);
        _state.content[key] = std::move(d);
      }
    } else if (part[0] == "created") {
      discussion_query q;
      q.tag = tag;
      q.limit = 20;
      q.truncate_body = 1024;
      auto trending_disc = get_discussions_by_created({variant(q)});

      auto &didx = _state.discussion_idx[tag];
      for (const auto &d : trending_disc) {
        auto key = d.author + "/" + d.permlink;
        didx.created.push_back(key);
        if (d.author.size()) accounts.insert(d.author);
        _state.content[key] = std::move(d);
      }
    } else if (part[0] == "recent") {
      discussion_query q;
      q.tag = tag;
      q.limit = 20;
      q.truncate_body = 1024;
      auto trending_disc = get_discussions_by_created({variant(q)});

      auto &didx = _state.discussion_idx[tag];
      for (const auto &d : trending_disc) {
        auto key = d.author + "/" + d.permlink;
        didx.created.push_back(key);
        if (d.author.size()) accounts.insert(d.author);
        _state.content[key] = std::move(d);
      }
    } else if (part[0] == "tags") {
      _state.tag_idx.trending.clear();
      auto trending_tags = get_trending_tags(get_trending_tags_args{std::string(), 250});
      for (const auto &t : trending_tags) {
        string name = t.name;
        _state.tag_idx.trending.push_back(name);
        _state.tags[name] = t;
      }
    } else {
      elog("What... no matches");
    }

    for (const auto &a : accounts) {
      _state.accounts.erase("");
      _state.accounts[a] = extended_account(_db.get_account(a), _db);
      if (_follow_api) {
        _state.accounts[a].reputation = 0;  //_follow_api->get_account_reputations(a, 1)[0].reputation;
      }
    }
    for (auto &d : _state.content) {
      d.second.active_votes = get_active_votes({variant(d.second.author), variant(d.second.permlink)});
      d.second.comment_tips = get_comment_tips({variant(d.second.author), variant(d.second.permlink)});
    }

    _state.witness_schedule = _db.get_witness_schedule_object();

  } catch (const fc::exception &e) {
    _state.error = e.to_detail_string();
  }
  return _state;
}

void database_api_impl::set_pending_payout(discussion &d) const {
  asset pot;
  pot = _db.get_reward_fund(_db.get_comment(d.author, d.permlink)).reward_balance;

  u256 total_r2 = 0;
  total_r2 = wls::chain::util::to256(_db.get_reward_fund(_db.get_comment(d.author, d.permlink)).recent_claims);

  if (total_r2 > 0) {
    uint128_t vshares;
    const auto &rf = _db.get_reward_fund(_db.get_comment(d.author, d.permlink));
    vshares = d.net_rshares.value > 0 ? wls::chain::util::evaluate_reward_curve(d.net_rshares.value, rf.author_reward_curve, rf.content_constant) : 0;

    u256 r2 = wls::chain::util::to256(vshares);  // to256(abs_net_rshares);
    r2 *= pot.amount.value;
    r2 /= total_r2;

    d.pending_payout_value = asset(static_cast<uint64_t>(r2), pot.symbol);

    if (_follow_api) {
      d.author_reputation = 0;  // _follow_api->get_account_reputations(d.author, 1)[0].reputation;
    }
  }

  if (d.parent_author != WLS_ROOT_POST_PARENT) d.cashout_time = _db.calculate_discussion_payout_time(_db.get<comment_object>(d.id));

  if (d.body.size() > 1024 * 128) d.body = "body pruned due to size";
  if (d.parent_author.size() > 0 && d.body.size() > 1024 * 16) d.body = "comment pruned due to size";

  set_url(d);
}

void database_api_impl::recursively_fetch_content(state &_state, discussion &root, set<string> &referenced_accounts) const {
//  return _db.with_read_lock([&]() {
    try {
      if (root.author.size()) referenced_accounts.insert(root.author);

      auto replies = get_content_replies({variant(root.author), variant(root.permlink)});
      for (auto &r : replies) {
        try {
          recursively_fetch_content(_state, r, referenced_accounts);
          root.replies.push_back(r.author + "/" + r.permlink);
          _state.content[r.author + "/" + r.permlink] = std::move(r);
          if (r.author.size()) referenced_accounts.insert(r.author);
        } catch (const fc::exception &e) {
          edump((e.to_detail_string()));
        }
      }
    }
    FC_CAPTURE_AND_RETHROW((root.author)(root.permlink))
//  });
}

void database_api_impl::set_url(discussion &d) const {
  const comment_api_obj root(_db.get<comment_object, by_id>(d.root_comment));
  d.url = "/" + root.category + "/@" + root.author + "/" + root.permlink;
  d.root_title = root.title;
  if (root.id != d.id) d.url += "#@" + d.author + "/" + d.permlink;
}

get_active_witnesses_return database_api_impl::get_active_witnesses(const get_active_witnesses_args &args) const {
  const auto &wso = _db.get_witness_schedule_object();
  size_t n = wso.current_shuffled_witnesses.size();
  vector<account_name_type> result;
  result.reserve(n);
  for (size_t i = 0; i < n; i++) result.push_back(wso.current_shuffled_witnesses[i]);
  return result;
}

get_dynamic_global_properties_return database_api_impl::get_dynamic_global_properties(const get_dynamic_global_properties_args &args) const {
  CHECK_ARG_SIZE(0)
  return _db.get_dynamic_global_properties();
}

get_tags_used_by_author_return database_api_impl::get_tags_used_by_author(const get_tags_used_by_author_args &args) const {
  CHECK_ARG_SIZE(1)
  string author = args[0].as<string>();

  vector<pair<string, uint32_t>> result;
  const auto *acnt = _db.find_account(author);
  FC_ASSERT(acnt != nullptr);
  const auto &tidx = _db.get_index<tags::author_tag_stats_index>().indices().get<tags::by_author_posts_tag>();
  auto itr = tidx.lower_bound(boost::make_tuple(acnt->id, 0));
  while (itr != tidx.end() && itr->author == acnt->id && result.size() < 1000) {
    result.push_back(std::make_pair(itr->tag, itr->total_posts));
    ++itr;
  }
  return result;
}

get_account_history_return database_api_impl::get_account_history(const get_account_history_args &args) const {
  CHECK_ARG_SIZE(3)
  string account = args[0].as<string>();
  uint32_t from = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 100, "Limit of ${l} is greater than maxmimum allowed", ("l", limit));
  FC_ASSERT(from >= limit, "From must be greater than limit");
  // idump((account)(from)(limit));

  get_account_history_return result;

  if (_account_history_plugin == nullptr) return result;

  auto head_obj = _account_history_plugin->get_account_history_state_head_object(account);
  if (head_obj == nullptr) return result;
  if (head_obj->sequence == 0) return result;
  if (from >= head_obj->sequence) from = head_obj->sequence - 1;

  boost::system::error_code ec;
  uint32_t n = 0;
  while (true) {
    if (n >= limit) break;
    if (from < n) break;

    auto idx = from-n;
    wls::plugins::account_history::account_history_key obj_key{account, idx};
    wls::plugins::account_history::account_history_object obj = _account_history_plugin->get_nudbstore_database().fetch(obj_key, ec);
    applied_operation ao(obj);
//    result[idx] = ao;
    result.emplace_back(idx, obj);

    ++n;
  }

  return result;
}

get_account_notification_return database_api_impl::get_account_notification(const get_account_notification_args &args) const {
  CHECK_ARG_SIZE(3)
  string account = args[0].as<string>();
  uint32_t from = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 100, "Limit of ${l} is greater than maxmimum allowed", ("l", limit));
  FC_ASSERT(from >= limit, "From must be greater than limit");
  // idump((account)(from)(limit));

  get_account_notification_return result;

  auto head_obj = _notification_plugin->get_notification_state_head_object(account);
  if (head_obj == nullptr) return result;
  if (head_obj->sequence == 0) return result;
  if (from >= head_obj->sequence) from = head_obj->sequence - 1;

  boost::system::error_code ec;
  uint32_t n = 0;
  while (true) {
    if (n >= limit) break;
    if (from < n) break;

    auto idx = from-n;
    wls::plugins::notification::notification_key obj_key{account, idx};
    wls::plugins::notification::notification_object obj = _notification_plugin->get_nudbstore_database().fetch(obj_key, ec);
    applied_operation ao(obj);
//    result[idx] = ao;
    result.emplace_back(idx, obj);

    ++n;
  }

  return result;
}

get_witnesses_by_vote_return database_api_impl::get_witnesses_by_vote(const get_witnesses_by_vote_args &args) const {
  CHECK_ARG_SIZE(2)
  string from = args[0].as<string>();
  uint32_t limit = args[1].as<uint32_t>();

  // idump((from)(limit));
  FC_ASSERT(limit <= 100);

  vector<witness_api_obj> result;
  result.reserve(limit);

  const auto &name_idx = _db.get_index<witness_index>().indices().get<by_name>();
  const auto &vote_idx = _db.get_index<witness_index>().indices().get<by_vote_name>();

  auto itr = vote_idx.begin();
  if (from.size()) {
    auto nameitr = name_idx.find(from);
    FC_ASSERT(nameitr != name_idx.end(), "invalid witness name ${n}", ("n", from));
    itr = vote_idx.iterator_to(*nameitr);
  }

  while (itr != vote_idx.end() && result.size() < limit && itr->votes > 0) {
    result.push_back(witness_api_obj(*itr));
    ++itr;
  }
  return result;
}

get_comment_discussions_by_payout_return database_api_impl::get_comment_discussions_by_payout(const get_comment_discussions_by_payout_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  auto tag = fc::to_lower(query.tag);
  auto parent = comment_id_type(1);

  const auto &tidx = _db.get_index<tags::tag_index>().indices().get<tags::by_reward_fund_net_rshares>();
  auto tidx_itr = tidx.lower_bound(boost::make_tuple(tag, false));

  return get_discussions(query, tag, parent, tidx, tidx_itr, query.truncate_body, [](const comment_api_obj &c) { return c.net_rshares <= 0; }, exit_default,
                         tag_exit_default, true);
}

template <typename Index, typename StartItr>
vector<discussion> database_api_impl::get_discussions(const discussion_query &q, const string &tag, comment_id_type parent, const Index &tidx,
                                                      StartItr tidx_itr, uint32_t truncate_body, const std::function<bool(const comment_api_obj &)> &filter,
                                                      const std::function<bool(const comment_api_obj &)> &exit,
                                                      const std::function<bool(const tags::tag_object &)> &tag_exit, bool ignore_parent) const {
  // idump((query));
  vector<discussion> result;

  const auto &cidx = _db.get_index<tags::tag_index>().indices().get<tags::by_comment>();
  comment_id_type start;

  if (q.start_author && q.start_permlink) {
    start = _db.get_comment(*q.start_author, *q.start_permlink).id;
    auto itr = cidx.find(start);
    while (itr != cidx.end() && itr->comment == start) {
      if (itr->tag == tag) {
        tidx_itr = tidx.iterator_to(*itr);
        break;
      }
      ++itr;
    }
  }

  uint32_t count = q.limit;
  uint64_t itr_count = 0;
  uint64_t filter_count = 0;
  uint64_t exc_count = 0;
  uint64_t max_itr_count = 10 * q.limit;
  while (count > 0 && tidx_itr != tidx.end()) {
    ++itr_count;
    if (itr_count > max_itr_count) {
      wlog("Maximum iteration count exceeded serving query: ${q}", ("q", q));
      wlog("count=${count}   itr_count=${itr_count}   filter_count=${filter_count}   exc_count=${exc_count}",
           ("count", count)("itr_count", itr_count)("filter_count", filter_count)("exc_count", exc_count));
      break;
    }
    if (tidx_itr->tag != tag || (!ignore_parent && tidx_itr->parent != parent)) break;
    try {
      result.push_back(get_discussion(tidx_itr->comment, truncate_body));

      if (filter(result.back())) {
        result.pop_back();
        ++filter_count;
      } else if (exit(result.back()) || tag_exit(*tidx_itr)) {
        result.pop_back();
        break;
      } else
        --count;
    } catch (const fc::exception &e) {
      ++exc_count;
      edump((e.to_detail_string()));
    }
    ++tidx_itr;
  }

  return result;
}

discussion database_api_impl::get_discussion(comment_id_type id, uint32_t truncate_body) const {
  discussion d = _db.get(id);
  set_url(d);
  set_pending_payout(d);
  d.active_votes = get_active_votes({variant(d.author), variant(d.permlink)});
  d.comment_tips = get_comment_tips({variant(d.author), variant(d.permlink)});
  d.body_length = d.body.size();
  if (truncate_body) {
    d.body = d.body.substr(0, truncate_body);

    if (!fc::is_utf8(d.body)) d.body = fc::prune_invalid_utf8(d.body);
  }
  return d;
}

get_discussions_by_children_return database_api_impl::get_discussions_by_children(const get_discussions_by_children_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  auto tag = fc::to_lower(query.tag);
  auto parent = get_parent(query);

  const auto &tidx = _db.get_index<tags::tag_index>().indices().get<tags::by_parent_children>();
  auto tidx_itr = tidx.lower_bound(boost::make_tuple(tag, parent, std::numeric_limits<int32_t>::max()));

  return get_discussions(query, tag, parent, tidx, tidx_itr, query.truncate_body);
}

comment_id_type database_api_impl::get_parent(const discussion_query &q) const {
//  return _db.with_read_lock([&]() {
    comment_id_type parent;
    if (q.parent_author && q.parent_permlink) {
      parent = _db.get_comment(*q.parent_author, *q.parent_permlink).id;
    }
    return parent;
//  });
}

get_discussions_by_hot_return database_api_impl::get_discussions_by_hot(const get_discussions_by_hot_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  auto tag = fc::to_lower(query.tag);
  auto parent = get_parent(query);

  const auto &tidx = _db.get_index<tags::tag_index>().indices().get<tags::by_parent_hot>();
  auto tidx_itr = tidx.lower_bound(boost::make_tuple(tag, parent, std::numeric_limits<double>::max()));

  return get_discussions(query, tag, parent, tidx, tidx_itr, query.truncate_body, [](const comment_api_obj &c) { return c.net_rshares <= 0; });
}

get_discussions_by_votes_return database_api_impl::get_discussions_by_votes(const get_discussions_by_votes_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  auto tag = fc::to_lower(query.tag);
  auto parent = get_parent(query);

  const auto &tidx = _db.get_index<tags::tag_index>().indices().get<tags::by_parent_net_votes>();
  auto tidx_itr = tidx.lower_bound(boost::make_tuple(tag, parent, std::numeric_limits<int32_t>::max()));

  return get_discussions(query, tag, parent, tidx, tidx_itr, query.truncate_body);
}

get_discussions_by_cashout_return database_api_impl::get_discussions_by_cashout(const get_discussions_by_cashout_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  vector<discussion> result;

  auto tag = fc::to_lower(query.tag);
  auto parent = get_parent(query);

  const auto &tidx = _db.get_index<tags::tag_index>().indices().get<tags::by_cashout>();
  auto tidx_itr = tidx.lower_bound(boost::make_tuple(tag, fc::time_point::now() - fc::minutes(60)));

  vector<discussion> discussions =
      get_discussions(query, tag, parent, tidx, tidx_itr, query.truncate_body, [](const comment_api_obj &c) { return c.net_rshares < 0; });
  return get_discussions_by_cashout_return{discussions};
}

get_discussions_by_active_return database_api_impl::get_discussions_by_active(const get_discussions_by_active_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  auto tag = fc::to_lower(query.tag);
  auto parent = get_parent(query);

  const auto &tidx = _db.get_index<tags::tag_index>().indices().get<tags::by_parent_active>();
  auto tidx_itr = tidx.lower_bound(boost::make_tuple(tag, parent, fc::time_point_sec::maximum()));

  return get_discussions(query, tag, parent, tidx, tidx_itr, query.truncate_body);
}

get_discussions_by_created_return database_api_impl::get_discussions_by_created(const get_discussions_by_created_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  auto tag = fc::to_lower(query.tag);
  auto parent = get_parent(query);

  const auto &tidx = _db.get_index<tags::tag_index>().indices().get<tags::by_parent_created>();
  auto tidx_itr = tidx.lower_bound(boost::make_tuple(tag, parent, fc::time_point_sec::maximum()));

  return get_discussions(query, tag, parent, tidx, tidx_itr, query.truncate_body);
}

get_active_votes_return database_api_impl::get_active_votes(const get_active_votes_args &args) const {
  CHECK_ARG_SIZE(2)
  string author = args[0].as<string>();
  string permlink = args[1].as<string>();

  vector<vote_state> result;
  const auto &comment = _db.get_comment(author, permlink);
  const auto &idx = _db.get_index<comment_vote_index>().indices().get<by_comment_voter>();
  comment_id_type cid(comment.id);
  auto itr = idx.lower_bound(cid);
  while (itr != idx.end() && itr->comment == cid) {
    const auto &vo = _db.get(itr->voter);
    vote_state vstate;
    vstate.voter = vo.name;
    vstate.weight = itr->weight;
    vstate.rshares = itr->rshares;
    vstate.percent = itr->vote_percent;
    vstate.time = itr->last_update;

    //if (_follow_api_plugin) {
    //   auto reps = 0; //_follow_api->get_account_reputations(vo.name, 1);
    //   if (reps.size())
    //      vstate.reputation = reps[0].reputation;
    //}

    result.push_back(vstate);
    ++itr;
  }
  return result;
}

get_comment_tips_return database_api_impl::get_comment_tips(const get_comment_tips_args &args) const {
  CHECK_ARG_SIZE(2)
  string author = args[0].as<string>();
  string permlink = args[1].as<string>();

  vector<comment_tip> result;
  const auto &comment = _db.get_comment(author, permlink);
  const auto &idx = _db.get_index<comment_tip_index>().indices().get<by_comment_tipper>();
  comment_id_type cid(comment.id);
  auto itr = idx.lower_bound(cid);
  while (itr != idx.end() && itr->comment == cid) {
    const auto &vo = _db.get(itr->tipper);

    comment_tip tip;
    tip.tipper = vo.name;
    tip.amount = asset(itr->amount, WLS_SYMBOL);

    result.push_back(tip);
    ++itr;
  }
  return result;
}

get_content_replies_return database_api_impl::get_content_replies(const get_content_replies_args &args) const {
  CHECK_ARG_SIZE(2)
  string parent = args[0].as<string>();
  string parent_permlink = args[1].as<string>();

  account_name_type acc_name = account_name_type(parent);
  const auto &by_permlink_idx = _db.get_index<comment_index>().indices().get<by_parent>();
  auto itr = by_permlink_idx.find(boost::make_tuple(acc_name, parent_permlink));
  vector<discussion> result;
  while (itr != by_permlink_idx.end() && itr->parent_author == parent && to_string(itr->parent_permlink) == parent_permlink) {
    result.push_back(discussion(*itr));
    set_pending_payout(result.back());
    ++itr;
  }
  return result;
}

get_content_return database_api_impl::get_content(const get_content_args &args) const {
  CHECK_ARG_SIZE(2)
  string author = args[0].as<string>();
  string permlink = args[1].as<string>();

  const auto &by_permlink_idx = _db.get_index<comment_index>().indices().get<by_permlink>();
  auto itr = by_permlink_idx.find(boost::make_tuple(author, permlink));
  if (itr != by_permlink_idx.end()) {
    discussion result(*itr);
    set_pending_payout(result);
    result.active_votes = get_active_votes(get_active_votes_args{author, permlink});
    result.comment_tips = get_comment_tips(get_active_votes_args{author, permlink});
    return result;
  }
  return discussion();
}

get_replies_by_last_update_return database_api_impl::get_replies_by_last_update(const get_replies_by_last_update_args &args) const {
  CHECK_ARG_SIZE(3)
  account_name_type start_author = args[0].as<account_name_type>();
  string start_permlink = args[1].as<string>();
  uint32_t limit = args[2].as<uint32_t>();
	FC_UNUSED(start_author,limit);
  vector<discussion> result;

#ifndef IS_LOW_MEM
  FC_ASSERT(limit <= 100);
  const auto &last_update_idx = _db.get_index<comment_index>().indices().get<by_last_update>();
  auto itr = last_update_idx.begin();
  const account_name_type *parent_author = &start_author;

  if (start_permlink.size()) {
    const auto &comment = _db.get_comment(start_author, start_permlink);
    itr = last_update_idx.iterator_to(comment);
    parent_author = &comment.parent_author;
  } else if (start_author.size()) {
    itr = last_update_idx.lower_bound(start_author);
  }

  result.reserve(limit);

  while (itr != last_update_idx.end() && result.size() < limit && itr->parent_author == *parent_author) {
    result.push_back(*itr);
    set_pending_payout(result.back());
    result.back().active_votes = get_active_votes({variant(itr->author), variant(to_string(itr->permlink))});
    result.back().comment_tips = get_comment_tips({variant(itr->author), variant(to_string(itr->permlink))});
    ++itr;
  }
#endif
  return result;
}

get_discussions_by_trending_return database_api_impl::get_discussions_by_trending(const get_discussions_by_trending_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  auto tag = fc::to_lower(query.tag);
  auto parent = get_parent(query);

  const auto &tidx = _db.get_index<tags::tag_index>().indices().get<tags::by_parent_trending>();
  auto tidx_itr = tidx.lower_bound(boost::make_tuple(tag, parent, std::numeric_limits<double>::max()));

  return get_discussions(query, tag, parent, tidx, tidx_itr, query.truncate_body, [](const comment_api_obj &c) { return c.net_rshares <= 0; });
}

get_post_discussions_by_payout_return database_api_impl::get_post_discussions_by_payout(const get_post_discussions_by_payout_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  auto tag = fc::to_lower(query.tag);
  auto parent = comment_id_type();

  const auto &tidx = _db.get_index<tags::tag_index>().indices().get<tags::by_reward_fund_net_rshares>();
  auto tidx_itr = tidx.lower_bound(boost::make_tuple(tag, true));

  return get_discussions(
      query, tag, parent, tidx, tidx_itr, query.truncate_body,
      [](const comment_api_obj &c) { return c.net_rshares <= 0; },
      exit_default, tag_exit_default, true);
}

get_block_header_return database_api_impl::get_block_header(const get_block_header_args &args) const {
  // FC_ASSERT( !_disable_get_block, "get_block_header is disabled on this node." );
  CHECK_ARG_SIZE(1)
  uint32_t block_num = args[0].as<uint32_t>();

  auto block = _db.fetch_block_by_number(block_num);
  if (block) return *block;

  return {};
}

get_block_return database_api_impl::get_block(const get_block_args &args) const {
  // FC_ASSERT( !_disable_get_block, "get_block is disabled on this node." );
  CHECK_ARG_SIZE(1)
  uint32_t block_num = args[0].as<uint32_t>();
  return _db.fetch_block_by_number(block_num);
}

get_blocks_missed_return database_api_impl::get_blocks_missed(const get_blocks_missed_args &args) const {
  CHECK_ARG_SIZE(2)
  uint32_t from = args[0].as<uint32_t>();
  uint32_t limit = args[1].as<uint32_t>();

  FC_ASSERT(limit <= 100, "Limit of ${l} is greater than maxmimum allowed", ("l", limit));
  FC_ASSERT(from >= limit, "From must be greater than limit");
  // idump((from)(limit));

  get_blocks_missed_return result;
  if (_block_missed_plugin == nullptr) return result;

  auto head_obj = _block_missed_plugin->get_block_missed_state_head_object();
  if (head_obj == nullptr) return result;
  if (head_obj->sequence == 0) return result;
  if (from >= head_obj->sequence) from = head_obj->sequence - 1;

  boost::system::error_code ec;
  uint32_t n = 0;
  while (true) {
    if (n >= limit) break;
    if (from < n) break;

    auto idx = from-n;
    wls::plugins::block_missed::block_missed_key obj_key{idx};
    wls::plugins::block_missed::block_missed_object obj = _block_missed_plugin->get_nudbstore_database().fetch(obj_key, ec);
    result.emplace_back(idx, obj);

    ++n;
  }

  return result;
}

get_ops_in_block_return database_api_impl::get_ops_in_block(const get_ops_in_block_args &args) const {
  FC_ASSERT(args.size() == 1 || args.size() == 2, "Expected 1-2 arguments, was ${n}", ("n", args.size()));

  uint32_t block_num = args[0].as<uint32_t>();
  bool only_virtual = true;
  if (args.size() == 2) {
    only_virtual = args[1].as<bool>();
  }

  vector<applied_operation> operations;

  if (_block_history_plugin != nullptr) {
    boost::system::error_code ec;
    vector<wls::plugins::block_history::block_history_applied_operation> items = _block_history_plugin->get_nudbstore_database().fetch(block_num, ec);
    for (const auto &item : items) {
      applied_operation ao(item);
//      {
//        ao.trx_id = item.trx_id;
//        ao.block = item.block;
//        ao.trx_in_block = item.trx_in_block;
//        ao.op_in_trx = item.op_in_trx;
//        ao.virtual_op = item.virtual_op;
//        ao.timestamp = item.timestamp;
//        ao.op = item.op;
//      }

      if (only_virtual) {
        if (!is_virtual_operation(item.op)) operations.push_back(ao);
      } else {
        operations.push_back(ao);
      }
    }
  } else{
    const auto &idx = _db.get_index<operation_index>().indices().get<by_location>();
    auto itr = idx.lower_bound(block_num);
    applied_operation temp;
    while (itr != idx.end() && itr->block == block_num) {
      temp = *itr;
      if (!only_virtual || is_virtual_operation(temp.op)) operations.push_back(temp);
      ++itr;
    }
  }

  return operations;
}

get_ops_in_blocks_return database_api_impl::get_ops_in_blocks(const get_ops_in_blocks_args &args) const {
  FC_ASSERT( _self.max_blocks > 0, "get_ops_in_blocks is disabled on this node." );
  FC_ASSERT(args.size() == 2 || args.size() == 3, "Expected 2-3 arguments, was ${n}", ("n", args.size()));
  uint32_t from = args[0].as<uint32_t>(); // from block num
  uint32_t limit = args[1].as<uint32_t>(); // next n blocks
  FC_ASSERT(limit <= _self.max_blocks);

  bool only_virtual = true;
  if (args.size() == 3) {
    only_virtual = args[2].as<bool>();
  }

  vector<applied_operation> operations;

  if (_block_history_plugin != nullptr) {
    for (uint32_t block_num = from; block_num < from + limit; block_num++) {
      boost::system::error_code ec;
      vector<wls::plugins::block_history::block_history_applied_operation> items = _block_history_plugin->get_nudbstore_database().fetch(block_num, ec);

      for (const auto &item : items) {
        applied_operation ao(item);
//        {
//          ao.trx_id = item.trx_id;
//          ao.block = item.block;
//          ao.trx_in_block = item.trx_in_block;
//          ao.op_in_trx = item.op_in_trx;
//          ao.virtual_op = item.virtual_op;
//          ao.timestamp = item.timestamp;
//          ao.op = item.op;
//        }

        if (only_virtual) {
          if (!is_virtual_operation(item.op)) operations.push_back(ao);
        } else {
          operations.push_back(ao);
        }
      }
    }
  } else {
    const auto &idx = _db.get_index<operation_index>().indices().get<by_location>();
    auto itr = idx.lower_bound(from);
    applied_operation temp;
    while (itr != idx.end() && (itr->block < (from + limit))) {
      temp = *itr;
      if (!only_virtual || is_virtual_operation(temp.op)) operations.push_back(temp);
      ++itr;
    }
  }

  return operations;
}

get_config_return database_api_impl::get_config(const get_config_args &args) const {
  CHECK_ARG_SIZE(0)
  return wls::protocol::get_config();
}

get_chain_properties_return database_api_impl::get_chain_properties(const get_chain_properties_args &args) const {
  CHECK_ARG_SIZE(0)
  return _db.get_witness_schedule_object().median_props;
}

witness_schedule_api_obj database_api_impl::get_witness_schedule(const get_witness_schedule_args &args) const {
  CHECK_ARG_SIZE(0)
  return _db.get(witness_schedule_id_type());
}

get_hardfork_version_return database_api_impl::get_hardfork_version(const get_hardfork_version_args &args) const {
  CHECK_ARG_SIZE(0)
  return _db.get(hardfork_property_id_type()).current_hardfork_version;
}

get_next_scheduled_hardfork_return database_api_impl::get_next_scheduled_hardfork(const get_next_scheduled_hardfork_args &args) const {
  CHECK_ARG_SIZE(0)
  scheduled_hardfork shf;
  const auto &hpo = _db.get(hardfork_property_id_type());
  shf.hf_version = hpo.next_hardfork;
  shf.live_time = hpo.next_hardfork_time;
  return shf;
}

get_reward_fund_return database_api_impl::get_reward_fund(const get_reward_fund_args &args) const {
  CHECK_ARG_SIZE(1)
  string name = args[0].as<string>();

  auto fund = _db.find<reward_fund_object, by_name>(name);
  FC_ASSERT(fund != nullptr, "Invalid reward fund name");
  return *fund;
}

get_vesting_payout_fund_return database_api_impl::get_vesting_payout_fund(const get_vesting_payout_fund_args &args) const {
  return _db.get_vesting_payout_fund();
}

get_key_references_return database_api_impl::get_key_references(const get_key_references_args &args) const {
  FC_ASSERT(false, "database_api::get_key_references has been deprecated. Please use account_by_key_api::get_key_references instead.");
  get_key_references_return final_result;
  return final_result;
}

get_accounts_return database_api_impl::get_accounts(const get_accounts_args &args) const {
  CHECK_ARG_SIZE(1)
  vector<string> names = args[0].as<vector<string>>();

  const auto &idx = _db.get_index<account_index>().indices().get<by_name>();
  const auto &vidx = _db.get_index<witness_vote_index>().indices().get<by_account_witness>();
#ifndef IS_LOW_MEM
  const auto &pod_member_idx = _db.get_index<pod_member_index>().indices().get<by_account_pod>();
#endif
  vector<extended_account> results;

  for (auto name : names) {
    auto itr = idx.find(name);
    if (itr != idx.end()) {
      results.push_back(extended_account(*itr, _db));

//      if (_follow_api) {
//        results.back().reputation = 0;  // _follow_api->get_account_reputations(itr->name, 1)[0].reputation;
//      }

      auto vitr = vidx.lower_bound(boost::make_tuple(itr->id, witness_id_type()));
      while (vitr != vidx.end() && vitr->account == itr->id) {
        results.back().witness_votes.insert(_db.get(vitr->witness).owner);
        ++vitr;
      }

      //--
#ifndef IS_LOW_MEM
      auto pod_itr = pod_member_idx.lower_bound(boost::make_tuple(name, pod_name_type()));
      while (pod_itr != pod_member_idx.end() && pod_itr->account == name) {
        results.back().pods.insert(pod_itr->pod);
        ++pod_itr;
      }
#endif
    }
  }

  return results;
}

get_accounts_by_tag_return database_api_impl::get_accounts_by_tag(const get_accounts_by_tag_args &args) const {
  CHECK_ARG_SIZE(3)
  string tag = args[0].as<string>();
  string start = args[1].as<string>(); // start pod
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 100);

  vector<account_api_obj> result;

  if (tag == "") {
    const auto &idx = _db.get_index<account_index>().indices().get<by_name>();
    auto itr = idx.lower_bound(start);

    uint32_t n = 0;
    while (true) {
      if (itr == idx.end()) break;
      if (n >= limit) break;

      result.push_back(account_api_obj(*itr, _db));

      ++itr;
      ++n;
    }
  } else {
    if (_account_tags_plugin == nullptr) return result;

    const auto &idx = _db.get_index<account_tags::account_tag_index>().indices().get<account_tags::by_tag_account>();
    auto itr = idx.lower_bound(boost::make_tuple(tag, start));

    uint32_t n = 0;
    while (true) {
      if (itr == idx.end()) break;
      if (n >= limit) break;
      if (itr->tag != tag) break;

      const auto &acc = _db.get_account(itr->account);
      result.push_back(account_api_obj(acc, _db));

      ++itr;
      ++n;
    }
  }

  return result;
}

lookup_account_names_return database_api_impl::lookup_account_names(const lookup_account_names_args &args) const {
  CHECK_ARG_SIZE(1)
  vector<string> account_names = args[0].as<vector<string>>();

  vector<optional<account_api_obj>> result;
  result.reserve(account_names.size());

  for (auto &name : account_names) {
    auto itr = _db.find<account_object, by_name>(name);

    if (itr) {
      result.push_back(account_api_obj(*itr, _db));
    } else {
      result.push_back(optional<account_api_obj>());
    }
  }

  return result;
}

lookup_accounts_return database_api_impl::lookup_accounts(const lookup_accounts_args &args) const {
  CHECK_ARG_SIZE(2)
  string lower_bound_name = args[0].as<string>();
  uint32_t limit = args[1].as<uint32_t>();

  FC_ASSERT(limit <= 1000);
  const auto &accounts_by_name = _db.get_index<account_index>().indices().get<by_name>();
  set<string> result;

  for (auto itr = accounts_by_name.lower_bound(lower_bound_name); limit-- && itr != accounts_by_name.end(); ++itr) {
    result.insert(itr->name);
  }

  return result;
}

get_account_count_return database_api_impl::get_account_count(const get_account_count_args &args) const {
  CHECK_ARG_SIZE(0)
  return _db.get_index<account_index>().indices().size();
}

get_withdraw_routes_return database_api_impl::get_withdraw_routes(const get_withdraw_routes_args &args) const {
  FC_ASSERT(args.size() == 1 || args.size() == 2, "Expected 1-2 arguments, was ${n}", ("n", args.size()));

  string account = args[0].as<string>();
  withdraw_route_type type = outgoing;
  if (args.size() == 2) {
    type = args[1].as<withdraw_route_type>();
  }

  vector<withdraw_route> result;

  const auto &acc = _db.get_account(account);

  if (type == outgoing || type == all) {
    const auto &by_route = _db.get_index<withdraw_vesting_route_index>().indices().get<by_withdraw_route>();
    auto route = by_route.lower_bound(acc.id);

    while (route != by_route.end() && route->from_account == acc.id) {
      withdraw_route r;
      r.from_account = account;
      r.to_account = _db.get(route->to_account).name;
      r.percent = route->percent;
      r.auto_vest = route->auto_vest;

      result.push_back(r);

      ++route;
    }
  }

  if (type == incoming || type == all) {
    const auto &by_dest = _db.get_index<withdraw_vesting_route_index>().indices().get<by_destination>();
    auto route = by_dest.lower_bound(acc.id);

    while (route != by_dest.end() && route->to_account == acc.id) {
      withdraw_route r;
      r.from_account = _db.get(route->from_account).name;
      r.to_account = account;
      r.percent = route->percent;
      r.auto_vest = route->auto_vest;

      result.push_back(r);

      ++route;
    }
  }

  return result;
}

get_account_bandwidth_return database_api_impl::get_account_bandwidth(const get_account_bandwidth_args &args) const {
  CHECK_ARG_SIZE(2)
  string account = args[0].as<string>();
  witness::bandwidth_type type = args[1].as<witness::bandwidth_type>();

  optional<account_bandwidth_api_obj> result;

  if (_db.has_index<witness::account_bandwidth_index>()) {
    auto band = _db.find<witness::account_bandwidth_object, witness::by_account_bandwidth_type>(boost::make_tuple(account, type));
    if (band != nullptr) result = *band;
  }

  return result;
}

get_account_daily_bandwidth_return database_api_impl::get_account_daily_bandwidth(const get_account_daily_bandwidth_args &args) const {
  CHECK_ARG_SIZE(1)
  string account = args[0].as<string>();

  optional<account_daily_bandwidth_api_obj> result;

  if (_db.has_index<witness::account_daily_bandwidth_index>()) {
    auto bw = _db.find<witness::account_daily_bandwidth_object, witness::by_account>(account);
    if (bw != nullptr) result = *bw;
  }

  return result;
}

get_witnesses_return database_api_impl::get_witnesses(const get_witnesses_args &args) const {
  CHECK_ARG_SIZE(1)
  vector<witness_id_type> witness_ids = args[0].as<vector<witness_id_type>>();

  vector<optional<witness_api_obj>> result;
  result.reserve(witness_ids.size());
  std::transform(witness_ids.begin(), witness_ids.end(), std::back_inserter(result), [this](witness_id_type id) -> optional<witness_api_obj> {
    if (auto o = _db.find(id)) return witness_api_obj(*o);
    return {};
  });
  return result;
}

get_witness_by_account_return database_api_impl::get_witness_by_account(const get_witness_by_account_args &args) const {
  CHECK_ARG_SIZE(1)
  string account_name = args[0].as<string>();

  const auto &idx = _db.get_index<witness_index>().indices().get<by_name>();
  auto itr = idx.find(account_name);
  if (itr != idx.end()) return witness_api_obj(*itr);

  return {};
}

lookup_witness_accounts_return database_api_impl::lookup_witness_accounts(const lookup_witness_accounts_args &args) const {
  CHECK_ARG_SIZE(2)
  string lower_bound_name = args[0].as<string>();
  uint32_t limit = args[1].as<uint32_t>();

  FC_ASSERT(limit <= 1000);
  const auto &witnesses_by_id = _db.get_index<witness_index>().indices().get<by_id>();

  // get all the names and look them all up, sort them, then figure out what
  // records to return.  This could be optimized, but we expect the
  // number of witnesses to be few and the frequency of calls to be rare
  set<account_name_type> result;
  for (const witness_api_obj &witness : witnesses_by_id)
    if (witness.owner >= lower_bound_name)  // we can ignore anything below lower_bound_name
      result.insert(witness.owner);

  auto end_iter = result.begin();
  while (end_iter != result.end() && limit--) ++end_iter;
  result.erase(end_iter, result.end());
  return result;
}

get_witness_count_return database_api_impl::get_witness_count(const get_witness_count_args &args) const {
  CHECK_ARG_SIZE(0)
  return _db.get_index<witness_index>().indices().size();
}

get_transaction_hex_return database_api_impl::get_transaction_hex(const get_transaction_hex_args &args) const {
  CHECK_ARG_SIZE(1)
  signed_transaction trx = args[0].as<signed_transaction>();
  return get_transaction_hex_return{fc::to_hex(fc::raw::pack(trx))};
}

get_required_signatures_return database_api_impl::get_required_signatures(const get_required_signatures_args &args) const {
  CHECK_ARG_SIZE(2)
  signed_transaction trx = args[0].as<signed_transaction>();
  flat_set<public_key_type> available_keys = args[1].as<flat_set<public_key_type>>();

  //   wdump((trx)(available_keys));
  auto result = trx.get_required_signatures(
      WLS_CHAIN_ID, available_keys, [&](string account_name) { return authority(_db.get<account_authority_object, by_account>(account_name).active); },
      [&](string account_name) { return authority(_db.get<account_authority_object, by_account>(account_name).owner); },
      [&](string account_name) { return authority(_db.get<account_authority_object, by_account>(account_name).posting); }, WLS_MAX_SIG_CHECK_DEPTH);
  //   wdump((result));
  return result;
}

get_potential_signatures_return database_api_impl::get_potential_signatures(const get_potential_signatures_args &args) const {
  CHECK_ARG_SIZE(1)
  signed_transaction trx = args[0].as<signed_transaction>();

  //   wdump((trx));
  set<public_key_type> result;
  trx.get_required_signatures(WLS_CHAIN_ID, flat_set<public_key_type>(),
                              [&](account_name_type account_name) {
                                const auto &auth = _db.get<account_authority_object, by_account>(account_name).active;
                                for (const auto &k : auth.get_keys()) result.insert(k);
                                return authority(auth);
                              },
                              [&](account_name_type account_name) {
                                const auto &auth = _db.get<account_authority_object, by_account>(account_name).owner;
                                for (const auto &k : auth.get_keys()) result.insert(k);
                                return authority(auth);
                              },
                              [&](account_name_type account_name) {
                                const auto &auth = _db.get<account_authority_object, by_account>(account_name).posting;
                                for (const auto &k : auth.get_keys()) result.insert(k);
                                return authority(auth);
                              },
                              WLS_MAX_SIG_CHECK_DEPTH);

  //   wdump((result));
  return result;
}

verify_authority_return database_api_impl::verify_authority(const verify_authority_args &args) const {
  CHECK_ARG_SIZE(1)
  signed_transaction trx = args[0].as<signed_transaction>();

  trx.verify_authority(WLS_CHAIN_ID, [&](string account_name) { return authority(_db.get<account_authority_object, by_account>(account_name).active); },
                       [&](string account_name) { return authority(_db.get<account_authority_object, by_account>(account_name).owner); },
                       [&](string account_name) { return authority(_db.get<account_authority_object, by_account>(account_name).posting); },
                       WLS_MAX_SIG_CHECK_DEPTH);
  return true;
}

/**
 * bool           verify_account_authority( const string& name_or_id, const flat_set<public_key_type>& signers )const;
 */
verify_account_authority_return database_api_impl::verify_account_authority(const verify_account_authority_args &args) const {
  CHECK_ARG_SIZE(2)
  string name_or_id = args[0].as<string>();
  flat_set<public_key_type> signers = args[1].as<flat_set<public_key_type>>();

  FC_ASSERT(name_or_id.size() > 0);
  auto account = _db.find<account_object, by_name>(name_or_id);
  FC_ASSERT(account, "no such account");

  /// reuse trx.verify_authority by creating a dummy transfer
  signed_transaction trx;
  transfer_operation op;
  op.from = account->name;
  trx.operations.emplace_back(op);

  return verify_authority({variant(trx)});
}

get_account_votes_return database_api_impl::get_account_votes(const get_account_votes_args &args) const {
  CHECK_ARG_SIZE(1)
  string voter = args[0].as<string>();

  vector<account_vote> result;

  const auto &voter_acnt = _db.get_account(voter);
  const auto &idx = _db.get_index<comment_vote_index>().indices().get<by_voter_comment>();

  account_id_type aid(voter_acnt.id);
  auto itr = idx.lower_bound(aid);
  auto end = idx.upper_bound(aid);
  while (itr != end) {
    const auto &vo = _db.get(itr->comment);
    account_vote avote;
    avote.authorperm = vo.author + "/" + to_string(vo.permlink);
    avote.weight = itr->weight;
    avote.rshares = itr->rshares;
    avote.percent = itr->vote_percent;
    avote.time = itr->last_update;
    result.push_back(avote);
    ++itr;
  }
  return result;
}

get_discussions_by_payout_return database_api_impl::get_discussions_by_payout(const get_discussions_by_payout_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  auto tag = fc::to_lower(query.tag);
  auto parent = get_parent(query);

  const auto &tidx = _db.get_index<tags::tag_index>().indices().get<tags::by_net_rshares>();
  auto tidx_itr = tidx.lower_bound(tag);

  return get_discussions(query, tag, parent, tidx, tidx_itr, query.truncate_body, [](const comment_api_obj &c) { return c.net_rshares <= 0; }, exit_default,
                         tag_exit_default, true);
}

get_discussions_by_feed_return database_api_impl::get_discussions_by_feed(const get_discussions_by_feed_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  FC_ASSERT(_follow_api, "Node is not running the follow plugin");
  auto start_author = query.start_author ? *(query.start_author) : "";
  auto start_permlink = query.start_permlink ? *(query.start_permlink) : "";

  const auto &account = _db.get_account(query.tag);

  const auto &c_idx = _db.get_index<follow::feed_index>().indices().get<follow::by_comment>();
  const auto &f_idx = _db.get_index<follow::feed_index>().indices().get<follow::by_feed>();
  auto feed_itr = f_idx.lower_bound(account.name);

  if (start_author.size() || start_permlink.size()) {
    auto start_c = c_idx.find(boost::make_tuple(_db.get_comment(start_author, start_permlink).id, account.name));
    FC_ASSERT(start_c != c_idx.end(), "Comment is not in account's feed");
    feed_itr = f_idx.iterator_to(*start_c);
  }

  vector<discussion> result;
  result.reserve(query.limit);

  while (result.size() < query.limit && feed_itr != f_idx.end()) {
    if (feed_itr->account != account.name) break;
    try {
      result.push_back(get_discussion(feed_itr->comment));
      if (feed_itr->first_reblogged_by != WLS_ROOT_POST_PARENT) {
        result.back().reblogged_by = vector<account_name_type>(feed_itr->reblogged_by.begin(), feed_itr->reblogged_by.end());
        result.back().first_reblogged_by = feed_itr->first_reblogged_by;
        result.back().first_reblogged_on = feed_itr->first_reblogged_on;
      }
    } catch (const fc::exception &e) {
      edump((e.to_detail_string()));
    }

    ++feed_itr;
  }
  return result;
}

get_discussions_by_blog_return database_api_impl::get_discussions_by_blog(const get_discussions_by_blog_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  query.validate();
  FC_ASSERT(_follow_api, "Node is not running the follow plugin");
  auto start_author = query.start_author ? *(query.start_author) : "";
  auto start_permlink = query.start_permlink ? *(query.start_permlink) : "";
  const auto &account = _db.get_account(query.tag);
  const auto &tag_idx = _db.get_index<tags::tag_index>().indices().get<tags::by_comment>();
  const auto &c_idx = _db.get_index<follow::blog_index>().indices().get<follow::by_comment>();
  const auto &b_idx = _db.get_index<follow::blog_index>().indices().get<follow::by_blog>();
  auto blog_itr = b_idx.lower_bound(account.name);

  if (start_author.size() || start_permlink.size()) {
    auto start_c = c_idx.find(boost::make_tuple(_db.get_comment(start_author, start_permlink).id, account.name));
    FC_ASSERT(start_c != c_idx.end(), "Comment is not in account's blog");
    blog_itr = b_idx.iterator_to(*start_c);
  }

  vector<discussion> result;
  result.reserve(query.limit);

  while (result.size() < query.limit && blog_itr != b_idx.end()) {
    if (blog_itr->account != account.name) break;
    try {
      if (query.select_authors.size() && query.select_authors.find(blog_itr->account) == query.select_authors.end()) {
        ++blog_itr;
        continue;
      }

      if (query.select_tags.size()) {
        auto tag_itr = tag_idx.lower_bound(blog_itr->comment);

        bool found = false;
        while (tag_itr != tag_idx.end() && tag_itr->comment == blog_itr->comment) {
          if (query.select_tags.find(tag_itr->tag) != query.select_tags.end()) {
            found = true;
            break;
          }
          ++tag_itr;
        }
        if (!found) {
          ++blog_itr;
          continue;
        }
      }

      result.push_back(get_discussion(blog_itr->comment, query.truncate_body));
      if (blog_itr->reblogged_on > time_point_sec()) {
        result.back().first_reblogged_on = blog_itr->reblogged_on;
      }
    } catch (const fc::exception &e) {
      edump((e.to_detail_string()));
    }

    ++blog_itr;
  }
  return result;
}

get_discussions_by_comments_return database_api_impl::get_discussions_by_comments(const get_discussions_by_comments_args &args) const {
  CHECK_ARG_SIZE(1)
  discussion_query query = args[0].as<discussion_query>();

  vector<discussion> result;
#ifndef IS_LOW_MEM
  query.validate();
  FC_ASSERT(query.start_author, "Must get comments for a specific author");
  auto start_author = *(query.start_author);
  auto start_permlink = query.start_permlink ? *(query.start_permlink) : "";

  const auto &c_idx = _db.get_index<comment_index>().indices().get<by_permlink>();
  const auto &t_idx = _db.get_index<comment_index>().indices().get<by_author_last_update>();
  auto comment_itr = t_idx.lower_bound(start_author);

  if (start_permlink.size()) {
    auto start_c = c_idx.find(boost::make_tuple(start_author, start_permlink));
    FC_ASSERT(start_c != c_idx.end(), "Comment is not in account's comments");
    comment_itr = t_idx.iterator_to(*start_c);
  }

  result.reserve(query.limit);

  while (result.size() < query.limit && comment_itr != t_idx.end()) {
    if (comment_itr->author != start_author) break;
    if (comment_itr->parent_author.size() > 0) {
      try {
        result.push_back(get_discussion(comment_itr->id));
      } catch (const fc::exception &e) {
        edump((e.to_detail_string()));
      }
    }

    ++comment_itr;
  }
#endif
  return result;
}

get_discussions_by_author_before_date_return database_api_impl::get_discussions_by_author_before_date(get_discussions_by_author_before_date_args args) const {
  CHECK_ARG_SIZE(4)
  string author = args[0].as<string>();
  string start_permlink = args[1].as<string>();
  time_point_sec before_date = args[2].as<time_point_sec>();
  uint32_t limit = args[3].as<uint32_t>();
	
  try {
    vector<discussion> result;
#ifndef IS_LOW_MEM
    FC_ASSERT(limit <= 100);
    result.reserve(limit);
    uint32_t count = 0;
    const auto &didx = _db.get_index<comment_index>().indices().get<by_author_last_update>();

    if (before_date == time_point_sec()) before_date = time_point_sec::maximum();

    auto itr = didx.lower_bound(boost::make_tuple(author, time_point_sec::maximum()));
    if (start_permlink.size()) {
      const auto &comment = _db.get_comment(author, start_permlink);
      if (comment.created < before_date) itr = didx.iterator_to(comment);
    }

    while (itr != didx.end() && itr->author == author && count < limit) {
      if (itr->parent_author.size() == 0) {
        result.push_back(*itr);
        set_pending_payout(result.back());
        result.back().active_votes = get_active_votes({variant(itr->author), variant(to_string(itr->permlink))});
        result.back().comment_tips = get_comment_tips({variant(itr->author), variant(to_string(itr->permlink))});
        ++count;
      }
      ++itr;
    }

#endif
	FC_UNUSED(before_date,limit);
    return result;
  }
  FC_CAPTURE_AND_RETHROW((args))
}

lookup_pods_return database_api_impl::lookup_pods(const lookup_pods_args &args) const {
  CHECK_ARG_SIZE(2)
  string lower_bound_name = args[0].as<string>();
  uint32_t limit = args[1].as<uint32_t>();

  FC_ASSERT(limit <= 100);
  const auto &idx = _db.get_index<pod_index>().indices().get<by_name>();
  auto itr = idx.lower_bound(lower_bound_name);
  vector<pod_api_obj> results;
  uint32_t n = 0;
  while (true) {
    if (itr == idx.end()) break;
    if (n >= limit) break;

    results.push_back(pod_api_obj(*itr, _db));

    ++itr;
    ++n;
  }
  return results;
}

get_pods_return database_api_impl::get_pods(const get_pods_args &args) const {
  CHECK_ARG_SIZE(1)
  vector<string> names = args[0].as<vector<string>>();
  FC_ASSERT(names.size() <= 100, "Limit size 100 pods.");
  const auto &idx = _db.get_index<pod_index>().indices().get<by_name>();
  vector<pod_api_obj> results;

  for (auto name : names) {
    auto itr = idx.find(name);
    if (itr != idx.end()) results.push_back(pod_api_obj(*itr, _db));
  }

  return results;
}

get_pods_by_tag_return database_api_impl::get_pods_by_tag(const get_pods_by_tag_args &args) const {
  CHECK_ARG_SIZE(3)
  string tag = args[0].as<string>();
  string start = args[1].as<string>(); // start pod
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 100);

  vector<pod_api_obj> result;

  if (_podtags_plugin == nullptr) return result;

  const auto &idx = _db.get_index<podtags::podtag_index>().indices().get<podtags::by_tag_pod>();
  auto itr = idx.lower_bound(boost::make_tuple(tag, start));

  uint32_t n = 0;
  while (true) {
    if (itr == idx.end()) break;
    if (n >= limit) break;
    if (itr->tag != tag) break;

    const auto& pod = _db.get_pod(itr->pod);
    result.push_back(pod_api_obj(pod, _db));

    ++itr;
    ++n;
  }

  return result;
}

lookup_pod_members_return database_api_impl::lookup_pod_members(const lookup_pod_members_args &args) const {
  CHECK_ARG_SIZE(3)
  string pod_name = args[0].as<string>();
  string lower_bound_name = args[1].as<string>();
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 100);

  const auto &idx = _db.get_index<pod_member_index>().indices().get<by_pod_account>();
  auto itr = idx.lower_bound(boost::make_tuple(pod_name, lower_bound_name));

  vector<pod_member_api_obj> results;
  uint32_t n = 0;
  while (true) {
    if (itr == idx.end()) break;
    if (n >= limit) break;
    if (itr->pod != pod_name) break;

    results.push_back(*itr);

    ++itr;
    ++n;
  }

  return results;
}

get_pod_members_by_user_return database_api_impl::get_pod_members_by_user(const get_pod_members_by_user_args &args) const {
  CHECK_ARG_SIZE(3)
  account_name_type account = args[0].as<account_name_type>();
  string lower_bound_name = args[1].as<string>();
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 100);

  vector<pod_member_api_obj> results;

#ifndef IS_LOW_MEM
  const auto &idx = _db.get_index<pod_member_index>().indices().get<by_account_pod>();
  auto itr = idx.lower_bound(boost::make_tuple(account, lower_bound_name));

  uint32_t n = 0;
  while (true) {
    if (itr == idx.end()) break;
    if (n >= limit) break;
    if (itr->account != account) break;

    results.push_back(*itr);

    ++itr;
    ++n;
  }
#endif
FC_UNUSED(account);
  return results;
}

get_pod_join_requests_return database_api_impl::get_pod_join_requests(const get_pod_join_requests_args &args) const {
  CHECK_ARG_SIZE(3)
  string pod_name = args[0].as<string>();
  string lower_bound_name = args[1].as<string>();
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 100);

  const auto &idx = _db.get_index<pod_join_request_pending_index>().indices().get<by_pod_account>();
  auto itr = idx.lower_bound(boost::make_tuple(pod_name, lower_bound_name));

  vector<pod_join_request_api_obj> results;
  uint32_t n = 0;
  while (true) {
    if (itr == idx.end()) break;
    if (n >= limit) break;
    if (itr->pod != pod_name) break;

    results.push_back(*itr);

    ++itr;
    ++n;
  }

  return results;
}

get_pod_join_requests_by_user_return database_api_impl::get_pod_join_requests_by_user(const get_pod_join_requests_by_user_args &args) const {
  CHECK_ARG_SIZE(3)
  account_name_type account = args[0].as<string>();
  string lower_bound_name = args[1].as<string>();
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 100);

  vector<pod_join_request_api_obj> results;

#ifndef IS_LOW_MEM
  const auto &idx = _db.get_index<pod_join_request_pending_index>().indices().get<by_account_pod>();
  auto itr = idx.lower_bound(boost::make_tuple(account, lower_bound_name));

  uint32_t n = 0;
  while (true) {
    if (itr == idx.end()) break;
    if (n >= limit) break;
    if (itr->account != account) break;

    results.push_back(*itr);

    ++itr;
    ++n;
  }
#endif
FC_UNUSED(account);
  return results;
}

get_friends_return database_api_impl::get_friends(const get_friends_args &args) const {
  CHECK_ARG_SIZE(3)
  account_name_type account = args[0].as<account_name_type>();
  account_name_type start = args[1].as<account_name_type>();
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 1000);

  get_friends_return result;
  result.reserve(limit);

  const auto &idx = _db.get_index<friend_index>().indices().get<by_account_another>();
  auto itr = idx.lower_bound(std::make_tuple(account, start));
  uint32_t n = 0;
  while (true) {
    if (itr == idx.end()) break;
    if (n >= limit) break;
    if (itr->account != account) break;

    result.push_back(itr->another);

    ++itr;
    ++n;
  }

  return result;
}

get_friend_requests_from_return database_api_impl::get_friend_requests_from(const get_friend_requests_from_args &args) const {
  CHECK_ARG_SIZE(3)
  account_name_type account = args[0].as<account_name_type>();
  account_name_type start = args[1].as<account_name_type>();
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 100);

  const auto &idx = _db.get_index<friend_request_pending_index>().indices().get<by_account_another>();
  auto itr = idx.lower_bound(boost::make_tuple(account, start));

  vector<friend_request_api_obj> result;
  uint32_t n = 0;
  while (true) {
    if (itr == idx.end()) break;
    if (n >= limit) break;
    if (itr->account != account) break;

    result.push_back(*itr);

    ++itr;
    ++n;
  }

  return result;
}

get_friend_requests_to_return database_api_impl::get_friend_requests_to(const get_friend_requests_to_args &args) const {
  CHECK_ARG_SIZE(3)
  account_name_type account = args[0].as<account_name_type>();
  account_name_type start = args[1].as<account_name_type>();
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 100);

  vector<friend_request_api_obj> result;

#ifndef IS_LOW_MEM
  const auto &idx = _db.get_index<friend_request_pending_index>().indices().get<by_another_account>();
  auto itr = idx.lower_bound(boost::make_tuple(account, start));

  uint32_t n = 0;
  while (true) {
    if (itr == idx.end()) break;
    if (n >= limit) break;
    if (itr->another != account) break;

    result.push_back(*itr);

    ++itr;
    ++n;
  }
#endif
FC_UNUSED(account,start);
  return result;
}

get_user_feed_return database_api_impl::get_user_feed(const get_user_feed_args &args) const {
  FC_ASSERT(args.size() == 3, "Expected #s argument(s), was ${n}", ("n", args.size()));
  string account = args[0].as<string>();
  uint32_t from = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 20, "Limit of ${l} is greater than maxmimum allowed", ("l", limit));
  FC_ASSERT(from >= limit, "From must be greater than limit");

  get_user_feed_return result;
  if (_myfeed_plugin == nullptr) return result;

  // find the head
//  auto head_obj = _db.find<wls::plugins::myfeed::myfeed_state_head_object, by_account>(account);
  auto head_obj = _myfeed_plugin->get_myfeed_state_head_object(account);
  if (head_obj == nullptr) return result;
  if (head_obj->sequence == 0) return result;
  if (from >= head_obj->sequence) from = head_obj->sequence - 1;

//  ilog("get_user_feed: head_obj=${head_obj}", ("head_obj", *head_obj));

  boost::system::error_code ec;
  uint32_t n = 0;
  while (true) {
    if (n >= limit) break;
    if (from < n) break;

    auto idx = from-n;
    wls::plugins::myfeed::myfeed_key obj_key{account, idx};
    wls::plugins::myfeed::myfeed_object obj = _myfeed_plugin->get_nudbstore_database().fetch(obj_key, ec);

    const auto *comment = _db.find_comment(obj.author, obj.permlink);
    if (comment != nullptr) {
      // discussion d = _db.get_comment(obj.author, obj.permlink);
      discussion d(*comment);
      set_url(d);
      set_pending_payout(d);
      d.active_votes = get_active_votes({variant(d.author), variant(d.permlink)});
      d.comment_tips = get_comment_tips({variant(d.author), variant(d.permlink)});
      d.body_length = d.body.size();
      result.emplace_back(idx, d);
    } else {
      discussion d;
      d.author = obj.author;
      d.permlink = obj.permlink;
      d.url += "/@" + d.author + "/" + d.permlink;
      d.pending_payout_value = asset(0, WLS_SYMBOL);
      d.body = "this comment is deleted";
      d.body_length = d.body.size();
      result.emplace_back(idx, d);
    }

    ++n;
  }

  return result;
}

get_pod_feed_return database_api_impl::get_pod_feed(const get_pod_feed_args &args) const {
  FC_ASSERT(args.size() == 3, "Expected #s argument(s), was ${n}", ("n", args.size()));
  string pod = args[0].as<string>();
  uint32_t from = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 20, "Limit of ${l} is greater than maxmimum allowed", ("l", limit));
  FC_ASSERT(from >= limit, "From must be greater than limit");

  get_pod_feed_return result;
  if (_podfeed_plugin == nullptr) return result;

  // find the head
  //  auto head_obj = _db.find<wls::plugins::myfeed::myfeed_state_head_object, by_account>(account);
  auto head_obj = _podfeed_plugin->get_podfeed_state_head_object(pod);
  if (head_obj == nullptr) return result;
  if (head_obj->sequence == 0) return result;
  if (from >= head_obj->sequence) from = head_obj->sequence - 1;

  boost::system::error_code ec;
  uint32_t n = 0;
  while (true) {
    if (n >= limit) break;
    if (from < n) break;

    auto idx = from-n;
    wls::plugins::podfeed::podfeed_key obj_key{pod, idx};
    wls::plugins::podfeed::podfeed_object obj = _podfeed_plugin->get_nudbstore_database().fetch(obj_key, ec);

    const auto *comment = _db.find_comment(obj.author, obj.permlink);
    if (comment != nullptr) {
      // discussion d = _db.get_comment(obj.author, obj.permlink);
      discussion d(*comment);
      set_url(d);
      set_pending_payout(d);
      d.active_votes = get_active_votes({variant(d.author), variant(d.permlink)});
      d.comment_tips = get_comment_tips({variant(d.author), variant(d.permlink)});
      d.body_length = d.body.size();
      result.emplace_back(idx, d);
    } else {
      discussion d;
      d.author = obj.author;
      d.permlink = obj.permlink;
      d.url += "/@" + d.author + "/" + d.permlink;
      d.pending_payout_value = asset(0, WLS_SYMBOL);
      d.body = "this comment is deleted";
      d.body_length = d.body.size();
      result.emplace_back(idx, d);
    }

    ++n;
  }

  return result;
}

get_pod_announcements_return database_api_impl::get_pod_announcements(const get_pod_announcements_args &args) const {
  FC_ASSERT(args.size() == 3, "Expected #s argument(s), was ${n}", ("n", args.size()));
  string pod = args[0].as<string>();
  uint32_t from = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 20, "Limit of ${l} is greater than maxmimum allowed", ("l", limit));
  FC_ASSERT(from >= limit, "From must be greater than limit");

  get_pod_announcements_return result;
  if (_pod_announcement_plugin == nullptr) return result;

  // find the head
  auto head_obj = _pod_announcement_plugin->get_pod_announcement_state_head_object(pod);
  if (head_obj == nullptr) return result;
  if (head_obj->sequence == 0) return result;
  if (from >= head_obj->sequence) from = head_obj->sequence - 1;

  boost::system::error_code ec;
  uint32_t n = 0;
  while (true) {
    if (n >= limit) break;
    if (from < n) break;

    auto idx = from - n;
    wls::plugins::pod_announcement::pod_announcement_key obj_key{pod, idx};
    wls::plugins::pod_announcement::pod_announcement_object obj = _pod_announcement_plugin->get_nudbstore_database().fetch(obj_key, ec);

    const auto *comment = _db.find_comment(obj.author, obj.permlink);
    if (comment != nullptr) {
      discussion d(*comment);
      set_url(d);
      set_pending_payout(d);
      d.active_votes = get_active_votes({variant(d.author), variant(d.permlink)});
      d.comment_tips = get_comment_tips({variant(d.author), variant(d.permlink)});
      d.body_length = d.body.size();
      result.emplace_back(idx, d);
    } else {
      discussion d;
      d.author = obj.author;
      d.permlink = obj.permlink;
      d.url += "/@" + d.author + "/" + d.permlink;
      d.pending_payout_value = asset(0, WLS_SYMBOL);
      d.body = "this comment is deleted";
      d.body_length = d.body.size();
      result.emplace_back(idx, d);
    }

    ++n;
  }

  return result;
}

get_blog_feed_return database_api_impl::get_blog_feed(const get_blog_feed_args &args) const {
  FC_ASSERT(args.size() == 3, "Expected #s argument(s), was ${n}", ("n", args.size()));
  string account = args[0].as<string>();
  uint32_t from = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 20, "Limit of ${l} is greater than maxmimum allowed", ("l", limit));
  FC_ASSERT(from >= limit, "From must be greater than limit");

  get_blog_feed_return result;
  if (_blogfeed_plugin == nullptr) return result;

  // find the head
  auto head_obj = _blogfeed_plugin->get_blogfeed_state_head_object(account);
  if (head_obj == nullptr) return result;
  if (head_obj->sequence == 0) return result;
  if (from >= head_obj->sequence) from = head_obj->sequence - 1;

  boost::system::error_code ec;
  uint32_t n = 0;
  while (true) {
    if (n >= limit) break;
    if (from < n) break;

    auto idx = from-n;
    wls::plugins::blogfeed::blogfeed_key obj_key{account, idx};
    wls::plugins::blogfeed::blogfeed_object obj = _blogfeed_plugin->get_nudbstore_database().fetch(obj_key, ec);

    const auto *comment = _db.find_comment(obj.author, obj.permlink);
    if (comment != nullptr) {
      // discussion d = _db.get_comment(obj.author, obj.permlink);
      discussion d(*comment);
      set_url(d);
      set_pending_payout(d);
      d.active_votes = get_active_votes({variant(d.author), variant(d.permlink)});
      d.comment_tips = get_comment_tips({variant(d.author), variant(d.permlink)});
      d.body_length = d.body.size();
      result.emplace_back(idx, d);
    } else {
      discussion d;
      d.author = obj.author;
      d.permlink = obj.permlink;
      d.url += "/@" + d.author + "/" + d.permlink;
      d.pending_payout_value = asset(0, WLS_SYMBOL);
      d.body = "this comment is deleted";
      d.body_length = d.body.size();
      result.emplace_back(idx, d);
    }

    ++n;
  }

  return result;
}

get_shares_feed_return database_api_impl::get_shares_feed(const get_shares_feed_args &args) const {
  FC_ASSERT(args.size() == 3, "Expected #s argument(s), was ${n}", ("n", args.size()));
  string account = args[0].as<string>();
  uint32_t from = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 20, "Limit of ${l} is greater than maxmimum allowed", ("l", limit));
  FC_ASSERT(from >= limit, "From must be greater than limit");

  get_shares_feed_return result;
  if (_sharesfeed_plugin == nullptr) return result;

  // find the head
  auto head_obj = _sharesfeed_plugin->get_sharesfeed_state_head_object(account);
  if (head_obj == nullptr) return result;
  if (head_obj->sequence == 0) return result;
  if (from >= head_obj->sequence) from = head_obj->sequence - 1;

  boost::system::error_code ec;
  uint32_t n = 0;
  while (true) {
    if (n >= limit) break;
    if (from < n) break;

    auto idx = from-n;
    wls::plugins::sharesfeed::sharesfeed_key obj_key{account, idx};
    wls::plugins::sharesfeed::sharesfeed_object obj = _sharesfeed_plugin->get_nudbstore_database().fetch(obj_key, ec);

    const auto *comment = _db.find_comment(obj.author, obj.permlink);
    if (comment != nullptr) {
      // discussion d = _db.get_comment(obj.author, obj.permlink);
      discussion d(*comment);
      set_url(d);
      set_pending_payout(d);
      d.active_votes = get_active_votes({variant(d.author), variant(d.permlink)});
      d.comment_tips = get_comment_tips({variant(d.author), variant(d.permlink)});
      d.body_length = d.body.size();
      result.emplace_back(idx, d);
    } else {
      discussion d;
      d.author = obj.author;
      d.permlink = obj.permlink;
      d.url += "/@" + d.author + "/" + d.permlink;
      d.pending_payout_value = asset(0, WLS_SYMBOL);
      d.body = "this comment is deleted";
      d.body_length = d.body.size();
      result.emplace_back(idx, d);
    }

    ++n;
  }

  return result;
}

get_htlcs_from_return database_api_impl::get_htlcs_from(const get_htlcs_from_args &args) const {
  CHECK_ARG_SIZE(3)
  account_name_type account = args[0].as<account_name_type>();
  htlc_ref_id_type start = args[1].as<htlc_ref_id_type>();
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 100);

  const auto &idx = _db.get_index<htlc_index>().indices().get<by_from_refid>();
  auto itr = idx.lower_bound(boost::make_tuple(account, start));

  vector<htlc_api_obj> result;
  uint32_t n = 0;
  while (true) {
    if (itr == idx.end()) break;
    if (n >= limit) break;
    if (itr->from != account) break;

    result.push_back(*itr);

    ++itr;
    ++n;
  }

  return result;
}

get_htlcs_to_return database_api_impl::get_htlcs_to(const get_htlcs_to_args &args) const {
  CHECK_ARG_SIZE(3)
  account_name_type account = args[0].as<account_name_type>();
  htlc_ref_id_type start = args[1].as<htlc_ref_id_type>();
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 100);

  const auto &idx = _db.get_index<htlc_index>().indices().get<by_to_refid>();
  auto itr = idx.lower_bound(boost::make_tuple(account, start));

  vector<htlc_api_obj> result;
  uint32_t n = 0;
  while (true) {
    if (itr == idx.end()) break;
    if (n >= limit) break;
    if (itr->to != account) break;

    result.push_back(*itr);

    ++itr;
    ++n;
  }

  return result;
}

get_tips_from_return database_api_impl::get_tips_from(const get_tips_from_args &args) const {
  CHECK_ARG_SIZE(3)
  string account = args[0].as<string>();
  uint32_t from = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 100, "Limit of ${l} is greater than maxmimum allowed", ("l", limit));

  get_tips_from_return result;
  if (_tips_sent_plugin == nullptr) return result;

  auto head_obj = _tips_sent_plugin->get_tips_sent_state_head_object(account);
  if (head_obj == nullptr) return result;
  if (head_obj->sequence == 0) return result;
  if (from >= head_obj->sequence) from = head_obj->sequence - 1;

  boost::system::error_code ec;
  uint32_t n = 0;
  while (true) {
    if (n >= limit) break;
    if (from < n) break;

    auto idx = from-n;
    wls::plugins::tips_sent::tips_sent_key obj_key{account, idx};
    wls::plugins::tips_sent::tips_sent_object obj = _tips_sent_plugin->get_nudbstore_database().fetch(obj_key, ec);
    applied_operation ao(obj);
    result.emplace_back(idx, obj);

    ++n;
  }

  return result;
}


get_tips_to_return database_api_impl::get_tips_to(const get_tips_to_args &args) const {
  CHECK_ARG_SIZE(3)
  string account = args[0].as<string>();
  uint32_t from = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 100, "Limit of ${l} is greater than maxmimum allowed", ("l", limit));

  get_tips_to_return result;
  if (_tips_received_plugin == nullptr) return result;

  auto head_obj = _tips_received_plugin->get_tips_received_state_head_object(account);
  if (head_obj == nullptr) return result;
  if (head_obj->sequence == 0) return result;
  if (from >= head_obj->sequence) from = head_obj->sequence - 1;

  boost::system::error_code ec;
  uint32_t n = 0;
  while (true) {
    if (n >= limit) break;
    if (from < n) break;

    auto idx = from-n;
    wls::plugins::tips_received::tips_received_key obj_key{account, idx};
    wls::plugins::tips_received::tips_received_object obj = _tips_received_plugin->get_nudbstore_database().fetch(obj_key, ec);
    applied_operation ao(obj);
    result.emplace_back(idx, obj);

    ++n;
  }

  return result;
}

//get_schema_return database_api_impl::get_schema(const get_schema_args &arg) const {
//   return get_schema_return();
//}

}  // namespace detail

database_api::database_api() : my(new detail::database_api_impl(*this)) { JSON_RPC_REGISTER_API(WLS_DATABASE_API_PLUGIN_NAME); }

database_api::~database_api() {}
// clang-format off
DEFINE_READ_APIS(database_api,
  (get_health_check)
  (get_trending_tags)
  (get_state)
  (get_active_witnesses)
  (get_block_header)
  (get_block)
  (get_blocks_missed)
  (get_ops_in_block)
  (get_ops_in_blocks)
  (get_config)
// (get_schema)
  (get_dynamic_global_properties)
  (get_chain_properties)
  (get_witness_schedule)
  (get_hardfork_version)
  (get_next_scheduled_hardfork)
  (get_reward_fund)
  (get_vesting_payout_fund)
  (get_key_references)
  (get_accounts)
  (get_accounts_by_tag)
  (lookup_account_names)
  (lookup_accounts)
  (get_account_count)
  (get_withdraw_routes)
  (get_account_bandwidth)
  (get_account_daily_bandwidth)
  (get_witnesses)
  (get_witness_by_account)
  (get_witnesses_by_vote)
  (lookup_witness_accounts)
  (get_witness_count)
  (get_transaction_hex)
  (get_required_signatures)
  (get_potential_signatures)
  (verify_authority)
  (verify_account_authority)
  (get_active_votes)
  (get_account_votes)
  (get_content)
  (get_content_replies)
  (get_tags_used_by_author)
  (get_discussions_by_payout)
  (get_post_discussions_by_payout)
  (get_comment_discussions_by_payout)
  (get_discussions_by_trending)
  (get_discussions_by_created)
  (get_discussions_by_active)
  (get_discussions_by_cashout)
  (get_discussions_by_votes)
  (get_discussions_by_children)
  (get_discussions_by_hot)
  (get_discussions_by_feed)
  (get_discussions_by_blog)
  (get_discussions_by_comments)
  (get_replies_by_last_update)
  (get_discussions_by_author_before_date)
  (get_account_history)
  (get_account_notification)
  (lookup_pods)
  (get_pods)
  (get_pods_by_tag)
  (lookup_pod_members)
  (get_pod_members_by_user)
  (get_pod_join_requests)
  (get_pod_join_requests_by_user)
  (get_friends)
  (get_friend_requests_from)
  (get_friend_requests_to)
  (get_user_feed)
  (get_pod_feed)
  (get_pod_announcements)
  (get_blog_feed)
  (get_shares_feed)
  (get_htlcs_from)
  (get_htlcs_to)
  (get_tips_from)
  (get_tips_to)
)
// clang-format on
}  // namespace database_api
}  // namespace plugins
}  // namespace wls
