#pragma once

#include <appbase/application.hpp>

#include <wls/plugins/json_rpc/json_rpc_plugin.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/tags/tags_plugin.hpp>

#define WLS_DATABASE_API_PLUGIN_NAME "database_api"

namespace wls {
namespace plugins {
namespace database_api {

using namespace appbase;

class database_api_plugin : public appbase::plugin<database_api_plugin> {
 public:
  APPBASE_PLUGIN_REQUIRES((wls::plugins::json_rpc::json_rpc_plugin)(wls::plugins::chain::chain_plugin)(wls::plugins::tags::tags_plugin))

  database_api_plugin();

  virtual ~database_api_plugin();

  static const std::string &name() {
    static std::string name = WLS_DATABASE_API_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  std::shared_ptr<class database_api> api;
};

}  // namespace database_api
}  // namespace plugins
}  // namespace wls
