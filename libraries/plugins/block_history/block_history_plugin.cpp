#include <wls/plugins/block_history/block_history_plugin.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/protocol/config.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/history_object.hpp>
#include <wls/chain/transaction_object.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/chain/util/signal.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>

#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace block_history {

using namespace wls::protocol;

using chain::database;
using chain::operation_notification;
//using chain::operation_object;

namespace detail {

class block_history_plugin_impl {
 public:
  block_history_plugin_impl(block_history_plugin &_plugin, const string &path) : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin), _nudbstore_database{path} {}

  virtual ~block_history_plugin_impl() {}

  void on_operation(const operation_notification &note);

  void on_irreversible_block( uint32_t block_num );

  vector<block_history_operation_object> get_ops_recent_block(uint32_t block_num);

  database &_db;
  block_history_plugin &_self;

  nudbstore<ops_in_block_key, ops_in_block_object> _nudbstore_database;
  boost::signals2::connection _post_apply_operation_con;
  boost::signals2::connection _irreversible_block_con;
};

//struct operation_visitor {
//  operation_visitor(database &db, const operation_notification &note, const operation_object *&n, account_name_type i, bool prune)
//      : _db(db), _note(note), new_obj(n), item(i) {}
//
//  typedef void result_type;
//
//  database &_db;
//  const operation_notification &_note;
//  const operation_object *&new_obj;
//  account_name_type item;
//
//  template <typename Op>
//  void operator()(Op &&) const {
////    const auto &hist_idx = _db.get_index<chain::account_history_index>().indices().get<chain::by_account>();
////    if (!new_obj) {
////      new_obj = &_db.create<operation_object>([&](operation_object &obj) {
////        obj.trx_id = _note.trx_id;
////        obj.block = _note.block;
////        obj.trx_in_block = _note.trx_in_block;
////        obj.op_in_trx = _note.op_in_trx;
////        obj.virtual_op = _note.virtual_op;
////        obj.timestamp = _db.head_block_time();
////        // fc::raw::pack( obj.serialized_op , _note.op);  //call to 'pack' is ambiguous
////        auto size = fc::raw::pack_size(_note.op);
////        obj.serialized_op.resize(size);
////        fc::datastream<char *> ds(obj.serialized_op.data(), size);
////        fc::raw::pack(ds, _note.op);
////      });
////    }
////
////    auto hist_itr = hist_idx.lower_bound(boost::make_tuple(item, uint32_t(-1)));
////    uint32_t sequence = 1;
////    if (hist_itr != hist_idx.end() && hist_itr->account == item) sequence = hist_itr->sequence + 1;
////
////    _db.create<chain::account_history_object>([&](chain::account_history_object &ahist) {
////      ahist.account = item;
////      ahist.sequence = sequence;
////      ahist.op = new_obj->id;
////    });
//  }
//};

void block_history_plugin_impl::on_operation(const operation_notification &note) {
//  note.op.visit(operation_visitor(_db, note, new_obj, item));
//  ilog("note.op ${op}", ("op", note.op));

  // const auto &new_obj =
  _db.create<block_history_operation_object>([&](block_history_operation_object &obj) {
    obj.trx_id = note.trx_id;
    obj.block = note.block;
    obj.trx_in_block = note.trx_in_block;
    obj.op_in_trx = note.op_in_trx;
    obj.virtual_op = note.virtual_op;
    obj.timestamp = _db.head_block_time();
    // fc::raw::pack( obj.serialized_op , _note.op);  //call to 'pack' is ambiguous
    auto size = fc::raw::pack_size(note.op);
    obj.serialized_op.resize(size);
    fc::datastream<char *> ds(obj.serialized_op.data(), size);
    fc::raw::pack(ds, note.op);
  });

//  ilog("create ${new_obj}", ("new_obj", new_obj));
}

vector<block_history_operation_object> block_history_plugin_impl::get_ops_recent_block(uint32_t block_num) {
//  uint32_t block_num = _db.last_non_undoable_block_num();
  vector<block_history_operation_object> operations;
  const auto &idx = _db.get_index<block_history_operation_index>().indices().get<by_block>();
  auto itr = idx.lower_bound(block_num);

  while ( (itr != idx.end()) && (itr->block == block_num) ) {
    operations.push_back(*itr);
    ++itr;
  }

  return operations;
}

void block_history_plugin_impl::on_irreversible_block(uint32_t block_num) {
  /**
   * Work in this function:
   * - get ops in last_irreversible_block_num from chainbase
   * - put these ops to disk storage
   * - remove old ops from chainbase
   */

  vector<block_history_operation_object> operations = this->get_ops_recent_block(block_num);

  //--

  {
    ops_in_block_object newitem;

    for (const auto item : operations) {
      block_history_applied_operation temp;

      temp.trx_id = item.trx_id;
      temp.block = item.block;
      temp.trx_in_block = item.trx_in_block;
      temp.op_in_trx = item.op_in_trx;
      temp.virtual_op = item.virtual_op;
      temp.timestamp = item.timestamp;
      temp.op = fc::raw::unpack<operation>(item.serialized_op);

      newitem.push_back(temp);
    }

    // insert
    ops_in_block_key new_key(block_num);
//    ilog("new_key: ${new_key}", ("new_key", new_key));
    boost::system::error_code ec;
    _nudbstore_database.insert(new_key, newitem, ec);
    if (ec) {
      if ((ec == nudb::error::key_exists) && (ec.category() == nudb::nudb_category())) {
        // ignore key_exists
      } else {
        FC_ASSERT(true, "${msg}", ("msg", ec.message()));
      }
    }
  }

  //--
  {
    if (block_num > 1) { // to remove the previous block only
      uint32_t prv_block = block_num - 1;
      vector<const block_history_operation_object *> to_remove;

      const auto &idx = _db.get_index<block_history_operation_index>().indices().get<by_block>();
      auto itr = idx.lower_bound(prv_block);

      while ( (itr != idx.end()) && (itr->block == prv_block) ) {
        to_remove.push_back(&(*itr));
        ++itr;
      }

      for (const auto *item : to_remove) {
        _db.remove(*item);
      }
    }
  }

//  { // for debug only, should be comment out
//    const auto &idx_by_id = _db.get_index<block_history_operation_index>().indices().get<by_id>();
//    ilog("block_history_operation_index size = ${s}", ("s", idx_by_id.size()));
//  }
}

}  // end namespace detail

block_history_plugin::block_history_plugin() {}

block_history_plugin::~block_history_plugin() {}

void block_history_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
//  cli.add_options()(
//  );
  // clang-format on
  cfg.add(cli);
}

void block_history_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  try {
    ilog("Initializing block_history plugin");

    string data_dir = appbase::app().data_dir().string();
    if (data_dir == "") data_dir = ".";
    string path = data_dir + "/plugins/block_history/";
    my = std::make_unique<detail::block_history_plugin_impl>(*this, path);

#ifndef IS_TEST_NET
    {
      bool replay = options.at("replay-blockchain").as<bool>();
      bool replay_blockchain = options.at("replay-blockchain").as<bool>();
      bool resync_blockchain = options.at("resync-blockchain").as<bool>();

      if (replay || replay_blockchain || resync_blockchain) my->_nudbstore_database.clean();
    }

    my->_nudbstore_database.open();
    my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });
#endif

    my->_post_apply_operation_con = my->_db.post_apply_operation.connect([&](const operation_notification &note) { my->on_operation(note); });
    wls::chain::add_plugin_index<block_history_operation_index>(my->_db);

  } FC_CAPTURE_AND_RETHROW()
}

void block_history_plugin::plugin_startup() {
  try {
    ilog("block_history plugin: plugin_startup()");
  } FC_CAPTURE_AND_RETHROW()
}

void block_history_plugin::plugin_shutdown() {
  try {
    ilog("block_history_plugin::plugin_shutdown");
    chain::util::disconnect_signal(my->_post_apply_operation_con);
    chain::util::disconnect_signal(my->_irreversible_block_con);

    my->_nudbstore_database.close();
  } FC_CAPTURE_AND_RETHROW()
}

vector<block_history_operation_object> block_history_plugin::get_ops_recent_block(uint32_t block_num) {
  return my->get_ops_recent_block(block_num);
}

nudbstore<ops_in_block_key, ops_in_block_object>& block_history_plugin::get_nudbstore_database() {
  return my->_nudbstore_database;
}

}  // namespace block_history
}  // namespace plugins
}  // namespace wls
