#pragma once

#include <appbase/application.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/nudbstore/nudbstore.hpp>

#define BLOCK_HISTORY_PLUGIN_NAME "block_history"

namespace wls {
namespace plugins {
namespace block_history {

namespace detail {
class block_history_plugin_impl;
}

using namespace appbase;
using wls::protocol::account_name_type;
using nudbstore::nudbstore;

/**
 *  This plugin is designed to track a range of operations by account so that one node
 *  doesn't need to hold the full operation history in memory.
 */
class block_history_plugin : public plugin<block_history_plugin> {
 public:
  block_history_plugin();

  virtual ~block_history_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin))

  static const std::string &name() {
    static std::string name = BLOCK_HISTORY_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  //--
  vector<block_history_operation_object> get_ops_recent_block(uint32_t block_num);

  nudbstore<ops_in_block_key, ops_in_block_object>& get_nudbstore_database();

  // friend class detail::block_history_plugin_impl;

 private:
  std::unique_ptr<detail::block_history_plugin_impl> my;
};

}  // namespace block_history
}  // namespace plugins
}  // namespace wls
