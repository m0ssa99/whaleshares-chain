#pragma once

#include <appbase/application.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/follow/follow_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>
#include <wls/plugins/myfeed/myfeed_objects.hpp>
#include <wls/nudbstore/nudbstore.hpp>

#define MYFEED_PLUGIN_NAME "myfeed"

namespace wls {
namespace plugins {
namespace myfeed {

namespace detail {
class myfeed_plugin_impl;
}

using namespace appbase;
using wls::protocol::account_name_type;
using nudbstore::nudbstore;

/**
 * Note: Considering
 * - using RocksDB for hot data (not payout)
 * - NuDB for archived data OR no need archived data at all.
 */
class myfeed_plugin : public plugin<myfeed_plugin> {
 public:
  myfeed_plugin();

  virtual ~myfeed_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin)(wls::plugins::block_history::block_history_plugin)(wls::plugins::follow::follow_plugin))

  static const std::string &name() {
    static std::string name = MYFEED_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  const myfeed_state_head_object* get_myfeed_state_head_object(const account_name_type &account);

  nudbstore<myfeed_key, myfeed_object>& get_nudbstore_database();

  // friend class detail::myfeed_plugin_impl;

 private:
  std::unique_ptr<detail::myfeed_plugin_impl> my;
};

}  // namespace myfeed
}  // namespace plugins
}  // namespace wls
