#pragma once

#include <wls/chain/wls_object_types.hpp>

#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace myfeed {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;

class myfeed_key {
 public:
  myfeed_key(const account_name_type &_account, const uint32_t _sequence)
      : account(_account), sequence(_sequence) {}

  myfeed_key() {}

  account_name_type account;
  uint32_t sequence = 0;
};

class myfeed_object {
 public:
  myfeed_object() {}

  myfeed_object(const account_name_type &_author, const std::string &permlink) : author(_author), permlink(permlink) {}

  account_name_type author;
  std::string permlink;

  std::set<std::string> shared_by;
};


//------------------------------------------------------------------------------

#ifndef MYFEED_SPACE_ID
#define MYFEED_SPACE_ID 13
#endif

enum myfeed_object_types {
  myfeed_state_object_type = (MYFEED_SPACE_ID << 8)
};

/**
 * to track last sequence of an account ( the head of the snake )
 */
class myfeed_state_head_object : public object<myfeed_state_object_type, myfeed_state_head_object> {
 public:
  template <typename Constructor, typename Allocator>
  myfeed_state_head_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  account_name_type account;
  uint32_t sequence = 0;
};

//------------------------------------------------------------------------------

typedef myfeed_state_head_object::id_type myfeed_state_head_object_id_type;

// clang-format off
struct by_account;
typedef multi_index_container<
    myfeed_state_head_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< myfeed_state_head_object, myfeed_state_head_object_id_type, &myfeed_state_head_object::id > >,
        ordered_unique< tag< by_account >, member< myfeed_state_head_object, account_name_type, &myfeed_state_head_object::account > >
    >,
    allocator< myfeed_state_head_object >
> myfeed_state_head_object_index;
// clang-format on


}  // namespace myfeed
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::myfeed::myfeed_key, (account)(sequence))
FC_REFLECT(wls::plugins::myfeed::myfeed_object, (author)(permlink)(shared_by))
FC_REFLECT(wls::plugins::myfeed::myfeed_state_head_object, (id)(account)(sequence))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::myfeed::myfeed_state_head_object, wls::plugins::myfeed::myfeed_state_head_object_index)
