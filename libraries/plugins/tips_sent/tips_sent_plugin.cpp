#include <wls/plugins/tips_sent/tips_sent_plugin.hpp>
#include <wls/plugins/tips_sent/tips_sent_objects.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/history_object.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/chain/util/signal.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>

#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace tips_sent {

using namespace wls::protocol;

using chain::database;
using chain::operation_notification;

namespace detail {

class tips_sent_plugin_impl {
 public:
  tips_sent_plugin_impl(tips_sent_plugin &_plugin, const string &path) : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin), _nudbstore_database{path} {
    _block_history_plugin = appbase::app().find_plugin<wls::plugins::block_history::block_history_plugin>();
  }

  virtual ~tips_sent_plugin_impl() {}

  void on_irreversible_block( uint32_t block_num );

  /**
   * high level insert which appends item to feed and updates head state
   * @param account
   * @param item
   */
  void insert(const account_name_type &account, const tips_sent_object &item);

  const tips_sent_state_head_object* get_tips_sent_state_head_object(const account_name_type &account);

  //--

  database &_db;
  tips_sent_plugin &_self;

  nudbstore<tips_sent_key, tips_sent_object> _nudbstore_database;
  boost::signals2::connection _irreversible_block_con;

  wls::plugins::block_history::block_history_plugin *_block_history_plugin;
};

const tips_sent_state_head_object *tips_sent_plugin_impl::get_tips_sent_state_head_object(const account_name_type &account) {
  try {
    return _db.find<tips_sent_state_head_object, by_account>(account);
  }
  FC_CAPTURE_AND_RETHROW((account))
}

void tips_sent_plugin_impl::insert(const account_name_type &account, const tips_sent_object &item) {
  auto head = this->get_tips_sent_state_head_object(account);
  if (head == nullptr) {
    head = &_db.create<tips_sent_state_head_object>([&](tips_sent_state_head_object &obj) { obj.account = account; });
  }

  // insert
  tips_sent_key new_key{account, head->sequence};
  // ilog("new_key: ${new_key}, item: ${item}", ("new_key", new_key)("item", item));
  boost::system::error_code ec;
  _nudbstore_database.insert(new_key, item, ec);
  if (ec) {
    if ((ec == nudb::error::key_exists) && (ec.category() == nudb::nudb_category())) {
      // ignore key_exists
    } else {
      FC_ASSERT(true, "${msg}", ("msg", ec.message()));
    }
  }

  // update last sequence to account head state
  _db.modify(*head, [&](tips_sent_state_head_object& obj) { obj.sequence++; });
}

void tips_sent_plugin_impl::on_irreversible_block(uint32_t block_num) {
  FC_ASSERT(_block_history_plugin != nullptr, "block_history_plugin is required for tips_sent_plugin");

  try {
    vector<wls::plugins::block_history::block_history_operation_object> ops = _block_history_plugin->get_ops_recent_block(block_num);
    for (const auto &item : ops) {
      operation op = fc::raw::unpack<operation>(item.serialized_op);
      switch (op.which()) {
        case operation::tag<social_action_operation>::value: {
          auto sao = op.get<social_action_operation>();
          switch (sao.action.which()) {
            case social_action::tag<social_action_user_tip>::value:
            case social_action::tag<social_action_comment_tip>::value: {
              tips_sent_object temp;
              {
                temp.trx_id = item.trx_id;
                temp.block = item.block;
                temp.trx_in_block = item.trx_in_block;
                temp.op_in_trx = item.op_in_trx;
                temp.virtual_op = item.virtual_op;
                temp.timestamp = item.timestamp;
                temp.op = fc::raw::unpack<operation>(item.serialized_op);
              }
              this->insert(sao.account, temp);
            } break;
            default:
              break;
          }
        } break;
        default:
          break;
      }
    }
  }
  FC_CAPTURE_AND_RETHROW()
}

}  // end namespace detail

tips_sent_plugin::tips_sent_plugin() {}

tips_sent_plugin::~tips_sent_plugin() {}

void tips_sent_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
//  cli.add_options()(
//  );
  // clang-format on
  cfg.add(cli);
}

void tips_sent_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  try {
    ilog("Initializing tips_sent plugin");

    string data_dir = appbase::app().data_dir().string();
    if (data_dir == "") data_dir = ".";
    string path = data_dir + "/plugins/tips_sent/";
    my = std::make_unique<detail::tips_sent_plugin_impl>(*this, path);

    {
      bool replay = options.at("replay-blockchain").as<bool>();
      bool replay_blockchain = options.at("replay-blockchain").as<bool>();
      bool resync_blockchain = options.at("resync-blockchain").as<bool>();

      if (replay || replay_blockchain || resync_blockchain) my->_nudbstore_database.clean();
    }

    my->_nudbstore_database.open();
    my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });

    wls::chain::add_plugin_index<tips_sent_state_head_object_index>(my->_db);

  } FC_CAPTURE_AND_RETHROW()
}

void tips_sent_plugin::plugin_startup() {
  try {
    ilog("tips_sent_plugin: plugin_startup()");
  } FC_CAPTURE_AND_RETHROW()
}

void tips_sent_plugin::plugin_shutdown() {
  try {
    ilog("tips_sent::plugin_shutdown");
    chain::util::disconnect_signal(my->_irreversible_block_con);

    my->_nudbstore_database.close();
  } FC_CAPTURE_AND_RETHROW()
}

nudbstore<tips_sent_key, tips_sent_object>& tips_sent_plugin::get_nudbstore_database() {
  return my->_nudbstore_database;
}

const tips_sent_state_head_object *tips_sent_plugin::get_tips_sent_state_head_object(const account_name_type &account) {
  return my->get_tips_sent_state_head_object(account);
}

}  // namespace tips_sent
}  // namespace plugins
}  // namespace wls
