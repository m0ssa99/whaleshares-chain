#pragma once

#include <appbase/application.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>
#include <wls/plugins/tips_sent/tips_sent_objects.hpp>
#include <wls/nudbstore/nudbstore.hpp>

#define TIPS_SENT_PLUGIN_NAME "tips_sent"

namespace wls {
namespace plugins {
namespace tips_sent {

namespace detail {
class tips_sent_plugin_impl;
}

using namespace appbase;
using wls::protocol::account_name_type;
using nudbstore::nudbstore;

class tips_sent_plugin : public plugin<tips_sent_plugin> {
 public:
  tips_sent_plugin();

  virtual ~tips_sent_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin)(wls::plugins::block_history::block_history_plugin))

  static const std::string &name() {
    static std::string name = TIPS_SENT_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  const tips_sent_state_head_object* get_tips_sent_state_head_object(const account_name_type &account);

  nudbstore<tips_sent_key, tips_sent_object>& get_nudbstore_database();

  // friend class detail::tips_sent_plugin_impl;

 private:
  std::unique_ptr<detail::tips_sent_plugin_impl> my;
};

}  // namespace tips_sent
}  // namespace plugins
}  // namespace wls
