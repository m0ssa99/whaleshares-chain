#pragma once

#include <fc/container/flat.hpp>
#include <wls/protocol/operations.hpp>
#include <wls/protocol/transaction.hpp>
#include <wls/chain/wls_object_types.hpp>

#include <fc/string.hpp>

namespace wls {
namespace app {

using namespace fc;

void operation_get_impacted_accounts(const wls::protocol::operation &op, fc::flat_set<protocol::account_name_type> &result);

void transaction_get_impacted_accounts(const wls::protocol::transaction &tx, fc::flat_set<protocol::account_name_type> &result);

}  // namespace app
}  // namespace wls
