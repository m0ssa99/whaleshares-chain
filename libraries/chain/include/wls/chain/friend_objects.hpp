#pragma once

#include <wls/protocol/authority.hpp>
#include <wls/protocol/wls_operations.hpp>

#include <wls/chain/wls_object_types.hpp>
#include <wls/chain/witness_objects.hpp>

#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace chain {

class friend_object : public object<friend_object_type, friend_object> {
  friend_object() = delete;

 public:
  template <typename Constructor, typename Allocator>
  friend_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  account_name_type account;
  account_name_type another;
};

// clang-format off
struct by_account_another;
typedef multi_index_container<
  friend_object,
  indexed_by<
     ordered_unique<tag<by_id>, member<friend_object, friend_object_id_type, &friend_object::id> >,
     ordered_unique<tag<by_account_another>,
        composite_key < friend_object,
           member<friend_object, account_name_type, &friend_object::account>,
           member<friend_object, account_name_type, &friend_object::another>
        >,
        composite_key_compare < std::less<account_name_type>, std::less<account_name_type> >
     >
  >,
  allocator<friend_object>
> friend_index;
// clang-format on

//------------------------------------------------------------------------------

class friend_request_object : public object<friend_request_object_type, friend_request_object> {
  friend_request_object() = delete;

 public:
  template <typename Constructor, typename Allocator>
  friend_request_object(Constructor&& c, allocator<Allocator> a) : memo(a) {
    c(*this);
  };

  id_type id;

  account_name_type account;         /// account
  account_name_type another;         /// another account
  time_point_sec created;            /// when this request is created
  shared_string memo;                /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.
};

/**
 * friend_request_pending_index
 *
 * this index holds pending requests only, these requests not resolved yet (accepted or rejected) by another account.
 * object should be removed from this index when resolved
 */

// clang-format off
struct by_account_another; // from_to
struct by_another_account; // to_from
typedef multi_index_container<
  friend_request_object,
  indexed_by<
     ordered_unique<tag<by_id>, member<friend_request_object, friend_request_object_id_type, &friend_request_object::id> >,
     ordered_unique<tag<by_account_another>,
        composite_key < friend_request_object,
           member<friend_request_object, account_name_type, &friend_request_object::account>,
           member<friend_request_object, account_name_type, &friend_request_object::another>
        >,
        composite_key_compare < std::less<account_name_type>, std::less<account_name_type> >
     >
#ifndef IS_LOW_MEM
     ,
     ordered_unique<tag<by_another_account>,
        composite_key < friend_request_object,
           member<friend_request_object, account_name_type, &friend_request_object::another>,
           member<friend_request_object, account_name_type, &friend_request_object::account>
        >,
        composite_key_compare < std::less<account_name_type>, std::less<account_name_type> >
     >
#endif
  >,
  allocator<friend_request_object>
> friend_request_pending_index;
// clang-format on

}  // namespace chain
}  // namespace wls


FC_REFLECT(wls::chain::friend_object, (id)(account)(another))
FC_REFLECT(wls::chain::friend_request_object, (id)(account)(another)(created)(memo))

CHAINBASE_SET_INDEX_TYPE(wls::chain::friend_object, wls::chain::friend_index)
CHAINBASE_SET_INDEX_TYPE(wls::chain::friend_request_object, wls::chain::friend_request_pending_index)