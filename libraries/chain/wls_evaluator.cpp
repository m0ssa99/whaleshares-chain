#include <wls/chain/wls_evaluator.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/custom_operation_interpreter.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/witness_objects.hpp>
#include <wls/chain/block_summary_object.hpp>

#include <wls/chain/util/reward.hpp>
#include <fc/macros.hpp>
#ifndef IS_LOW_MEM
#include <diff_match_patch.h>
#include <boost/locale/encoding_utf.hpp>

using boost::locale::conv::utf_to_utf;

std::wstring utf8_to_wstring(const std::string& str) { return utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size()); }

std::string wstring_to_utf8(const std::wstring& str) { return utf_to_utf<char>(str.c_str(), str.c_str() + str.size()); }

#endif

#include <fc/uint128.hpp>
#include <fc/utf8.hpp>

#include <limits>

namespace wls {
namespace chain {
using fc::uint128_t;

inline void validate_permlink_0_1(const string& permlink) {
  FC_ASSERT(permlink.size() > WLS_MIN_PERMLINK_LENGTH && permlink.size() < WLS_MAX_PERMLINK_LENGTH, "Permlink is not a valid size.");

  for (auto c : permlink) {
    switch (c) {
      case 'a':
      case 'b':
      case 'c':
      case 'd':
      case 'e':
      case 'f':
      case 'g':
      case 'h':
      case 'i':
      case 'j':
      case 'k':
      case 'l':
      case 'm':
      case 'n':
      case 'o':
      case 'p':
      case 'q':
      case 'r':
      case 's':
      case 't':
      case 'u':
      case 'v':
      case 'w':
      case 'x':
      case 'y':
      case 'z':
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
      case '-':
        break;
      default:
        FC_ASSERT(false, "Invalid permlink character: ${s}", ("s", std::string() + c));
    }
  }
}

struct strcmp_equal {
  bool operator()(const shared_string& a, const string& b) { return a.size() == b.size() || std::strcmp(a.c_str(), b.c_str()) == 0; }
};

void witness_update_evaluator::do_apply(const witness_update_operation& o) {
  _db.get_account(o.owner);  // verify owner exists

  FC_ASSERT(o.url.size() <= WLS_MAX_WITNESS_URL_LENGTH, "URL is too long");
  FC_ASSERT(o.props.account_creation_fee.symbol == WLS_SYMBOL);

  // #pragma message( "TODO: This needs to be part of HF 20 and moved to validate if not triggered in previous blocks" )
  if (_db.is_producing()) {
    FC_ASSERT(o.props.maximum_block_size <= WLS_SOFT_MAX_BLOCK_SIZE, "Max block size cannot be more than 2MiB");
  }

  const auto& by_witness_name_idx = _db.get_index<witness_index>().indices().get<by_name>();
  auto wit_itr = by_witness_name_idx.find(o.owner);
  if (wit_itr != by_witness_name_idx.end()) {
    _db.modify(*wit_itr, [&](witness_object& w) {
      from_string(w.url, o.url);
      w.signing_key = o.block_signing_key;
      w.props = o.props;
    });
  } else {
    _db.create<witness_object>([&](witness_object& w) {
      w.owner = o.owner;
      from_string(w.url, o.url);
      w.signing_key = o.block_signing_key;
      w.created = _db.head_block_time();
      w.props = o.props;
    });
  }
}

void account_create_evaluator::do_apply(const account_create_operation& o) {
#ifdef IS_TEST_NET
  bool testnet = true;
#else
  bool testnet = false;
#endif

  bool has_hf3 = false;
  if (testnet || _db.has_hardfork(WLS_HARDFORK_0_3)) has_hf3 = true;

  const auto& creator = _db.get_account(o.creator);
  const auto& props = _db.get_dynamic_global_properties();

  if (has_hf3) {
    /**
     * figuring out the required fee:
     * - base fee (10 WLS) + account name fee (depends on account name length): (this fee get burnt)
     *   >12 chars:   0 WLS
     *    12 chars:   1 WLS
     *    11 chars:   2 WLS
     *    10 chars:   4 WLS
     *     9 chars:   8 WLS
     *     8 chars:  16 WLS
     *     7 chars:  32 WLS
     *     6 chars:  64 WLS
     *     5 chars: 128 WLS
     *     4 chars: 256 WLS
     *     3 chars: 512 WLS
     * - remain goes to new account stake, min WLS_MIN_ACCOUNT_CREATION_FEE
     */

    auto base_fee = asset(10000, WLS_SYMBOL); // 10 WLS base fee
    auto name_fee = asset(0, WLS_SYMBOL);

    const size_t len = o.new_account_name.size();
    if (len <= 12) {
      auto name_fee_value = int64_t(1000) * (uint16_t(1) << (12-len));
      name_fee = asset(name_fee_value, WLS_SYMBOL);
    }

    FC_ASSERT(name_fee <= asset(512000, WLS_SYMBOL), "name fee max 512 WLS.");
    auto required_fee = base_fee + name_fee + asset(WLS_MIN_ACCOUNT_CREATION_FEE, WLS_SYMBOL); // base_fee + name_fee + min_stake

    FC_ASSERT(creator.balance >= o.fee, "Insufficient balance: ${f} required, ${p} balance.", ("f", required_fee)("p", o.fee));
    FC_ASSERT(creator.balance >= required_fee, "Insufficient balance to create account.", ("creator.balance", creator.balance)("required", required_fee));
    FC_ASSERT(o.fee >= required_fee, "Insufficient Fee: ${f} required, ${p} provided.", ("f", required_fee)("p", o.fee));

    auto to_burn = base_fee + name_fee;
    _db.pay_fee(_db.get_account(creator.name), to_burn); // burn the fee

    auto to_stake = o.fee - to_burn;
    FC_ASSERT(to_stake >= asset(WLS_MIN_ACCOUNT_CREATION_FEE, WLS_SYMBOL), "Need min stake for new account", ("to_stake", to_stake));

    for (auto& a : o.owner.account_auths) _db.get_account(a.first);
    for (auto& a : o.active.account_auths) _db.get_account(a.first);
    for (auto& a : o.posting.account_auths) _db.get_account(a.first);

    _db.modify(_db.get_account(creator.name), [&](account_object& c) { c.balance -= to_stake; });

    const auto& new_account = _db.create<account_object>([&](account_object& acc) {
      acc.name = o.new_account_name;
      acc.memo_key = o.memo_key;
      acc.created = props.time;
      acc.last_vote_time = props.time;
      acc.recovery_account = o.creator;

#ifndef IS_LOW_MEM
      from_string(acc.json_metadata, o.json_metadata);
#endif
    });

    _db.create<account_authority_object>([&](account_authority_object& auth) {
      auth.account = o.new_account_name;
      auth.owner = o.owner;
      auth.active = o.active;
      auth.posting = o.posting;
      auth.last_owner_update = fc::time_point_sec::min();
    });

    _db.create_vesting(new_account, to_stake);

    // new account not allowed to claim vesting_reward current cycle.
    _db.modify(_db.get_account(o.new_account_name), [&](account_object& acc) { acc.vr_snaphost_next_cycle = props.vr_cycle; });
    _db.update_account_vr_snaphost(o.new_account_name);  // update account vesting_reward info
  } else {
    FC_ASSERT(o.creator == WLS_WHALESHARE_ACC_NAME, "only wls account can create new account at prelauch period.");
    FC_ASSERT(creator.balance >= o.fee, "Insufficient balance to create account.", ("creator.balance", creator.balance)("required", o.fee));

    const witness_schedule_object& wso = _db.get_witness_schedule_object();
    FC_ASSERT(o.fee >= asset(wso.median_props.account_creation_fee.amount, WLS_SYMBOL), "Insufficient Fee: ${f} required, ${p} provided.",
              ("f", wso.median_props.account_creation_fee)("p", o.fee));

    for (auto& a : o.owner.account_auths) _db.get_account(a.first);
    for (auto& a : o.active.account_auths) _db.get_account(a.first);
    for (auto& a : o.posting.account_auths) _db.get_account(a.first);

    _db.modify(creator, [&](account_object& c) { c.balance -= o.fee; });

    const auto& new_account = _db.create<account_object>([&](account_object& acc) {
      acc.name = o.new_account_name;
      acc.memo_key = o.memo_key;
      acc.created = props.time;
      acc.last_vote_time = props.time;
      acc.recovery_account = o.creator;

#ifndef IS_LOW_MEM
      from_string(acc.json_metadata, o.json_metadata);
#endif
    });

    _db.create<account_authority_object>([&](account_authority_object& auth) {
      auth.account = o.new_account_name;
      auth.owner = o.owner;
      auth.active = o.active;
      auth.posting = o.posting;
      auth.last_owner_update = fc::time_point_sec::min();
    });

    if (o.fee.amount > 0) _db.create_vesting(new_account, o.fee);
  }
}

void account_update_evaluator::do_apply(const account_update_operation& o) {
  FC_ASSERT(o.account != WLS_TEMP_ACCOUNT, "Cannot update temp account.");

  if (o.posting) o.posting->validate();

  const auto& account = _db.get_account(o.account);
  const auto& account_auth = _db.get<account_authority_object, by_account>(o.account);

  if (o.owner) {
#ifndef IS_TEST_NET
    FC_ASSERT(_db.head_block_time() - account_auth.last_owner_update > WLS_OWNER_UPDATE_LIMIT, "Owner authority can only be updated once an hour.");
#endif

    for (auto a : o.owner->account_auths) _db.get_account(a.first);

    _db.update_owner_authority(account, *o.owner);
  }

  if (o.active)
    for (auto a : o.active->account_auths) _db.get_account(a.first);

  if (o.posting)
    for (auto a : o.posting->account_auths) _db.get_account(a.first);

  _db.modify(account, [&](account_object& acc) {
    if (o.memo_key != public_key_type()) acc.memo_key = o.memo_key;

    acc.last_account_update = _db.head_block_time();

#ifndef IS_LOW_MEM
    if (o.json_metadata.size() > 0) from_string(acc.json_metadata, o.json_metadata);
#endif
  });

  if (o.active || o.posting) {
    _db.modify(account_auth, [&](account_authority_object& auth) {
      if (o.active) auth.active = *o.active;
      if (o.posting) auth.posting = *o.posting;
    });
  }
}

/**
 *  Because net_rshares is 0 there is no need to update any pending payout calculations or parent posts.
 */
void delete_comment_evaluator::do_apply(const delete_comment_operation& o) {
  bool is_disabled = false;
#ifdef IS_TEST_NET
  is_disabled = true;
#else
  if (_db.has_hardfork(WLS_HARDFORK_0_2)) is_disabled = true;
#endif

  if (is_disabled) {
    FC_ASSERT(false, "This operation is disable since HF2, use social_action instead");
  }

  const auto& comment = _db.get_comment(o.author, o.permlink);
  FC_ASSERT(comment.children == 0, "Cannot delete a comment with replies.");
  FC_ASSERT(comment.cashout_time != fc::time_point_sec::maximum());
  FC_ASSERT(comment.net_rshares <= 0, "Cannot delete a comment with net positive votes.");

  if (comment.net_rshares > 0) return;

  const auto& vote_idx = _db.get_index<comment_vote_index>().indices().get<by_comment_voter>();

  auto vote_itr = vote_idx.lower_bound(comment_id_type(comment.id));
  while (vote_itr != vote_idx.end() && vote_itr->comment == comment.id) {
    const auto& cur_vote = *vote_itr;
    ++vote_itr;
    _db.remove(cur_vote);
  }

  /// this loop can be skipped for validate-only nodes as it is merely gathering stats for indices
  if (comment.parent_author != WLS_ROOT_POST_PARENT) {
    auto parent = &_db.get_comment(comment.parent_author, comment.parent_permlink);
    auto now = _db.head_block_time();
    while (parent) {
      _db.modify(*parent, [&](comment_object& p) {
        p.children--;
        p.active = now;
      });
#ifndef IS_LOW_MEM
      if (parent->parent_author != WLS_ROOT_POST_PARENT)
        parent = &_db.get_comment(parent->parent_author, parent->parent_permlink);
      else
#endif
        parent = nullptr;
    }
  }

  _db.remove(comment);
}

struct comment_options_extension_visitor {
  comment_options_extension_visitor(const comment_object& c, database& db) : _c(c), _db(db) {}

  typedef void result_type;

  const comment_object& _c;
  database& _db;

  void operator()(const comment_payout_beneficiaries& cpb) const {
    FC_ASSERT(_c.beneficiaries.size() == 0, "Comment already has beneficiaries specified.");
    FC_ASSERT(_c.abs_rshares == 0, "Comment must not have been voted on before specifying beneficiaries.");

    _db.modify(_c, [&](comment_object& c) {
      for (auto& b : cpb.beneficiaries) {
        auto acc = _db.find<account_object, by_name>(b.account);
        FC_ASSERT(acc != nullptr, "Beneficiary \"${a}\" must exist.", ("a", b.account));
        c.beneficiaries.push_back(b);
      }
    });
  }
};

void comment_options_evaluator::do_apply(const comment_options_operation& o) {
  if (_db.has_hardfork(WLS_HARDFORK_0_4)) FC_ASSERT(false, "This operation is disable since hard fork 4.");

#ifdef IS_TEST_NET
  bool testnet = true;
#else
  bool testnet = false;
#endif
  bool has_hf3 = false;

  if (testnet || _db.has_hardfork(WLS_HARDFORK_0_3)) has_hf3 = true;

  const auto& comment = _db.get_comment(o.author, o.permlink);

  if (has_hf3) FC_ASSERT(comment.cashout_time != fc::time_point_sec::maximum(), "The comment is archived");

  // because SBD_SYMBOL is disable, so we dont care ASSET type, compare the amount only.
  if (!o.allow_curation_rewards || !o.allow_votes || o.max_accepted_payout.amount < comment.max_accepted_payout.amount)
    FC_ASSERT(comment.abs_rshares == 0, "One of the included comment options requires the comment to have no rshares allocated to it.");

  FC_ASSERT(comment.allow_curation_rewards >= o.allow_curation_rewards, "Curation rewards cannot be re-enabled.");
  FC_ASSERT(comment.allow_votes >= o.allow_votes, "Voting cannot be re-enabled.");
  // FC_ASSERT( comment.max_accepted_payout >= o.max_accepted_payout, "A comment cannot accept a greater payout." );
  FC_ASSERT(comment.max_accepted_payout.amount.value >= o.max_accepted_payout.amount.value, "A comment cannot accept a greater payout.");

  _db.modify(comment, [&](comment_object& c) {
    c.max_accepted_payout = o.max_accepted_payout;
    c.allow_votes = o.allow_votes;
    c.allow_curation_rewards = o.allow_curation_rewards;
  });

  for (auto& e : o.extensions) e.visit(comment_options_extension_visitor(comment, _db));
}

void comment_evaluator::do_apply(const comment_operation& o) {
  try {
    bool is_disabled = false;
#ifdef IS_TEST_NET
    is_disabled = true;
#else
    if (_db.has_hardfork(WLS_HARDFORK_0_2)) is_disabled = true;
#endif

    if (is_disabled) {
      FC_ASSERT(false, "This operation is disable since hard fork 2, use social_action instead");
    }

    FC_ASSERT(o.title.size() + o.body.size() + o.json_metadata.size(), "Cannot update comment because nothing appears to be changing.");

    const auto& by_permlink_idx = _db.get_index<comment_index>().indices().get<by_permlink>();
    auto itr = by_permlink_idx.find(boost::make_tuple(o.author, o.permlink));
    const auto& auth = _db.get_account(o.author);  /// prove it exists
    comment_id_type id;
    const comment_object* parent = nullptr;
    if (o.parent_author != WLS_ROOT_POST_PARENT) {
      parent = &_db.get_comment(o.parent_author, o.parent_permlink);
      // dont need this anymore
//      FC_ASSERT(parent->depth < WLS_MAX_COMMENT_DEPTH, "Comment is nested ${x} posts deep, maximum depth is ${y}.",
//                ("x", parent->depth)("y", WLS_MAX_COMMENT_DEPTH));

      // moved from witness plugin
      FC_ASSERT(parent->depth < WLS_SOFT_MAX_COMMENT_DEPTH, "Comment reached maximum depth.");

#ifndef IS_TEST_NET
      if (_db.has_hardfork(WLS_HARDFORK_0_2)) FC_ASSERT(parent->cashout_time != fc::time_point_sec::maximum(), "The comment is archived");
#endif
    }

    if (o.json_metadata.size()) FC_ASSERT(fc::is_utf8(o.json_metadata), "JSON Metadata must be UTF-8");
    auto now = _db.head_block_time();
    if (itr == by_permlink_idx.end()) {
      if (o.parent_author != WLS_ROOT_POST_PARENT) FC_ASSERT(_db.get(parent->root_comment).allow_replies, "The parent comment has disabled replies.");

#ifndef IS_TEST_NET
      if (_db.head_block_num() > 28800) {  // <== a period to import data from testnet2
#endif

        if (o.parent_author == WLS_ROOT_POST_PARENT)
          FC_ASSERT((now - auth.last_root_post) > WLS_MIN_ROOT_COMMENT_INTERVAL, "You may only post once every 5 minutes.");
        else
          FC_ASSERT((now - auth.last_post) > WLS_MIN_REPLY_INTERVAL, "You may only comment once every 20 seconds.");

#ifndef IS_TEST_NET
      }
#endif

      uint16_t reward_weight = WLS_100_PERCENT;
      uint64_t post_bandwidth = auth.post_bandwidth;

      _db.modify(auth, [&](account_object& a) {
        if (o.parent_author == WLS_ROOT_POST_PARENT) {
          a.last_root_post = now;
          a.post_bandwidth = uint32_t(post_bandwidth);
        }
        a.last_post = now;
        a.post_count++;
      });

#ifdef IS_TEST_NET
      uint64_t cashout_window = uint64_t(WLS_CASHOUT_WINDOW_SECONDS);
#else
      uint64_t cashout_window = _db.has_hardfork(WLS_HARDFORK_0_1) ? uint64_t(WLS_HARDFORK_0_1_CASHOUT_WINDOW_SECONDS) : uint64_t(WLS_CASHOUT_WINDOW_SECONDS);
#endif

      const auto& new_comment = _db.create<comment_object>([&](comment_object& com) {
        validate_permlink_0_1(o.parent_permlink);
        validate_permlink_0_1(o.permlink);
        com.author = o.author;
        from_string(com.permlink, o.permlink);
        com.last_update = _db.head_block_time();
        com.created = com.last_update;
        com.active = com.last_update;
        com.last_payout = fc::time_point_sec::min();
        com.max_cashout_time = fc::time_point_sec::maximum();
        com.reward_weight = reward_weight;
        com.cashout_time = com.created + cashout_window;

        if (o.parent_author == WLS_ROOT_POST_PARENT) {
          com.parent_author = "";
          from_string(com.parent_permlink, o.parent_permlink);
          from_string(com.category, o.parent_permlink);
          com.root_comment = com.id;
        } else {
          com.parent_author = parent->author;
          com.parent_permlink = parent->permlink;
          com.depth = parent->depth + 1;
          com.category = parent->category;
          com.root_comment = parent->root_comment;
        }

#ifndef IS_LOW_MEM
        from_string(com.title, o.title);
        if (o.body.size() < 1024 * 1024 * 128) {
          from_string(com.body, o.body);
        }
        if (fc::is_utf8(o.json_metadata))
          from_string(com.json_metadata, o.json_metadata);
        else
          wlog("Comment ${a}/${p} contains invalid UTF-8 metadata", ("a", o.author)("p", o.permlink));
#endif
      });

      id = new_comment.id;

      /// this loop can be skiped for validate-only nodes as it is merely gathering stats for indicies
      auto now = _db.head_block_time();
      while (parent) {
        _db.modify(*parent, [&](comment_object& p) {
          p.children++;
          p.active = now;
        });
#ifndef IS_LOW_MEM
        if (parent->parent_author != WLS_ROOT_POST_PARENT)
          parent = &_db.get_comment(parent->parent_author, parent->parent_permlink);
        else
#endif
          parent = nullptr;
      }

    } else {  // start edit case
      const auto& comment = *itr;
#ifndef IS_TEST_NET
      if (_db.has_hardfork(WLS_HARDFORK_0_2)) FC_ASSERT(comment.cashout_time != fc::time_point_sec::maximum(), "The comment is archived");
#endif

      _db.modify(comment, [&](comment_object& com) {
        com.last_update = _db.head_block_time();
        com.active = com.last_update;
        strcmp_equal equal;

        if (!parent) {
          FC_ASSERT(com.parent_author == WLS_ROOT_POST_PARENT, "The parent of a comment cannot change.");
          FC_ASSERT(equal(com.parent_permlink, o.parent_permlink), "The permlink of a comment cannot change.");
        } else {
          FC_ASSERT(com.parent_author == o.parent_author, "The parent of a comment cannot change.");
          FC_ASSERT(equal(com.parent_permlink, o.parent_permlink), "The permlink of a comment cannot change.");
        }

#ifndef IS_LOW_MEM
        if (o.title.size()) from_string(com.title, o.title);
        if (o.json_metadata.size()) {
          if (fc::is_utf8(o.json_metadata))
            from_string(com.json_metadata, o.json_metadata);
          else
            wlog("Comment ${a}/${p} contains invalid UTF-8 metadata", ("a", o.author)("p", o.permlink));
        }

        if (o.body.size()) {
          try {
            diff_match_patch<std::wstring> dmp;
            auto patch = dmp.patch_fromText(utf8_to_wstring(o.body));
            if (patch.size()) {
              auto result = dmp.patch_apply(patch, utf8_to_wstring(to_string(com.body)));
              auto patched_body = wstring_to_utf8(result.first);
              if (!fc::is_utf8(patched_body)) {
                idump(("invalid utf8")(patched_body));
                from_string(com.body, fc::prune_invalid_utf8(patched_body));
              } else
                from_string(com.body, patched_body);
            } else // replace
              from_string(com.body, o.body);
          } catch (...) {
            from_string(com.body, o.body);
          }
        }
#endif
      });

    }  // end EDIT case
  }
  FC_CAPTURE_AND_RETHROW((o))
}

void transfer_evaluator::do_apply(const transfer_operation& o) {
  const auto& from_account = _db.get_account(o.from);
  const auto& to_account = _db.get_account(o.to);
  FC_ASSERT(_db.get_balance(from_account, o.amount.symbol) >= o.amount, "Account does not have sufficient funds for transfer.");
  _db.adjust_balance(from_account, -o.amount);
  _db.adjust_balance(to_account, o.amount);
}

void transfer_to_vesting_evaluator::do_apply(const transfer_to_vesting_operation& o) {
  const auto& from_account = _db.get_account(o.from);
  const auto& to_account = o.to.size() ? _db.get_account(o.to) : from_account;
  FC_ASSERT(_db.get_balance(from_account, WLS_SYMBOL) >= o.amount, "Account does not have sufficient WLS for transfer.");
  _db.adjust_balance(from_account, -o.amount);
  _db.create_vesting(to_account, o.amount);
}

void withdraw_vesting_evaluator::do_apply(const withdraw_vesting_operation& o) {
  const auto& account = _db.get_account(o.account);
  if (o.vesting_shares.amount < 0) {
    FC_ASSERT(false, "Cannot withdraw negative VESTS. account: ${account}, vests:${vests}", ("account", o.account)("vests", o.vesting_shares));
    return; // else, no-op
  }

  FC_ASSERT(account.vesting_shares >= asset(0, VESTS_SYMBOL), "Account does not have sufficient vesting for withdraw.");
  FC_ASSERT(account.vesting_shares >= o.vesting_shares, "Account does not have sufficient WHALESTAKE for withdraw.");

  /////////////////////////////////////////////////////////////////////////////
#ifndef IS_TEST_NET
  // max powerdown = 100% - current inflation rate
  // https://gitlab.com/beyondbitcoin/whaleshares-chain/issues/9
  int64_t current_inflation_rate = _db.get_current_inflation_rate();
  share_type max_amount_powerdown_allowed = (account.vesting_shares.amount * (int64_t(WLS_100_PERCENT) - current_inflation_rate)) / int64_t(WLS_100_PERCENT);
  FC_ASSERT(o.vesting_shares.amount <= max_amount_powerdown_allowed, "Max amount withdraw vesting is ${max_amount_powerdown_allowed}",
            ("max_amount_powerdown_allowed", max_amount_powerdown_allowed));
#endif

  {
    const auto& props = _db.get_dynamic_global_properties();
    const witness_schedule_object& wso = _db.get_witness_schedule_object();
    asset min_vests = wso.median_props.account_creation_fee * props.get_vesting_share_price();
    min_vests.amount.value *= 10;

    FC_ASSERT(account.vesting_shares > min_vests || o.vesting_shares.amount == 0,
              "Account registered by another account requires 10x account creation fee worth of WHALESTAKE before it can be powered down.");
  }

  if (o.vesting_shares.amount == 0) {
    FC_ASSERT(account.vesting_withdraw_rate.amount != 0, "This operation would not change the vesting withdraw rate.");

    _db.modify(account, [&](account_object& a) {
      a.vesting_withdraw_rate = asset(0, VESTS_SYMBOL);
      a.next_vesting_withdrawal = time_point_sec::maximum();
      a.to_withdraw = 0;
      a.withdrawn = 0;
    });
  } else {
    int vesting_withdraw_intervals = WLS_VESTING_WITHDRAW_INTERVALS;  /// 13 weeks = 1 quarter of a year
    _db.modify(account, [&](account_object& a) {
      auto new_vesting_withdraw_rate = asset(o.vesting_shares.amount / vesting_withdraw_intervals, VESTS_SYMBOL);
      if (new_vesting_withdraw_rate.amount == 0) new_vesting_withdraw_rate.amount = 1;

      FC_ASSERT(account.vesting_withdraw_rate != new_vesting_withdraw_rate, "This operation would not change the vesting withdraw rate.");

      a.vesting_withdraw_rate = new_vesting_withdraw_rate;
      a.next_vesting_withdrawal = _db.head_block_time() + fc::seconds(WLS_VESTING_WITHDRAW_INTERVAL_SECONDS);
      a.to_withdraw = o.vesting_shares.amount;
      a.withdrawn = 0;
    });
  }
}

void set_withdraw_vesting_route_evaluator::do_apply(const set_withdraw_vesting_route_operation& o) {
  try {
    const auto& from_account = _db.get_account(o.from_account);
    const auto& to_account = _db.get_account(o.to_account);
    const auto& wd_idx = _db.get_index<withdraw_vesting_route_index>().indices().get<by_withdraw_route>();
    auto itr = wd_idx.find(boost::make_tuple(from_account.id, to_account.id));

    if (itr == wd_idx.end()) {
      FC_ASSERT(o.percent != 0, "Cannot create a 0% destination.");
      FC_ASSERT(from_account.withdraw_routes < WLS_MAX_WITHDRAW_ROUTES, "Account already has the maximum number of routes.");

      _db.create<withdraw_vesting_route_object>([&](withdraw_vesting_route_object& wvdo) {
        wvdo.from_account = from_account.id;
        wvdo.to_account = to_account.id;
        wvdo.percent = o.percent;
        wvdo.auto_vest = o.auto_vest;
      });
      _db.modify(from_account, [&](account_object& a) { a.withdraw_routes++; });
    } else if (o.percent == 0) {
      _db.remove(*itr);
      _db.modify(from_account, [&](account_object& a) { a.withdraw_routes--; });
    } else {
      _db.modify(*itr, [&](withdraw_vesting_route_object& wvdo) {
        wvdo.from_account = from_account.id;
        wvdo.to_account = to_account.id;
        wvdo.percent = o.percent;
        wvdo.auto_vest = o.auto_vest;
      });
    }

    itr = wd_idx.upper_bound(boost::make_tuple(from_account.id, account_id_type()));
    uint16_t total_percent = 0;

    while (itr->from_account == from_account.id && itr != wd_idx.end()) {
      total_percent += itr->percent;
      ++itr;
    }

    FC_ASSERT(total_percent <= WLS_100_PERCENT, "More than 100% of vesting withdrawals allocated to destinations.");
  }
  FC_CAPTURE_AND_RETHROW()
}

void account_witness_proxy_evaluator::do_apply(const account_witness_proxy_operation& o) {
  const auto& account = _db.get_account(o.account);
  FC_ASSERT(account.proxy != o.proxy, "Proxy must change.");

  /// remove all current votes
  std::array<share_type, WLS_MAX_PROXY_RECURSION_DEPTH + 1> delta;
  delta[0] = -account.vesting_shares.amount;
  for (int i = 0; i < WLS_MAX_PROXY_RECURSION_DEPTH; ++i) delta[i + 1] = -account.proxied_vsf_votes[i];
  _db.adjust_proxied_witness_votes(account, delta);

  if (o.proxy.size()) {
    const auto& new_proxy = _db.get_account(o.proxy);
    flat_set<account_id_type> proxy_chain({account.id, new_proxy.id});
    proxy_chain.reserve(WLS_MAX_PROXY_RECURSION_DEPTH + 1);

    /// check for proxy loops and fail to update the proxy if it would create a loop
    auto cprox = &new_proxy;
    while (cprox->proxy.size() != 0) {
      const auto next_proxy = _db.get_account(cprox->proxy);
      FC_ASSERT(proxy_chain.insert(next_proxy.id).second, "This proxy would create a proxy loop.");
      cprox = &next_proxy;
      FC_ASSERT(proxy_chain.size() <= WLS_MAX_PROXY_RECURSION_DEPTH, "Proxy chain is too long.");
    }

    _db.clear_witness_votes(account); /// clear all individual vote records
    _db.modify(account, [&](account_object& a) { a.proxy = o.proxy; });

    /// add all new votes
    for (int i = 0; i <= WLS_MAX_PROXY_RECURSION_DEPTH; ++i) delta[i] = -delta[i];
    _db.adjust_proxied_witness_votes(account, delta);
  } else {  /// we are clearing the proxy which means we simply update the account
    _db.modify(account, [&](account_object& a) { a.proxy = o.proxy; });
  }
}

void account_witness_vote_evaluator::do_apply(const account_witness_vote_operation& o) {
  const auto& voter = _db.get_account(o.account);
  FC_ASSERT(voter.proxy.size() == 0, "A proxy is currently set, please clear the proxy before voting for a witness.");

  const auto& witness = _db.get_witness(o.witness);
  if (_db.has_hardfork(WLS_HARDFORK_0_4) && (witness.signing_key == public_key_type()) && o.approve) {
    FC_ASSERT(false, "Not allowed to vote for disabled witness since hard fork 4.");
  }

  const auto& by_account_witness_idx = _db.get_index<witness_vote_index>().indices().get<by_account_witness>();
  auto itr = by_account_witness_idx.find(boost::make_tuple(voter.id, witness.id));

  if (itr == by_account_witness_idx.end()) {
    FC_ASSERT(o.approve, "Vote doesn't exist, user must indicate a desire to approve witness.");
    FC_ASSERT(voter.witnesses_voted_for < WLS_MAX_ACCOUNT_WITNESS_VOTES, "Account has voted for too many witnesses.");  // TODO: Remove after hardfork 2
    _db.create<witness_vote_object>([&](witness_vote_object& v) {
      v.witness = witness.id;
      v.account = voter.id;
    });
    _db.adjust_witness_vote(witness, voter.witness_vote_weight());
    _db.modify(voter, [&](account_object& a) { a.witnesses_voted_for++; });
  } else {
    FC_ASSERT(!o.approve, "Vote currently exists, user must indicate a desire to reject witness.");
    _db.adjust_witness_vote(witness, -voter.witness_vote_weight());
    _db.modify(voter, [&](account_object& a) { a.witnesses_voted_for--; });
    _db.remove(*itr);
  }
}

void vote_evaluator::do_apply(const vote_operation& o) {
  try {
    if (_db.has_hardfork(WLS_HARDFORK_0_4)) FC_ASSERT(false, "This operation is disable since hard fork 4.");

    const auto& comment = _db.get_comment(o.author, o.permlink);
    const auto& voter = _db.get_account(o.voter);

    bool check_permission = false;
#ifdef IS_TEST_NET
    check_permission = true;
#else
    if (_db.has_hardfork(WLS_HARDFORK_0_2)) check_permission = true;
#endif

    if (check_permission) { // check that user allowed to interact with this comment
      FC_ASSERT(o.weight >= 0, "no negative weight since hardfork 2");

      const auto& root = _db.get(comment.root_comment); // get the root-comment

      /**
       * Need to check user allowed to interact (comment or vote) here.
       * - If the root-comment having pod specified, only pod members can interact
       * - if the root-comment having no pod specified and
       *     + and allow_friends is false: anyone can interact
       *     + and allow_friends is true: only friends can interact
       */
      if (root.pod != WLS_ROOT_POST_PARENT) {
        // If the root-comment having pod specified, only pod members can interact
        // check this author is in pod or not
        const auto *pmember = _db.find_pod_member(root.pod, voter.name);
        FC_ASSERT(pmember != nullptr, "only member of this pod can interact with the post", ("pod", root.pod));
      } else {
        /**
         * if the root-comment having no pod specified and
         *   + and allow_friends is false: anyone can interact
         *   + and allow_friends is true: only friends can interact
         */
        if (root.allow_friends) {
          if (voter.name != root.author) {  // to make sure author can interact to his/her post
            const auto* pfriend = _db.find_friend(root.author, voter.name);
            FC_ASSERT(pfriend != nullptr, "only friends of author can interact with the post.", ("author", root.author));
          }
        }
      }
    }

    if (o.weight > 0) FC_ASSERT(comment.allow_votes, "Votes are not allowed on the comment.");
    if (_db.calculate_discussion_payout_time(comment) == fc::time_point_sec::maximum()) {
#ifndef CLEAR_VOTES
      const auto& comment_vote_idx = _db.get_index<comment_vote_index>().indices().get<by_comment_voter>();
      auto itr = comment_vote_idx.find(std::make_tuple(comment.id, voter.id));

      if (itr == comment_vote_idx.end())
        _db.create<comment_vote_object>([&](comment_vote_object& cvo) {
          cvo.voter = voter.id;
          cvo.comment = comment.id;
          cvo.vote_percent = o.weight;
          cvo.last_update = _db.head_block_time();
        });
      else
        _db.modify(*itr, [&](comment_vote_object& cvo) {
          cvo.vote_percent = o.weight;
          cvo.last_update = _db.head_block_time();
        });
#endif
      return;
    }

    const auto& comment_vote_idx = _db.get_index<comment_vote_index>().indices().get<by_comment_voter>();
    auto itr = comment_vote_idx.find(std::make_tuple(comment.id, voter.id));
    int64_t elapsed_seconds = (_db.head_block_time() - voter.last_vote_time).to_seconds();
    FC_ASSERT(elapsed_seconds >= WLS_MIN_VOTE_INTERVAL_SEC, "Can only vote once every 3 seconds.");
    int64_t regenerated_power = (WLS_100_PERCENT * elapsed_seconds) / WLS_VOTE_REGENERATION_SECONDS;
    int64_t current_power = std::min(int64_t(voter.voting_power + regenerated_power), int64_t(WLS_100_PERCENT));
    FC_ASSERT(current_power > 0, "Account currently does not have voting power.");
    int64_t abs_weight = abs(o.weight);
    int64_t used_power = (current_power * abs_weight) / WLS_100_PERCENT;
    const dynamic_global_property_object& dgpo = _db.get_dynamic_global_properties();
    // The second multiplication is rounded up as of HF 259
    int64_t max_vote_denom = dgpo.vote_power_reserve_rate * WLS_VOTE_REGENERATION_SECONDS / (60 * 60 * 24);
    FC_ASSERT(max_vote_denom > 0);
    used_power = (used_power + max_vote_denom - 1) / max_vote_denom;
    FC_ASSERT(used_power <= current_power, "Account does not have enough power to vote.");
    int64_t abs_rshares = ((uint128_t(voter.vesting_shares.amount.value) * used_power) / (WLS_100_PERCENT)).to_uint64();

#ifndef IS_TEST_NET
    if (_db.has_hardfork(WLS_HARDFORK_0_1)) {
      // downvote or self-rewarding on comments cost 3x more MANA
      if ((o.weight < 0) || ((comment.parent_author != WLS_ROOT_POST_PARENT) && (o.voter == o.author) && (o.weight > 0))) {
        abs_rshares = (abs_rshares / 3);
      }
    } else {
      if (o.weight < 0) abs_rshares = (abs_rshares / 3);  // downvote cost 3x more
    }
#endif

    FC_ASSERT(abs_rshares > WLS_VOTE_DUST_THRESHOLD || o.weight == 0, "Voting weight is too small, please accumulate more WHALESTAKE.");

    // Lazily delete vote
    if (itr != comment_vote_idx.end() && itr->num_changes == -1) {
      FC_ASSERT(false, "Cannot vote again on a comment after payout.");
      _db.remove(*itr);
      itr = comment_vote_idx.end();
    }

    if (itr == comment_vote_idx.end()) {
      FC_ASSERT(o.weight != 0, "Vote weight cannot be 0.");
      int64_t rshares = o.weight < 0 ? -abs_rshares : abs_rshares; /// this is the rshares voting for or against the post
      if (rshares > 0) {
        FC_ASSERT(_db.head_block_time() < comment.cashout_time - WLS_UPVOTE_LOCKOUT, "Cannot increase payout within last twelve hours before payout.");
      }

      _db.modify(voter, [&](account_object& a) {
        a.voting_power = current_power - used_power;
        a.last_vote_time = _db.head_block_time();
      });

      /// if the current net_rshares is less than 0, the post is getting 0 rewards so it is not factored into total rshares^2
      fc::uint128_t old_rshares = std::max(comment.net_rshares.value, int64_t(0));
      const auto& root = _db.get(comment.root_comment);
      fc::uint128_t avg_cashout_sec;
      FC_ASSERT(abs_rshares > 0, "Cannot vote with 0 rshares.");
      auto old_vote_rshares = comment.vote_rshares;

      _db.modify(comment, [&](comment_object& c) {
        c.net_rshares += rshares;
        c.abs_rshares += abs_rshares;
        if (rshares > 0) c.vote_rshares += rshares;
        if (rshares > 0)
          c.net_votes++;
        else
          c.net_votes--;
      });

      _db.modify(root, [&](comment_object& c) { c.children_abs_rshares += abs_rshares; });
      fc::uint128_t new_rshares = std::max(comment.net_rshares.value, int64_t(0));

      /// calculate rshares2 value
      new_rshares = util::evaluate_reward_curve(new_rshares);
      old_rshares = util::evaluate_reward_curve(old_rshares);
      uint64_t max_vote_weight = 0;

      /** this verifies uniqueness of voter
       *
       *  cv.weight / c.total_vote_weight ==> % of rshares increase that is accounted for by the vote
       *
       *  W(R) = B * R / ( R + 2S )
       *  W(R) is bounded above by B. B is fixed at 2^64 - 1, so all weights fit in a 64 bit integer.
       *
       *  The equation for an individual vote is:
       *    W(R_N) - W(R_N-1), which is the delta increase of proportional weight
       *
       *  c.total_vote_weight =
       *    W(R_1) - W(R_0) +
       *    W(R_2) - W(R_1) + ...
       *    W(R_N) - W(R_N-1) = W(R_N) - W(R_0)
       *
       *  Since W(R_0) = 0, c.total_vote_weight is also bounded above by B and will always fit in a 64 bit integer.
       *
       **/
      _db.create<comment_vote_object>([&](comment_vote_object& cv) {
        cv.voter = voter.id;
        cv.comment = comment.id;
        cv.rshares = rshares;
        cv.vote_percent = o.weight;
        cv.last_update = _db.head_block_time();

        bool curation_reward_eligible = rshares > 0 && (comment.last_payout == fc::time_point_sec()) && comment.allow_curation_rewards;
        if (curation_reward_eligible) curation_reward_eligible = _db.get_curation_rewards_percent(comment) > 0;
        if (curation_reward_eligible) {
          const auto& reward_fund = _db.get_reward_fund(comment);
          auto curve = reward_fund.curation_reward_curve;
          uint64_t old_weight = util::evaluate_reward_curve(old_vote_rshares.value, curve, reward_fund.content_constant).to_uint64();
          uint64_t new_weight = util::evaluate_reward_curve(comment.vote_rshares.value, curve, reward_fund.content_constant).to_uint64();
          cv.weight = new_weight - old_weight;
          max_vote_weight = cv.weight;

          /// start enforcing this prior to the hardfork, discount weight by time
          uint128_t w(max_vote_weight);
          uint64_t delta_t = std::min(uint64_t((cv.last_update - comment.created).to_seconds()), uint64_t(WLS_REVERSE_AUCTION_WINDOW_SECONDS));

          w *= delta_t;
          w /= WLS_REVERSE_AUCTION_WINDOW_SECONDS;
          cv.weight = w.to_uint64();
        } else {
          cv.weight = 0;
        }
      });

      if (max_vote_weight) _db.modify(comment, [&](comment_object& c) { c.total_vote_weight += max_vote_weight; });  // Optimization
    } else {
      FC_ASSERT(itr->num_changes < WLS_MAX_VOTE_CHANGES, "Voter has used the maximum number of vote changes on this comment.");
      FC_ASSERT(itr->vote_percent != o.weight, "You have already voted in a similar way.");
      int64_t rshares = o.weight < 0 ? -abs_rshares : abs_rshares; /// this is the rshares voting for or against the post

      if (itr->rshares < rshares)
        FC_ASSERT(_db.head_block_time() < comment.cashout_time - WLS_UPVOTE_LOCKOUT, "Cannot increase payout within last twelve hours before payout.");

      _db.modify(voter, [&](account_object& a) {
        a.voting_power = current_power - used_power;
        a.last_vote_time = _db.head_block_time();
      });

      /// if the current net_rshares is less than 0, the post is getting 0 rewards so it is not factored into total rshares^2
      fc::uint128_t old_rshares = std::max(comment.net_rshares.value, int64_t(0));
      const auto& root = _db.get(comment.root_comment);
      fc::uint128_t avg_cashout_sec;

      _db.modify(comment, [&](comment_object& c) {
        c.net_rshares -= itr->rshares;
        c.net_rshares += rshares;
        c.abs_rshares += abs_rshares;

        /// TODO: figure out how to handle remove a vote (rshares == 0 )
        if (rshares > 0 && itr->rshares < 0)
          c.net_votes += 2;
        else if (rshares > 0 && itr->rshares == 0)
          c.net_votes += 1;
        else if (rshares == 0 && itr->rshares < 0)
          c.net_votes += 1;
        else if (rshares == 0 && itr->rshares > 0)
          c.net_votes -= 1;
        else if (rshares < 0 && itr->rshares == 0)
          c.net_votes -= 1;
        else if (rshares < 0 && itr->rshares > 0)
          c.net_votes -= 2;
      });

      _db.modify(root, [&](comment_object& c) { c.children_abs_rshares += abs_rshares; });
      fc::uint128_t new_rshares = std::max(comment.net_rshares.value, int64_t(0));

      /// calculate rshares2 value
      new_rshares = util::evaluate_reward_curve(new_rshares);
      old_rshares = util::evaluate_reward_curve(old_rshares);

      _db.modify(comment, [&](comment_object& c) { c.total_vote_weight -= itr->weight; });
      _db.modify(*itr, [&](comment_vote_object& cv) {
        cv.rshares = rshares;
        cv.vote_percent = o.weight;
        cv.last_update = _db.head_block_time();
        cv.weight = 0;
        cv.num_changes += 1;
      });
    }
  }
  FC_CAPTURE_AND_RETHROW((o))
}

void custom_evaluator::do_apply(const custom_operation& o) {}

void custom_json_evaluator::do_apply(const custom_json_operation& o) {
  database& d = db();
  std::shared_ptr<custom_operation_interpreter> eval = d.get_custom_json_evaluator(o.id);
  if (!eval) return;

  try {
    eval->apply(o);
  } catch (const fc::exception& e) {
    if (d.is_producing()) throw e;
  } catch (...) {
    elog("Unexpected exception applying custom json evaluator.");
  }
}

void custom_binary_evaluator::do_apply(const custom_binary_operation& o) {
  database& d = db();

  std::shared_ptr<custom_operation_interpreter> eval = d.get_custom_json_evaluator(o.id);
  if (!eval) return;

  try {
    eval->apply(o);
  } catch (const fc::exception& e) {
    if (d.is_producing()) throw e;
  } catch (...) {
    elog("Unexpected exception applying custom json evaluator.");
  }
}

void claim_reward_balance_evaluator::do_apply(const claim_reward_balance_operation& op) {
  const auto& acnt = _db.get_account(op.account);

  FC_ASSERT(op.reward_steem <= acnt.reward_steem_balance, "Cannot claim that much WLS. Claim: ${c} Actual: ${a}",
            ("c", op.reward_steem)("a", acnt.reward_steem_balance));
  FC_ASSERT(op.reward_vests <= acnt.reward_vesting_balance, "Cannot claim that much VESTS. Claim: ${c} Actual: ${a}",
            ("c", op.reward_vests)("a", acnt.reward_vesting_balance));

  //------------------------------------------
  // for liquid WLS: reward_steem must be converted to VESTS when claiming since HF4
  if (op.reward_steem.amount > 0) {
    if (_db.has_hardfork(WLS_HARDFORK_0_4)) {
      _db.adjust_reward_balance(acnt, -op.reward_steem);
      _db.create_vesting(acnt, op.reward_steem);  // move reward to user vests balance
    } else {
      _db.adjust_reward_balance(acnt, -op.reward_steem);
      _db.adjust_balance(acnt, op.reward_steem);
    }
  }

  //------------------------------------------
  // for VESTS
  if (op.reward_vests.amount > 0) {
    const auto& new_acnt = _db.get_account(op.account);

    asset reward_vesting_wls_to_move = asset(0, WLS_SYMBOL);
    if (op.reward_vests == new_acnt.reward_vesting_balance) reward_vesting_wls_to_move = new_acnt.reward_vesting_steem;
    else reward_vesting_wls_to_move = asset(((uint128_t(op.reward_vests.amount.value) * uint128_t(new_acnt.reward_vesting_steem.amount.value)) / uint128_t(new_acnt.reward_vesting_balance.amount.value)).to_uint64(), WLS_SYMBOL);

    _db.modify(new_acnt, [&](account_object& a) {
      a.vesting_shares += op.reward_vests;
      a.reward_vesting_balance -= op.reward_vests;
      a.reward_vesting_steem -= reward_vesting_wls_to_move;
    });
    _db.modify(_db.get_dynamic_global_properties(), [&](dynamic_global_property_object& gpo) {
      gpo.total_vesting_shares += op.reward_vests;
      gpo.total_vesting_fund_steem += reward_vesting_wls_to_move;
      gpo.pending_rewarded_vesting_shares -= op.reward_vests;
      gpo.pending_rewarded_vesting_steem -= reward_vesting_wls_to_move;
    });

    _db.adjust_proxied_witness_votes(new_acnt, op.reward_vests.amount);
    if (_db.has_hardfork(WLS_HARDFORK_0_3)) _db.update_account_vr_snaphost(acnt.name); // update account vesting_reward info
  }
}

/**
 * escrow (WLS_ESCROW_POD_NAME) holds pod join fee
 * - user creates request to join => send WLS to escrow
 * - user cancel => send back WLS from escrow to user
 * - pod owner accepts request => send WLS from escrow to pod owner
 * - pod owner rejects request => send back WLS from escrow to user
 */
struct pod_action_apply_visitor {
  pod_action_apply_visitor(const account_object& a, const pod_object& c, database& db) : _a(a), _c(c), _db(db) {}

  typedef void result_type;

  const account_object& _a;
  const pod_object& _c;
  database& _db;

  void operator()(const pod_action_join_request& ca) const {
    FC_ASSERT(_a.pod_count <= WLS_MAX_POD_MEMBER_PER_ACCOUNT, "account reached max pods.");
    FC_ASSERT(_c.allow_join, "pod does not accept new member.");
    FC_ASSERT(_c.name != _a.name, "not allowed creator to make a join request to own pod.");
    FC_ASSERT(_c.member_count <= WLS_POD_MAX_MEMBER_COUNT, "pod reached max members.", ("max", WLS_POD_MAX_MEMBER_COUNT));

    // check user not member of this pod
    const auto *pmember = _db.find_pod_member(_c.name, _a.name);
    FC_ASSERT(pmember == nullptr, "this account is already member of this pod");

    // validate fee
    auto min_request_fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
    auto total_fee = ca.fee + ca.join_fee;

    FC_ASSERT(ca.fee >= min_request_fee, "fee >= min request fee (${min_request_fee})", ("min_request_fee", min_request_fee));
    FC_ASSERT(ca.join_fee >= _c.fee, "join_fee >= pod fee", ("join_fee", ca.join_fee)("fee", _c.fee));
    FC_ASSERT(_a.balance >= total_fee, "Insufficient fee.", ("balance", _a.balance)("total_fee", total_fee)("request_fee", ca.fee)("join_fee", ca.join_fee));

    // create request object
    const auto &join_obj = _db.create<pod_join_request_object>([&](pod_join_request_object &obj) {
      obj.account = _a.name;
      obj.pod = _c.name;
      obj.created = _db.head_block_time();
      obj.fee = ca.join_fee;
      from_string(obj.memo, ca.memo);
    });

    // transfer the pod join fee to escrow if fee > 0
    if (ca.join_fee.amount > 0) {
      const auto &escrow_pod = _db.get_escrow_pod();
      _db.modify(escrow_pod, [&](escrow_object& e) { e.balance += join_obj.fee; });
      _db.adjust_balance(_a, -join_obj.fee);
      _db.push_virtual_operation(pod_virtual_operation(pod_escrow_transfer_virtual_action(join_obj.account, join_obj.pod, join_obj.fee)));
    }

    // transfer the fee (the request fee to avoid spam, not the pod join fee) to WLS_DEV_FUND_ACC_NAME
    _db.adjust_balance(_a, -ca.fee);
    _db.adjust_balance(_db.get_account(WLS_DEV_FUND_ACC_NAME), ca.fee);
  }

  void operator()(const pod_action_cancel_join_request& ca) const {
    const auto &join_obj = _db.get_pod_join_request(_c.name, _a.name);

    // transfer the pod join fee back from escrow if fee > 0
    if (join_obj.fee.amount > 0) {
      const auto &escrow_pod = _db.get_escrow_pod();
      _db.modify(escrow_pod, [&](escrow_object& e) { e.balance -= join_obj.fee; });
      _db.adjust_balance(_a, join_obj.fee);
      _db.push_virtual_operation(pod_virtual_operation(pod_escrow_release_virtual_action(join_obj.account, join_obj.pod, join_obj.fee)));
    }
    _db.remove(join_obj); // delete the request from index
  }

  void operator()(const pod_action_accept_join_request& ca) const {
    FC_ASSERT(_a.pod_count <= WLS_MAX_POD_MEMBER_PER_ACCOUNT, "account reached max pods.");
    FC_ASSERT(_c.name == _a.name, "only pod owner can accept the join request.");
    FC_ASSERT(_c.member_count <= WLS_POD_MAX_MEMBER_COUNT, "pod reached max members.", ("max", WLS_POD_MAX_MEMBER_COUNT));

    const auto &join_obj = _db.get_pod_join_request(_c.name, ca.account);

    // transfer the pod join fee to pod owner from escrow if fee > 0
    if (join_obj.fee.amount > 0) {
      const auto &escrow_pod = _db.get_escrow_pod();
      _db.modify(escrow_pod, [&](escrow_object& e) { e.balance -= join_obj.fee; });
      _db.adjust_balance(_a, join_obj.fee);
      _db.push_virtual_operation(pod_virtual_operation(pod_escrow_release_virtual_action(join_obj.pod, join_obj.pod, join_obj.fee)));
    }

    // add account to pod members
    _db.create<pod_member_object>([&](pod_member_object &obj) {
      obj.pod = _c.name;
      obj.account = join_obj.account;
      obj.joined = _db.head_block_time();
      obj.total_posts = 0;
      obj.total_comments = 0;
    });
    _db.remove(join_obj);                                                       // delete the request from index
    _db.modify(_c, [&](pod_object& c) { c.member_count += 1; });       // update member_count

    const auto& new_member_acc = _db.get_account(ca.account);
    _db.modify(new_member_acc, [&](account_object& acc) { acc.pod_count += 1; });  // update account pod_count
  }

  void operator()(const pod_action_reject_join_request& ca) const {
    FC_ASSERT(_c.name == _a.name, "only pod owner can reject the join request.");
    const auto &join_obj = _db.get_pod_join_request(_c.name, ca.account);

    // transfer the pod join fee back to user from escrow if fee > 0
    if (join_obj.fee.amount > 0) {
      const auto &escrow_pod = _db.get_escrow_pod();
      _db.modify(escrow_pod, [&](escrow_object& e) { e.balance -= join_obj.fee; });
      _db.adjust_balance(_db.get_account(join_obj.account), join_obj.fee);
      _db.push_virtual_operation(pod_virtual_operation(pod_escrow_release_virtual_action(join_obj.account, join_obj.pod, join_obj.fee)));
    }
    _db.remove(join_obj); // delete the request from index
  }

  void operator()(const pod_action_leave& ca) const {
    FC_ASSERT(_c.name != _a.name, "not allowed creator to leave pod.");
    // remove from pod members
    const auto &member_obj = _db.get_pod_member(_c.name, _a.name);
    _db.remove(member_obj);
    _db.modify(_c, [&](pod_object& c) { c.member_count -= 1; });       // update member_count
    _db.modify(_a, [&](account_object& acc) { acc.pod_count -= 1; });  // update account pod_count
  }

  void operator()(const pod_action_update& ca) const {
    FC_ASSERT(_c.name == _a.name, "only pod owner can update.");

    if (ca.join_fee) {
      FC_ASSERT(ca.join_fee->amount <= WLS_POD_MAX_JOIN_FEE, "join_fee can not be greater than max ${max}", ("max", asset(WLS_POD_MAX_JOIN_FEE, WLS_SYMBOL)));
    }

    _db.modify(_c, [&](pod_object& obj) {
      if (ca.join_fee) obj.fee = *ca.join_fee;
#ifndef IS_LOW_MEM
      if (ca.json_metadata) from_string(obj.json_metadata, *ca.json_metadata);
#endif
      if (ca.allow_join) obj.allow_join = *ca.allow_join;
    });
  }

  void operator()(const pod_action_kick& ca) const {
    FC_ASSERT(_c.name == _a.name, "only pod owner can kick.");
    FC_ASSERT(ca.account != _c.name, "not able to kick pod owner.");

    const auto& account_kicked = _db.get_account(ca.account);
    const auto &member_obj = _db.get_pod_member(_c.name, ca.account);

    _db.remove(member_obj);
    _db.modify(_c, [&](pod_object& c) { c.member_count -= 1; });       // update member_count
    _db.modify(account_kicked, [&](account_object& acc) { acc.pod_count -= 1; });  // update account pod_count
  }
};

void pod_action_evaluator::do_apply(const pod_action_operation& o) {
#ifndef IS_TEST_NET
  FC_ASSERT(_db.has_hardfork(WLS_HARDFORK_0_2), "This operation is not active.");
#endif

  // validating special accounts
  FC_ASSERT(o.account != WLS_TEMP_ACCOUNT, "temp account is not allowed.");
  FC_ASSERT(o.account != WLS_NULL_ACCOUNT, "null account is not allowed.");
  const auto& account = _db.get_account(o.account);  // account create this action
  const auto& pod = _db.get_pod(o.pod);              // on which pod
  o.action.visit(pod_action_apply_visitor(account, pod, _db));
}

struct friend_action_apply_visitor {
  friend_action_apply_visitor(const account_object& from, const account_object& to, database& db) : _from(from), _to(to), _db(db) {}

  typedef void result_type;

  const account_object& _from;
  const account_object& _to;
  database& _db;

  void operator()(const friend_action_send_request& fa) const {
    FC_ASSERT(_from.friend_count <= WLS_MAX_FRIEND_PER_ACCOUNT, "account reached max friend.");
    FC_ASSERT(_to.friend_count <= WLS_MAX_FRIEND_PER_ACCOUNT, "account reached max friend.");
    FC_ASSERT(_from.name != _to.name, "requesting to you're own account is not allowed.");

    // check to make sure not a friend yet
    const auto *pfriend = _db.find_friend(_from.name, _to.name);
    FC_ASSERT(pfriend == nullptr, "this account is friend already.");

    // check to make sure there is no existing request from another account
    const auto *pfr_another = _db.find_friend(_to.name, _from.name);
    FC_ASSERT(pfr_another == nullptr, "there is a friend request already.");

    // create friend request object
    _db.create<friend_request_object>([&](friend_request_object &obj) {
      obj.account = _from.name;
      obj.another = _to.name;
      obj.created = _db.head_block_time();
      from_string(obj.memo, fa.memo);
    });
  }

  void operator()(const friend_action_cancel_request& fa) const {
    const auto &fr_obj = _db.get_friend_request(_from.name, _to.name);
    _db.remove(fr_obj); // delete the request from index
  }

  void operator()(const friend_action_accept_request& fa) const {
    FC_ASSERT(_from.friend_count <= WLS_MAX_FRIEND_PER_ACCOUNT, "account reached max friend.");
    FC_ASSERT(_to.friend_count <= WLS_MAX_FRIEND_PER_ACCOUNT, "account reached max friend.");

    const auto &fr_obj = _db.get_friend_request(_to.name, _from.name);

    // add account to friend, add 2 objects AB and BA
    _db.create<friend_object>([&](friend_object &obj) {
      obj.account = _from.name;
      obj.another = _to.name;
    });
    _db.create<friend_object>([&](friend_object &obj) {
      obj.account = _to.name;
      obj.another = _from.name;
    });

    _db.remove(fr_obj); // delete the request from index

    // update friend_count for both 2 accounts
    _db.modify(_from, [&](account_object& obj) { obj.friend_count += 1; });
    _db.modify(_to, [&](account_object& obj) { obj.friend_count += 1; });
  }

  void operator()(const friend_action_reject_request& fa) const {
    const auto &fr_obj = _db.get_friend_request(_to.name, _from.name);

    // delete the request from index
    _db.remove(fr_obj);
  }

  void operator()(const friend_action_unfriend& fa) const {
    // remove from index
    const auto &f_obj_ab = _db.get_friend(_from.name, _to.name);
    const auto &f_obj_ba = _db.get_friend(_to.name, _from.name);
    _db.remove(f_obj_ab);
    _db.remove(f_obj_ba);

    // update friend_count for both 2 accounts
    _db.modify(_from, [&](account_object& obj) { obj.friend_count -= 1; });
    _db.modify(_to, [&](account_object& obj) { obj.friend_count -= 1; });
  }
};

void friend_action_evaluator::do_apply(const friend_action_operation& o) {
#ifndef IS_TEST_NET
  FC_ASSERT(_db.has_hardfork(WLS_HARDFORK_0_2), "This operation is not active.");
#endif

  // validating special accounts
  FC_ASSERT(o.account != WLS_TEMP_ACCOUNT, "temp account is not allowed.");
  FC_ASSERT(o.account != WLS_NULL_ACCOUNT, "null account is not allowed.");
  FC_ASSERT(o.another != WLS_TEMP_ACCOUNT, "temp account is not allowed.");
  FC_ASSERT(o.another != WLS_NULL_ACCOUNT, "null account is not allowed.");
  FC_ASSERT(o.account != o.another, "action can not be on the same account.");
  const auto& account = _db.get_account(o.account);  // account create this action
  const auto& another = _db.get_account(o.another);  // another account
  o.action.visit(friend_action_apply_visitor(account, another, _db));
}

struct account_action_apply_visitor {
  account_action_apply_visitor(const account_object& a, database& db) : _a(a), _db(db) {}

  typedef void result_type;

  const account_object& _a;
  database& _db;

  void operator()(const account_action_create_pod& aa) const {
    // validating special accounts
    FC_ASSERT(_a.name != WLS_TEMP_ACCOUNT, "temp account is not allowed.");
    FC_ASSERT(_a.name != WLS_NULL_ACCOUNT, "null account is not allowed.");

    const auto& props = _db.get_dynamic_global_properties();

    if (aa.join_fee)
      FC_ASSERT(aa.join_fee->amount <= WLS_POD_MAX_JOIN_FEE, "join_fee can not be greater than max ${max}", ("max", asset(WLS_POD_MAX_JOIN_FEE, WLS_SYMBOL)));


    auto min_fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
    FC_ASSERT(aa.fee >= min_fee, "fee >= min fee (${min_fee})", ("min_fee", min_fee));
    FC_ASSERT(_a.balance >= aa.fee, "Insufficient fee to create pod.", ("account.balance", _a.balance)("fee", aa.fee));

    _db.adjust_balance(_a, -aa.fee);
    _db.adjust_balance(_db.get_account(WLS_DEV_FUND_ACC_NAME), aa.fee);

    _db.create<pod_object>([&](pod_object& obj) {
      obj.name = _a.name;
      if (aa.join_fee) obj.fee = *aa.join_fee;
      if (aa.allow_join) obj.allow_join = *aa.allow_join;
      obj.created = props.time;
#ifndef IS_LOW_MEM
      if (aa.json_metadata) from_string(obj.json_metadata, *aa.json_metadata);
#endif
      obj.member_count += 1; // update member_count, creator is the 1st member
    });

    // add creator to pod members
    _db.create<pod_member_object>([&](pod_member_object &obj) {
      obj.pod = _a.name;
      obj.account = _a.name;
      obj.joined = _db.head_block_time();
      obj.total_posts = 0;
      obj.total_comments = 0;
    });

    _db.modify(_a, [&](account_object& acc) { acc.pod_count += 1; });  // update account pod_count to creator
  }

  void operator()(const account_action_transfer_to_tip& aa) const {
    try {
      FC_ASSERT(_db.has_hardfork(WLS_HARDFORK_0_4), "This operation is not active."); // TODO: remove this after HF4 applied
      FC_ASSERT(_a.balance >= aa.amount, "Account does not have sufficient funds for transfer.");

      _db.modify(_a, [&](account_object& acc) {
        acc.balance -= aa.amount;
        acc.reward_steem_balance += aa.amount;
      });
    }
    FC_CAPTURE_AND_RETHROW((aa))
  }

  void operator()(const account_action_htlc_create& aa) const {
    try {
      FC_ASSERT(_db.has_hardfork(WLS_HARDFORK_0_4), "This operation is not active."); // TODO: remove this after HF4 applied

      _db.get_account(aa.to); // verify to account exists
      const auto &escrow_htlc = _db.get_escrow_htlc();
      auto total_amount = aa.amount + aa.reward;

      FC_ASSERT(_a.balance >= (aa.fee + aa.reward + aa.amount), "Insufficient Fund");

      if (aa.fee.amount > 0) _db.pay_fee(_db.get_account(_a.name), aa.fee); // burn the fee

      _db.adjust_balance(_db.get_account(_a.name), -total_amount);
      _db.modify(escrow_htlc, [&](escrow_object& e) {
        e.balance += total_amount;
        e.last_update = _db.head_block_time();
      });

      _db.create<htlc_object>([&](htlc_object& ho) {
        std::stringstream store;
        store << string(_a.name) << string(aa.to) << aa.preimage_hash.get<htlc_sha256>().htlc_hash.str();
        ho.refid = fc::ripemd160::hash( store.str() );

        ho.from = _a.name;
        ho.to = aa.to;
        ho.amount = aa.amount;
        ho.reward = aa.reward;
        ho.preimage_hash = aa.preimage_hash;
        ho.preimage_size = aa.preimage_size;
        ho.expiration = _db.head_block_time() + fc::seconds(aa.expiration);
      });
    }
    FC_CAPTURE_AND_RETHROW((aa))
  }

  void operator()(const account_action_htlc_update& aa) const {
    try {
      FC_ASSERT(_db.has_hardfork(WLS_HARDFORK_0_4), "This operation is not active."); // TODO: remove this after HF4 applied

      const auto& htlc = _db.get<htlc_object, by_refid>(aa.htlc_id);
      const auto &escrow_htlc = _db.get_escrow_htlc();
      auto total_amount = aa.fee + aa.reward;

      FC_ASSERT(_a.name == htlc.from, "HTLC may only be extended by its creator.");
      FC_ASSERT(htlc.expiration.sec_since_epoch() + static_cast<uint64_t>(aa.seconds) < fc::time_point_sec::maximum().sec_since_epoch(),
                "Extension would cause an invalid date");
      FC_ASSERT(htlc.expiration + fc::seconds(aa.seconds) <= _db.head_block_time() + fc::seconds(60 * 60 * 24 * 30), "Extension pushes contract too far into the future");
      FC_ASSERT(_a.balance >= total_amount, "Insufficient Fund");
      if (aa.fee.amount > 0) _db.pay_fee(_db.get_account(_a.name), aa.fee); // burn the fee

      _db.adjust_balance(_db.get_account(_a.name), -total_amount);
      _db.modify(escrow_htlc, [&](escrow_object& e) {
        e.balance += aa.reward;
        e.last_update = _db.head_block_time();
      });

      _db.modify(htlc, [&](htlc_object& ho) {
        ho.expiration += fc::seconds(aa.seconds);
        ho.reward += aa.reward;
      });
    }
    FC_CAPTURE_AND_RETHROW((aa))
  }

  void operator()(const account_action_htlc_redeem& aa) const {
    try {
      FC_ASSERT(_db.has_hardfork(WLS_HARDFORK_0_4), "This operation is not active."); // TODO: remove this after HF4 applied

      const auto& htlc = _db.get<htlc_object, by_refid>(aa.htlc_id);
      const auto &escrow_htlc = _db.get_escrow_htlc();
      auto total_amount = htlc.amount + htlc.reward;

      // FC_ASSERT(_a.name == htlc.to, "Only recipient can redeem."); // disable this to allow 3rt party to redeem
      FC_ASSERT(htlc.expiration > _db.head_block_time(), "This HTLC is expired.");
      if (htlc.preimage_size > 0) FC_ASSERT(aa.preimage.size() == htlc.preimage_size, "Preimage size mismatch.");

      const auto& redeem_hash = fc::sha256::hash((const char*)aa.preimage.data(), (uint32_t)aa.preimage.size());
      FC_ASSERT(redeem_hash == htlc.preimage_hash.get<htlc_sha256>().htlc_hash, "Provided preimage does not generate correct hash.");
      FC_ASSERT(_a.balance >= aa.fee, "Insufficient Fund");
      FC_ASSERT(escrow_htlc.balance >= total_amount, "Insufficient Fund in HTLC locked fund.");

      if (aa.fee.amount > 0) _db.pay_fee(_db.get_account(_a.name), aa.fee); // burn the fee
      if (htlc.reward.amount > 0) _db.adjust_balance(_db.get_account(_a.name), htlc.reward); // paying reward to redeemer in liquid WLS

      _db.adjust_balance(_db.get_account(htlc.to), htlc.amount);
      _db.modify(escrow_htlc, [&](escrow_object& e) {
        e.balance -= total_amount;
        e.last_update = _db.head_block_time();
      });

      // virtual op, notify related parties
      _db.push_virtual_operation(htlc_virtual_operation(htlc_redeemed_virtual_action(htlc.refid, htlc.from, htlc.to, _a.name, htlc.amount)));

      _db.remove(htlc);
    }
    FC_CAPTURE_AND_RETHROW((aa))
  }
};

void account_action_evaluator::do_apply(const account_action_operation& o) {
#ifndef IS_TEST_NET
  FC_ASSERT(_db.has_hardfork(WLS_HARDFORK_0_2), "This operation is not active.");
#endif

  // validating special accounts
  FC_ASSERT(o.account != WLS_TEMP_ACCOUNT, "temp account is not allowed.");
  FC_ASSERT(o.account != WLS_NULL_ACCOUNT, "null account is not allowed.");
  const auto& account = _db.get_account(o.account);
  o.action.visit(account_action_apply_visitor(account, _db));
}

struct social_action_apply_visitor {
  social_action_apply_visitor(const account_object& a, database& db) : _a(a), _db(db) {}

  typedef void result_type;

  const account_object& _a;
  database& _db;

  void operator()(const social_action_comment_create& sa) const {
    try {
      // make sure comment not exist
      {
        const auto& by_permlink_idx = _db.get_index<comment_index>().indices().get<by_permlink>();
        auto itr = by_permlink_idx.find(boost::make_tuple(_a.name, sa.permlink));
        FC_ASSERT(itr == by_permlink_idx.end(), "can not create duplicated comment.");
      }

      auto now = _db.head_block_time();
      const comment_object* parent = nullptr;

      // default
      pod_name_type pod;
      asset max_accepted_payout = asset(1000000000, WLS_SYMBOL);
      bool allow_replies = true;
      bool allow_votes = true;
      bool allow_curation_rewards = true;
      bool allow_friends = false; // false: everyone, true: only friends

      bool is_pod_post_count = false; // if this post is count for pod ranking
      bool is_pod_comment_count = false; // if this comment is count for pod ranking
	FC_UNUSED(is_pod_post_count,is_pod_comment_count);
      if (sa.parent_author == WLS_ROOT_POST_PARENT) { // this is root post
        FC_ASSERT((now - _a.last_root_post) > WLS_MIN_ROOT_COMMENT_INTERVAL, "You may only post once every 5 minutes.");
        if (sa.pod) {
          const auto &pod_obj = _db.find_pod(*sa.pod);
          FC_ASSERT(pod_obj != nullptr, "pod \"${pod}\" must exist.", ("pod", sa.pod));
          pod = *sa.pod;
          // make sure author is member of this pod
          const auto *pmember = _db.find_pod_member(pod, _a.name);
          FC_ASSERT(pmember != nullptr, "author is not a member of this pod", ("pod", pod));
          is_pod_post_count = true;
        }
        if (sa.allow_friends) allow_friends = *sa.allow_friends;
      } else { // this is NOT a root post
        parent = &_db.get_comment(sa.parent_author, sa.parent_permlink);
        const auto& the_root_post = _db.get(parent->root_comment);

        // FC_ASSERT(parent->depth < WLS_MAX_COMMENT_DEPTH, "Comment is nested maximum depth.");
        FC_ASSERT(parent->depth < WLS_SOFT_MAX_COMMENT_DEPTH, "Comment is nested maximum depth.");
        FC_ASSERT(the_root_post.allow_replies, "The parent comment has disabled replies.");
        if (_db.has_hardfork(WLS_HARDFORK_0_4)) {
          FC_ASSERT(the_root_post.active < _db.head_block_time() + fc::days(30), "The comment is archived after 30 days inactive");
        } else {
          FC_ASSERT(the_root_post.cashout_time != fc::time_point_sec::maximum(), "The comment is archived");
        }
        FC_ASSERT((now - _a.last_post) > WLS_MIN_REPLY_INTERVAL, "You may only comment once every 20 seconds.");
        if (sa.pod) FC_ASSERT(sa.pod != WLS_ROOT_POST_PARENT, "Not allowed to set pod option in non-root comment");
        pod = the_root_post.pod; // copy pod value from root post
        FC_ASSERT(!sa.allow_friends, "allow_friends option is not allowed in non root post");

        {
          /**
           * Need to check user allowed to interact (comment or vote) here.
           * - If the root-comment having pod specified, only pod members can interact
           * - if the root-comment having no pod specified and
           *     + and allow_friends is false: anyone can interact
           *     + and allow_friends is true: only friends can interact
           */
          if (pod != WLS_ROOT_POST_PARENT) {
            // If the root-comment having pod specified, only pod members can interact
            // check this author is in pod or not
            const auto *pmember = _db.find_pod_member(pod, _a.name);
            FC_ASSERT(pmember != nullptr, "only member of this pod can interact with the post", ("pod", pod));
            is_pod_comment_count = true;
          } else {
            /**
             * if the root-comment having no pod specified and
             *   + and allow_friends is false: anyone can interact
             *   + and allow_friends is true: only friends can interact
             */

            if (_db.has_hardfork(WLS_HARDFORK_0_4)) {
              /**
               * fix issue that non-friend can comment on a nested post.
               * Eg.,
               * A and B a friend, B and C are friend.
               * A creates a post P1 (root-post)
               * B add a comment C1 to P1
               * C add a comment C2 to C1 <== this! (C should not be allowed to add comment if C is not A friend).
               */

              if (the_root_post.allow_friends) {
                if (_a.name != the_root_post.author) {  // to make sure author can interact to his/her post
                  const auto* pfriend = _db.find_friend(the_root_post.author, _a.name);
                  FC_ASSERT(pfriend != nullptr, "only friends of author can interact with the post.", ("author", parent->author));
                }
              }
            } else {
              if (parent->allow_friends) {
                if (_a.name != parent->author) {  // to make sure author can interact to his/her post
                  const auto* pfriend = _db.find_friend(parent->author, _a.name);
                  FC_ASSERT(pfriend != nullptr, "only friends of author can interact with the post.", ("author", parent->author));
                }
              }
            }
          }
        }
      }

      if (sa.json_metadata.size()) FC_ASSERT(fc::is_utf8(sa.json_metadata), "JSON Metadata must be UTF-8");
      if (sa.max_accepted_payout) max_accepted_payout = *sa.max_accepted_payout;
      if (sa.allow_replies) allow_replies = *sa.allow_replies;
      if (sa.allow_votes) allow_votes = *sa.allow_votes;
      if (sa.allow_curation_rewards) allow_curation_rewards = *sa.allow_curation_rewards;

      uint16_t reward_weight = WLS_100_PERCENT;
      uint64_t post_bandwidth = _a.post_bandwidth;

      _db.modify(_a, [&](account_object& a) {
        if (sa.parent_author == WLS_ROOT_POST_PARENT) {
          a.last_root_post = now;
          a.post_bandwidth = uint32_t(post_bandwidth);
        }
        a.last_post = now;
        a.post_count++;
      });

#ifndef IS_LOW_MEM
      if (is_pod_post_count) {
        _db.modify(_db.get_pod_member(pod, _a.name), [&](pod_member_object& p) {
          p.total_posts++;
        });
      } else if (is_pod_comment_count) {
        _db.modify(_db.get_pod_member(pod, _a.name), [&](pod_member_object& p) {
          p.total_comments++;
        });
      }
#endif

#ifdef IS_TEST_NET
      uint64_t cashout_window = uint64_t(WLS_CASHOUT_WINDOW_SECONDS);
#else
      uint64_t cashout_window = _db.has_hardfork(WLS_HARDFORK_0_1) ? uint64_t(WLS_HARDFORK_0_1_CASHOUT_WINDOW_SECONDS) : uint64_t(WLS_CASHOUT_WINDOW_SECONDS);
#endif

      _db.create<comment_object>([&](comment_object& com) {
        validate_permlink_0_1(sa.parent_permlink);
        validate_permlink_0_1(sa.permlink);
        com.author = _a.name;
        from_string(com.permlink, sa.permlink);
        com.last_update = _db.head_block_time();
        com.created = com.last_update;
        com.active = com.last_update;
        com.last_payout = fc::time_point_sec::min();
        com.max_cashout_time = fc::time_point_sec::maximum();
        com.reward_weight = reward_weight;
        com.cashout_time = com.created + cashout_window;
        //-- options
        com.pod = pod;

        com.max_accepted_payout = max_accepted_payout;
        com.allow_replies = allow_replies;
        com.allow_votes = allow_votes;
        com.allow_curation_rewards = allow_curation_rewards;
        com.allow_friends = allow_friends;

        if (sa.parent_author == WLS_ROOT_POST_PARENT) {
          com.parent_author = "";
          from_string(com.parent_permlink, sa.parent_permlink);
          from_string(com.category, sa.parent_permlink);
          com.root_comment = com.id;
        } else {
          com.parent_author = parent->author;
          com.parent_permlink = parent->permlink;
          com.depth = parent->depth + 1;
          com.category = parent->category;
          com.root_comment = parent->root_comment;
        }

#ifndef IS_LOW_MEM
        from_string(com.title, sa.title);
        from_string(com.body, sa.body);
        if (fc::is_utf8(sa.json_metadata))
          from_string(com.json_metadata, sa.json_metadata);
        else
          wlog("Comment ${a}/${p} contains invalid UTF-8 metadata", ("a", _a.name)("p", sa.permlink));
#endif
      });

      /// this loop can be skipped for validate-only nodes as it is merely gathering stats for indices
      while (parent) {
        _db.modify(*parent, [&](comment_object& p) {
          p.children++;
          p.active = now;
        });
#ifndef IS_LOW_MEM
        if (parent->parent_author != WLS_ROOT_POST_PARENT)
          parent = &_db.get_comment(parent->parent_author, parent->parent_permlink);
        else
#endif
        parent = nullptr;
      }
    }
    FC_CAPTURE_AND_RETHROW((sa))
  }

  void operator()(const social_action_comment_update& sa) const {
    try {
      const auto& by_permlink_idx = _db.get_index<comment_index>().indices().get<by_permlink>();
      auto itr = by_permlink_idx.find(boost::make_tuple(_a.name, sa.permlink));
      FC_ASSERT(itr != by_permlink_idx.end(), "comment not found");
      const auto& comment = *itr;

      if (_db.has_hardfork(WLS_HARDFORK_0_4)) {
        if (comment.parent_author == WLS_ROOT_POST_PARENT) { // this is root post
          FC_ASSERT(comment.active < _db.head_block_time() + fc::days(30), "The comment is archived after 30 days inactive");
        } else {
          const auto& the_root_post = _db.get(comment.root_comment);
          FC_ASSERT(the_root_post.active < _db.head_block_time() + fc::days(30), "The comment is archived after 30 days inactive");
        }
      } else {
        FC_ASSERT(comment.cashout_time != fc::time_point_sec::maximum(), "The comment is archived");
      }

      _db.modify(comment, [&](comment_object& com) {
        com.last_update = _db.head_block_time();
        com.active = com.last_update;

#ifndef IS_LOW_MEM
        if (sa.title) if (sa.title->size()) from_string(com.title, *sa.title);
        if (sa.json_metadata) if (sa.json_metadata->size()) from_string(com.json_metadata, *sa.json_metadata);

        if (sa.body) {
          if (sa.body->size()) {
            try {
              diff_match_patch<std::wstring> dmp;
              auto patch = dmp.patch_fromText(utf8_to_wstring(*sa.body));
              if (patch.size()) {
                auto result = dmp.patch_apply(patch, utf8_to_wstring(to_string(com.body)));
                auto patched_body = wstring_to_utf8(result.first);
                from_string(com.body, patched_body);
              } else {  // replace
                from_string(com.body, *sa.body);
              }
            } catch (...) {
              from_string(com.body, *sa.body);
            }
          }
        }
#endif
      });
    }
    FC_CAPTURE_AND_RETHROW((sa))
  }

  void operator()(const social_action_comment_delete& sa) const {
    FC_ASSERT(false, "Comment can not be deleted, use social_action_comment_update to update the content.");
  }

  void operator()(const social_action_claim_vesting_reward& sa) const {
    try {
#ifdef IS_TEST_NET
      bool testnet = true;
#else
      bool testnet = false;
#endif

      bool has_hf3 = false;
      if (testnet || _db.has_hardfork(WLS_HARDFORK_0_3)) {
        has_hf3 = true;
      }
      FC_ASSERT(has_hf3, "This operation is not enabled yet.");

      const auto& cprops = _db.get_dynamic_global_properties();
      _db.update_account_vr_snaphost(_a.name);    // always update to reset out the claimed amount if needed.
      const auto& acc = _db.get_account(_a.name); // get the new account object in case it is modified after run update_account_vr_snaphost

      FC_ASSERT(cprops.vr_cycle > 0, "Waiting for next cycle.");
      FC_ASSERT(cprops.vr_snaphost_shares.amount.value > 0, "vesting_reward fund is 0.");
      FC_ASSERT(acc.vr_claimed_cycle <= cprops.vr_cycle, "Waiting for next cycle.");

      // figure out using vesting_shares or vr_snaphost_shares
      int64_t account_vshares = 0;
      if (acc.vr_snaphost_next_cycle < cprops.vr_cycle) account_vshares = acc.vesting_shares.amount.value;
      else if (acc.vr_snaphost_cycle < cprops.vr_cycle) account_vshares = acc.vr_snaphost_shares.amount.value;

      // figure out how much user could claim
      uint128_t vest(account_vshares);
      uint128_t total_vest(cprops.vr_snaphost_shares.amount.value);
      uint128_t fund(cprops.vr_snaphost_payout.amount.value);
      uint128_t max_uint128 = vest * fund / total_vest;
      const auto max = max_uint128.to_uint64();
      const auto remain = (acc.vr_claimed_cycle == cprops.vr_cycle) ? int64_t(max) - acc.vr_claimed_amount.amount.value : int64_t(max);

      FC_ASSERT(remain > 0, "Nothing to claim.");
      FC_ASSERT(sa.amount.amount.value <= remain, "Cannot claim that much WLS. Claim: ${c} Actual: ${a}", ("c", sa.amount)("a", asset(remain, WLS_SYMBOL)));

      _db.modify(_db.get_vesting_payout_fund(), [&](escrow_object& e) {
        e.balance -= sa.amount;
        e.last_update = cprops.time;
      });

      bool is_to_other_account = false;
      if (sa.to) if (*sa.to != WLS_ROOT_POST_PARENT) is_to_other_account = true;

      const account_object& to_account = is_to_other_account ? _db.get_account(*sa.to) : acc;
      _db.create_vesting(to_account, sa.amount);                           // move reward to user vests balance
      _db.modify(acc, [&](account_object& a) {
        a.vr_claimed_amount += sa.amount;
        a.vr_claimed_cycle = cprops.vr_cycle;                              // update cycle to avoid double claiming
      });
      _db.update_account_vr_snaphost(acc.name);                            // update account vesting_reward info
      if (is_to_other_account) _db.update_account_vr_snaphost(*sa.to);     // update account vesting_reward info
    }
    FC_CAPTURE_AND_RETHROW((sa))
  }

  /**
   * Most of code is similar to social_action_claim_vesting_reward
   */
  void operator()(const social_action_claim_vesting_reward_tip& sa) const {
    try {
      FC_ASSERT(_db.has_hardfork(WLS_HARDFORK_0_4), "This operation is not active."); // TODO: remove this after HF4 applied

      const auto& cprops = _db.get_dynamic_global_properties();
      _db.update_account_vr_snaphost(_a.name);    // always update to reset out the claimed amount if needed.
      const auto& acc = _db.get_account(_a.name); // get the new account object in case it is modified after run update_account_vr_snaphost

      FC_ASSERT(cprops.vr_cycle > 0, "Waiting for next cycle.");
      FC_ASSERT(cprops.vr_snaphost_shares.amount.value > 0, "vesting_reward fund is 0.");
      FC_ASSERT(acc.vr_claimed_cycle <= cprops.vr_cycle, "Waiting for next cycle.");

      // figure out using vesting_shares or vr_snaphost_shares
      int64_t account_vshares = 0;
      if (acc.vr_snaphost_next_cycle < cprops.vr_cycle) account_vshares = acc.vesting_shares.amount.value;
      else if (acc.vr_snaphost_cycle < cprops.vr_cycle) account_vshares = acc.vr_snaphost_shares.amount.value;

      // figure out how much user could claim
      uint128_t vest(account_vshares);
      uint128_t total_vest(cprops.vr_snaphost_shares.amount.value);
      uint128_t fund(cprops.vr_snaphost_payout.amount.value);
      uint128_t max_uint128 = vest * fund / total_vest;
      const auto max = max_uint128.to_uint64();
      const auto remain = (acc.vr_claimed_cycle == cprops.vr_cycle) ? int64_t(max) - acc.vr_claimed_amount.amount.value : int64_t(max);

      FC_ASSERT(remain > 0, "Nothing to claim.");
      FC_ASSERT(sa.amount.amount.value <= remain, "Cannot claim that much WLS. Claim: ${c} Actual: ${a}", ("c", sa.amount)("a", asset(remain, WLS_SYMBOL)));

      _db.modify(_db.get_vesting_payout_fund(), [&](escrow_object& e) {
        e.balance -= sa.amount;
        e.last_update = cprops.time;
      });

      _db.modify(acc, [&](account_object& a) {
        a.reward_steem_balance += sa.amount;
        a.vr_claimed_amount += sa.amount;
        a.vr_claimed_cycle = cprops.vr_cycle; // update cycle to avoid double claiming
      });
    }
    FC_CAPTURE_AND_RETHROW((sa))
  }

  void operator()(const social_action_user_tip& sa) const {
    try {
      FC_ASSERT(_db.has_hardfork(WLS_HARDFORK_0_4), "This operation is not active."); // TODO: remove this after HF4 applied

      const auto& to_account = sa.to ? ((*sa.to).size() ? _db.get_account(*sa.to) : _a) : _a;

      FC_ASSERT(_a.reward_steem_balance >= sa.amount, "Account does not have sufficient WLS for transfer.");
      _db.modify(_a, [&](account_object& a) {
        a.reward_steem_balance -= sa.amount;
      });

      _db.create_vesting(to_account, sa.amount, true);
    }
    FC_CAPTURE_AND_RETHROW((sa))
  }

  void operator()(const social_action_comment_tip& sa) const {
    try {
      FC_ASSERT(_db.has_hardfork(WLS_HARDFORK_0_4), "This operation is not active."); // TODO: remove this after HF4 applied

      const auto& comment = _db.get_comment(sa.author, sa.permlink);
      const auto& author = _db.get_account(sa.author);

//      { // TODO: for next HF or patch - comment is archived after 30 days inactive
//        if (comment.parent_author == WLS_ROOT_POST_PARENT) {  // this is root post
//          FC_ASSERT(comment.active < _db.head_block_time() + fc::days(30), "The comment is archived after 30 days inactive");
//        } else {  // this is NOT a root post
//          const auto& the_root_post = _db.get(comment.root_comment);
//          FC_ASSERT(the_root_post.active < _db.head_block_time() + fc::days(30), "The comment is archived after 30 days inactive");
//        }
//      }

      { // powerup author
        FC_ASSERT(_a.reward_steem_balance >= sa.amount, "Account does not have sufficient WLS for transfer.");
        _db.modify(_a, [&](account_object& a) {
          a.reward_steem_balance -= sa.amount;
        });
        _db.create_vesting(author, sa.amount, true);
      }

      { // create or update comment_tip_object
        const auto& comment_tip_idx = _db.get_index<comment_tip_index>().indices().get<by_comment_tipper>();
        auto itr = comment_tip_idx.find(std::make_tuple(comment.id, _a.id));

        if (itr == comment_tip_idx.end()) {
          _db.create<comment_tip_object>([&](comment_tip_object& ct) {
            ct.tipper = _a.id;
            ct.comment = comment.id;
            ct.amount = uint64_t(sa.amount.amount.value);
            ct.tips_count = 1;
          });
        } else {
          _db.modify(*itr, [&](comment_tip_object& ct) {
            ct.amount += uint64_t(sa.amount.amount.value); // adding up to the amount if tip this comment multiple times
            if (ct.tips_count < 8) ct.tips_count ++; // dont need to track counting more than 8
          });
        }
      }

      { // update total_payout_value <== used to track total tip value received
#ifndef IS_LOW_MEM
        _db.modify(comment, [&](comment_object& c) {
          c.total_payout_value += sa.amount;
        });
#endif
      }
    }
    FC_CAPTURE_AND_RETHROW((sa))
  }
};

void social_action_evaluator::do_apply(const social_action_operation& o) {
#ifndef IS_TEST_NET
  FC_ASSERT(_db.has_hardfork(WLS_HARDFORK_0_2), "This operation is not active.");
#endif

  // validating special accounts
  FC_ASSERT(o.account != WLS_TEMP_ACCOUNT, "temp account is not allowed.");
  FC_ASSERT(o.account != WLS_NULL_ACCOUNT, "null account is not allowed.");
  const auto& account = _db.get_account(o.account);
  o.action.visit(social_action_apply_visitor(account, _db));
}


}  // namespace chain
}  // namespace wls
