#pragma once

#include <wls/protocol/operation_util.hpp>
#include <wls/protocol/wls_operations.hpp>
#include <wls/protocol/wls_virtual_operations.hpp>

namespace wls {
namespace protocol {

/**
 * NOTE: do not change the order of any operations prior to the virtual operations or it will trigger a hardfork.
 */
// clang-format off
typedef fc::static_variant<
    /*  0 */ vote_operation,
    /*  1 */ comment_operation,

    /*  2 */ transfer_operation,
    /*  3 */ transfer_to_vesting_operation,
    /*  4 */ withdraw_vesting_operation,

    /*  5 */ account_create_operation,
    /*  6 */ account_update_operation,

    /*  7 */ account_action_operation, /// single operation for all account actions so adding more actions later wont break anything
    /*  8 */ social_action_operation, /// single operation for all social actions (using posting key) so adding more actions later wont break anything

    /*  9 */ witness_update_operation,
    /* 10 */ account_witness_vote_operation,
    /* 11 */ account_witness_proxy_operation,

    /* 12 */ custom_operation,

    /* 13 */ delete_comment_operation,
    /* 14 */ custom_json_operation,
    /* 15 */ comment_options_operation,
    /* 16 */ set_withdraw_vesting_route_operation,
    /* 17 */ custom_binary_operation,
    /* 18 */ claim_reward_balance_operation,

    /* 19 */ friend_action_operation,  /// single operation for all pod actions so adding more actions later wont break anything
    /* 20 */ pod_action_operation,     /// single operation for all pod actions so adding more actions later wont break anything

    /// virtual operations below this point
    /* 21 */ author_reward_operation,
    /* 22 */ curation_reward_operation,
    /* 23 */ comment_reward_operation,
    /* 24 */ fill_vesting_withdraw_operation,
    /* 25 */ shutdown_witness_operation,
    /* 26 */ hardfork_operation,
    /* 27 */ comment_payout_update_operation,
    /* 28 */ comment_benefactor_reward_operation,
    /* 29 */ producer_reward_operation,
    /* 30 */ devfund_operation,
    /* 31 */ pod_virtual_operation,
    /* 32 */ htlc_virtual_operation,
    /* 33 */ witness_block_missed_operation
  > operation;
// clang-format on

/*void operation_get_required_authorities( const operation& op,
                                        flat_set<string>& active,
                                        flat_set<string>& owner,
                                        flat_set<string>& posting,
                                        vector<authority>&  other );

void operation_validate( const operation& op );*/

bool is_market_operation( const operation& op );

bool is_virtual_operation( const operation& op );

}  // namespace protocol
}  // namespace wls

/*namespace fc {
void to_variant( const wls::protocol::operation& var,  fc::variant& vo );
void from_variant( const fc::variant& var,  wls::protocol::operation& vo );
}*/

DECLARE_OPERATION_TYPE( wls::protocol::operation )
FC_REFLECT_TYPENAME( wls::protocol::operation )
