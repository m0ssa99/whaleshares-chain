#pragma once
#include <wls/protocol/base.hpp>
#include <wls/protocol/block_header.hpp>
#include <wls/protocol/asset.hpp>
#include <wls/protocol/types.hpp>

#include <fc/utf8.hpp>

namespace wls {
namespace protocol {

using pod_name_type = account_name_type;

inline void validate_account_name(const string& name) { FC_ASSERT(is_valid_account_name(name), "Account name ${n} is invalid", ("n", name)); }

inline void validate_permlink(const string& permlink) {
  FC_ASSERT(permlink.size() < WLS_MAX_PERMLINK_LENGTH, "permlink is too long");
  FC_ASSERT(fc::is_utf8(permlink), "permlink not formatted in UTF8");
}

struct account_create_operation : public base_operation {
  asset             fee;
  account_name_type creator;
  account_name_type new_account_name;
  authority         owner;
  authority         active;
  authority         posting;
  public_key_type   memo_key;
  string            json_metadata;

  void validate() const;
  void get_required_active_authorities(flat_set<account_name_type>& a) const { a.insert(creator); }
};

struct account_update_operation : public base_operation {
  account_name_type             account;
  optional< authority >         owner;
  optional< authority >         active;
  optional< authority >         posting;
  public_key_type               memo_key;
  string                        json_metadata;

  void validate()const;

  void get_required_owner_authorities(flat_set<account_name_type>& a) const {
    if (owner) a.insert(account);
  }

  void get_required_active_authorities(flat_set<account_name_type>& a) const {
    if (!owner) a.insert(account);
  }
};

struct comment_operation : public base_operation {
  account_name_type parent_author;
  string            parent_permlink;

  account_name_type author;
  string            permlink;

  string            title;
  string            body;
  string            json_metadata;

  void validate() const;
  void get_required_posting_authorities(flat_set<account_name_type>& a) const { a.insert(author); }
};

struct beneficiary_route_type {
  beneficiary_route_type() {}
  beneficiary_route_type( const account_name_type& a, const uint16_t& w ) : account( a ), weight( w ){}

  account_name_type account;
  uint16_t          weight;

  // For use by std::sort such that the route is sorted first by name (ascending)
  bool operator < ( const beneficiary_route_type& o )const { return account < o.account; }
};

struct comment_payout_beneficiaries {
  vector<beneficiary_route_type> beneficiaries;

  void validate() const;
};

typedef static_variant<
  comment_payout_beneficiaries
> comment_options_extension;

typedef flat_set< comment_options_extension > comment_options_extensions_type;

/**
 *  Authors of posts may not want all of the benefits that come from creating a post. This operation allows authors to update properties associated with
 *  their post.
 *
 *  The max_accepted_payout may be decreased, but never increased.
 */
struct comment_options_operation : public base_operation {
  account_name_type author;
  string permlink;

  asset max_accepted_payout = asset(1000000000, WLS_SYMBOL);  /// WLS_SYMBOL value of the maximum payout this post will receive
  bool allow_votes = true;                                    /// allows a post to receive votes;
  bool allow_curation_rewards = true;                         /// allows voters to recieve curation rewards. Rewards return to reward fund.

  comment_options_extensions_type extensions;

  void validate()const;
  void get_required_posting_authorities( flat_set<account_name_type>& a )const{ a.insert(author); }
};

struct delete_comment_operation : public base_operation {
  account_name_type author;
  string            permlink;

  void validate()const;
  void get_required_posting_authorities( flat_set<account_name_type>& a )const{ a.insert(author); }
};

struct vote_operation : public base_operation {
  account_name_type    voter;
  account_name_type    author;
  string               permlink;
  int16_t              weight = 0;

  void validate()const;
  void get_required_posting_authorities( flat_set<account_name_type>& a )const{ a.insert(voter); }
};

/**
 * @ingroup operations
 *
 * @brief Transfers WLS from one account to another.
 */
struct transfer_operation : public base_operation {
  account_name_type from;
  account_name_type to;  /// Account to transfer asset to
  asset amount;          /// The amount of asset to transfer from @ref from to @ref to
  string memo;           /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.

  void validate() const;
  void get_required_active_authorities(flat_set<account_name_type>& a) const {
    if (amount.symbol != VESTS_SYMBOL) a.insert(from);
  }
  void get_required_owner_authorities(flat_set<account_name_type>& a) const {
    if (amount.symbol == VESTS_SYMBOL) a.insert(from);
  }
};

/**
 * This operation converts WLS into VFS (Vesting Fund Shares) at the current exchange rate.
 * With this operation it is possible to give another account vesting shares so that faucets can pre-fund new accounts with vesting shares.
 */
struct transfer_to_vesting_operation : public base_operation {
  account_name_type from;
  account_name_type to;  ///< if null, then same as from
  asset amount;          ///< must be WLS

  void validate()const;
  void get_required_active_authorities( flat_set<account_name_type>& a )const{ a.insert(from); }
};

/**
 * At any given point in time an account can be withdrawing from their vesting shares. A user may change the number of shares they wish to cash out at any
 * time between 0 and their total vesting stake.
 *
 * After applying this operation, vesting_shares will be withdrawn at a rate of vesting_shares/104 per week for two years starting one week after this
 * operation is included in the blockchain.
 *
 * This operation is not valid if the user has no vesting shares.
 */
struct withdraw_vesting_operation : public base_operation {
  account_name_type account;
  asset             vesting_shares;

  void validate()const;
  void get_required_active_authorities( flat_set<account_name_type>& a )const{ a.insert(account); }
};

/**
 * Allows an account to setup a vesting withdraw but with the additional request for the funds to be transferred directly to another account's balance rather
 * than the withdrawing account. In addition, those funds can be immediately vested again, circumventing the conversion from vests to WLS and back,
 * guaranteeing they maintain their value.
 */
struct set_withdraw_vesting_route_operation : public base_operation {
  account_name_type from_account;
  account_name_type to_account;
  uint16_t          percent = 0;
  bool              auto_vest = false;

  void validate()const;
  void get_required_active_authorities( flat_set<account_name_type>& a )const { a.insert( from_account ); }
};

/**
 * Witnesses must vote on how to set certain chain properties to ensure a smooth and well functioning network.
 * Any time @owner is in the active set of witnesses these properties will be used to control the blockchain configuration.
 */
struct chain_properties {
  /**
   * This fee, paid in WLS, is converted into VESTING SHARES for the new account. Accounts without vesting shares cannot earn usage rations and therefore are
   * powerless. This minimum fee requires all accounts to have some kind of commitment to the network that includes the ability to vote and make transactions.
   */
  asset account_creation_fee = asset(WLS_MIN_ACCOUNT_CREATION_FEE, WLS_SYMBOL);

  /**
   * This witnesses vote for the maximum_block_size which is used by the network to tune rate limiting and capacity
   */
  uint32_t maximum_block_size = WLS_MIN_BLOCK_SIZE_LIMIT * 2;

  void validate() const {
    FC_ASSERT(account_creation_fee.amount >= WLS_MIN_ACCOUNT_CREATION_FEE);
    FC_ASSERT(maximum_block_size >= WLS_MIN_BLOCK_SIZE_LIMIT);
  }
};

/**
 * Users who wish to become a witness must pay a fee acceptable to the current witnesses to apply for the position and allow voting to begin.
 *
 * If the owner isn't a witness they will become a witness.  Witnesses are charged a fee equal to 1 weeks worth of witness pay which in turn is derived from
 * the current share supply. The fee is only applied if the owner is not already a witness.
 *
 * If the block_signing_key is null then the witness is removed from contention.  The network will pick the top 21 witnesses for producing blocks.
 */
struct witness_update_operation : public base_operation {
  account_name_type owner;
  string            url;
  public_key_type   block_signing_key;
  chain_properties  props;
  asset             fee; ///< the fee paid to register a new witness, should be 10x current block production pay

  void validate()const;
  void get_required_active_authorities( flat_set<account_name_type>& a )const{ a.insert(owner); }
};

/**
 * All accounts with a VFS can vote for or against any witness. If a proxy is specified then all existing votes are removed.
 */
struct account_witness_vote_operation : public base_operation {
  account_name_type account;
  account_name_type witness;
  bool              approve = true;

  void validate() const;
  void get_required_active_authorities( flat_set<account_name_type>& a )const{ a.insert(account); }
};


struct account_witness_proxy_operation : public base_operation {
  account_name_type account;
  account_name_type proxy;

  void validate()const;
  void get_required_active_authorities( flat_set<account_name_type>& a )const{ a.insert(account); }
};

/**
 * @brief provides a generic way to add higher level protocols on top of witness consensus
 * @ingroup operations
 *
 * There is no validation for this operation other than that required auths are valid
 */
struct custom_operation : public base_operation {
  flat_set< account_name_type > required_auths;
  uint16_t                      id = 0;
  vector< char >                data;

  void validate()const;
  void get_required_active_authorities( flat_set<account_name_type>& a )const{ for( const auto& i : required_auths ) a.insert(i); }
};

/**
 * serves the same purpose as custom_operation but also supports required posting authorities.
 * Unlike custom_operation,this operation is designed to be human readable/developer friendly.
 **/
struct custom_json_operation : public base_operation {
  flat_set< account_name_type > required_auths;
  flat_set< account_name_type > required_posting_auths;
  string                        id; ///< must be less than 32 characters long
  string                        json; ///< must be proper utf8 / JSON string.

  void validate()const;
  void get_required_active_authorities( flat_set<account_name_type>& a )const{ for( const auto& i : required_auths ) a.insert(i); }
  void get_required_posting_authorities( flat_set<account_name_type>& a )const{ for( const auto& i : required_posting_auths ) a.insert(i); }
};


struct custom_binary_operation : public base_operation {
  flat_set< account_name_type > required_owner_auths;
  flat_set< account_name_type > required_active_auths;
  flat_set< account_name_type > required_posting_auths;
  vector< authority >           required_auths;

  string                        id; ///< must be less than 32 characters long
  vector< char >                data;

  void validate()const;
  void get_required_owner_authorities( flat_set<account_name_type>& a )const{ for( const auto& i : required_owner_auths ) a.insert(i); }
  void get_required_active_authorities( flat_set<account_name_type>& a )const{ for( const auto& i : required_active_auths ) a.insert(i); }
  void get_required_posting_authorities( flat_set<account_name_type>& a )const{ for( const auto& i : required_posting_auths ) a.insert(i); }
  void get_required_authorities( vector< authority >& a )const{ for( const auto& i : required_auths ) a.push_back( i ); }
};

struct claim_reward_balance_operation : public base_operation {
  account_name_type account;
  asset             reward_steem;
  asset             reward_vests;

  void get_required_posting_authorities( flat_set< account_name_type >& a )const{ a.insert( account ); }
  void validate() const;
};

/**
 * user creates a request asking to join a pod. Note that there are 2 types of fees
 */
struct pod_action_join_request {
  asset join_fee = asset(0, WLS_SYMBOL);                         /// must be matched to pod join_fee when validating
  asset fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);   /// small fee to make the request to avoid spamming (it is not join_fee)
  string memo;                                                   /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.

  void validate() const;
};

/**
 * user cancels the request
 */
struct pod_action_cancel_join_request {
  /// dont need any fields

  void validate() const;
};

/**
 * pod owner accepts a join request
 */
struct pod_action_accept_join_request {
  account_name_type account;  /// account accepted

  void validate() const;
};

/**
 * pod owner rejects a request, there is a memo to say why
 */
struct pod_action_reject_join_request {
  account_name_type account;  /// account rejected
  string memo;                /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.

  void validate() const;
};

/**
 * user leaves the pod
 */
struct pod_action_leave {
  /// dont need any fields

  void validate() const;
};

/**
 * pod owner kicks a user out of pod, there is a memo to say why
 */
struct pod_action_kick {
  account_name_type account;  /// account kicked
  string memo;                /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.

  void validate() const;
};

/**
 * pod owner update settings to the pod
 */
struct pod_action_update {
  optional< asset > join_fee = asset(0, WLS_SYMBOL);  /// fee to join this pod
  optional< string > json_metadata;
  optional< bool > allow_join = true;                 /// open for member to make a request to join

  void validate() const;
};

/**
 * NOTE: do not change the order of any actions or it will trigger a hardfork.
 */
typedef static_variant<
  /* 0 */ pod_action_join_request,
  /* 1 */ pod_action_cancel_join_request,
  /* 2 */ pod_action_accept_join_request,  /// for pod owner
  /* 3 */ pod_action_reject_join_request,  /// for pod owner
  /* 4 */ pod_action_leave,
  /* 5 */ pod_action_kick,                 /// for pod owner
  /* 6 */ pod_action_update                /// for pod owner
> pod_action;

/**
 * all actions related to pod
 */
struct pod_action_operation : public base_operation {
  account_name_type account;  /// who creates the action
  pod_name_type pod;          /// on which pod
  pod_action action;          /// what action

  void validate() const;

  void get_required_active_authorities(flat_set<account_name_type>& a) const { a.insert(account); }
};

struct friend_action_send_request {
  string memo;  /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.

  void validate() const;
};

struct friend_action_cancel_request {
  /// dont need any fields

  void validate() const;
};

struct friend_action_accept_request {
  /// dont need any fields

  void validate() const;
};

struct friend_action_reject_request {
  /// dont need any fields

  void validate() const;
};

struct friend_action_unfriend {
  /// dont need any fields

  void validate() const;
};

/**
 * NOTE: do not change the order of any actions or it will trigger a hardfork.
 */
typedef static_variant<
  /* 0 */ friend_action_send_request,
  /* 1 */ friend_action_cancel_request,
  /* 2 */ friend_action_accept_request,
  /* 3 */ friend_action_reject_request,
  /* 4 */ friend_action_unfriend
> friend_action;

/**
 * all actions related to friend
 */
struct friend_action_operation : public base_operation {
  account_name_type account;  /// who creates the action
  account_name_type another;  /// on which account
  friend_action action;       /// what action

  void validate() const;

  void get_required_posting_authorities(flat_set<account_name_type>& a) const { a.insert(account); }
};

struct account_action_create_pod {
  asset fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);   /// fee to create pod

  optional< asset > join_fee = asset(0, WLS_SYMBOL);         /// fee to join this pod
  optional< string > json_metadata;
  optional< bool > allow_join = true;                        /// open for member to make a request to join

  void validate() const;
};

/**
 * transfer from main_balance to tip_balance
 */
struct account_action_transfer_to_tip {
  asset amount;                    /// The amount of WLS

  void validate() const;
};

//-----------------------------------------
//-- for HTLC

typedef fc::ripemd160 htlc_ref_id_type;  // ripemd160(from,to,preimage_hash)
//typedef fc::sha256    htlc_sha256;

struct htlc_sha256 {
  htlc_sha256() {}
  htlc_sha256( const fc::sha256& _htlc_hash ) : htlc_hash( _htlc_hash ) {}

  fc::sha256 htlc_hash;

  void validate() const;
};

// support sha256 only
typedef static_variant<
    htlc_sha256
> htlc_hash_type;

struct account_action_htlc_create {
  asset             fee;           /// paid to network
  asset             reward;        /// optional to reward service provider (allow 3rd party to redeem and get reward)
  account_name_type to;            /// where the held monies will go if the preimage is provided
  asset             amount;        /// the amount to hold
  htlc_hash_type    preimage_hash; /// the (typed) hash of the preimage
  uint16_t          preimage_size; /// the size of the preimage, 0 means ignored when validating
  uint32_t          expiration;    /// The time the funds will be returned to the source if not claimed
  optional<string>  memo;          /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.

  void validate()const;
};

struct account_action_htlc_update {
  asset             fee;           /// paid to network
  htlc_ref_id_type  htlc_id;       /// the object we are attempting to update
  uint32_t          seconds;       /// how much time to add
  asset             reward;        /// how much reward to add
  optional<string>  memo;          /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.

  void validate()const;
};

struct account_action_htlc_redeem {
  asset             fee;           /// paid to network
  htlc_ref_id_type  htlc_id;       /// the object we are attempting to update
  std::vector<char> preimage;      /// the preimage (not used if after epoch timeout)
  optional<string>  memo;          /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.

  void validate()const;
};

/**
 * NOTE: do not change the order of any actions or it will trigger a hardfork.
 */
typedef static_variant<
  /* 0 */ account_action_create_pod,
  /* 1 */ account_action_transfer_to_tip,
  /* 2 */ account_action_htlc_create,
  /* 3 */ account_action_htlc_update,
  /* 4 */ account_action_htlc_redeem
> account_action;

/**
 * all new account actions here
 */
struct account_action_operation : public base_operation {
  account_name_type account;
  account_action action;

  void validate() const;
  void get_required_active_authorities(flat_set<account_name_type>& a) const { a.insert(account); }
};

/**
 * to replace comment_operation
 */
struct social_action_comment_create {
  /**
   * not editable fields
   */
  string permlink;
  account_name_type parent_author;
  string parent_permlink;
  optional<pod_name_type> pod;            /// empty means post to personal blog for friends
  optional<asset> max_accepted_payout;    /// WLS_SYMBOL value of the maximum payout this post will receive
  optional<bool> allow_replies;           /// allows a post to disable replies.
  optional<bool> allow_votes;             /// allows a post to receive votes;
  optional<bool> allow_curation_rewards;  /// allows voters to receive curation rewards. Rewards return to reward fund.
  optional<bool> allow_friends;           /// only friends can interact to the post

  /**
   * editable fields
   */
  string title;
  string body;
  string json_metadata;

  void validate() const;
};

/**
 * to update content of a comment, splitted from social_action_comment_create to make it cleaner.
 * Should add versioning to the comment later.
 */
struct social_action_comment_update {
  /**
   * not editable fields
   */
  string permlink;

  /**
   * editable fields
   */
  optional<string> title;
  optional<string> body;
  optional<string> json_metadata;

  void validate() const;
};

/**
 * replace delete_comment_operation
 */
struct social_action_comment_delete {
  string permlink;

  void validate() const;
};

/**
 * daily_vesting_reward is considered as activity_reward. Visiting & claiming daily is participating.
 * put to social action so user could claim from social site instead of wallet.
 */
struct social_action_claim_vesting_reward {
  asset amount;                    /// The amount of WLS, will be converted to VESTS when claiming

  optional<account_name_type> to;  ///< if null, then same as account
  optional<string> memo;           /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.

  void validate() const;
};

/**
 * same to social_action_claim_vesting_reward but claim to tip_balance (reward_steem_balance) instead
 */
struct social_action_claim_vesting_reward_tip {
  asset amount;                    /// The amount of WLS

  void validate() const;
};

/**
 * transfer from tip_balance to vesting
 */
struct social_action_user_tip {
  asset amount;                    ///< must be WLS
  optional<account_name_type> to;  ///< if null, then same as from
  optional<string> memo;           /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.

  void validate()const;
};

struct social_action_comment_tip {
  account_name_type    author;
  string               permlink;
  asset                amount;     /// The amount of WLS, will be converted to VESTS
  optional<string>     memo;       /// The memo is plain-text, any encryption on the memo is up to a higher level protocol.

  void validate() const;
};

/**
 * NOTE: do not change the order of any actions or it will trigger a hardfork.
 */
typedef static_variant<
  /* 0 */ social_action_comment_create,
  /* 1 */ social_action_comment_update,
  /* 2 */ social_action_comment_delete,
  /* 3 */ social_action_claim_vesting_reward,
  /* 4 */ social_action_claim_vesting_reward_tip,
  /* 5 */ social_action_user_tip,
  /* 6 */ social_action_comment_tip
> social_action;

/**
 * all new social actions here (post/share/vote/claim.....)
 */
struct social_action_operation : public base_operation {
  account_name_type account;
  social_action action;

  void validate() const;
  void get_required_posting_authorities(flat_set<account_name_type>& a) const { a.insert(account); }
};

}  // namespace protocol
}  // namespace wls

FC_REFLECT(wls::protocol::chain_properties, (account_creation_fee)(maximum_block_size));
FC_REFLECT(wls::protocol::account_create_operation, (fee)(creator)(new_account_name)(owner)(active)(posting)(memo_key)(json_metadata))
FC_REFLECT(wls::protocol::account_update_operation, (account)(owner)(active)(posting)(memo_key)(json_metadata))
FC_REFLECT(wls::protocol::transfer_operation, (from)(to)(amount)(memo))
FC_REFLECT(wls::protocol::transfer_to_vesting_operation, (from)(to)(amount))
FC_REFLECT(wls::protocol::withdraw_vesting_operation, (account)(vesting_shares))
FC_REFLECT(wls::protocol::set_withdraw_vesting_route_operation, (from_account)(to_account)(percent)(auto_vest))
FC_REFLECT(wls::protocol::witness_update_operation, (owner)(url)(block_signing_key)(props)(fee))
FC_REFLECT(wls::protocol::account_witness_vote_operation, (account)(witness)(approve))
FC_REFLECT(wls::protocol::account_witness_proxy_operation, (account)(proxy))
FC_REFLECT(wls::protocol::comment_operation, (parent_author)(parent_permlink)(author)(permlink)(title)(body)(json_metadata))
FC_REFLECT(wls::protocol::vote_operation, (voter)(author)(permlink)(weight))
FC_REFLECT(wls::protocol::custom_operation, (required_auths)(id)(data))
FC_REFLECT(wls::protocol::custom_json_operation, (required_auths)(required_posting_auths)(id)(json))
FC_REFLECT(wls::protocol::custom_binary_operation, (required_owner_auths)(required_active_auths)(required_posting_auths)(required_auths)(id)(data))
FC_REFLECT(wls::protocol::delete_comment_operation, (author)(permlink))
FC_REFLECT(wls::protocol::beneficiary_route_type, (account)(weight))
FC_REFLECT(wls::protocol::comment_payout_beneficiaries, (beneficiaries))
FC_REFLECT_TYPENAME(wls::protocol::comment_options_extension)
FC_REFLECT(wls::protocol::comment_options_operation, (author)(permlink)(max_accepted_payout)(allow_votes)(allow_curation_rewards)(extensions))
FC_REFLECT(wls::protocol::claim_reward_balance_operation, (account)(reward_steem)(reward_vests))
FC_REFLECT_TYPENAME(wls::protocol::pod_action)
FC_REFLECT(wls::protocol::pod_action_join_request, (join_fee)(fee)(memo))
FC_REFLECT(wls::protocol::pod_action_cancel_join_request, )
FC_REFLECT(wls::protocol::pod_action_accept_join_request, (account))
FC_REFLECT(wls::protocol::pod_action_reject_join_request, (account)(memo))
FC_REFLECT(wls::protocol::pod_action_leave, )
FC_REFLECT(wls::protocol::pod_action_kick, (account)(memo))
FC_REFLECT(wls::protocol::pod_action_update, (join_fee)(json_metadata)(allow_join))
FC_REFLECT(wls::protocol::pod_action_operation, (account)(pod)(action))
FC_REFLECT_TYPENAME(wls::protocol::friend_action)
FC_REFLECT(wls::protocol::friend_action_send_request, (memo))
FC_REFLECT(wls::protocol::friend_action_cancel_request, )
FC_REFLECT(wls::protocol::friend_action_accept_request, )
FC_REFLECT(wls::protocol::friend_action_reject_request, )
FC_REFLECT(wls::protocol::friend_action_unfriend, )
FC_REFLECT(wls::protocol::friend_action_operation, (account)(another)(action))
FC_REFLECT(wls::protocol::account_action_create_pod, (fee)(join_fee)(json_metadata)(allow_join))
FC_REFLECT(wls::protocol::account_action_transfer_to_tip, (amount))
FC_REFLECT(wls::protocol::htlc_sha256, (htlc_hash))
FC_REFLECT_TYPENAME(wls::protocol::htlc_hash_type)
FC_REFLECT(wls::protocol::account_action_htlc_create, (fee)(reward)(to)(amount)(preimage_hash)(preimage_size)(expiration)(memo))
FC_REFLECT(wls::protocol::account_action_htlc_update, (fee)(htlc_id)(seconds)(reward)(memo))
FC_REFLECT(wls::protocol::account_action_htlc_redeem, (fee)(htlc_id)(preimage)(memo))
FC_REFLECT_TYPENAME(wls::protocol::account_action)
FC_REFLECT(wls::protocol::account_action_operation, (account)(action))
FC_REFLECT(wls::protocol::social_action_comment_create, (permlink)(parent_author)(parent_permlink)(pod)(max_accepted_payout)(allow_replies)(allow_votes)(
                                                            allow_curation_rewards)(allow_friends)(title)(body)(json_metadata))
FC_REFLECT(wls::protocol::social_action_comment_update, (permlink)(title)(body)(json_metadata))
FC_REFLECT(wls::protocol::social_action_comment_delete, (permlink))
FC_REFLECT(wls::protocol::social_action_claim_vesting_reward, (amount)(to)(memo))
FC_REFLECT(wls::protocol::social_action_claim_vesting_reward_tip, (amount))
FC_REFLECT(wls::protocol::social_action_user_tip, (amount)(to)(memo))
FC_REFLECT(wls::protocol::social_action_comment_tip, (author)(permlink)(amount)(memo))
FC_REFLECT_TYPENAME(wls::protocol::social_action)
FC_REFLECT(wls::protocol::social_action_operation, (account)(action))
