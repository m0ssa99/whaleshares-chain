# Introducing Whaleshares Blockchain (beta)

Use Steem HF19 as starting point.

  - Currency symbol WLS. Removed SBD/Vesting Reward/Promoted/Market..
  - 100% APR inflation, half 50% every year to 5%.
  - 85% of inflation to author and curation reward.
  - 10% of inflation to block producers.
  - 5% of inflation to development fund.

# Public Announcement & Discussion
TBD

# No Support & No Warranty

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.

# Whitepaper

[Whale_paper_v1](https://gitlab.com/beyondbitcoin/whaleshares-web/uploads/40a04e43d66482289b988bd189c26cef/Whale_paper_v1.pdf)

# Quickstart

No docker images availble atm. See `Building`

# Building

See [build instructions](doc/building.md) for Linux (Ubuntu LTS) and macOS X.

# Seed Nodes

A list of some seed nodes to get you started can be found in
[doc/seednodes.txt](doc/seednodes.txt).

# Testing

See [doc/testing.md](doc/testing.md) for test build targets and info
on how to use lcov to check code test coverage.

# Full Node Configuration (x86_64-linux)

Downloading the block_log will dramatically speed up your deployment: 
```
> mkdir /opt/whaleshares
> cd /opt/whaleshares
> sudo apt-get install p7zip-full
> wget https://s3.nl-ams.scw.cloud/wls-blocklog/whaleshares-fullnode.7z
> 7z x whaleshares-fullnode.7z
> chmod +x whaled
> mv config.ini witness_node_data_dir   <-- warning: this may overwrite previous config.ini, if available
> ./whaled --replay-blockchain          (use --replay-blockchain first time only)
```

# Witness Node Configuration (x86_64-linux)

Downloading the block_log will dramatically speed up your deployment: 
```
> mkdir /opt/whaleshares
> cd /opt/whaleshares
> sudo apt-get install p7zip-full
> wget https://s3.nl-ams.scw.cloud/wls-blocklog/whaleshares-fullnode.7z
> 7z x whaleshares-fullnode.7z
> mv whaled-lm whaled
> chmod +x whaled
> nano config.ini                        <-- comment out plugins for fullnode and uncomment plugins for witness
> mv config.ini witness_node_data_dir    <-- warning: this may overwrite previous config.ini, if available
> ./whaled --replay-blockchain           (use --replay-blockchain first time only)
```

# Latest `whaled` Releases (included in above block_log archive) can be downloaded here...

https://gitlab.com/beyondbitcoin/whaleshares-chain/-/releases/v0.4.5

# config types
```
# for seed-node (whaled-lm)
plugin = chain p2p json_rpc webserver witness account_by_key account_by_key_api database_api network_broadcast_api
# shared-file-size = 2G

# for witness-node (whaled-lm)
# plugin = witness account_by_key account_by_key_api database_api network_broadcast_api
# shared-file-size = 4G

# for full-node (whaled)
#plugin = chain p2p json_rpc webserver witness account_by_key tags follow block_history block_missed myfeed podfeed blogfeed sharesfeed account_history notification podtags account_tags tips_sent tips_received account_by_key_api follow_api database_api network_broadcast_api
# shared-file-size = 6G
```

# update seed nodes in config.ini as follows
```
seed-node = 161.97.107.130:2001  #seed01.whaleshares.io
seed-node = 193.37.152.116:2001  #seed02.whaleshares.io
```

# cli_wallet for witnesses
```
> cd /opt/whaleshares
> wget https://s3.nl-ams.scw.cloud/wls-blocklog/whaleshares-cliwallet.7z
> 7z x whaleshares-cliwallet.7z
> ./cli_wallet -s https://pubrpc.whaleshares.io
```
