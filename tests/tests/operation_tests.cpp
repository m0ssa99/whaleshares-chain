#ifdef IS_TEST_NET

#include "../common/database_fixture.hpp"

#include <wls/protocol/exceptions.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/database_exceptions.hpp>
#include <wls/chain/hardfork.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/util/reward.hpp>
#include <wls/plugins/witness/witness_objects.hpp>
#include <wls/plugins/witness/simple_daily_bandwidth_limit.hpp>

#include <fc/crypto/digest.hpp>

#include <boost/test/unit_test.hpp>

#include <cmath>
#include <iostream>
#include <stdexcept>

using namespace wls;
using namespace wls::chain;
using namespace wls::protocol;
using fc::string;

namespace wpw = wls::plugins::witness;

BOOST_FIXTURE_TEST_SUITE(operation_tests, clean_database_fixture)

BOOST_AUTO_TEST_CASE(account_create_validate) {
  try {
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_create_authorities) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_create_authorities");

    account_create_operation op;
    op.creator = "alice";
    op.new_account_name = "bob";

    flat_set<account_name_type> auths;
    flat_set<account_name_type> expected;

    BOOST_TEST_MESSAGE("--- Testing owner authority");
    op.get_required_owner_authorities(auths);
    BOOST_REQUIRE(auths == expected);

    BOOST_TEST_MESSAGE("--- Testing active authority");
    expected.insert("alice");
    op.get_required_active_authorities(auths);
    BOOST_REQUIRE(auths == expected);

    BOOST_TEST_MESSAGE("--- Testing posting authority");
    expected.clear();
    auths.clear();
    op.get_required_posting_authorities(auths);
    BOOST_REQUIRE(auths == expected);
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_create_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_create_apply");

    signed_transaction tx;
    private_key_type priv_key = generate_private_key("alice");

    const account_object &init = db->get_account(WLS_INIT_MINER_NAME);
    asset init_starting_balance = init.balance;

    const auto &gpo = db->get_dynamic_global_properties();

    account_create_operation op;

    op.fee = asset(1000000, WLS_SYMBOL);
    op.new_account_name = "alice";
    op.creator = WLS_INIT_MINER_NAME;
    op.owner = authority(1, priv_key.get_public_key(), 1);
    op.active = authority(2, priv_key.get_public_key(), 2);
    op.memo_key = priv_key.get_public_key();
    op.json_metadata = "{\"foo\":\"bar\"}";

    BOOST_TEST_MESSAGE("--- Test normal account creation");
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.operations.push_back(op);
    tx.sign(init_account_priv_key, db->get_chain_id());
    tx.validate();
    db->push_transaction(tx, 0);

    const account_object &acct = db->get_account("alice");
    const account_authority_object &acct_auth = db->get<account_authority_object, by_account>("alice");

    auto vest_shares = gpo.total_vesting_shares;
    auto vests = gpo.total_vesting_fund_steem;

    BOOST_REQUIRE(acct.name == "alice");
    BOOST_REQUIRE(acct_auth.owner == authority(1, priv_key.get_public_key(), 1));
    BOOST_REQUIRE(acct_auth.active == authority(2, priv_key.get_public_key(), 2));
    BOOST_REQUIRE(acct.memo_key == priv_key.get_public_key());
    BOOST_REQUIRE(acct.proxy == "");
    BOOST_REQUIRE(acct.created == db->head_block_time());
    BOOST_REQUIRE(acct.balance.amount.value == ASSET("0.000 TESTS").amount.value);
    BOOST_REQUIRE(acct.id._id == acct_auth.id._id);

    /// because init_witness has created vesting shares and blocks have been produced, 100 TESTS is worth less than 100 vesting shares due to rounding
    auto base_fee = asset(10000, WLS_SYMBOL); // 10 WLS base fee
    auto name_fee = asset(0, WLS_SYMBOL);
    { // figure out name_fee - depends on character count
      const size_t len = op.new_account_name.size();
      if (len <= 12) {
        auto name_fee_value = int64_t(1000) * (uint16_t(1) << (12 - len));
        name_fee = asset(name_fee_value, WLS_SYMBOL);
      }
    }
    const auto burnt_fee = base_fee + name_fee;

    BOOST_REQUIRE(acct.vesting_shares.amount.value == ((op.fee - burnt_fee) * (vest_shares / vests)).amount.value);
    BOOST_REQUIRE(acct.vesting_withdraw_rate.amount.value == ASSET("0.000000 VESTS").amount.value);
    BOOST_REQUIRE(acct.proxied_vsf_votes_total().value == 0);
    BOOST_REQUIRE(init_starting_balance.amount.value - op.fee.amount.value == init.balance.amount.value);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test failure of duplicate account creation");
    BOOST_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), fc::exception);

    BOOST_REQUIRE(acct.name == "alice");
    BOOST_REQUIRE(acct_auth.owner == authority(1, priv_key.get_public_key(), 1));
    BOOST_REQUIRE(acct_auth.active == authority(2, priv_key.get_public_key(), 2));
    BOOST_REQUIRE(acct.memo_key == priv_key.get_public_key());
    BOOST_REQUIRE(acct.proxy == "");
    BOOST_REQUIRE(acct.created == db->head_block_time());
    BOOST_REQUIRE(acct.balance.amount.value == ASSET("0.000 TESTS ").amount.value);
    BOOST_REQUIRE(acct.vesting_shares.amount.value == ((op.fee - burnt_fee) * (vest_shares / vests)).amount.value);
    BOOST_REQUIRE(acct.vesting_withdraw_rate.amount.value == ASSET("0.000000 VESTS").amount.value);
    BOOST_REQUIRE(acct.proxied_vsf_votes_total().value == 0);
    BOOST_REQUIRE(init_starting_balance.amount.value - op.fee.amount.value == init.balance.amount.value);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test failure when creator cannot cover fee");
    tx.signatures.clear();
    tx.operations.clear();
    op.fee = asset(db->get_account(WLS_INIT_MINER_NAME).balance.amount + 1, WLS_SYMBOL);
    op.new_account_name = "bob";
    tx.operations.push_back(op);
    tx.sign(init_account_priv_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test failure covering witness fee");
    generate_block();
    // TODO: fix this!
//      db_plugin->debug_update( [=]( database& db )
//      {
//         db->modify( db->get_witness_schedule_object(), [&]( witness_schedule_object& wso )
//         {
//            wso.median_props.account_creation_fee = ASSET( "10.000 TESTS" );
//         });
//      });
//      generate_block();
//
//      tx.clear();
//      op.fee = ASSET( "1.000 TESTS" );
//      tx.operations.push_back( op );
//      tx.sign( init_account_priv_key, db->get_chain_id() );
//      WLS_REQUIRE_THROW( db->push_transaction( tx, 0 ), fc::exception );
     validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_update_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_update_validate");

    ACTORS((alice))

    account_update_operation op;
    op.account = "alice";
    op.posting = authority();
    op.posting->weight_threshold = 1;
    op.posting->add_authorities("abcdefghijklmnopq", 1);

    try {
      op.validate();

      signed_transaction tx;
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      BOOST_FAIL("An exception was not thrown for an invalid account name");
    } catch (fc::exception &) {
    }

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_update_authorities) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_update_authorities");

    ACTORS((alice)(bob))
    private_key_type active_key = generate_private_key("new_key");

    db->modify(db->get<account_authority_object, by_account>("alice"),
               [&](account_authority_object &a) { a.active = authority(1, active_key.get_public_key(), 1); });

    account_update_operation op;
    op.account = "alice";
    op.json_metadata = "{\"success\":true}";

    signed_transaction tx;
    tx.operations.push_back(op);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);

    BOOST_TEST_MESSAGE("  Tests when owner authority is not updated ---");
    BOOST_TEST_MESSAGE("--- Test failure when no signature");
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when wrong signature");
    tx.sign(bob_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when containing additional incorrect signature");
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_irrelevant_sig);

    BOOST_TEST_MESSAGE("--- Test failure when containing duplicate signatures");
    tx.signatures.clear();
    tx.sign(active_key, db->get_chain_id());
    tx.sign(active_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_duplicate_sig);

    BOOST_TEST_MESSAGE("--- Test success on active key");
    tx.signatures.clear();
    tx.sign(active_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    BOOST_TEST_MESSAGE("--- Test success on owner key alone");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, database::skip_transaction_dupe_check);

    BOOST_TEST_MESSAGE("  Tests when owner authority is updated ---");
    BOOST_TEST_MESSAGE("--- Test failure when updating the owner authority with an active key");
    tx.signatures.clear();
    tx.operations.clear();
    op.owner = authority(1, active_key.get_public_key(), 1);
    tx.operations.push_back(op);
    tx.sign(active_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_owner_auth);

    BOOST_TEST_MESSAGE("--- Test failure when owner key and active key are present");
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_irrelevant_sig);

    BOOST_TEST_MESSAGE("--- Test failure when incorrect signature");
    tx.signatures.clear();
    tx.sign(alice_post_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_owner_auth);

    BOOST_TEST_MESSAGE("--- Test failure when duplicate owner keys are present");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_duplicate_sig);

    BOOST_TEST_MESSAGE("--- Test success when updating the owner authority with an owner key");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_update_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_update_apply");

    ACTORS((alice))
    private_key_type new_private_key = generate_private_key("new_key");

    BOOST_TEST_MESSAGE("--- Test normal update");

    account_update_operation op;
    op.account = "alice";
    op.owner = authority(1, new_private_key.get_public_key(), 1);
    op.active = authority(2, new_private_key.get_public_key(), 2);
    op.memo_key = new_private_key.get_public_key();
    op.json_metadata = "{\"bar\":\"foo\"}";

    signed_transaction tx;
    tx.operations.push_back(op);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    const account_object &acct = db->get_account("alice");
    const account_authority_object &acct_auth = db->get<account_authority_object, by_account>("alice");

    BOOST_REQUIRE(acct.name == "alice");
    BOOST_REQUIRE(acct_auth.owner == authority(1, new_private_key.get_public_key(), 1));
    BOOST_REQUIRE(acct_auth.active == authority(2, new_private_key.get_public_key(), 2));
    BOOST_REQUIRE(acct.memo_key == new_private_key.get_public_key());

    /* This is being moved out of consensus
 #ifndef IS_LOW_MEM
    BOOST_REQUIRE( acct.json_metadata == "{\"bar\":\"foo\"}" );
 #else
    BOOST_REQUIRE( acct.json_metadata == "" );
 #endif
 */

    validate_database();

    BOOST_TEST_MESSAGE("--- Test failure when updating a non-existent account");
    tx.operations.clear();
    tx.signatures.clear();
    op.account = "bob";
    tx.operations.push_back(op);
    tx.sign(new_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception)
    validate_database();

    BOOST_TEST_MESSAGE("--- Test failure when account authority does not exist");
    tx.clear();
    op = account_update_operation();
    op.account = "alice";
    op.posting = authority();
    op.posting->weight_threshold = 1;
    op.posting->add_authorities("dave", 1);
    tx.operations.push_back(op);
    tx.sign(new_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(comment_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: comment_validate");

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(comment_authorities) {
  try {
//     BOOST_TEST_MESSAGE("Testing: comment_authorities");
//
//     ACTORS((alice)(bob));
//     generate_blocks(60 / WLS_BLOCK_INTERVAL);
//
//     comment_operation op;
//     op.author = "alice";
//     op.permlink = "lorem";
//     op.parent_author = "";
//     op.parent_permlink = "ipsum";
//     op.title = "Lorem Ipsum";
//     op.body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
//     op.json_metadata = "{\"foo\":\"bar\"}";
//
//     signed_transaction tx;
//     tx.operations.push_back(op);
//     tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
//
//     BOOST_TEST_MESSAGE("--- Test failure when no signatures");
//     WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_posting_auth);
//
//     BOOST_TEST_MESSAGE("--- Test failure when duplicate signatures");
//     tx.sign(alice_post_key, db->get_chain_id());
//     tx.sign(alice_post_key, db->get_chain_id());
//     WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_duplicate_sig);
//
//     BOOST_TEST_MESSAGE("--- Test success with post signature");
//     tx.signatures.clear();
//     tx.sign(alice_post_key, db->get_chain_id());
//     db->push_transaction(tx, 0);
//
//     BOOST_TEST_MESSAGE("--- Test failure when signed by an additional signature not in the creator's authority");
//     tx.sign(bob_private_key, db->get_chain_id());
//     WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), tx_irrelevant_sig);
//
//     BOOST_TEST_MESSAGE("--- Test failure when signed by a signature not in the creator's authority");
//     tx.signatures.clear();
//     tx.sign(bob_private_key, db->get_chain_id());
//     WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), tx_missing_posting_auth);
//
//     validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(comment_apply) {
  try {
//     BOOST_TEST_MESSAGE("Testing: comment_apply");
//
//     ACTORS((alice)(bob)(sam))
//     generate_blocks(60 / WLS_BLOCK_INTERVAL);
//
//     comment_operation op;
//     op.author = "alice";
//     op.permlink = "lorem";
//     op.parent_author = "";
//     op.parent_permlink = "ipsum";
//     op.title = "Lorem Ipsum";
//     op.body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
//     op.json_metadata = "{\"foo\":\"bar\"}";
//
//     signed_transaction tx;
//     tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
//
//     BOOST_TEST_MESSAGE("--- Test Alice posting a root comment");
//     tx.operations.push_back(op);
//     tx.sign(alice_private_key, db->get_chain_id());
//     db->push_transaction(tx, 0);
//
//     const comment_object &alice_comment = db->get_comment("alice", string("lorem"));
//
//     BOOST_REQUIRE(alice_comment.author == op.author);
//     BOOST_REQUIRE(to_string(alice_comment.permlink) == op.permlink);
//     BOOST_REQUIRE(to_string(alice_comment.parent_permlink) == op.parent_permlink);
//     BOOST_REQUIRE(alice_comment.last_update == db->head_block_time());
//     BOOST_REQUIRE(alice_comment.created == db->head_block_time());
//     BOOST_REQUIRE(alice_comment.net_rshares.value == 0);
//     BOOST_REQUIRE(alice_comment.abs_rshares.value == 0);
//     BOOST_REQUIRE(alice_comment.cashout_time == fc::time_point_sec(db->head_block_time() + fc::seconds(WLS_CASHOUT_WINDOW_SECONDS)));
//
//#ifndef IS_LOW_MEM
//     BOOST_REQUIRE(to_string(alice_comment.title) == op.title);
//     BOOST_REQUIRE(to_string(alice_comment.body) == op.body);
//     //BOOST_REQUIRE( alice_comment.json_metadata == op.json_metadata );
//#else
//                                                                                                                             BOOST_REQUIRE( to_string( alice_comment.title ) == "" );
//     BOOST_REQUIRE( to_string( alice_comment.body ) == "" );
//     //BOOST_REQUIRE( alice_comment.json_metadata == "" );
//#endif
//
//     validate_database();
//
//     BOOST_TEST_MESSAGE("--- Test Bob posting a comment on a non-existent comment");
//     op.author = "bob";
//     op.permlink = "ipsum";
//     op.parent_author = "alice";
//     op.parent_permlink = "foobar";
//
//     tx.signatures.clear();
//     tx.operations.clear();
//     tx.operations.push_back(op);
//     tx.sign(bob_private_key, db->get_chain_id());
//     WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
//
//     BOOST_TEST_MESSAGE("--- Test Bob posting a comment on Alice's comment");
//     op.parent_permlink = "lorem";
//
//     tx.signatures.clear();
//     tx.operations.clear();
//     tx.operations.push_back(op);
//     tx.sign(bob_private_key, db->get_chain_id());
//     db->push_transaction(tx, 0);
//
//     const comment_object &bob_comment = db->get_comment("bob", string("ipsum"));
//
//     BOOST_REQUIRE(bob_comment.author == op.author);
//     BOOST_REQUIRE(to_string(bob_comment.permlink) == op.permlink);
//     BOOST_REQUIRE(bob_comment.parent_author == op.parent_author);
//     BOOST_REQUIRE(to_string(bob_comment.parent_permlink) == op.parent_permlink);
//     BOOST_REQUIRE(bob_comment.last_update == db->head_block_time());
//     BOOST_REQUIRE(bob_comment.created == db->head_block_time());
//     BOOST_REQUIRE(bob_comment.net_rshares.value == 0);
//     BOOST_REQUIRE(bob_comment.abs_rshares.value == 0);
//     BOOST_REQUIRE(bob_comment.cashout_time == bob_comment.created + WLS_CASHOUT_WINDOW_SECONDS);
//     BOOST_REQUIRE(bob_comment.root_comment == alice_comment.id);
//     validate_database();
//
//     BOOST_TEST_MESSAGE("--- Test Sam posting a comment on Bob's comment");
//
//     op.author = "sam";
//     op.permlink = "dolor";
//     op.parent_author = "bob";
//     op.parent_permlink = "ipsum";
//
//     tx.signatures.clear();
//     tx.operations.clear();
//     tx.operations.push_back(op);
//     tx.sign(sam_private_key, db->get_chain_id());
//     db->push_transaction(tx, 0);
//
//     const comment_object &sam_comment = db->get_comment("sam", string("dolor"));
//
//     BOOST_REQUIRE(sam_comment.author == op.author);
//     BOOST_REQUIRE(to_string(sam_comment.permlink) == op.permlink);
//     BOOST_REQUIRE(sam_comment.parent_author == op.parent_author);
//     BOOST_REQUIRE(to_string(sam_comment.parent_permlink) == op.parent_permlink);
//     BOOST_REQUIRE(sam_comment.last_update == db->head_block_time());
//     BOOST_REQUIRE(sam_comment.created == db->head_block_time());
//     BOOST_REQUIRE(sam_comment.net_rshares.value == 0);
//     BOOST_REQUIRE(sam_comment.abs_rshares.value == 0);
//     BOOST_REQUIRE(sam_comment.cashout_time == sam_comment.created + WLS_CASHOUT_WINDOW_SECONDS);
//     BOOST_REQUIRE(sam_comment.root_comment == alice_comment.id);
//     validate_database();
//
//     generate_blocks(60 * 5 / WLS_BLOCK_INTERVAL + 1);
//
//     BOOST_TEST_MESSAGE("--- Test modifying a comment");
//     const auto &mod_sam_comment = db->get_comment("sam", string("dolor"));
////      const auto& mod_bob_comment = db->get_comment( "bob", string( "ipsum" ) );
////      const auto& mod_alice_comment = db->get_comment( "alice", string( "lorem" ) );
//     fc::time_point_sec created = mod_sam_comment.created;
//
//     db->modify(mod_sam_comment, [&](comment_object &com) {
//        com.net_rshares = 10;
//        com.abs_rshares = 10;
//     });
//
//     db->modify(db->get_dynamic_global_properties(), [&](dynamic_global_property_object &o) {
//        o.total_reward_shares2 = wls::chain::util::evaluate_reward_curve(10);
//     });
//
//     tx.signatures.clear();
//     tx.operations.clear();
//     op.title = "foo";
//     op.body = "bar";
//     op.json_metadata = "{\"bar\":\"foo\"}";
//     tx.operations.push_back(op);
//     tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
//     tx.sign(sam_private_key, db->get_chain_id());
//     db->push_transaction(tx, 0);
//
//     BOOST_REQUIRE(mod_sam_comment.author == op.author);
//     BOOST_REQUIRE(to_string(mod_sam_comment.permlink) == op.permlink);
//     BOOST_REQUIRE(mod_sam_comment.parent_author == op.parent_author);
//     BOOST_REQUIRE(to_string(mod_sam_comment.parent_permlink) == op.parent_permlink);
//     BOOST_REQUIRE(mod_sam_comment.last_update == db->head_block_time());
//     BOOST_REQUIRE(mod_sam_comment.created == created);
//     BOOST_REQUIRE(mod_sam_comment.cashout_time == mod_sam_comment.created + WLS_CASHOUT_WINDOW_SECONDS);
//     validate_database();
//
//     BOOST_TEST_MESSAGE("--- Test failure posting withing 1 minute");
//
//     op.permlink = "sit";
//     op.parent_author = "";
//     op.parent_permlink = "test";
//     tx.operations.clear();
//     tx.signatures.clear();
//     tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
//     tx.operations.push_back(op);
//     tx.sign(sam_private_key, db->get_chain_id());
//     db->push_transaction(tx, 0);
//
//     generate_blocks(60 * 5 / WLS_BLOCK_INTERVAL);
//
//     op.permlink = "amet";
//     tx.operations.clear();
//     tx.signatures.clear();
//     tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
//     tx.operations.push_back(op);
//     tx.sign(sam_private_key, db->get_chain_id());
//     WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
//
//     validate_database();
//
//     generate_block();
//     db->push_transaction(tx, 0);
//     validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(comment_delete_apply) {
  try {
//    BOOST_TEST_MESSAGE("Testing: comment_delete_apply");
//    ACTORS((alice))
//    generate_block();
//    vest("alice", ASSET("1000.000 TESTS"));
//    generate_block();
//
//    signed_transaction tx;
//    comment_operation comment;
//    vote_operation vote;
//    delete_comment_operation op;
//
//    {
//      comment.author = "alice";
//      comment.permlink = "test1";
//      comment.title = "test";
//      comment.body = "foo bar";
//      comment.parent_permlink = "test";
//      vote.voter = "alice";
//      vote.author = "alice";
//      vote.permlink = "test1";
//      vote.weight = WLS_100_PERCENT;
//      tx.operations.push_back(comment);
//      tx.operations.push_back(vote);
//      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//    }
//
//    BOOST_TEST_MESSAGE("--- Test failure deleting a comment with positive rshares");
//    {
//      op.author = "alice";
//      op.permlink = "test1";
//      tx.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::assert_exception)
//    }
//
//    BOOST_TEST_MESSAGE("--- Test success deleting a comment with negative rshares");
//    {
//      generate_block();
//      vote.weight = -1 * WLS_100_PERCENT;
//      tx.clear();
//      tx.operations.push_back(vote);
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      auto test_comment = db->find<comment_object, by_permlink>(boost::make_tuple("alice", string("test1")));
//      BOOST_REQUIRE(test_comment == nullptr);
//    }
//
//    BOOST_TEST_MESSAGE("--- Test failure deleting a comment past cashout");
//    {
//      generate_blocks(WLS_MIN_ROOT_COMMENT_INTERVAL.to_seconds() / WLS_BLOCK_INTERVAL);
//
//      tx.clear();
//      tx.operations.push_back(comment);
//      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      generate_blocks(WLS_CASHOUT_WINDOW_SECONDS / WLS_BLOCK_INTERVAL);
//      BOOST_REQUIRE(db->get_comment("alice", string("test1")).cashout_time == fc::time_point_sec::maximum());
//
//      tx.clear();
//      tx.operations.push_back(op);
//      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
//      tx.sign(alice_private_key, db->get_chain_id());
//      WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::assert_exception)
//    }
//
//    BOOST_TEST_MESSAGE("--- Test failure deleting a comment with a reply");
//    {
//      comment.permlink = "test2";
//      comment.parent_author = "";
//      comment.parent_permlink = "test";
//      tx.clear();
//      tx.operations.push_back(comment);
//      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      generate_blocks(WLS_MIN_ROOT_COMMENT_INTERVAL.to_seconds() / WLS_BLOCK_INTERVAL);
//      comment.permlink = "test3";
//      comment.parent_author = "alice";
//      comment.parent_permlink = "test2";
//      tx.clear();
//      tx.operations.push_back(comment);
//      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      op.permlink = "test2";
//      tx.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::assert_exception)
//    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(vote_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: vote_validate");

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(vote_authorities) {
  try {
    BOOST_TEST_MESSAGE("Testing: vote_authorities");

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(vote_apply) {
  try {
    // TODO: use social_action to create new comment

//    BOOST_TEST_MESSAGE("Testing: vote_apply");
//    generate_blocks(100);
//    ACTORS((alice)(bob)(sam)(dave))
//    generate_block();
//
//    vest("alice", ASSET("10.000 TESTS"));
//    validate_database();
//    vest("bob", ASSET("10.000 TESTS"));
//    vest("sam", ASSET("10.000 TESTS"));
//    vest("dave", ASSET("10.000 TESTS"));
//    generate_block();
//
//    const auto &vote_idx = db->get_index<comment_vote_index>().indices().get<by_comment_voter>();
//
//    {
//      const auto &alice = db->get_account("alice");
//
//      signed_transaction tx;
//      comment_operation comment_op;
//      comment_op.author = "alice";
//      comment_op.permlink = "foo";
//      comment_op.parent_permlink = "test";
//      comment_op.title = "bar";
//      comment_op.body = "foo bar";
//      tx.operations.push_back(comment_op);
//      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      BOOST_TEST_MESSAGE("--- Testing voting on a non-existent comment");
//
//      tx.operations.clear();
//      tx.signatures.clear();
//
//      vote_operation op;
//      op.voter = "alice";
//      op.author = "bob";
//      op.permlink = "foo";
//      op.weight = WLS_100_PERCENT;
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//
//      WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
//
//      validate_database();
//
//      BOOST_TEST_MESSAGE("--- Testing voting with a weight of 0");
//
//      op.weight = (int16_t)0;
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//
//      WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
//
//      validate_database();
//
//      BOOST_TEST_MESSAGE("--- Testing success");
//
//      auto old_voting_power = alice.voting_power;
//
//      op.weight = WLS_100_PERCENT;
//      op.author = "alice";
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//
//      db->push_transaction(tx, 0);
//
//      auto &alice_comment = db->get_comment("alice", string("foo"));
//      auto itr = vote_idx.find(std::make_tuple(alice_comment.id, alice.id));
//      int64_t max_vote_denom = (db->get_dynamic_global_properties().vote_power_reserve_rate * WLS_VOTE_REGENERATION_SECONDS) / (60 * 60 * 24);
//
//      BOOST_REQUIRE(alice.voting_power == old_voting_power - ((old_voting_power + max_vote_denom - 1) / max_vote_denom));
//      BOOST_REQUIRE(alice.last_vote_time == db->head_block_time());
//      BOOST_REQUIRE(alice_comment.net_rshares.value == alice.vesting_shares.amount.value * (old_voting_power - alice.voting_power) / WLS_100_PERCENT);
//      BOOST_REQUIRE(alice_comment.cashout_time == alice_comment.created + WLS_CASHOUT_WINDOW_SECONDS);
//      BOOST_REQUIRE(itr->rshares == alice.vesting_shares.amount.value * (old_voting_power - alice.voting_power) / WLS_100_PERCENT);
//      BOOST_REQUIRE(itr != vote_idx.end());
//      validate_database();
//
//      BOOST_TEST_MESSAGE("--- Test reduced power for quick voting");
//
//      generate_blocks(db->head_block_time() + WLS_MIN_VOTE_INTERVAL_SEC);
//
//      old_voting_power = db->get_account("alice").voting_power;
//
//      comment_op.author = "bob";
//      comment_op.permlink = "foo";
//      comment_op.title = "bar";
//      comment_op.body = "foo bar";
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(comment_op);
//      tx.sign(bob_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      op.weight = WLS_100_PERCENT / 2;
//      op.voter = "alice";
//      op.author = "bob";
//      op.permlink = "foo";
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      const auto &bob_comment = db->get_comment("bob", string("foo"));
//      itr = vote_idx.find(std::make_tuple(bob_comment.id, alice.id));
//
//      BOOST_REQUIRE(db->get_account("alice").voting_power ==
//                    old_voting_power - ((old_voting_power + max_vote_denom - 1) * WLS_100_PERCENT / (2 * max_vote_denom * WLS_100_PERCENT)));
//      BOOST_REQUIRE(bob_comment.net_rshares.value ==
//                    alice.vesting_shares.amount.value * (old_voting_power - db->get_account("alice").voting_power) / WLS_100_PERCENT);
//      BOOST_REQUIRE(bob_comment.cashout_time == bob_comment.created + WLS_CASHOUT_WINDOW_SECONDS);
//      BOOST_REQUIRE(itr != vote_idx.end());
//      validate_database();
//
//      BOOST_TEST_MESSAGE("--- Test payout time extension on vote");
//
//      old_voting_power = db->get_account("bob").voting_power;
//      auto old_abs_rshares = db->get_comment("alice", string("foo")).abs_rshares.value;
//
//      generate_blocks(db->head_block_time() + fc::seconds((WLS_CASHOUT_WINDOW_SECONDS / 2)), true);
//
//      const auto &new_bob = db->get_account("bob");
//      const auto &new_alice_comment = db->get_comment("alice", string("foo"));
//
//      op.weight = WLS_100_PERCENT;
//      op.voter = "bob";
//      op.author = "alice";
//      op.permlink = "foo";
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(op);
//      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
//      tx.sign(bob_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      itr = vote_idx.find(std::make_tuple(new_alice_comment.id, new_bob.id));
//      uint128_t new_cashout_time = db->head_block_time().sec_since_epoch() + WLS_CASHOUT_WINDOW_SECONDS;
//
//      BOOST_REQUIRE(new_bob.voting_power == WLS_100_PERCENT - ((WLS_100_PERCENT + max_vote_denom - 1) / max_vote_denom));
//      BOOST_REQUIRE(new_alice_comment.net_rshares.value ==
//                    old_abs_rshares + new_bob.vesting_shares.amount.value * (old_voting_power - new_bob.voting_power) / WLS_100_PERCENT);
//      BOOST_REQUIRE(new_alice_comment.cashout_time == new_alice_comment.created + WLS_CASHOUT_WINDOW_SECONDS);
//      BOOST_REQUIRE(itr != vote_idx.end());
//      validate_database();
//
//      BOOST_TEST_MESSAGE("--- Test negative vote");
//      const auto &new_sam = db->get_account("sam");
//      const auto &new_bob_comment = db->get_comment("bob", string("foo"));
//
//      old_abs_rshares = new_bob_comment.abs_rshares.value;
//
//      op.weight = -1 * WLS_100_PERCENT / 2;
//      op.voter = "sam";
//      op.author = "bob";
//      op.permlink = "foo";
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(op);
//      tx.sign(sam_private_key, db->get_chain_id());
//      WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
//      validate_database();
//
//      BOOST_TEST_MESSAGE("--- Test nested voting on nested comments");
//
//      old_abs_rshares = new_alice_comment.children_abs_rshares.value;
//      int64_t regenerated_power =
//          (WLS_100_PERCENT * (db->head_block_time() - db->get_account("alice").last_vote_time).to_seconds()) / WLS_VOTE_REGENERATION_SECONDS;
//      int64_t used_power = (db->get_account("alice").voting_power + regenerated_power + max_vote_denom - 1) / max_vote_denom;
//
//      comment_op.author = "sam";
//      comment_op.permlink = "foo";
//      comment_op.title = "bar";
//      comment_op.body = "foo bar";
//      comment_op.parent_author = "alice";
//      comment_op.parent_permlink = "foo";
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(comment_op);
//      tx.sign(sam_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      op.weight = WLS_100_PERCENT;
//      op.voter = "alice";
//      op.author = "sam";
//      op.permlink = "foo";
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      auto new_rshares = ((fc::uint128_t(db->get_account("alice").vesting_shares.amount.value) * used_power) / WLS_100_PERCENT).to_uint64();
//
//      BOOST_REQUIRE(db->get_comment("alice", string("foo")).cashout_time == db->get_comment("alice", string("foo")).created + WLS_CASHOUT_WINDOW_SECONDS);
//
//      validate_database();
//
//      BOOST_TEST_MESSAGE("--- Test increasing vote rshares");
//
//      generate_blocks(db->head_block_time() + WLS_MIN_VOTE_INTERVAL_SEC);
//
//      auto new_alice = db->get_account("alice");
//      auto alice_bob_vote = vote_idx.find(std::make_tuple(new_bob_comment.id, new_alice.id));
//      auto old_vote_rshares = alice_bob_vote->rshares;
//      auto old_net_rshares = new_bob_comment.net_rshares.value;
//      old_abs_rshares = new_bob_comment.abs_rshares.value;
//      used_power = ((WLS_1_PERCENT * 25 * (new_alice.voting_power) / WLS_100_PERCENT) + max_vote_denom - 1) / max_vote_denom;
//      auto alice_voting_power = new_alice.voting_power - used_power;
//
//      op.voter = "alice";
//      op.weight = WLS_1_PERCENT * 25;
//      op.author = "bob";
//      op.permlink = "foo";
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//      alice_bob_vote = vote_idx.find(std::make_tuple(new_bob_comment.id, new_alice.id));
//
//      new_rshares = ((fc::uint128_t(new_alice.vesting_shares.amount.value) * used_power) / WLS_100_PERCENT).to_uint64();
//
//      BOOST_REQUIRE(new_bob_comment.net_rshares == old_net_rshares - old_vote_rshares + new_rshares);
//      BOOST_REQUIRE(new_bob_comment.abs_rshares == old_abs_rshares + new_rshares);
//      BOOST_REQUIRE(new_bob_comment.cashout_time == new_bob_comment.created + WLS_CASHOUT_WINDOW_SECONDS);
//      BOOST_REQUIRE(alice_bob_vote->rshares == new_rshares);
//      BOOST_REQUIRE(alice_bob_vote->last_update == db->head_block_time());
//      BOOST_REQUIRE(alice_bob_vote->vote_percent == op.weight);
//      BOOST_REQUIRE(db->get_account("alice").voting_power == alice_voting_power);
//      validate_database();
//
//      BOOST_TEST_MESSAGE("--- Test decreasing vote rshares");
//
//      generate_blocks(db->head_block_time() + WLS_MIN_VOTE_INTERVAL_SEC);
//
//      old_vote_rshares = new_rshares;
//      old_net_rshares = new_bob_comment.net_rshares.value;
//      old_abs_rshares = new_bob_comment.abs_rshares.value;
//      used_power = (uint64_t(WLS_1_PERCENT) * 75 * uint64_t(alice_voting_power)) / WLS_100_PERCENT;
//      used_power = (used_power + max_vote_denom - 1) / max_vote_denom;
//      alice_voting_power -= used_power;
//
//      op.weight = WLS_1_PERCENT * 75;
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//      alice_bob_vote = vote_idx.find(std::make_tuple(new_bob_comment.id, new_alice.id));
//
//      new_rshares = ((fc::uint128_t(new_alice.vesting_shares.amount.value) * used_power) / WLS_100_PERCENT).to_uint64();
//
//      BOOST_REQUIRE(new_bob_comment.net_rshares == old_net_rshares - old_vote_rshares + new_rshares);
//      BOOST_REQUIRE(new_bob_comment.abs_rshares == old_abs_rshares + new_rshares);
//      BOOST_REQUIRE(new_bob_comment.cashout_time == new_bob_comment.created + WLS_CASHOUT_WINDOW_SECONDS);
//      BOOST_REQUIRE(alice_bob_vote->rshares == new_rshares);
//      BOOST_REQUIRE(alice_bob_vote->last_update == db->head_block_time());
//      BOOST_REQUIRE(alice_bob_vote->vote_percent == op.weight);
//      BOOST_REQUIRE(db->get_account("alice").voting_power == alice_voting_power);
//      validate_database();
//
//      BOOST_TEST_MESSAGE("--- Test changing a vote to 0 weight (aka: removing a vote)");
//
//      generate_blocks(db->head_block_time() + WLS_MIN_VOTE_INTERVAL_SEC);
//
//      old_vote_rshares = alice_bob_vote->rshares;
//      old_net_rshares = new_bob_comment.net_rshares.value;
//      old_abs_rshares = new_bob_comment.abs_rshares.value;
//
//      op.weight = 0;
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//      alice_bob_vote = vote_idx.find(std::make_tuple(new_bob_comment.id, new_alice.id));
//
//      BOOST_REQUIRE(new_bob_comment.net_rshares == old_net_rshares - old_vote_rshares);
//      BOOST_REQUIRE(new_bob_comment.abs_rshares == old_abs_rshares);
//      BOOST_REQUIRE(new_bob_comment.cashout_time == new_bob_comment.created + WLS_CASHOUT_WINDOW_SECONDS);
//      BOOST_REQUIRE(alice_bob_vote->rshares == 0);
//      BOOST_REQUIRE(alice_bob_vote->last_update == db->head_block_time());
//      BOOST_REQUIRE(alice_bob_vote->vote_percent == op.weight);
//      BOOST_REQUIRE(db->get_account("alice").voting_power == alice_voting_power);
//      validate_database();
//
//      BOOST_TEST_MESSAGE("--- Test failure when increasing rshares within lockout period");
//
//      generate_blocks(fc::time_point_sec((new_bob_comment.cashout_time - WLS_UPVOTE_LOCKOUT).sec_since_epoch() + WLS_BLOCK_INTERVAL), true);
//
//      op.weight = WLS_100_PERCENT;
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//
//      WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
//      validate_database();
//
////      BOOST_TEST_MESSAGE("--- Test success when reducing rshares within lockout period");
////
////      op.weight = WLS_1_PERCENT * 50;
////      tx.operations.clear();
////      tx.signatures.clear();
////      tx.operations.push_back(op);
////      tx.sign(alice_private_key, db->get_chain_id());
////      db->push_transaction(tx, 0);
////      validate_database();
//
//      BOOST_TEST_MESSAGE("--- Test failure with a new vote within lockout period");
//
//      op.weight = WLS_100_PERCENT;
//      op.voter = "dave";
//      tx.operations.clear();
//      tx.signatures.clear();
//      tx.operations.push_back(op);
//      tx.sign(dave_private_key, db->get_chain_id());
//      WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
//      validate_database();
//    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(transfer_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: transfer_validate");

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(transfer_authorities) {
  try {
    ACTORS((alice)(bob))
    fund("alice", 10000);

    BOOST_TEST_MESSAGE("Testing: transfer_authorities");

    transfer_operation op;
    op.from = "alice";
    op.to = "bob";
    op.amount = ASSET("2.500 TESTS");

    signed_transaction tx;
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.operations.push_back(op);

    BOOST_TEST_MESSAGE("--- Test failure when no signatures");
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when signed by a signature not in the account's authority");
    tx.sign(alice_post_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when duplicate signatures");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_duplicate_sig);

    BOOST_TEST_MESSAGE("--- Test failure when signed by an additional signature not in the creator's authority");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    tx.sign(bob_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_irrelevant_sig);

    BOOST_TEST_MESSAGE("--- Test success with witness signature");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(signature_stripping) {
  try {
    // Alice, Bob and Sam all have 2-of-3 multisig on corp.
    // Legitimate tx signed by (Alice, Bob) goes through.
    // Sam shouldn't be able to add or remove signatures to get the transaction to process multiple times.
    generate_blocks(100);
    ACTORS((alice)(bob)(sam)(corp))
    fund("corp", 10000);

    account_update_operation update_op;
    update_op.account = "corp";
    update_op.active = authority(2, "alice", 1, "bob", 1, "sam", 1);

    signed_transaction tx;
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.operations.push_back(update_op);

    tx.sign(corp_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    tx.operations.clear();
    tx.signatures.clear();

    transfer_operation transfer_op;
    transfer_op.from = "corp";
    transfer_op.to = "sam";
    transfer_op.amount = ASSET("1.000 TESTS");

    tx.operations.push_back(transfer_op);

    tx.sign(alice_private_key, db->get_chain_id());
    signature_type alice_sig = tx.signatures.back();
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);
    tx.sign(bob_private_key, db->get_chain_id());
    signature_type bob_sig = tx.signatures.back();
    tx.sign(sam_private_key, db->get_chain_id());
    signature_type sam_sig = tx.signatures.back();
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_irrelevant_sig);

    tx.signatures.clear();
    tx.signatures.push_back(alice_sig);
    tx.signatures.push_back(bob_sig);
    db->push_transaction(tx, 0);

    tx.signatures.clear();
    tx.signatures.push_back(alice_sig);
    tx.signatures.push_back(sam_sig);
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(transfer_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: transfer_apply");

    ACTORS((alice)(bob))
    fund("alice", 10000);

    BOOST_REQUIRE(alice.balance.amount.value == ASSET("10.000 TESTS").amount.value);
    BOOST_REQUIRE(bob.balance.amount.value == ASSET(" 0.000 TESTS").amount.value);

    signed_transaction tx;
    transfer_operation op;

    op.from = "alice";
    op.to = "bob";
    op.amount = ASSET("5.000 TESTS");

    BOOST_TEST_MESSAGE("--- Test normal transaction");
    tx.operations.push_back(op);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    BOOST_REQUIRE(alice.balance.amount.value == ASSET("5.000 TESTS").amount.value);
    BOOST_REQUIRE(bob.balance.amount.value == ASSET("5.000 TESTS").amount.value);
    validate_database();

    BOOST_TEST_MESSAGE("--- Generating a block");
    generate_block();

    const auto &new_alice = db->get_account("alice");
    const auto &new_bob = db->get_account("bob");

    BOOST_REQUIRE(new_alice.balance.amount.value == ASSET("5.000 TESTS").amount.value);
    BOOST_REQUIRE(new_bob.balance.amount.value == ASSET("5.000 TESTS").amount.value);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test emptying an account");
    tx.signatures.clear();
    tx.operations.clear();
    tx.operations.push_back(op);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, database::skip_transaction_dupe_check);

    BOOST_REQUIRE(new_alice.balance.amount.value == ASSET("0.000 TESTS").amount.value);
    BOOST_REQUIRE(new_bob.balance.amount.value == ASSET("10.000 TESTS").amount.value);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test transferring non-existent funds");
    tx.signatures.clear();
    tx.operations.clear();
    tx.operations.push_back(op);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), fc::exception);

    BOOST_REQUIRE(new_alice.balance.amount.value == ASSET("0.000 TESTS").amount.value);
    BOOST_REQUIRE(new_bob.balance.amount.value == ASSET("10.000 TESTS").amount.value);
    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(transfer_to_vesting_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: transfer_to_vesting_validate");

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(transfer_to_vesting_authorities) {
  try {
    ACTORS((alice)(bob))
    fund("alice", 10000);

    BOOST_TEST_MESSAGE("Testing: transfer_to_vesting_authorities");

    transfer_to_vesting_operation op;
    op.from = "alice";
    op.to = "bob";
    op.amount = ASSET("2.500 TESTS");

    signed_transaction tx;
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.operations.push_back(op);

    BOOST_TEST_MESSAGE("--- Test failure when no signatures");
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when signed by a signature not in the account's authority");
    tx.sign(alice_post_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when duplicate signatures");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_duplicate_sig);

    BOOST_TEST_MESSAGE("--- Test failure when signed by an additional signature not in the creator's authority");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    tx.sign(bob_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_irrelevant_sig);

    BOOST_TEST_MESSAGE("--- Test success with from signature");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(transfer_to_vesting_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: transfer_to_vesting_apply");

    ACTORS((alice)(bob))
    fund("alice", 10000);

    const auto &gpo = db->get_dynamic_global_properties();

    BOOST_REQUIRE(alice.balance == ASSET("10.000 TESTS"));

    auto shares = asset(gpo.total_vesting_shares.amount, VESTS_SYMBOL);
    auto vests = asset(gpo.total_vesting_fund_steem.amount, WLS_SYMBOL);
    auto alice_shares = alice.vesting_shares;
    auto bob_shares = bob.vesting_shares;

    transfer_to_vesting_operation op;
    op.from = "alice";
    op.to = "";
    op.amount = ASSET("7.500 TESTS");

    signed_transaction tx;
    tx.operations.push_back(op);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    auto new_vest = op.amount * (shares / vests);
    shares += new_vest;
    vests += op.amount;
    alice_shares += new_vest;

    BOOST_REQUIRE(alice.balance.amount.value == ASSET("2.500 TESTS").amount.value);
    BOOST_REQUIRE(alice.vesting_shares.amount.value == alice_shares.amount.value);
    BOOST_REQUIRE(gpo.total_vesting_fund_steem.amount.value == vests.amount.value);
    BOOST_REQUIRE(gpo.total_vesting_shares.amount.value == shares.amount.value);
    validate_database();

    op.to = "bob";
    op.amount = asset(2000, WLS_SYMBOL);
    tx.operations.clear();
    tx.signatures.clear();
    tx.operations.push_back(op);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    new_vest = asset((op.amount * (shares / vests)).amount, VESTS_SYMBOL);
    shares += new_vest;
    vests += op.amount;
    bob_shares += new_vest;

    BOOST_REQUIRE(alice.balance.amount.value == ASSET("0.500 TESTS").amount.value);
    BOOST_REQUIRE(alice.vesting_shares.amount.value == alice_shares.amount.value);
    BOOST_REQUIRE(bob.balance.amount.value == ASSET("0.000 TESTS").amount.value);
    BOOST_REQUIRE(bob.vesting_shares.amount.value == bob_shares.amount.value);
    BOOST_REQUIRE(gpo.total_vesting_fund_steem.amount.value == vests.amount.value);
    BOOST_REQUIRE(gpo.total_vesting_shares.amount.value == shares.amount.value);
    validate_database();

    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), fc::exception);

    BOOST_REQUIRE(alice.balance.amount.value == ASSET("0.500 TESTS").amount.value);
    BOOST_REQUIRE(alice.vesting_shares.amount.value == alice_shares.amount.value);
    BOOST_REQUIRE(bob.balance.amount.value == ASSET("0.000 TESTS").amount.value);
    BOOST_REQUIRE(bob.vesting_shares.amount.value == bob_shares.amount.value);
    BOOST_REQUIRE(gpo.total_vesting_fund_steem.amount.value == vests.amount.value);
    BOOST_REQUIRE(gpo.total_vesting_shares.amount.value == shares.amount.value);
    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(withdraw_vesting_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: withdraw_vesting_validate");

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(withdraw_vesting_authorities) {
  try {
    BOOST_TEST_MESSAGE("Testing: withdraw_vesting_authorities");

    ACTORS((alice)(bob))
    fund("alice", 10000);
    vest("alice", 10000);

    withdraw_vesting_operation op;
    op.account = "alice";
    op.vesting_shares = ASSET("0.001000 VESTS");

    signed_transaction tx;
    tx.operations.push_back(op);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);

    //      BOOST_TEST_MESSAGE( "--- Test failure - max amount withdraw vesting is 0." );
    //      WLS_REQUIRE_THROW( db->push_transaction( tx, 0 ), fc::exception );

    BOOST_TEST_MESSAGE("--- Test failure when no signature.");
    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when no signature.");
    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test success with account signature");
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, database::skip_transaction_dupe_check);

    BOOST_TEST_MESSAGE("--- Test failure with duplicate signature");
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), tx_duplicate_sig);

    BOOST_TEST_MESSAGE("--- Test failure with additional incorrect signature");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    tx.sign(bob_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), tx_irrelevant_sig);

    BOOST_TEST_MESSAGE("--- Test failure with incorrect signature");
    tx.signatures.clear();
    tx.sign(alice_post_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), tx_missing_active_auth)

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(withdraw_vesting_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: withdraw_vesting_apply");

    ACTORS((alice))
    generate_block();
    vest("alice", ASSET("10.000 TESTS"));

    const auto &alice_obj = db->get_account("alice");
    auto old_vesting_shares = alice_obj.vesting_shares;
    withdraw_vesting_operation op;
    signed_transaction tx;

    BOOST_TEST_MESSAGE("--- Test failure withdrawing negative VESTS");
    {
      op.account = "alice";
      op.vesting_shares = asset(-1, VESTS_SYMBOL);

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());

      // BOOST_TEST_MESSAGE( "--- Test failure - max amount withdraw vesting is 0." );
      // WLS_REQUIRE_THROW( db->push_transaction( tx, 0 ), fc::exception );
      WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::assert_exception)
    }

    BOOST_TEST_MESSAGE("--- Test withdraw of existing VESTS");
    {
      op.vesting_shares = asset(alice_obj.vesting_shares.amount / 2, VESTS_SYMBOL);

      tx.clear();
      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      BOOST_REQUIRE(alice_obj.vesting_shares.amount.value == old_vesting_shares.amount.value);
      BOOST_REQUIRE(alice_obj.vesting_withdraw_rate.amount.value == (old_vesting_shares.amount / (WLS_VESTING_WITHDRAW_INTERVALS * 2)).value);
      BOOST_REQUIRE(alice_obj.to_withdraw.value == op.vesting_shares.amount.value);
      BOOST_REQUIRE(alice_obj.next_vesting_withdrawal == db->head_block_time() + WLS_VESTING_WITHDRAW_INTERVAL_SECONDS);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test changing vesting withdrawal");
    {
      tx.operations.clear();
      tx.signatures.clear();

      op.vesting_shares = asset(alice_obj.vesting_shares.amount / 3, VESTS_SYMBOL);
      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      BOOST_REQUIRE(alice_obj.vesting_shares.amount.value == old_vesting_shares.amount.value);
      BOOST_REQUIRE(alice_obj.vesting_withdraw_rate.amount.value == (old_vesting_shares.amount / (WLS_VESTING_WITHDRAW_INTERVALS * 3)).value);
      BOOST_REQUIRE(alice_obj.to_withdraw.value == op.vesting_shares.amount.value);
      BOOST_REQUIRE(alice_obj.next_vesting_withdrawal == db->head_block_time() + WLS_VESTING_WITHDRAW_INTERVAL_SECONDS);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test withdrawing more vests than available");
    {
      auto old_withdraw_amount = alice_obj.to_withdraw;
      tx.operations.clear();
      tx.signatures.clear();

      op.vesting_shares = asset(alice_obj.vesting_shares.amount * 2, VESTS_SYMBOL);
      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);

      BOOST_REQUIRE(alice_obj.vesting_shares.amount.value == old_vesting_shares.amount.value);
      BOOST_REQUIRE(alice_obj.vesting_withdraw_rate.amount.value == (old_vesting_shares.amount / (WLS_VESTING_WITHDRAW_INTERVALS * 3)).value);
      BOOST_REQUIRE(alice_obj.next_vesting_withdrawal == db->head_block_time() + WLS_VESTING_WITHDRAW_INTERVAL_SECONDS);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test withdrawing 0 to reset vesting withdraw");
    {
      tx.operations.clear();
      tx.signatures.clear();

      op.vesting_shares = asset(0, VESTS_SYMBOL);
      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      BOOST_REQUIRE(alice_obj.vesting_shares.amount.value == old_vesting_shares.amount.value);
      BOOST_REQUIRE(alice_obj.vesting_withdraw_rate.amount.value == 0);
      BOOST_REQUIRE(alice_obj.to_withdraw.value == 0);
      BOOST_REQUIRE(alice_obj.next_vesting_withdrawal == fc::time_point_sec::maximum());
    }

//    BOOST_TEST_MESSAGE("--- Test cancelling a withdraw when below the account creation fee");
//    {
//      op.vesting_shares = alice_obj.vesting_shares;
//      tx.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//      generate_block();
//
//      db_plugin->debug_update(
//          [=](database &db) {
//            auto &wso = db.get_witness_schedule_object();
//            db.modify(wso, [&](witness_schedule_object &w) { w.median_props.account_creation_fee = ASSET("10.000 TESTS"); });
//            db.modify(db.get_dynamic_global_properties(), [&](dynamic_global_property_object &gpo) {
//              gpo.current_supply += wso.median_props.account_creation_fee - ASSET("0.001 TESTS") - gpo.total_vesting_fund_steem;
//              gpo.total_vesting_fund_steem = wso.median_props.account_creation_fee - ASSET("0.001 TESTS");
//            });
//          }, database::skip_witness_signature);
//
//      withdraw_vesting_operation op;
//      signed_transaction tx;
//      op.account = "alice";
//      op.vesting_shares = ASSET("0.000000 VESTS");
//      tx.operations.push_back(op);
//      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      BOOST_REQUIRE(db->get_account("alice").vesting_withdraw_rate == ASSET("0.000000 VESTS"));
//      validate_database();
//    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(witness_update_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: withness_update_validate");

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(witness_update_authorities) {
  try {
    BOOST_TEST_MESSAGE("Testing: witness_update_authorities");

    ACTORS((alice)(bob));
    fund("alice", 10000);

    private_key_type signing_key = generate_private_key("new_key");

    witness_update_operation op;
    op.owner = "alice";
    op.url = "foo.bar";
    op.fee = ASSET("1.000 TESTS");
    op.block_signing_key = signing_key.get_public_key();

    signed_transaction tx;
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.operations.push_back(op);

    BOOST_TEST_MESSAGE("--- Test failure when no signatures");
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when signed by a signature not in the account's authority");
    tx.sign(alice_post_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when duplicate signatures");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_duplicate_sig);

    BOOST_TEST_MESSAGE("--- Test failure when signed by an additional signature not in the creator's authority");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    tx.sign(bob_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_irrelevant_sig);

    BOOST_TEST_MESSAGE("--- Test success with witness signature");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    tx.signatures.clear();
    tx.sign(signing_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), tx_missing_active_auth);
    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(witness_update_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: witness_update_apply");

    ACTORS((alice))
    fund("alice", 10000);

    private_key_type signing_key = generate_private_key("new_key");

    BOOST_TEST_MESSAGE("--- Test upgrading an account to a witness");

    witness_update_operation op;
    op.owner = "alice";
    op.url = "foo.bar";
    op.fee = ASSET("1.000 TESTS");
    op.block_signing_key = signing_key.get_public_key();
    op.props.account_creation_fee = asset(WLS_MIN_ACCOUNT_CREATION_FEE + 10, WLS_SYMBOL);
    op.props.maximum_block_size = WLS_MIN_BLOCK_SIZE_LIMIT + 100;

    signed_transaction tx;
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    const witness_object &alice_witness = db->get_witness("alice");

    BOOST_REQUIRE(alice_witness.owner == "alice");
    BOOST_REQUIRE(alice_witness.created == db->head_block_time());
    BOOST_REQUIRE(to_string(alice_witness.url) == op.url);
    BOOST_REQUIRE(alice_witness.signing_key == op.block_signing_key);
    BOOST_REQUIRE(alice_witness.props.account_creation_fee == op.props.account_creation_fee);
    BOOST_REQUIRE(alice_witness.props.maximum_block_size == op.props.maximum_block_size);
    BOOST_REQUIRE(alice_witness.total_missed == 0);
    BOOST_REQUIRE(alice_witness.last_aslot == 0);
    BOOST_REQUIRE(alice_witness.last_confirmed_block_num == 0);
    //      BOOST_REQUIRE( alice_witness.pow_worker == 0 );
    BOOST_REQUIRE(alice_witness.votes.value == 0);
    BOOST_REQUIRE(alice_witness.virtual_last_update == 0);
    BOOST_REQUIRE(alice_witness.virtual_position == 0);
    BOOST_REQUIRE(alice_witness.virtual_scheduled_time == fc::uint128_t::max_value());
    BOOST_REQUIRE(alice.balance.amount.value == ASSET("10.000 TESTS").amount.value);  // No fee
    validate_database();

    BOOST_TEST_MESSAGE("--- Test updating a witness");

    tx.signatures.clear();
    tx.operations.clear();
    op.url = "bar.foo";
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    BOOST_REQUIRE(alice_witness.owner == "alice");
    BOOST_REQUIRE(alice_witness.created == db->head_block_time());
    BOOST_REQUIRE(to_string(alice_witness.url) == "bar.foo");
    BOOST_REQUIRE(alice_witness.signing_key == op.block_signing_key);
    BOOST_REQUIRE(alice_witness.props.account_creation_fee == op.props.account_creation_fee);
    BOOST_REQUIRE(alice_witness.props.maximum_block_size == op.props.maximum_block_size);
    BOOST_REQUIRE(alice_witness.total_missed == 0);
    BOOST_REQUIRE(alice_witness.last_aslot == 0);
    BOOST_REQUIRE(alice_witness.last_confirmed_block_num == 0);
    //      BOOST_REQUIRE( alice_witness.pow_worker == 0 );
    BOOST_REQUIRE(alice_witness.votes.value == 0);
    BOOST_REQUIRE(alice_witness.virtual_last_update == 0);
    BOOST_REQUIRE(alice_witness.virtual_position == 0);
    BOOST_REQUIRE(alice_witness.virtual_scheduled_time == fc::uint128_t::max_value());
    BOOST_REQUIRE(alice.balance.amount.value == ASSET("10.000 TESTS").amount.value);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test failure when upgrading a non-existent account");

    tx.signatures.clear();
    tx.operations.clear();
    op.owner = "bob";
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_witness_vote_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_witness_vote_validate");

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_witness_vote_authorities) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_witness_vote_authorities");

    ACTORS((alice)(bob)(sam))

    fund("alice", 1000);
    private_key_type alice_witness_key = generate_private_key("alice_witness");
    witness_create("alice", alice_private_key, "foo.bar", alice_witness_key.get_public_key(), 1000);

    account_witness_vote_operation op;
    op.account = "bob";
    op.witness = "alice";

    signed_transaction tx;
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.operations.push_back(op);

    BOOST_TEST_MESSAGE("--- Test failure when no signatures");
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when signed by a signature not in the account's authority");
    tx.sign(bob_post_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when duplicate signatures");
    tx.signatures.clear();
    tx.sign(bob_private_key, db->get_chain_id());
    tx.sign(bob_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_duplicate_sig);

    BOOST_TEST_MESSAGE("--- Test failure when signed by an additional signature not in the creator's authority");
    tx.signatures.clear();
    tx.sign(bob_private_key, db->get_chain_id());
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_irrelevant_sig);

    BOOST_TEST_MESSAGE("--- Test success with witness signature");
    tx.signatures.clear();
    tx.sign(bob_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    BOOST_TEST_MESSAGE("--- Test failure with proxy signature");
    proxy("bob", "sam");
    tx.signatures.clear();
    tx.sign(sam_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), tx_missing_active_auth);

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_witness_vote_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_witness_vote_apply");

    ACTORS((alice)(bob)(sam))
    fund("alice", 5000);
    vest("alice", 5000);
    fund("sam", 1000);

    private_key_type sam_witness_key = generate_private_key("sam_key");
    witness_create("sam", sam_private_key, "foo.bar", sam_witness_key.get_public_key(), 1000);
    const witness_object &sam_witness = db->get_witness("sam");

    const auto &witness_vote_idx = db->get_index<witness_vote_index>().indices().get<by_witness_account>();

    BOOST_TEST_MESSAGE("--- Test normal vote");
    account_witness_vote_operation op;
    op.account = "alice";
    op.witness = "sam";
    op.approve = true;

    signed_transaction tx;
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    BOOST_REQUIRE(sam_witness.votes == alice.vesting_shares.amount);
    BOOST_REQUIRE(witness_vote_idx.find(std::make_tuple(sam_witness.id, alice.id)) != witness_vote_idx.end());
    validate_database();

    BOOST_TEST_MESSAGE("--- Test revoke vote");
    op.approve = false;
    tx.operations.clear();
    tx.signatures.clear();
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);
    BOOST_REQUIRE(sam_witness.votes.value == 0);
    BOOST_REQUIRE(witness_vote_idx.find(std::make_tuple(sam_witness.id, alice.id)) == witness_vote_idx.end());

    BOOST_TEST_MESSAGE("--- Test failure when attempting to revoke a non-existent vote");

    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), fc::exception);
    BOOST_REQUIRE(sam_witness.votes.value == 0);
    BOOST_REQUIRE(witness_vote_idx.find(std::make_tuple(sam_witness.id, alice.id)) == witness_vote_idx.end());

    BOOST_TEST_MESSAGE("--- Test proxied vote");
    proxy("alice", "bob");
    tx.operations.clear();
    tx.signatures.clear();
    op.approve = true;
    op.account = "bob";
    tx.operations.push_back(op);
    tx.sign(bob_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    BOOST_REQUIRE(sam_witness.votes == (bob.proxied_vsf_votes_total() + bob.vesting_shares.amount));
    BOOST_REQUIRE(witness_vote_idx.find(std::make_tuple(sam_witness.id, bob.id)) != witness_vote_idx.end());
    BOOST_REQUIRE(witness_vote_idx.find(std::make_tuple(sam_witness.id, alice.id)) == witness_vote_idx.end());

    BOOST_TEST_MESSAGE("--- Test vote from a proxied account");
    tx.operations.clear();
    tx.signatures.clear();
    op.account = "alice";
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), fc::exception);

    BOOST_REQUIRE(sam_witness.votes == (bob.proxied_vsf_votes_total() + bob.vesting_shares.amount));
    BOOST_REQUIRE(witness_vote_idx.find(std::make_tuple(sam_witness.id, bob.id)) != witness_vote_idx.end());
    BOOST_REQUIRE(witness_vote_idx.find(std::make_tuple(sam_witness.id, alice.id)) == witness_vote_idx.end());

    BOOST_TEST_MESSAGE("--- Test revoke proxied vote");
    tx.operations.clear();
    tx.signatures.clear();
    op.account = "bob";
    op.approve = false;
    tx.operations.push_back(op);
    tx.sign(bob_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    BOOST_REQUIRE(sam_witness.votes.value == 0);
    BOOST_REQUIRE(witness_vote_idx.find(std::make_tuple(sam_witness.id, bob.id)) == witness_vote_idx.end());
    BOOST_REQUIRE(witness_vote_idx.find(std::make_tuple(sam_witness.id, alice.id)) == witness_vote_idx.end());

    BOOST_TEST_MESSAGE("--- Test failure when voting for a non-existent account");
    tx.operations.clear();
    tx.signatures.clear();
    op.witness = "dave";
    op.approve = true;
    tx.operations.push_back(op);
    tx.sign(bob_private_key, db->get_chain_id());

    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test failure when voting for an account that is not a witness");
    tx.operations.clear();
    tx.signatures.clear();
    op.witness = "alice";
    tx.operations.push_back(op);
    tx.sign(bob_private_key, db->get_chain_id());

    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_witness_proxy_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_witness_proxy_validate");

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_witness_proxy_authorities) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_witness_proxy_authorities");

    ACTORS((alice)(bob))

    account_witness_proxy_operation op;
    op.account = "bob";
    op.proxy = "alice";

    signed_transaction tx;
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.operations.push_back(op);

    BOOST_TEST_MESSAGE("--- Test failure when no signatures");
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when signed by a signature not in the account's authority");
    tx.sign(bob_post_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_missing_active_auth);

    BOOST_TEST_MESSAGE("--- Test failure when duplicate signatures");
    tx.signatures.clear();
    tx.sign(bob_private_key, db->get_chain_id());
    tx.sign(bob_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_duplicate_sig);

    BOOST_TEST_MESSAGE("--- Test failure when signed by an additional signature not in the creator's authority");
    tx.signatures.clear();
    tx.sign(bob_private_key, db->get_chain_id());
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), tx_irrelevant_sig);

    BOOST_TEST_MESSAGE("--- Test success with witness signature");
    tx.signatures.clear();
    tx.sign(bob_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    BOOST_TEST_MESSAGE("--- Test failure with proxy signature");
    tx.signatures.clear();
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), tx_missing_active_auth);

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_witness_proxy_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_witness_proxy_apply");
    generate_blocks(100);
    ACTORS((alice)(bob)(sam)(dave))
    fund("alice", 1000);
    vest("alice", 1000);
    fund("bob", 3000);
    vest("bob", 3000);
    fund("sam", 5000);
    vest("sam", 5000);
    fund("dave", 7000);
    vest("dave", 7000);

    BOOST_TEST_MESSAGE("--- Test setting proxy to another account from self.");
    // bob -> alice

    account_witness_proxy_operation op;
    op.account = "bob";
    op.proxy = "alice";

    signed_transaction tx;
    tx.operations.push_back(op);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.sign(bob_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    BOOST_REQUIRE(bob.proxy == "alice");
    BOOST_REQUIRE(bob.proxied_vsf_votes_total().value == 0);
    BOOST_REQUIRE(alice.proxy == WLS_PROXY_TO_SELF_ACCOUNT);
    BOOST_REQUIRE(alice.proxied_vsf_votes_total() == bob.vesting_shares.amount);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test changing proxy");
    // bob->sam

    tx.operations.clear();
    tx.signatures.clear();
    op.proxy = "sam";
    tx.operations.push_back(op);
    tx.sign(bob_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    BOOST_REQUIRE(bob.proxy == "sam");
    BOOST_REQUIRE(bob.proxied_vsf_votes_total().value == 0);
    BOOST_REQUIRE(alice.proxied_vsf_votes_total().value == 0);
    BOOST_REQUIRE(sam.proxy == WLS_PROXY_TO_SELF_ACCOUNT);
    BOOST_REQUIRE(sam.proxied_vsf_votes_total().value == bob.vesting_shares.amount);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test failure when changing proxy to existing proxy");

    WLS_REQUIRE_THROW(db->push_transaction(tx, database::skip_transaction_dupe_check), fc::exception);

    BOOST_REQUIRE(bob.proxy == "sam");
    BOOST_REQUIRE(bob.proxied_vsf_votes_total().value == 0);
    BOOST_REQUIRE(sam.proxy == WLS_PROXY_TO_SELF_ACCOUNT);
    BOOST_REQUIRE(sam.proxied_vsf_votes_total() == bob.vesting_shares.amount);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test adding a grandparent proxy");
    // bob->sam->dave

    tx.operations.clear();
    tx.signatures.clear();
    op.proxy = "dave";
    op.account = "sam";
    tx.operations.push_back(op);
    tx.sign(sam_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    BOOST_REQUIRE(bob.proxy == "sam");
    BOOST_REQUIRE(bob.proxied_vsf_votes_total().value == 0);
    BOOST_REQUIRE(sam.proxy == "dave");
    BOOST_REQUIRE(sam.proxied_vsf_votes_total() == bob.vesting_shares.amount);
    BOOST_REQUIRE(dave.proxy == WLS_PROXY_TO_SELF_ACCOUNT);
    BOOST_REQUIRE(dave.proxied_vsf_votes_total() == (sam.vesting_shares + bob.vesting_shares).amount);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test adding a grandchild proxy");
    // alice \
  // bob->  sam->dave

    tx.operations.clear();
    tx.signatures.clear();
    op.proxy = "sam";
    op.account = "alice";
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    BOOST_REQUIRE(alice.proxy == "sam");
    BOOST_REQUIRE(alice.proxied_vsf_votes_total().value == 0);
    BOOST_REQUIRE(bob.proxy == "sam");
    BOOST_REQUIRE(bob.proxied_vsf_votes_total().value == 0);
    BOOST_REQUIRE(sam.proxy == "dave");
    BOOST_REQUIRE(sam.proxied_vsf_votes_total() == (bob.vesting_shares + alice.vesting_shares).amount);
    BOOST_REQUIRE(dave.proxy == WLS_PROXY_TO_SELF_ACCOUNT);
    BOOST_REQUIRE(dave.proxied_vsf_votes_total() == (sam.vesting_shares + bob.vesting_shares + alice.vesting_shares).amount);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test removing a grandchild proxy");
    // alice->sam->dave

    tx.operations.clear();
    tx.signatures.clear();
    op.proxy = WLS_PROXY_TO_SELF_ACCOUNT;
    op.account = "bob";
    tx.operations.push_back(op);
    tx.sign(bob_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    BOOST_REQUIRE(alice.proxy == "sam");
    BOOST_REQUIRE(alice.proxied_vsf_votes_total().value == 0);
    BOOST_REQUIRE(bob.proxy == WLS_PROXY_TO_SELF_ACCOUNT);
    BOOST_REQUIRE(bob.proxied_vsf_votes_total().value == 0);
    BOOST_REQUIRE(sam.proxy == "dave");
    BOOST_REQUIRE(sam.proxied_vsf_votes_total() == alice.vesting_shares.amount);
    BOOST_REQUIRE(dave.proxy == WLS_PROXY_TO_SELF_ACCOUNT);
    BOOST_REQUIRE(dave.proxied_vsf_votes_total() == (sam.vesting_shares + alice.vesting_shares).amount);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test votes are transferred when a proxy is added");
    account_witness_vote_operation vote;
    vote.account = "bob";
    vote.witness = WLS_INIT_MINER_NAME;
    tx.operations.clear();
    tx.signatures.clear();
    tx.operations.push_back(vote);
    tx.sign(bob_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    tx.operations.clear();
    tx.signatures.clear();
    op.account = "alice";
    op.proxy = "bob";
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    BOOST_REQUIRE(db->get_witness(WLS_INIT_MINER_NAME).votes == (alice.vesting_shares + bob.vesting_shares).amount);
    validate_database();

    BOOST_TEST_MESSAGE("--- Test votes are removed when a proxy is removed");
    op.proxy = WLS_PROXY_TO_SELF_ACCOUNT;
    tx.signatures.clear();
    tx.operations.clear();
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());

    db->push_transaction(tx, 0);

    BOOST_REQUIRE(db->get_witness(WLS_INIT_MINER_NAME).votes == bob.vesting_shares.amount);
    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(custom_authorities) {
  custom_operation op;
  op.required_auths.insert("alice");
  op.required_auths.insert("bob");

  flat_set<account_name_type> auths;
  flat_set<account_name_type> expected;

  op.get_required_owner_authorities(auths);
  BOOST_REQUIRE(auths == expected);

  op.get_required_posting_authorities(auths);
  BOOST_REQUIRE(auths == expected);

  expected.insert("alice");
  expected.insert("bob");
  op.get_required_active_authorities(auths);
  BOOST_REQUIRE(auths == expected);
}

BOOST_AUTO_TEST_CASE(custom_json_authorities) {
  custom_json_operation op;
  op.required_auths.insert("alice");
  op.required_posting_auths.insert("bob");

  flat_set<account_name_type> auths;
  flat_set<account_name_type> expected;

  op.get_required_owner_authorities(auths);
  BOOST_REQUIRE(auths == expected);

  expected.insert("alice");
  op.get_required_active_authorities(auths);
  BOOST_REQUIRE(auths == expected);

  auths.clear();
  expected.clear();
  expected.insert("bob");
  op.get_required_posting_authorities(auths);
  BOOST_REQUIRE(auths == expected);
}

BOOST_AUTO_TEST_CASE(custom_binary_authorities) {
  ACTORS((alice))

  custom_binary_operation op;
  op.required_owner_auths.insert("alice");
  op.required_active_auths.insert("bob");
  op.required_posting_auths.insert("sam");
  op.required_auths.push_back(db->get<account_authority_object, by_account>("alice").posting);

  flat_set<account_name_type> acc_auths;
  flat_set<account_name_type> acc_expected;
  vector<authority> auths;
  vector<authority> expected;

  acc_expected.insert("alice");
  op.get_required_owner_authorities(acc_auths);
  BOOST_REQUIRE(acc_auths == acc_expected);

  acc_auths.clear();
  acc_expected.clear();
  acc_expected.insert("bob");
  op.get_required_active_authorities(acc_auths);
  BOOST_REQUIRE(acc_auths == acc_expected);

  acc_auths.clear();
  acc_expected.clear();
  acc_expected.insert("sam");
  op.get_required_posting_authorities(acc_auths);
  BOOST_REQUIRE(acc_auths == acc_expected);

  expected.push_back(db->get<account_authority_object, by_account>("alice").posting);
  op.get_required_authorities(auths);
  BOOST_REQUIRE(auths == expected);
}

BOOST_AUTO_TEST_CASE(account_bandwidth) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_bandwidth");
    ACTORS((alice)(bob))
    generate_block();
    vest("alice", ASSET("10.000 TESTS"));
    fund("alice", 10000);
    vest("bob", ASSET("10.000 TESTS"));
    generate_block();
    db->skip_transaction_delta_check = false;

    signed_transaction tx;
    transfer_operation op;

    share_type total_bandwidth;

    BOOST_TEST_MESSAGE("--- Test first tx in block");
    {
      op.from = "alice";
      op.to = "bob";
      op.amount = ASSET("1.000 TESTS");

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());

      db->push_transaction(tx, 0);

      auto last_bandwidth_update =
          db->get<wpw::account_bandwidth_object, wpw::by_account_bandwidth_type>(boost::make_tuple("alice", wpw::bandwidth_type::market)).last_bandwidth_update;
      auto average_bandwidth =
          db->get<wpw::account_bandwidth_object, wpw::by_account_bandwidth_type>(boost::make_tuple("alice", wpw::bandwidth_type::market)).average_bandwidth;
      BOOST_REQUIRE(last_bandwidth_update == db->head_block_time());
      BOOST_REQUIRE(average_bandwidth == fc::raw::pack_size(tx) * 10 * WLS_BANDWIDTH_PRECISION);
      total_bandwidth = average_bandwidth;
    }

    BOOST_TEST_MESSAGE("--- Test second tx in block");
    {
      op.amount = ASSET("0.100 TESTS");
      tx.clear();
      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());

      db->push_transaction(tx, 0);

      auto last_bandwidth_update =
          db->get<wpw::account_bandwidth_object, wpw::by_account_bandwidth_type>(boost::make_tuple("alice", wpw::bandwidth_type::market)).last_bandwidth_update;
      auto average_bandwidth =
          db->get<wpw::account_bandwidth_object, wpw::by_account_bandwidth_type>(boost::make_tuple("alice", wpw::bandwidth_type::market)).average_bandwidth;
      BOOST_REQUIRE(last_bandwidth_update == db->head_block_time());
      BOOST_REQUIRE(average_bandwidth == total_bandwidth + fc::raw::pack_size(tx) * 10 * WLS_BANDWIDTH_PRECISION);
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_daily_bandwidth) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_bandwidth");
    generate_blocks(WLS_BLOCKS_PER_DAY/24); // generate blocks for 1 day to fill up daily bandwidth for initminer
//    generate_block();
//    db->modify(db->get_dynamic_global_properties(), [&](dynamic_global_property_object &gpo) {
//      gpo.maximum_block_size = 130816;
//    });
    generate_block();
    ACTORS((whale)(testm1)(test0)(test01)(test1)(test2)(test3)(test4)(test5)(test6))
    generate_block();
    vest("whale", ASSET("9000000.000 TESTS"));
//    vest("testm1", ASSET("0.100 TESTS"));
    vest("test0", ASSET("1.000 TESTS"));
    vest("test01", ASSET("1.500 TESTS"));
    vest("test1", ASSET("10.000 TESTS"));
    vest("test2", ASSET("100.000 TESTS"));
    vest("test3", ASSET("1000.000 TESTS"));
    vest("test4", ASSET("10000.000 TESTS"));
    vest("test5", ASSET("100000.000 TESTS"));
    vest("test6", ASSET("1000000.000 TESTS"));
    generate_block();

    const auto &props = db->get_dynamic_global_properties();

    auto get_bandwidth_limit = [&] (const string& acc_name) {
      const auto &acc_obj = db->get_account(acc_name);
      auto whalestake = acc_obj.vesting_shares * props.get_vesting_share_price();
      auto daily_bw_limit = wls::plugins::witness::simple_daily_bandwidth_limit(whalestake.amount.value);
      return daily_bw_limit;
    };

    BOOST_TEST_MESSAGE("--- Test whale");
    {
      signed_transaction tx;
      custom_json_operation op;

      op.required_posting_auths.insert("whale");
      op.json = "{\"test\":\"value\"}";

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(whale_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      const auto &daba = db->get<wpw::account_daily_bandwidth_object, wpw::by_account>("whale");
      const auto max = get_bandwidth_limit("whale");
      ilog("bw=${b}, max=${max}", ("b", daba)("max", max));

      BOOST_REQUIRE((max >= 128*1024) && (max <= 256*1024)); // no account could have more than 1MB daily bandwidth no matter how many staked.
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test testm1 for 0.1 WHALESTAKE");
    {
      signed_transaction tx;
      custom_json_operation op;

      op.required_posting_auths.insert("testm1");
      op.json = "{\"test\":\"value\"}";

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(testm1_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      const auto &daba = db->get<wpw::account_daily_bandwidth_object, wpw::by_account>("testm1");
      const auto max = get_bandwidth_limit("testm1");
      ilog("bw=${b}, max=${max}", ("b", daba)("max", max));
      BOOST_REQUIRE(max <= 1024);
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test test0 for 1 WHALESTAKE");
    {
      signed_transaction tx;
      custom_json_operation op;

      op.required_posting_auths.insert("test0");
      op.json = "{\"test\":\"value\"}";

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(test0_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      const auto &daba = db->get<wpw::account_daily_bandwidth_object, wpw::by_account>("test0");
      const auto max = get_bandwidth_limit("test0");
      ilog("bw=${b}, max=${max}", ("b", daba)("max", max));
      BOOST_REQUIRE((max >= 2*1024) && (max <= 4*1024));
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test test01 for 1.5 WHALESTAKE");
    {
      signed_transaction tx;
      custom_json_operation op;

      op.required_posting_auths.insert("test01");
      op.json = "{\"test\":\"value\"}";

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(test01_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      const auto &daba = db->get<wpw::account_daily_bandwidth_object, wpw::by_account>("test01");
      const auto max = get_bandwidth_limit("test01");
      ilog("bw=${b}, max=${max}", ("b", daba)("max", max));
      BOOST_REQUIRE((max >= 2*1024) && (max <= 4*1024));
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test test1 for 10 WHALESTAKE");
    {
      signed_transaction tx;
      custom_json_operation op;

      op.required_posting_auths.insert("test1");
      op.json = "{\"test\":\"value\"}";

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(test1_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      const auto &daba = db->get<wpw::account_daily_bandwidth_object, wpw::by_account>("test1");
      const auto max = get_bandwidth_limit("test1");
      ilog("bw=${b}, max=${max}", ("b", daba)("max", max));
      BOOST_REQUIRE((max >= 4*1024) && (max <= 8*1024));
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test test2 for 100 WHALESTAKE");
    {
      signed_transaction tx;
      custom_json_operation op;

      op.required_posting_auths.insert("test2");
      op.json = "{\"test\":\"value\"}";

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(test2_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      const auto &daba = db->get<wpw::account_daily_bandwidth_object, wpw::by_account>("test2");
      const auto max = get_bandwidth_limit("test2");
      ilog("bw=${b}, max=${max}", ("b", daba)("max", max));
      BOOST_REQUIRE((max >= 8*1024) && (max <= 16*1024));
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test test3 for 1000 WHALESTAKE");
    {
      signed_transaction tx;
      custom_json_operation op;

      op.required_posting_auths.insert("test3");
      op.json = "{\"test\":\"value\"}";

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(test3_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      const auto &daba = db->get<wpw::account_daily_bandwidth_object, wpw::by_account>("test3");
      const auto max = get_bandwidth_limit("test3");
      ilog("bw=${b}, max=${max}", ("b", daba)("max", max));
      BOOST_REQUIRE((max >= 16*1024) && (max <= 32*1024));
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test test4 for 10000 WHALESTAKE");
    {
      signed_transaction tx;
      custom_json_operation op;

      op.required_posting_auths.insert("test4");
      op.json = "{\"test\":\"value\"}";

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(test4_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      const auto &daba = db->get<wpw::account_daily_bandwidth_object, wpw::by_account>("test4");
      const auto max = get_bandwidth_limit("test4");
      ilog("bw=${b}, max=${max}", ("b", daba)("max", max));
      BOOST_REQUIRE((max >= 32*1024) && (max <= 64*1024));
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test test5 for 100000 WHALESTAKE");
    {
      signed_transaction tx;
      custom_json_operation op;

      op.required_posting_auths.insert("test5");
      op.json = "{\"test\":\"value\"}";

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(test5_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      const auto &daba = db->get<wpw::account_daily_bandwidth_object, wpw::by_account>("test5");
      const auto max = get_bandwidth_limit("test5");
      ilog("bw=${b}, max=${max}", ("b", daba)("max", max));
      BOOST_REQUIRE((max >= 64*1024) && (max <= 128*1024));
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test test6 for 1000000 WHALESTAKE");
    {
      signed_transaction tx;
      custom_json_operation op;

      op.required_posting_auths.insert("test6");
      op.json = "{\"test\":\"value\"}";

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(test6_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      const auto &daba = db->get<wpw::account_daily_bandwidth_object, wpw::by_account>("test6");
      const auto max = get_bandwidth_limit("test6");
      ilog("bw=${b}, max=${max}", ("b", daba)("max", max));
      BOOST_REQUIRE((max >= 128*1024) && (max <= 256*1024));
      generate_block();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(claim_reward_balance_validate) {
  try {
     claim_reward_balance_operation op;
     op.account = "alice";
     op.reward_steem = ASSET("0.000 TESTS");
     op.reward_vests = ASSET("0.000000 VESTS");

     BOOST_TEST_MESSAGE("Testing all 0 amounts");
     WLS_REQUIRE_THROW(op.validate(), fc::assert_exception);


     BOOST_TEST_MESSAGE("Testing single reward claims");
     op.reward_steem.amount = 1000;
     op.validate();

     op.reward_vests.amount = 1000;
     op.validate();

     op.reward_vests.amount = 0;


     BOOST_TEST_MESSAGE("Testing wrong WLS symbol");
     op.reward_steem = ASSET("1.000 WRONG");
     WLS_REQUIRE_THROW(op.validate(), fc::assert_exception);

     BOOST_TEST_MESSAGE("Testing wrong VESTS symbol");
     op.reward_vests = ASSET("1.000000 WRONG");
     WLS_REQUIRE_THROW(op.validate(), fc::assert_exception);


     BOOST_TEST_MESSAGE("Testing a single negative amount");
     op.reward_steem.amount = -1000;
     WLS_REQUIRE_THROW(op.validate(), fc::assert_exception);
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(claim_reward_balance_authorities) {
  try {
     BOOST_TEST_MESSAGE("Testing: decline_voting_rights_authorities");

     claim_reward_balance_operation op;
     op.account = "alice";

     flat_set<account_name_type> auths;
     flat_set<account_name_type> expected;

     op.get_required_owner_authorities(auths);
     BOOST_REQUIRE(auths == expected);

     op.get_required_active_authorities(auths);
     BOOST_REQUIRE(auths == expected);

     expected.insert("alice");
     op.get_required_posting_authorities(auths);
     BOOST_REQUIRE(auths == expected);
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(claim_reward_balance_apply) {
  try {
     BOOST_TEST_MESSAGE("Testing: claim_reward_balance_apply");
     BOOST_TEST_MESSAGE("--- Setting up test state");

     ACTORS((alice))
     generate_block();

     // TODO: fix this!

//      db_plugin->debug_update( [=]( database& db )
//      {
//         db->modify( db->get_account( "alice" ), [&]( account_object& a )
//         {
//            a.reward_steem_balance = ASSET( "10.000 TESTS" );
//            a.reward_vesting_balance = ASSET( "10.000000 VESTS" );
//            a.reward_vesting_steem = ASSET( "10.000 TESTS" );
//         });
//
//         db->modify( db->get_dynamic_global_properties(), [&]( dynamic_global_property_object& gpo )
//         {
//            gpo.current_supply += ASSET( "20.000 TESTS" );
//            gpo.pending_rewarded_vesting_shares += ASSET( "10.000000 VESTS" );
//            gpo.pending_rewarded_vesting_steem += ASSET( "10.000 TESTS" );
//         });
//      });
//
//      generate_block();
//      validate_database();
//
//      auto alice_steem = db->get_account( "alice" ).balance;
//      auto alice_vests = db->get_account( "alice" ).vesting_shares;
//
//
//      BOOST_TEST_MESSAGE( "--- Attempting to claim more WLS than exists in the reward balance." );
//
//      claim_reward_balance_operation op;
//      signed_transaction tx;
//
//      op.account = "alice";
//      op.reward_steem = ASSET( "20.000 TESTS" );
//      op.reward_vests = ASSET( "0.000000 VESTS" );
//
//      tx.operations.push_back( op );
//      tx.set_expiration( db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION );
//      tx.sign( alice_private_key, db->get_chain_id() );
//      WLS_REQUIRE_THROW( db->push_transaction( tx, 0 ), fc::assert_exception );
//
//
//      BOOST_TEST_MESSAGE( "--- Claiming a partial reward balance" );
//
//      op.reward_steem = ASSET( "0.000 TESTS" );
//      op.reward_vests = ASSET( "5.000000 VESTS" );
//      tx.clear();
//      tx.operations.push_back( op );
//      tx.sign( alice_private_key, db->get_chain_id() );
//      db->push_transaction( tx, 0 );
//
//      BOOST_REQUIRE( db->get_account( "alice" ).balance == alice_steem + op.reward_steem );
//      BOOST_REQUIRE( db->get_account( "alice" ).reward_steem_balance == ASSET( "10.000 TESTS" ) );
//      BOOST_REQUIRE( db->get_account( "alice" ).vesting_shares == alice_vests + op.reward_vests );
//      BOOST_REQUIRE( db->get_account( "alice" ).reward_vesting_balance == ASSET( "5.000000 VESTS" ) );
//      BOOST_REQUIRE( db->get_account( "alice" ).reward_vesting_steem == ASSET( "5.000 TESTS" ) );
//      validate_database();
//
//      alice_vests += op.reward_vests;
//
//
//      BOOST_TEST_MESSAGE( "--- Claiming the full reward balance" );
//
//      op.reward_steem = ASSET( "10.000 TESTS" );
//      tx.clear();
//      tx.operations.push_back( op );
//      tx.sign( alice_private_key, db->get_chain_id() );
//      db->push_transaction( tx, 0 );
//
//      BOOST_REQUIRE( db->get_account( "alice" ).balance == alice_steem + op.reward_steem );
//      BOOST_REQUIRE( db->get_account( "alice" ).reward_steem_balance == ASSET( "0.000 TESTS" ) );
//      BOOST_REQUIRE( db->get_account( "alice" ).vesting_shares == alice_vests + op.reward_vests );
//      BOOST_REQUIRE( db->get_account( "alice" ).reward_vesting_balance == ASSET( "0.000000 VESTS" ) );
//      BOOST_REQUIRE( db->get_account( "alice" ).reward_vesting_steem == ASSET( "0.000 TESTS" ) );
     validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(comment_beneficiaries_validate) {
  try {
    BOOST_TEST_MESSAGE("Test Comment Beneficiaries Validate");
    comment_options_operation op;
    comment_payout_beneficiaries b;

    op.author = "alice";
    op.permlink = "test";

    BOOST_TEST_MESSAGE("--- Testing more than 100% weight on a single route");
    {
      b.beneficiaries.push_back(beneficiary_route_type(account_name_type("bob"), WLS_100_PERCENT + 1));
      op.extensions.insert(b);
      WLS_REQUIRE_THROW(op.validate(), fc::assert_exception)
    }

    BOOST_TEST_MESSAGE("--- Testing more than 100% total weight");
    {
      b.beneficiaries.clear();
      b.beneficiaries.push_back(beneficiary_route_type(account_name_type("bob"), WLS_1_PERCENT * 75));
      b.beneficiaries.push_back(beneficiary_route_type(account_name_type("sam"), WLS_1_PERCENT * 75));
      op.extensions.clear();
      op.extensions.insert(b);
      WLS_REQUIRE_THROW(op.validate(), fc::assert_exception);
    }

    BOOST_TEST_MESSAGE("--- Testing maximum number of routes");
    {
      b.beneficiaries.clear();
      for (size_t i = 0; i < 8; i++) {
        b.beneficiaries.push_back(beneficiary_route_type(account_name_type("foo" + fc::to_string(i)), 1));
      }

      op.extensions.clear();
      std::sort(b.beneficiaries.begin(), b.beneficiaries.end());
      op.extensions.insert(b);
      op.validate();
    }

    BOOST_TEST_MESSAGE("--- Testing one too many routes");
    {
      b.beneficiaries.push_back(beneficiary_route_type(account_name_type("bar"), 1));
      std::sort(b.beneficiaries.begin(), b.beneficiaries.end());
      op.extensions.clear();
      op.extensions.insert(b);
      WLS_REQUIRE_THROW(op.validate(), fc::assert_exception);
    }

    BOOST_TEST_MESSAGE("--- Testing duplicate accounts");
    {
      b.beneficiaries.clear();
      b.beneficiaries.push_back(beneficiary_route_type("bob", WLS_1_PERCENT * 2));
      b.beneficiaries.push_back(beneficiary_route_type("bob", WLS_1_PERCENT));
      op.extensions.clear();
      op.extensions.insert(b);
      WLS_REQUIRE_THROW(op.validate(), fc::assert_exception);
    }

    BOOST_TEST_MESSAGE("--- Testing incorrect account sort order");
    {
      b.beneficiaries.clear();
      b.beneficiaries.push_back(beneficiary_route_type("bob", WLS_1_PERCENT));
      b.beneficiaries.push_back(beneficiary_route_type("alice", 2 * WLS_1_PERCENT));
      op.extensions.clear();
      op.extensions.insert(b);
      WLS_REQUIRE_THROW(op.validate(), fc::assert_exception);
    }

    BOOST_TEST_MESSAGE("--- Testing correct account sort order");
    {
      b.beneficiaries.clear();
      b.beneficiaries.push_back(beneficiary_route_type("alice", WLS_1_PERCENT));
      b.beneficiaries.push_back(beneficiary_route_type("bob", WLS_1_PERCENT));
      op.extensions.clear();
      op.extensions.insert(b);
      op.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(comment_beneficiaries_apply) {
  try {
//    TODO: use social_action instead

//    BOOST_TEST_MESSAGE("Test Comment Beneficiaries");
//    generate_blocks(100);
//    ACTORS((alice)(bob)(sam)(dave))
//
//    comment_operation comment;
//    vote_operation vote;
//    comment_options_operation op;
//    comment_payout_beneficiaries b;
//    signed_transaction tx;
//
//    {
//      comment.author = "alice";
//      comment.permlink = "test";
//      comment.parent_permlink = "test";
//      comment.title = "test";
//      comment.body = "foobar";
//
//      tx.operations.push_back(comment);
//      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx);
//    }
//
//    BOOST_TEST_MESSAGE("--- Test failure on more than 8 benefactors");
//    {
//      b.beneficiaries.push_back(beneficiary_route_type(account_name_type("bob"), WLS_1_PERCENT));
//      for (size_t i = 0; i < 8; i++)
//        b.beneficiaries.push_back(beneficiary_route_type(account_name_type(WLS_INIT_MINER_NAME + fc::to_string(i)), WLS_1_PERCENT));
//
//      op.author = "alice";
//      op.permlink = "test";
//      op.allow_curation_rewards = false;
//      op.extensions.insert(b);
//      tx.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      WLS_REQUIRE_THROW(db->push_transaction(tx), fc::assert_exception)
//    }
//
//    BOOST_TEST_MESSAGE("--- Test specifying a non-existent benefactor");
//    {
//      b.beneficiaries.clear();
//      b.beneficiaries.push_back(beneficiary_route_type(account_name_type("doug"), WLS_1_PERCENT));
//      op.extensions.clear();
//      op.extensions.insert(b);
//      tx.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      WLS_REQUIRE_THROW(db->push_transaction(tx), fc::assert_exception);
//    }
//
//    BOOST_TEST_MESSAGE("--- Test setting when comment has been voted on");
//    {
//      vote.author = "alice";
//      vote.permlink = "test";
//      vote.voter = "bob";
//      vote.weight = WLS_100_PERCENT;
//
//      b.beneficiaries.clear();
//      b.beneficiaries.push_back(beneficiary_route_type(account_name_type("bob"), 25 * WLS_1_PERCENT));
//      b.beneficiaries.push_back(beneficiary_route_type(account_name_type("sam"), 50 * WLS_1_PERCENT));
//      op.extensions.clear();
//      op.extensions.insert(b);
//
//      tx.clear();
//      tx.operations.push_back(vote);
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      tx.sign(bob_private_key, db->get_chain_id());
//      WLS_REQUIRE_THROW(db->push_transaction(tx), fc::assert_exception);
//    }
//
//    BOOST_TEST_MESSAGE("--- Test success");
//    {
//      tx.clear();
//      tx.operations.push_back(op);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx);
//    }
//
//    BOOST_TEST_MESSAGE("--- Test setting when there are already beneficiaries");
//    {
//      b.beneficiaries.clear();
//      b.beneficiaries.push_back(beneficiary_route_type(account_name_type("dave"), 25 * WLS_1_PERCENT));
//      op.extensions.clear();
//      op.extensions.insert(b);
//      tx.sign(alice_private_key, db->get_chain_id());
//      WLS_REQUIRE_THROW(db->push_transaction(tx), fc::assert_exception);
//    }
//
//    BOOST_TEST_MESSAGE("--- Payout and verify rewards were split properly");
//    {
//      tx.clear();
//      tx.operations.push_back(vote);
//      tx.sign(bob_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//
//      generate_blocks(db->get_comment("alice", string("test")).cashout_time - WLS_BLOCK_INTERVAL);
//
//      db_plugin->debug_update([=](database &db) {
//        db.modify(db.get_dynamic_global_properties(), [&](dynamic_global_property_object &gpo) {
//          gpo.current_supply -= gpo.total_reward_fund_steem;
//          gpo.total_reward_fund_steem = ASSET("100.000 TESTS");
//          gpo.current_supply += gpo.total_reward_fund_steem;
//        });
//      });
//
//      generate_block();
//
//      BOOST_REQUIRE(db->get_account("bob").reward_steem_balance == ASSET("0.000 TESTS"));
//      BOOST_REQUIRE(db->get_account("bob").reward_vesting_steem.amount + db->get_account("sam").reward_vesting_steem.amount ==
//                    db->get_comment("alice", string("test")).beneficiary_payout_value.amount);
//      BOOST_REQUIRE((db->get_account("alice").reward_vesting_steem.amount) == db->get_account("bob").reward_vesting_steem.amount);
//      BOOST_REQUIRE((db->get_account("alice").reward_vesting_steem.amount) * 2 == db->get_account("sam").reward_vesting_steem.amount);
//    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_validate");
    /**
     * Break this test into:
     * - account_action_create_pod
     * - account_action_transfer_to_tip
     * - account_action_htlc_create
     * - account_action_htlc_update
     */
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_create_pod_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_create_pod_validate");

    ACTORS((alice))
    generate_block();
    fund("alice", WLS_MIN_POD_CREATION_FEE);
    generate_block();

    BOOST_TEST_MESSAGE("--- Test valid operation");
    {
      account_action_create_pod action;
      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();
    }

    BOOST_TEST_MESSAGE("--- Test failure on negative join_fee");
    {
      account_action_create_pod action;
      action.join_fee = asset(-1, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      WLS_REQUIRE_THROW(action.validate(), fc::assert_exception);
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_transfer_to_tip_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_transfer_to_tip_validate");
    generate_block();
    ACTORS((alice))

    BOOST_TEST_MESSAGE("--- Test valid operation");
    {
      account_action_transfer_to_tip action;
      action.amount = asset(1000, WLS_SYMBOL);
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_htlc_create_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_htlc_create_validate");
    generate_block();
    ACTORS((alice)(bob));
    fund("alice", 100000);

    BOOST_TEST_MESSAGE("--- Test valid operation");
    {
      account_action_htlc_create action;
      action.fee = asset(0, WLS_SYMBOL);
      action.reward = asset(0, WLS_SYMBOL);
      action.to = "bob";
      action.amount = asset(1000, WLS_SYMBOL);
      action.preimage_hash = htlc_sha256{fc::sha256::hash(std::string("secret"))};
      action.preimage_size = 0;
      action.expiration = 24 * 60 * 60;
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_htlc_update_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_htlc_update_validate");
    generate_block();
    ACTORS((alice)(bob));
    fund("alice", 100000);

    BOOST_TEST_MESSAGE("--- Test valid operation");
    {
      account_action_htlc_update action;
      action.fee = asset(0, WLS_SYMBOL);

      std::stringstream store;
      store << std::string("alice") << std::string("bob") << fc::sha256::hash(std::string("secret"));
      action.htlc_id = fc::ripemd160::hash( store.str() );
      action.seconds = 24 * 60 * 60;
      action.reward = asset(0, WLS_SYMBOL);

      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_htlc_redeem_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_htlc_redeem_validate");
    generate_block();
    ACTORS((alice)(bob));
    fund("alice", 100000);

    BOOST_TEST_MESSAGE("--- Test valid operation");
    {
      account_action_htlc_redeem action;
      std::string preimage = "test";
      action.fee = asset(0, WLS_SYMBOL);

      std::stringstream store;
      store << std::string("alice") << std::string("bob") <<  fc::sha256::hash(preimage);
      action.htlc_id = fc::ripemd160::hash( store.str() );

      std::vector<char> preimage_v(preimage.begin(), preimage.end());
      action.preimage = preimage_v;

      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_authorities) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_authorities");
    // TODO:

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_apply");
    /**
     * break this test into
     * - account_action_create_pod
     * - account_action_transfer_to_tip
     * - account_action_htlc_create
     * - account_action_htlc_update
     * - account_action_htlc_redeem
     */
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_create_pod_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_create_pod_apply");

    ACTORS((alice))
    generate_block();
    fund("alice", 2*WLS_MIN_POD_CREATION_FEE);
    generate_block();

    signed_transaction tx;
    account_action_operation op;
    account_action_create_pod action;

    {
      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "alice";
      op.action = action;
      op.validate();

      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    }

    BOOST_TEST_MESSAGE("--- Test failure on not existing account");
    {
      tx.signatures.clear();
      tx.operations.clear();

      op.account = "alice2";
      op.validate();

      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test failure on insufficient fee");
    {
      tx.signatures.clear();
      tx.operations.clear();

      action.fee = asset(WLS_MIN_POD_CREATION_FEE-1, WLS_SYMBOL);
      action.validate();

      op.account = "alice";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test failure on join_fee exceed max");
    {
      tx.signatures.clear();
      tx.operations.clear();

      action.join_fee = asset(WLS_POD_MAX_JOIN_FEE + 1, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "alice";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test normal");
    {
      tx.signatures.clear();
      tx.operations.clear();

      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "alice";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test failure on duplicated pod");
    {
      generate_block();
      fund("alice", WLS_MIN_POD_CREATION_FEE);
      generate_block();

      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);

      tx.signatures.clear();
      tx.operations.clear();

      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "alice";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_transfer_to_tip_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_transfer_to_tip_apply");

    ACTORS((alice))
    fund("alice", 100000);
    generate_block();

    signed_transaction tx;
    account_action_operation op;
    account_action_transfer_to_tip action;

    BOOST_TEST_MESSAGE("--- Test normal");
    {
      tx.signatures.clear();
      tx.operations.clear();

      action.amount = asset(1000, WLS_SYMBOL);
      action.validate();

      op.account = "alice";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_htlc_create_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_htlc_create_apply");

    ACTORS((alice)(bob))
    fund("alice", 100000);
    generate_block();

    signed_transaction tx;
    account_action_operation op;
    account_action_htlc_create action;

    BOOST_TEST_MESSAGE("--- Test normal");
    {
      tx.signatures.clear();
      tx.operations.clear();

      action.fee = asset(0, WLS_SYMBOL);
      action.reward = asset(0, WLS_SYMBOL);
      action.to = "bob";
      action.amount = asset(1000, WLS_SYMBOL);
      action.preimage_hash = htlc_sha256{fc::sha256::hash(std::string("secret"))};
      action.preimage_size = 0;
      action.expiration = 24 * 60 * 60;
      action.validate();

      op.account = "alice";
      op.action = action;
      op.validate();

      ilog("${op}", ("op", fc::json::to_string(op)));

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_htlc_update_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_htlc_update_apply");

    ACTORS((alice)(bob))
    fund("alice", 100000);
    generate_block();

    signed_transaction tx;
    account_action_operation op;
    account_action_htlc_update action;

    { // create a htlc first before updating it
      tx.signatures.clear();
      tx.operations.clear();

      account_action_htlc_create action_htlc_create;
      action_htlc_create.fee = asset(0, WLS_SYMBOL);
      action_htlc_create.reward = asset(0, WLS_SYMBOL);
      action_htlc_create.to = "bob";
      action_htlc_create.amount = asset(1000, WLS_SYMBOL);
      action_htlc_create.preimage_hash = htlc_sha256{fc::sha256::hash(std::string("secret"))};
      action_htlc_create.preimage_size = 0;
      action_htlc_create.expiration = 24 * 60 * 60;
      action_htlc_create.validate();

      op.account = "alice";
      op.action = action_htlc_create;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test normal");
    {
      tx.signatures.clear();
      tx.operations.clear();

      action.fee = asset(0, WLS_SYMBOL);

      std::stringstream store;
      store << std::string("alice") << std::string("bob") << fc::sha256::hash(std::string("secret")).str();
      action.htlc_id = fc::ripemd160::hash( store.str() );
      action.seconds = 24 * 60 * 60;
      action.reward = asset(0, WLS_SYMBOL);
      action.validate();

      op.account = "alice";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(account_action_htlc_redeem_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: account_action_htlc_redeem_apply");

    ACTORS((alice)(bob))
    fund("alice", 100000);
    generate_block();

    signed_transaction tx;
    account_action_operation op;
    account_action_htlc_redeem action;

    { // create a htlc first before redeem
      tx.signatures.clear();
      tx.operations.clear();

      account_action_htlc_create action_htlc_create;
      action_htlc_create.fee = asset(0, WLS_SYMBOL);
      action_htlc_create.reward = asset(0, WLS_SYMBOL);
      action_htlc_create.to = "bob";
      action_htlc_create.amount = asset(1000, WLS_SYMBOL);
      action_htlc_create.preimage_hash = htlc_sha256{fc::sha256::hash(std::string("secret"))};
      action_htlc_create.preimage_size = 0;
      action_htlc_create.expiration = 24 * 60 * 60;
      action_htlc_create.validate();

      op.account = "alice";
      op.action = action_htlc_create;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test normal");
    {
      tx.signatures.clear();
      tx.operations.clear();

      std::string preimage = "secret";
      action.fee = asset(0, WLS_SYMBOL);

      std::stringstream store;
      store << std::string("alice") << std::string("bob") << fc::sha256::hash(std::string("secret")).str();
      action.htlc_id = fc::ripemd160::hash( store.str() );

      std::vector<char> preimage_v(preimage.begin(), preimage.end());
      action.preimage = preimage_v;
      action.validate();

      op.account = "alice";
      op.action = action;
      op.validate();

      ilog("${op}", ("op", fc::json::to_string(op)));

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_validate) {
  /**
   * break this test into
   * - pod_action_join_request_validate
   * - pod_action_cancel_join_request_validate
   * - pod_action_accept_join_request_validate
   * - pod_action_reject_join_request_validate
   * - pod_action_leave_validate
   * - pod_action_update_validate
   * - pod_action_kick_validate
   */
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_validate");
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_join_request_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_join_request_validate");
    BOOST_TEST_MESSAGE("--- Test failure on invalid join_fee symbol");
    {
      pod_action_join_request action;
      action.join_fee = asset(0, VESTS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";
      BOOST_REQUIRE_THROW(action.validate(), fc::exception);
    }

    BOOST_TEST_MESSAGE("--- Test failure on negative join_fee");
    {
      pod_action_join_request action;
      action.join_fee = asset(-1, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";
      BOOST_REQUIRE_THROW(action.validate(), fc::exception);
    }

    BOOST_TEST_MESSAGE("--- Test valid on normal case");
    {
      pod_action_join_request action;
      action.join_fee = asset(0, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_cancel_join_request_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_cancel_join_request_validate");
    // nothing to test
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_accept_join_request_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_accept_join_request_validate");
    BOOST_TEST_MESSAGE("--- Test valid on normal case");
    {
      pod_action_accept_join_request action;
      action.account  = "alice";
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_reject_join_request_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_reject_join_request_validate");
    BOOST_TEST_MESSAGE("--- Test valid on normal case");
    {
      pod_action_reject_join_request action;
      action.account = "alice";
      action.memo = "a memo";
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_leave_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_leave_validate");
    // nothing to test
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_update_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_update_validate");

    BOOST_TEST_MESSAGE("--- Test valid operation");
    {
      pod_action_update action;
      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.allow_join = true;
      action.validate();
    }

    BOOST_TEST_MESSAGE("--- Test failure on negative join_fee");
    {
      pod_action_update action;
      action.join_fee = asset(-1, WLS_SYMBOL);
      action.json_metadata = "";
      action.allow_join = true;
      WLS_REQUIRE_THROW(action.validate(), fc::assert_exception);
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_kick_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_kick_validate");
    BOOST_TEST_MESSAGE("--- Test valid on normal case");
    {
      pod_action_kick action;
      action.account = "alice";
      action.memo = "a memo";
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_authorities) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_authorities");
    // TODO:

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_apply) {
  try {
    /**
     * break this test into
     * - pod_action_join_request_apply
     * - pod_action_cancel_join_request_apply
     * - pod_action_accept_join_request_apply
     * - pod_action_reject_join_request_apply
     * - pod_action_leave_apply
     * - pod_action_update_apply
     * - pod_action_kick_apply
     */
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_join_request_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_join_request_apply");

    ACTORS((alice)(pod)(nonpod))
    generate_block();
    fund("alice", 10000);
    fund("pod", WLS_MIN_POD_CREATION_FEE);
    generate_block();

    {
      signed_transaction tx;
      account_action_operation op;
      account_action_create_pod action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on unknown pod/account");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_join_request action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";

      op.account = "alice";
      op.pod = "unknown";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
    }

    BOOST_TEST_MESSAGE("--- Test failure on a plain account");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_join_request action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";

      op.account = "alice";
      op.pod = "nonpod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
    }

    BOOST_TEST_MESSAGE("--- Test normal");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_join_request action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";

      op.account = "alice";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on creator makes the pod join request.");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_join_request action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
      generate_block();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_cancel_join_request_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_cancel_join_request_apply");

    ACTORS((alice)(pod)(nonpod))
    generate_block();
    fund("alice", 10000);
    fund("pod", WLS_MIN_POD_CREATION_FEE);
    generate_block();

    {
      signed_transaction tx;
      account_action_operation op;
      account_action_create_pod action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure of cancel on non exist request");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_cancel_join_request action;

      op.account = "alice";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_accept_join_request_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_accept_join_request_apply");

    ACTORS((alice)(pod))
    generate_block();
    fund("alice", WLS_MIN_POD_JOIN_REQUEST_FEE);
    fund("pod", WLS_MIN_POD_CREATION_FEE);
    generate_block();

    {
      signed_transaction tx;
      account_action_operation op;
      account_action_create_pod action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_join_request action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";

      op.account = "alice";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on invalid pod creator");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_accept_join_request action;

      action.account = "alice";

      op.account = "nopod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on invalid account");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_accept_join_request action;

      action.account = "alice2";

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test normal");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_accept_join_request action;

      action.account = "alice";

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on accepting twice");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_accept_join_request action;

      action.account = "alice";

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
      generate_block();
    }

  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_reject_join_request_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_reject_join_request_apply");

    ACTORS((alice)(pod))
    generate_block();
    fund("alice", WLS_MIN_POD_JOIN_REQUEST_FEE);
    fund("pod", WLS_MIN_POD_CREATION_FEE);
    generate_block();

    {
      signed_transaction tx;
      account_action_operation op;
      account_action_create_pod action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_join_request action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";

      op.account = "alice";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test normal");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_reject_join_request action;

      action.account = "alice";
      action.memo = "a memo";

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_leave_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_leave_apply");

    ACTORS((alice)(pod))
    generate_block();
    fund("alice", WLS_MIN_POD_JOIN_REQUEST_FEE);
    fund("pod", WLS_MIN_POD_CREATION_FEE);
    generate_block();

    {
      signed_transaction tx;
      account_action_operation op;
      account_action_create_pod action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_join_request action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";

      op.account = "alice";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_accept_join_request action;

      action.account = "alice";

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on creator leaves");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_leave action;

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test normal");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_leave action;

      op.account = "alice";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_update_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: pod_action_update_apply");

    ACTORS((alice)(pod))
    generate_block();
    fund("alice", WLS_MIN_POD_JOIN_REQUEST_FEE);
    fund("pod", WLS_MIN_POD_CREATION_FEE);
    generate_block();

    {
      signed_transaction tx;
      account_action_operation op;
      account_action_create_pod action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on exceeding join_fee");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_update action;

      action.join_fee = asset(WLS_POD_MAX_JOIN_FEE + 1, WLS_SYMBOL);
      action.json_metadata = "";
      action.allow_join = true;
      action.validate();

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test normal");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_update action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.allow_join = true;
      action.validate();

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(pod_action_kick_apply) {
  try {
    ACTORS((alice)(bob)(pod))
    generate_block();
    fund("alice", 10000);
    fund("pod", WLS_MIN_POD_CREATION_FEE);
    generate_block();

    { // create pod
      signed_transaction tx;
      account_action_operation op;
      account_action_create_pod action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }
    { // join request
      signed_transaction tx;
      pod_action_operation op;
      pod_action_join_request action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";

      op.account = "alice";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }
    { // accept request
      signed_transaction tx;
      pod_action_operation op;
      pod_action_accept_join_request action;

      action.account = "alice";

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on a kick on a non member");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_kick action;

      action.account = "bob";
      action.memo = "a memo";
      action.validate();

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
    }

    BOOST_TEST_MESSAGE("--- Test normal");
    {
      signed_transaction tx;
      pod_action_operation op;
      pod_action_kick action;

      action.account = "alice";
      action.memo = "a memo";
      action.validate();

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(friend_action_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: friend_action_validate");

    ACTORS((alice)(bob))
    generate_block();

    friend_action_operation op;
    op.account = "alice";
    op.another = "bob";

    BOOST_TEST_MESSAGE("--- Test valid friend_action_send_request");
    {
      friend_action_send_request action;
      action.memo = "a memo";
      action.validate();

//      op.action = action;
//      op.validate();
    }

    /**
     * nothing to validate on
     * - friend_action_cancel_request
     * - friend_action_accept_request
     * - friend_action_reject_request
     * - friend_action_unfriend
     */
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(friend_action_authorities) {
  try {
    BOOST_TEST_MESSAGE("Testing: friend_action_authorities");
    // TODO:

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(friend_action_apply) {
  /**
   * break this test into
   * - friend_action_send_request
   * - friend_action_cancel_request
   * - friend_action_accept_request
   * - friend_action_reject_request
   * - friend_action_unfriend
   */

  try {
    BOOST_TEST_MESSAGE("Testing: friend_action_apply");
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(friend_action_send_request_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: friend_action_send_request");

    ACTORS((alice)(bob))
    generate_block();

    signed_transaction tx;
    friend_action_send_request action;
    friend_action_operation op;

    op.account = "alice";
    op.another = "bob";

    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);

    BOOST_TEST_MESSAGE("--- Test failure on invalid to account");
    {
      op.another = "invalid";

      action.memo = "a memo";
      action.validate();

      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test valid friend_action_send_request");
    {
      op.account = "alice";
      op.another = "bob";

      action.memo = "a memo";
      action.validate();

      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on duplicated requests");
    {
      op.account = "alice";
      op.another = "bob";

      action.memo = "a memo";
      action.validate();

      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(friend_action_cancel_request_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: friend_action_cancel_request_apply");

    ACTORS((alice)(bob)(charlie))
    generate_block();

    signed_transaction tx;
    friend_action_operation op;

    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);

    {
      friend_action_send_request action;

      action.memo = "a memo";
      action.validate();

      op.account = "alice";
      op.another = "bob";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on non exist request");
    {
      friend_action_cancel_request action;
      action.validate();

      op.account = "alice";
      op.another = "charlie";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test valid");
    {
      friend_action_cancel_request action;
      action.validate();

      op.account = "alice";
      op.another = "bob";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(friend_action_accept_request_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: friend_action_accept_request_apply");

    ACTORS((alice)(bob)(charlie))
    generate_block();

    signed_transaction tx;
    friend_action_operation op;

    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);

    {
      friend_action_send_request action;

      action.memo = "a memo";
      action.validate();

      op.account = "alice";
      op.another = "bob";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on non exist request");
    {
      friend_action_accept_request action;
      action.validate();

      op.account = "bob";
      op.another = "charlie";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(bob_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test valid");
    {
      friend_action_accept_request action;
      action.validate();

      op.account = "bob";
      op.another = "alice";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(bob_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(friend_action_reject_request_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: friend_action_accept_request_apply");

    ACTORS((alice)(bob)(charlie))
    generate_block();

    signed_transaction tx;
    friend_action_operation op;

    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);

    {
      friend_action_send_request action;

      action.memo = "a memo";
      action.validate();

      op.account = "alice";
      op.another = "bob";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on non exist request");
    {
      friend_action_reject_request action;
      action.validate();

      op.account = "bob";
      op.another = "charlie";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(bob_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test valid");
    {
      friend_action_reject_request action;
      action.validate();

      op.account = "bob";
      op.another = "alice";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(bob_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(friend_action_unfriend_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: friend_action_unfriend_apply");

    ACTORS((alice)(bob)(charlie))
    generate_block();

    signed_transaction tx;
    friend_action_operation op;

    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);

    {
      friend_action_send_request action;

      action.memo = "a memo";
      action.validate();

      op.account = "alice";
      op.another = "bob";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

    {
      friend_action_accept_request action;
      action.validate();

      op.account = "bob";
      op.another = "alice";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(bob_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on non exist friend");
    {
      friend_action_unfriend action;
      action.validate();

      op.account = "bob";
      op.another = "charlie";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(bob_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
    }

    BOOST_TEST_MESSAGE("--- Test valid");
    {
      friend_action_unfriend action;
      action.validate();

      op.account = "bob";
      op.another = "alice";
      op.action = action;
      op.validate();

      tx.operations.clear();
      tx.signatures.clear();
      tx.operations.push_back(op);
      tx.sign(bob_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_validate) {
  /**
   * break this test into
   * - social_action_comment_create
   * - social_action_comment_update
   * - social_action_comment_delete
   * - social_action_claim_vesting_reward
   * - social_action_claim_vesting_reward_tip
   * - social_action_user_tip
   * - social_action_comment_tip
   */
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_validate");
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_comment_create_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_comment_create_validate");

    ACTORS((alice))
    generate_block();
    fund("alice", WLS_MIN_POD_CREATION_FEE);
    generate_block();

    BOOST_TEST_MESSAGE("--- Test failure on specifying both pod and allow_friends at the same time");
    {
      social_action_comment_create action;
      action.permlink = "test";
      action.parent_author = "";
      action.parent_permlink = "";
      action.pod = "pod";
      action.allow_friends = true;
      action.title = "Test";
      action.body = "Test content.";
      action.json_metadata = "";
      BOOST_REQUIRE_THROW(action.validate(), fc::assert_exception);
    }

    BOOST_TEST_MESSAGE("--- Test valid");
    {
      social_action_comment_create action;
      action.permlink = "test";
      action.parent_author = "";
      action.parent_permlink = "";
      action.pod = "pod";
      // action.allow_friends = true;
      action.title = "Test";
      action.body = "Test content.";
      action.json_metadata = "";
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_comment_update_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_comment_update_validate");

    BOOST_TEST_MESSAGE("--- Test failure on nothing to update");
    {
      social_action_comment_update action;
      action.permlink = "test";
      BOOST_REQUIRE_THROW(action.validate(), fc::assert_exception);
    }

    BOOST_TEST_MESSAGE("--- Test valid");
    {
      social_action_comment_update action;
      action.permlink = "test";
      action.title = "Test";
      action.body = "test content.";
      action.json_metadata = "";
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_comment_delete_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_comment_delete_validate");

    BOOST_TEST_MESSAGE("--- Test valid");
    {
      social_action_comment_delete action;
      action.permlink = "test";
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_claim_vesting_reward_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_claim_vesting_reward_validate");

    BOOST_TEST_MESSAGE("--- Test valid");
    {
      social_action_claim_vesting_reward action;
      action.amount = asset(1, WLS_SYMBOL);
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_claim_vesting_reward_tip_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_claim_vesting_reward_tip_validate");

    BOOST_TEST_MESSAGE("--- Test valid");
    {
      social_action_claim_vesting_reward_tip action;
      action.amount = asset(1, WLS_SYMBOL);
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_user_tip_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_user_tip_validate");

    BOOST_TEST_MESSAGE("--- Test valid");
    {
      social_action_user_tip action;
      action.amount = asset(1, WLS_SYMBOL);
      action.to = "bob";
      action.memo = "test";
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_comment_tip_validate) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_comment_tip_validate");

    BOOST_TEST_MESSAGE("--- Test valid");
    {
      social_action_comment_tip action;
      action.author = "alice";
      action.permlink = "test";
      action.amount = asset(1, WLS_SYMBOL);
      action.memo = "test";
      action.validate();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_authorities) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_authorities");
    // TODO:

    validate_database();
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_apply) {
  /**
   * break this test into
   * - social_action_comment_create_apply
   * - social_action_comment_update_apply
   * - social_action_comment_delete_apply
   * - social_action_claim_vesting_reward_apply
   * - social_action_claim_vesting_reward_tip_apply
   * - social_action_user_tip_apply
   * - social_action_comment_tip_apply
   */

  try {
    BOOST_TEST_MESSAGE("Testing: social_action_apply");
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_comment_create_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_comment_create_apply");
    generate_blocks(WLS_BLOCKS_PER_DAY/24);
    /**
     * context:
     * - alice and bob are friends
     * - charlie and dave are members of pod
     */
    ACTORS((alice)(bob)(charlie)(dave)(pod))
    generate_block();
    fund("alice", WLS_MIN_POD_JOIN_REQUEST_FEE);
    fund("bob", WLS_MIN_POD_JOIN_REQUEST_FEE);
    fund("charlie", WLS_MIN_POD_JOIN_REQUEST_FEE);
    fund("dave", WLS_MIN_POD_JOIN_REQUEST_FEE);
    fund("pod", WLS_MIN_POD_CREATION_FEE);
    generate_block();

    {
      // alice sends a friend request to bob
      signed_transaction tx;
      friend_action_operation op;
      friend_action_send_request action;

      action.memo = "a memo";
      action.validate();

      op.account = "alice";
      op.another = "bob";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }
    {
      // bob accepts friend request of alice
      signed_transaction tx;
      friend_action_operation op;
      friend_action_accept_request action;

      action.validate();

      op.account = "bob";
      op.another = "alice";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(bob_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
    }
    {
      // create pod
      signed_transaction tx;
      account_action_operation op;
      account_action_create_pod action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.json_metadata = "";
      action.fee = asset(WLS_MIN_POD_CREATION_FEE, WLS_SYMBOL);
      action.validate();

      op.account = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }
    {
      // charlie sends join request to pod
      signed_transaction tx;
      pod_action_operation op;
      pod_action_join_request action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";

      op.account = "charlie";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(charlie_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }
    {
      // dave sends join request to pod
      signed_transaction tx;
      pod_action_operation op;
      pod_action_join_request action;

      action.join_fee = asset(0, WLS_SYMBOL);
      action.fee = asset(WLS_MIN_POD_JOIN_REQUEST_FEE, WLS_SYMBOL);
      action.memo = "a memo";

      op.account = "dave";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(dave_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }
    {
      // pod accepts join request of charlie
      signed_transaction tx;
      pod_action_operation op;
      pod_action_accept_join_request action;

      action.account = "charlie";

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }
    {
      // pod accepts join request of dave
      signed_transaction tx;
      pod_action_operation op;
      pod_action_accept_join_request action;

      action.account = "dave";

      op.account = "pod";
      op.pod = "pod";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test valid on public root comment");
    {
      signed_transaction tx;
      social_action_operation op;
      social_action_comment_create action;

      action.permlink = "alice-public-root-comment";
      action.parent_author = "";
      action.parent_permlink = "test";
      action.allow_friends = false;
      action.title = "Alice's public root comment";
      action.body = "test content.";
      action.json_metadata = "";
      action.validate();

      op.account = "alice";
      op.action = action;

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test valid on comment to public root comment");
    {
      signed_transaction tx;
      social_action_operation op;
      social_action_comment_create action;

      action.permlink = "alice-public-root-comment-charlie-reply";
      action.parent_author = "alice";
      action.parent_permlink = "alice-public-root-comment";
      action.title = "Charlie's comment to Alice's public root comment";
      action.body = "test content.";
      action.json_metadata = "";
      action.validate();

      op.account = "charlie";
      op.action = action;

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(charlie_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }

    generate_blocks(100); // generates 100 block to pass 5 minutes

    BOOST_TEST_MESSAGE("--- Test valid on private root comment");
    {
      signed_transaction tx;
      social_action_operation op;
      social_action_comment_create action;

      action.permlink = "alice-private-root-comment";
      action.parent_author = "";
      action.parent_permlink = "test";
      action.allow_friends = true;
      action.title = "Alice's private root comment";
      action.body = "test content.";
      action.json_metadata = "";
      action.validate();

      op.account = "alice";
      op.action = action;

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on comment to private root comment");
    {
      signed_transaction tx;
      social_action_operation op;
      social_action_comment_create action;

      action.permlink = "alice-private-root-comment-charlie-reply";
      action.parent_author = "alice";
      action.parent_permlink = "alice-private-root-comment";
      action.title = "Charlie's comment to Alice's private root comment";
      action.body = "test content.";
      action.json_metadata = "";
      action.validate();

      op.account = "charlie";
      op.action = action;

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(charlie_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test valid on comment to private root comment");
    {
      signed_transaction tx;
      social_action_operation op;
      social_action_comment_create action;

      action.permlink = "alice-private-root-comment-bob-reply";
      action.parent_author = "alice";
      action.parent_permlink = "alice-private-root-comment";
      action.title = "Bob's comment to Alice's private root comment";
      action.body = "test content.";
      action.json_metadata = "";
      action.validate();

      op.account = "bob";
      op.action = action;

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(bob_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on comment to nested private root comment");
    {
      /**
       * only friend can comment to root post or nested comment.
       * Eg.,
       * A and B a friend, B and C are friend.
       * A creates a post P1 (root-post)
       * B add a comment C1 to P1
       * C add a comment C2 to C1 <== this! (C should not be allowed to add comment if C is not A friend).
       */

      {
        // bob sends a friend request to charlie
        signed_transaction tx;
        friend_action_operation op;
        friend_action_send_request action;

        action.memo = "a memo";
        action.validate();

        op.account = "bob";
        op.another = "charlie";
        op.action = action;
        op.validate();

        tx.operations.push_back(op);
        tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
        tx.sign(bob_private_key, db->get_chain_id());
        db->push_transaction(tx, 0);
        validate_database();
        generate_block();
      }
      {
        // charlie accepts friend request of bob
        signed_transaction tx;
        friend_action_operation op;
        friend_action_accept_request action;

        action.validate();

        op.account = "charlie";
        op.another = "bob";
        op.action = action;
        op.validate();

        tx.operations.push_back(op);
        tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
        tx.sign(charlie_private_key, db->get_chain_id());
        db->push_transaction(tx, 0);
        validate_database();
      }
      {
        signed_transaction tx;
        social_action_operation op;
        social_action_comment_create action;

        action.permlink = "alice-private-root-comment-bob-reply-charlie-reply";
        action.parent_author = "bob";
        action.parent_permlink = "alice-private-root-comment-bob-reply";
        action.title = "Charlie's comment to Bob's comment which is comment of Alice's private root comment";
        action.body = "test content.";
        action.json_metadata = "";
        action.validate();

        op.account = "charlie";
        op.action = action;

        tx.operations.push_back(op);
        tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
        tx.sign(charlie_private_key, db->get_chain_id());
        WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
        validate_database();
        generate_block();
      }
    }

    BOOST_TEST_MESSAGE("--- Test valid on pod root comment of pod creator");
    {
      signed_transaction tx;
      social_action_operation op;
      social_action_comment_create action;

      action.permlink = "pod-pod-root-comment";
      action.parent_author = "";
      action.parent_permlink = "test";
      action.allow_friends = false;
      action.title = "Pod's root comment in pod";
      action.body = "test content.";
      action.json_metadata = "";
      action.pod = "pod";
      action.validate();

      op.account = "pod";
      op.action = action;

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(pod_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test valid on pod root comment");
    {
      signed_transaction tx;
      social_action_operation op;
      social_action_comment_create action;

      action.permlink = "charlie-pod-root-comment";
      action.parent_author = "";
      action.parent_permlink = "test";
      action.allow_friends = false;
      action.title = "Charlie's pod root comment";
      action.body = "test content.";
      action.json_metadata = "";
      action.validate();

      op.account = "charlie";
      op.action = action;

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(charlie_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();

      generate_block();
    }

    BOOST_TEST_MESSAGE("--- Test failure on comment of non member");
    {
      signed_transaction tx;
      social_action_operation op;
      social_action_comment_create action;

      action.permlink = "charlie-pod-root-comment-alice-reply";
      action.parent_author = "charlie";
      action.parent_permlink = "test";
      action.title = "Alice's comment on pod";
      action.body = "test content.";
      action.json_metadata = "";
      action.validate();

      op.account = "alice";
      op.action = action;

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
      tx.sign(charlie_private_key, db->get_chain_id());
      BOOST_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);
      validate_database();

      generate_block();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_claim_vesting_reward_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_claim_vesting_reward_apply");
//    resize_shared_mem (128 * 1024 * 1024); // 128MB
    generate_block();
    ACTORS((alice)(bob))
    fund("alice", 1000000);
    vest("alice", 1000000);

    BOOST_TEST_MESSAGE("Test normal");
    {
      generate_blocks(WLS_VESTING_REWARD_BLOCKS_PER_CYCLE - (db->get_dynamic_global_properties().head_block_number % WLS_VESTING_REWARD_BLOCKS_PER_CYCLE));

      signed_transaction tx;
      social_action_operation op;
      social_action_claim_vesting_reward action;

      {  // figure out how much user could claim
        const auto &new_alice = db->get_account("alice");
        const auto &cprops = db->get_dynamic_global_properties();

        int64_t account_vshares = 0;
        if (new_alice.vr_snaphost_next_cycle < cprops.vr_cycle) {
          account_vshares = new_alice.vesting_shares.amount.value;
        } else if (new_alice.vr_snaphost_cycle < cprops.vr_cycle) {
          account_vshares = new_alice.vr_snaphost_shares.amount.value;
        }

        uint128_t vest(account_vshares);
        uint128_t total_vest(cprops.vr_snaphost_shares.amount.value);
        uint128_t fund(cprops.vr_snaphost_payout.amount.value);
        uint128_t max_uint128 = vest * fund / total_vest;
        const auto max = max_uint128.to_uint64();

        action.amount = asset(max, WLS_SYMBOL);
      }

      action.validate();
      op.account = "alice";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_claim_vesting_reward_tip_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_claim_vesting_reward_tip_apply");
    generate_block();
    ACTORS((alice)(bob))
    fund("alice", 1000000);
    vest("alice", 1000000);

    BOOST_TEST_MESSAGE("Test normal");
    {
      generate_blocks(WLS_VESTING_REWARD_BLOCKS_PER_CYCLE - (db->get_dynamic_global_properties().head_block_number % WLS_VESTING_REWARD_BLOCKS_PER_CYCLE));

      signed_transaction tx;
      social_action_operation op;
      social_action_claim_vesting_reward_tip action;

      {  // figure out how much user could claim
        const auto &new_alice = db->get_account("alice");
        const auto &cprops = db->get_dynamic_global_properties();

        int64_t account_vshares = 0;
        if (new_alice.vr_snaphost_next_cycle < cprops.vr_cycle) {
          account_vshares = new_alice.vesting_shares.amount.value;
        } else if (new_alice.vr_snaphost_cycle < cprops.vr_cycle) {
          account_vshares = new_alice.vr_snaphost_shares.amount.value;
        }

        uint128_t vest(account_vshares);
        uint128_t total_vest(cprops.vr_snaphost_shares.amount.value);
        uint128_t fund(cprops.vr_snaphost_payout.amount.value);
        uint128_t max_uint128 = vest * fund / total_vest;
        const auto max = max_uint128.to_uint64();

        action.amount = asset(max, WLS_SYMBOL);
      }

      action.validate();
      op.account = "alice";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      BOOST_REQUIRE(db->get_account("alice").reward_steem_balance == action.amount );

      validate_database();
      generate_block();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_user_tip_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_user_tip_apply");
    generate_block();
    ACTORS((alice)(bob))
    fund("alice", 2000000);
    vest("alice", 1000000);

    BOOST_TEST_MESSAGE("Test normal");
    {
      signed_transaction tx;
      social_action_operation op;
      social_action_user_tip action;
      account_action_operation account_op;
      account_action_transfer_to_tip transfer_to_tip_action;

      { // transfer 1 TESTS to tip_balance
        transfer_to_tip_action.amount = asset(1000, WLS_SYMBOL);
        transfer_to_tip_action.validate();

        account_op.account = "alice";
        account_op.action = transfer_to_tip_action;
        account_op.validate();

        tx.clear();
        tx.operations.push_back(account_op);
        tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
        tx.sign(alice_private_key, db->get_chain_id());
        db->push_transaction(tx, 0);
      }

      action.amount = transfer_to_tip_action.amount;
      action.to = "";  ///< if null, then same as from
      action.memo = "test";
      action.validate();
      op.account = "alice";
      op.action = action;
      op.validate();

      tx.clear();
      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      BOOST_REQUIRE(db->get_account("alice").reward_vesting_steem == transfer_to_tip_action.amount );

      validate_database();
      generate_block();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(social_action_comment_tip_apply) {
  try {
    BOOST_TEST_MESSAGE("Testing: social_action_comment_tip_apply");
    generate_block();
    ACTORS((alice)(bob))
    fund("alice", 2000000);
    vest("alice", 1000000);

    BOOST_TEST_MESSAGE("Test normal");
    {
      signed_transaction tx;
      social_action_operation op;
      social_action_comment_tip action;
      social_action_comment_create comment_create_action;
      account_action_operation account_op;
      account_action_transfer_to_tip transfer_to_tip_action;

      { // transfer 1 TESTS to tip_balance
        transfer_to_tip_action.amount = asset(1000, WLS_SYMBOL);
        transfer_to_tip_action.validate();

        account_op.account = "alice";
        account_op.action = transfer_to_tip_action;
        account_op.validate();

        tx.clear();
        tx.operations.push_back(account_op);
        tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
        tx.sign(alice_private_key, db->get_chain_id());
        db->push_transaction(tx, 0);
      }

      { // create a comment
        comment_create_action.permlink = "test";
        comment_create_action.parent_author = "";
        comment_create_action.parent_permlink = "test";
        comment_create_action.allow_friends = false;
        comment_create_action.title = "test";
        comment_create_action.body = "test content.";
        comment_create_action.json_metadata = "";
        comment_create_action.validate();

        op.account = "alice";
        op.action = comment_create_action;
        op.validate();

        tx.clear();
        tx.operations.push_back(op);
        tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
        tx.sign(alice_private_key, db->get_chain_id());
        db->push_transaction(tx, 0);
      }

      action.author = "alice";
      action.permlink = "test";
      action.amount = transfer_to_tip_action.amount;
      action.memo = "test";
      action.validate();
      op.account = "alice";
      op.action = action;
      op.validate();

      tx.clear();
      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);

      BOOST_REQUIRE(db->get_account("alice").reward_vesting_steem == transfer_to_tip_action.amount );

      validate_database();
      generate_block();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_SUITE_END()
#endif
