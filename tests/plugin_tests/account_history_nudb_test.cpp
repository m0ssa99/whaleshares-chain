#include <boost/test/unit_test.hpp>

#include <wls/plugins/acc_hist_nudb/nudbstore_database.hpp>

using namespace wls::plugins::acc_hist_nudb;

BOOST_AUTO_TEST_SUITE(acc_hist_nudb_test)

BOOST_AUTO_TEST_CASE(pack_unpack_test) {
  BOOST_TEST_MESSAGE("--- Test pack_unpack");

  //-------------------------------------------------
  // pack this key
  acc_hist_object_nudb_key key{"test", 1};

  size_t packed_key_size = fc::raw::pack_size(key);
  char packed_key[packed_key_size];
  fc::raw::pack<acc_hist_object_nudb_key>(packed_key, packed_key_size, key);

  // unpack it
  acc_hist_object_nudb_key key2;
  fc::raw::unpack<acc_hist_object_nudb_key>(packed_key, packed_key_size, key2, 0);

//  BOOST_CHECK_EQUAL(key.account, key2.account);
  BOOST_CHECK_EQUAL(key.sequence, key2.sequence);

  //-------------------------------------------------
  // pack this obj
  acc_hist_object_nudb item;
  item.block = 1;

  size_t packed_obj_size = fc::raw::pack_size(item);
  char packed_obj[packed_obj_size];
  fc::raw::pack<acc_hist_object_nudb>(packed_obj, packed_obj_size, item);

  // unpack it
  acc_hist_object_nudb item2;
  fc::raw::unpack<acc_hist_object_nudb>(packed_obj, packed_obj_size, item2, 0);
  BOOST_CHECK_EQUAL(item.block, item2.block);
}

BOOST_AUTO_TEST_CASE(nudbstore_database_test) {
  BOOST_TEST_MESSAGE("--- Test nudbstore_database");

  nudbstore_database db;
  boost::system::error_code ec;

  // clean (delete) db if exist first
  db.clean();

  // try to open it
  db.open();

  acc_hist_object_nudb_key key{"test", 1};

  // test insert
  {
    BOOST_TEST_MESSAGE("--- test insert");
    acc_hist_object_nudb item;
    auto test_data = "test_data";

    // item.trx_id
    {
      wls::protocol::digest_type::encoder enc;
      fc::raw::pack(enc, test_data);
      wls::protocol::digest_type digest_data = enc.result();

      // put hash result to item.trx_id
      memcpy(item.trx_id._hash, digest_data._hash, std::min(sizeof(item.trx_id), sizeof(digest_data)));
    }

    item.block = 1;
    item.trx_in_block = 1;
    item.op_in_trx = 1;
    item.virtual_op = 0;
    item.timestamp = fc::time_point::now();

    ilog("insert: ${item}", ("item", item));
    BOOST_CHECK_NO_THROW(db.insert(key, item, ec));
  }

  // test fetch
  {
    BOOST_TEST_MESSAGE("--- test fetch");
    acc_hist_object_nudb item = db.fetch(key, ec);
    ilog("fetch: ${item}", ("item", item));

    // {"trx_id":"0a2c4497a1016e91745cecb8abe079c312ff745b","block":1,"trx_in_block":1,"op_in_trx":1,"virtual_op":0,"timestamp":"2019-04-22T15:27:55","serialized_op":"09746573745f64617461"}
    BOOST_CHECK_EQUAL(item.trx_id, fc::ripemd160("0a2c4497a1016e91745cecb8abe079c312ff745b"));
    BOOST_CHECK_EQUAL(item.block, 1);
    BOOST_CHECK_EQUAL(item.trx_in_block, 1);
    BOOST_CHECK_EQUAL(item.op_in_trx, 1);
    BOOST_CHECK_EQUAL(item.virtual_op, 0);
  }

  // close it
  db.close();
}

BOOST_AUTO_TEST_SUITE_END()