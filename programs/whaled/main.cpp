#include <appbase/application.hpp>

#include <wls/protocol/version.hpp>
#include <wls/utilities/git_revision.hpp>
#include <wls/utilities/key_conversion.hpp>

#include <wls/plugins/acc_hist_nudb/acc_hist_nudb_plugin.hpp>
#include <wls/plugins/account_by_key/account_by_key_plugin.hpp>
#include <wls/plugins/account_by_key_api/account_by_key_api_plugin.hpp>
#include <wls/plugins/account_history/account_history_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>
#include <wls/plugins/blogfeed/blogfeed_plugin.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/database_api/database_api_plugin.hpp>
#include <wls/plugins/debug_node/debug_node_plugin.hpp>
#include <wls/plugins/follow/follow_plugin.hpp>
#include <wls/plugins/follow_api/follow_api_plugin.hpp>
#include <wls/plugins/myfeed/myfeed_plugin.hpp>
#include <wls/plugins/network_broadcast_api/network_broadcast_api_plugin.hpp>
#include <wls/plugins/p2p/p2p_plugin.hpp>
#include <wls/plugins/podfeed/podfeed_plugin.hpp>
#include <wls/plugins/pod_announcement/pod_announcement_plugin.hpp>
#include <wls/plugins/sharesfeed/sharesfeed_plugin.hpp>
#include <wls/plugins/tags/tags_plugin.hpp>
#include <wls/plugins/webserver/webserver_plugin.hpp>
#include <wls/plugins/witness/witness_plugin.hpp>
#include <wls/plugins/snapshot/snapshot_plugin.hpp>
#include <wls/plugins/tips_sent/tips_sent_plugin.hpp>
#include <wls/plugins/tips_received/tips_received_plugin.hpp>
#include <wls/plugins/notification/notification_plugin.hpp>
#include <wls/plugins/podtags/podtags_plugin.hpp>
#include <wls/plugins/account_tags/account_tags_plugin.hpp>
#include <wls/plugins/block_missed/block_missed_plugin.hpp>

#include <fc/exception/exception.hpp>
#include <fc/thread/thread.hpp>
#include <fc/interprocess/signals.hpp>
#include <fc/log/console_appender.hpp>
#include <fc/log/file_appender.hpp>
#include <fc/log/logger.hpp>
#include <fc/log/logger_config.hpp>
#include <fc/git_revision.hpp>
#include <fc/stacktrace.hpp>

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

#include <iostream>
#include <fstream>
#include <csignal>
#include <vector>

namespace wls {
namespace plugins {

void register_plugins() {
  appbase::app().register_plugin< wls::plugins::acc_hist_nudb::acc_hist_nudb_plugin >();
  appbase::app().register_plugin< wls::plugins::account_by_key::account_by_key_plugin >();
  appbase::app().register_plugin< wls::plugins::account_by_key::account_by_key_api_plugin >();
  appbase::app().register_plugin< wls::plugins::account_history::account_history_plugin >();
  appbase::app().register_plugin< wls::plugins::block_history::block_history_plugin >();
  appbase::app().register_plugin< wls::plugins::blogfeed::blogfeed_plugin >();
  appbase::app().register_plugin< wls::plugins::chain::chain_plugin >();
  appbase::app().register_plugin< wls::plugins::database_api::database_api_plugin >();
  appbase::app().register_plugin< wls::plugins::debug_node::debug_node_plugin >();
  appbase::app().register_plugin< wls::plugins::follow::follow_plugin >();
  appbase::app().register_plugin< wls::plugins::follow::follow_api_plugin >();
  appbase::app().register_plugin< wls::plugins::myfeed::myfeed_plugin >();
  appbase::app().register_plugin< wls::plugins::network_broadcast_api::network_broadcast_api_plugin >();
  appbase::app().register_plugin< wls::plugins::p2p::p2p_plugin >();
  appbase::app().register_plugin< wls::plugins::podfeed::podfeed_plugin >();
  appbase::app().register_plugin< wls::plugins::pod_announcement::pod_announcement_plugin >();
  appbase::app().register_plugin< wls::plugins::sharesfeed::sharesfeed_plugin >();
  appbase::app().register_plugin< wls::plugins::tags::tags_plugin >();
  appbase::app().register_plugin< wls::plugins::webserver::webserver_plugin >();
  appbase::app().register_plugin< wls::plugins::witness::witness_plugin >();
  appbase::app().register_plugin< wls::plugins::snapshot::snapshot_plugin >();
  appbase::app().register_plugin< wls::plugins::tips_sent::tips_sent_plugin >();
  appbase::app().register_plugin< wls::plugins::tips_received::tips_received_plugin >();
  appbase::app().register_plugin< wls::plugins::notification::notification_plugin >();
  appbase::app().register_plugin< wls::plugins::podtags::podtags_plugin >();
  appbase::app().register_plugin< wls::plugins::account_tags::account_tags_plugin >();
  appbase::app().register_plugin< wls::plugins::block_missed::block_missed_plugin >();
}

}  // namespace plugins
}  // namespace wls


/////////////////////////////////////////////////////////////////////////////////////////////////////////////


using namespace wls;
using wls::protocol::version;
namespace bpo = boost::program_options;

void write_default_logging_config_to_stream(std::ostream &out);

fc::optional<fc::logging_config> load_logging_config_from_ini_file(const fc::path &config_ini_filename);

void logo() {
  // print ascii-art
  std::cerr << "\n\n\n";
  std::cerr << "                   ;s##5||||5##w;           \n";
  std::cerr << "               ;#||||||||||||||||||#m       \n";
  std::cerr << "             #||@@@8w|||||||||||||||||w     \n";
  std::cerr << "           {O||||@@@R5||||||;|||||||||||y   \n";
  std::cerr << "          #||||a@@@@@@@@@@@@@@@@w|||||||j#  \n";
  std::cerr << "         #|||#@@@@@@@@@@@@@@@@@@@@W|||||5|# \n";
  std::cerr << "        ]O|3@@@@@###@@@@@@@@@@@7@@@#||||]|@m\n";
  std::cerr << "        @|]@@@MO|||||||79@@@@@@@@@@@m|||O|@G\n";
  std::cerr << "        @5@@|@@U|||||||||||#@@@@@@@@F|J||]@M\n";
  std::cerr << "        ||@M@@@#||||#m|||||||||75**T|||||@@[\n";
  std::cerr << "         %##@@@@#||||||||||||||||||||||]@@|`\n";
  std::cerr << "         \"%]@@@@@@m||||||||||||||||||]@@@#>\n";
  std::cerr << "           #@@@@@@@@@#m]@@@####5|7#@@@@@|\\  \n";
  std::cerr << "            75@@@@@@@@@@@@@@@@m|||||#@#<    \n";
  std::cerr << "              |@@@@@@@@@@@@@@@@@@####7      \n";
  std::cerr << "                 \"7=5#@@@@@@@@##M|\\         \n";
  std::cerr << "                        `\"\"^`               \n";

#ifdef IS_TEST_NET
  std::cerr << "------------------------------------------------------\n\n";
  std::cerr << "            STARTING TEST NETWORK\n\n";
  std::cerr << "------------------------------------------------------\n";
  auto initminer_private_key = wls::utilities::key_to_wif(WLS_INIT_PRIVATE_KEY);
  std::cerr << "initminer public key: " << WLS_INIT_PUBLIC_KEY_STR << "\n";
  std::cerr << "initminer private key: " << initminer_private_key << "\n";
  std::cerr << "chain id: " << std::string(WLS_CHAIN_ID) << "\n";
  std::cerr << "blockchain version: " << fc::string(WLS_BLOCKCHAIN_VERSION) << "\n";
  std::cerr << "------------------------------------------------------\n";
#else
  std::cerr << "------------------------------------------------------\n\n";
  std::cerr << "            STARTING WHALESHARES NETWORK\n\n";
  std::cerr << "------------------------------------------------------\n";
  std::cerr << "initminer public key: " << WLS_INIT_PUBLIC_KEY_STR << "\n";
  std::cerr << "chain id: " << std::string(WLS_CHAIN_ID) << "\n";
  std::cerr << "blockchain version: " << fc::string(WLS_BLOCKCHAIN_VERSION) << "\n";
  std::cerr << "------------------------------------------------------\n";
#endif
}

string &version_string() {
  static string v_str = "whaleshares_blockchain_version: " + fc::string(WLS_BLOCKCHAIN_VERSION) + "\n" +
                        "whaleshares_git_revision:       " + fc::string(wls::utilities::git_revision_sha) + "\n" +
                        "fc_git_revision:                " + fc::string(fc::git_revision_sha) + "\n";
  return v_str;
}

int main(int argc, char **argv) {
  try {
    logo();

    //      bpo::options_description app_options("Whaleshares Daemon");
    //      bpo::options_description cfg_options("Whaleshares Daemon");
    //      app_options.add_options()(
    //         "help,h", "Print this help message and exit."
    //      )(
    //         "data-dir,d", bpo::value<boost::filesystem::path>()->default_value("witness_node_data_dir"),
    //         "Directory containing databases, configuration file, etc."
    //      )(
    //         "version,v", "Print whaled version and exit."
    //      );

    bpo::options_description options;
    options.add_options()("backtrace", bpo::value<string>()->default_value("yes"), "Whether to print backtrace on SIGSEGV");
    options.add_options()("lightnode", bpo::value<bool>()->default_value(false), "a fullnode or a light-node(delayed-node)");
    appbase::app().add_program_options(bpo::options_description(), options);

    wls::plugins::register_plugins();
    appbase::app().set_version_string(version_string());
    appbase::app().set_app_name("whaled");

    // These plugins are included in the default config
    // clang-format off
    appbase::app().set_default_plugins<
       wls::plugins::witness::witness_plugin,
       wls::plugins::account_by_key::account_by_key_plugin,
       wls::plugins::account_by_key::account_by_key_api_plugin
    >();

    // These plugins are loaded regardless of the config
    bool initialized = appbase::app().initialize<
       wls::plugins::chain::chain_plugin,
       wls::plugins::p2p::p2p_plugin,
       wls::plugins::webserver::webserver_plugin
    >(argc, argv);
    // clang-format on

    if (!initialized) return 0;

    auto &args = appbase::app().get_args();
    if (args.at("backtrace").as<string>() == "yes") {
      fc::print_stacktrace_on_segfault();
      ilog("Backtrace on segfault is enabled.");
    }

    fc::path data_dir = appbase::app().data_dir();

    //      fc::path config_ini_path = data_dir / "config.ini";
    //      if (fc::exists(config_ini_path)) {
    //         // get the basic options
    //         bpo::store(bpo::parse_config_file<char>(config_ini_path.preferred_string().c_str(), cfg_options, true), options);
    //      } else {
    //         ilog("Writing new config file at ${path}", ("path", config_ini_path));
    //         if (!fc::exists(data_dir))
    //            fc::create_directories(data_dir);
    //
    //         std::ofstream out_cfg(config_ini_path.preferred_string());
    //         for (const boost::shared_ptr<bpo::option_description> od : cfg_options.options()) {
    //            if (!od->description().empty())
    //               out_cfg << "# " << od->description() << "\n";
    //            boost::any store;
    //            if (!od->semantic()->apply_default(store))
    //               out_cfg << "# " << od->long_name() << " = \n";
    //            else {
    //               auto example = od->format_parameter();
    //               if (example.empty())
    //                  // This is a boolean switch
    //                  out_cfg << od->long_name() << " = " << "false\n";
    //               else {
    //                  // The string is formatted "arg (=<interesting part>)"
    //                  example.erase(0, 6);
    //                  example.erase(example.length() - 1);
    //                  out_cfg << od->long_name() << " = " << example << "\n";
    //               }
    //            }
    //            out_cfg << "\n";
    //         }
    //
    //         out_cfg.close();
    //      }

    //////////////////////////////
    // log config
    fc::path log_ini_path = data_dir / "log.ini";
    if (fc::exists(log_ini_path)) {
      // try to get logging options from the config file.
      try {
        fc::optional<fc::logging_config> logging_config = load_logging_config_from_ini_file(log_ini_path);
        if (logging_config) fc::configure_logging(*logging_config);
      } catch (const fc::exception &) {
        wlog("Error parsing logging config from config file ${config}, using default config", ("config", log_ini_path.preferred_string()));
      }
    } else {
      ilog("Writing new log config file at ${path}", ("path", log_ini_path));
      if (!fc::exists(data_dir)) fc::create_directories(data_dir);

      std::ofstream out_cfg(log_ini_path.preferred_string());
      write_default_logging_config_to_stream(out_cfg);
      out_cfg.close();

      // read the default logging config we just wrote out to the file and start using it
      fc::optional<fc::logging_config> logging_config = load_logging_config_from_ini_file(log_ini_path);
      if (logging_config) fc::configure_logging(*logging_config);
    }

    appbase::app().startup();
    appbase::app().exec();
    std::cout << "exited cleanly\n";

    return 0;
  } catch (const boost::exception &e) {
    std::cerr << boost::diagnostic_information(e) << "\n";
  } catch (const fc::exception &e) {
    std::cerr << e.to_detail_string() << "\n";
  } catch (const std::exception &e) {
    std::cerr << e.what() << "\n";
  } catch (...) {
    std::cerr << "unknown exception\n";
  }

  ilog("done");
  return 0;
}

// logging config is too complicated to be parsed by boost::program_options,
// so we do it by hand
//
// Currently, you can only specify the filenames and logging levels, which
// are all most users would want to change.  At a later time, options can
// be added to control rotation intervals, compression, and other seldom-
// used features
void write_default_logging_config_to_stream(std::ostream &out) {
  out << "# declare an appender named \"stderr\" that writes messages to the console\n"
         "[log.console_appender.stderr]\n"
         "stream=std_error\n\n"
         "# declare an appender named \"p2p\" that writes messages to p2p.log\n"
         "[log.file_appender.p2p]\n"
         "filename=logs/p2p/p2p.log\n"
         "# filename can be absolute or relative to this config file\n\n"
         "# route any messages logged to the default logger to the \"stderr\" logger we\n"
         "# declared above, if they are info level are higher\n"
         "[logger.default]\n"
         "level=info\n"
         "appenders=stderr\n\n"
         "# route messages sent to the \"p2p\" logger to the p2p appender declared above\n"
         "[logger.p2p]\n"
         "level=off\n"
         "appenders=p2p\n\n";
}

fc::optional<fc::logging_config> load_logging_config_from_ini_file(const fc::path &config_ini_filename) {
  try {
    fc::logging_config logging_config;
    bool found_logging_config = false;

    boost::property_tree::ptree config_ini_tree;
    boost::property_tree::ini_parser::read_ini(config_ini_filename.preferred_string().c_str(), config_ini_tree);
    for (const auto &section : config_ini_tree) {
      const std::string &section_name = section.first;
      const boost::property_tree::ptree &section_tree = section.second;

      const std::string console_appender_section_prefix = "log.console_appender.";
      const std::string file_appender_section_prefix = "log.file_appender.";
      const std::string logger_section_prefix = "logger.";

      if (boost::starts_with(section_name, console_appender_section_prefix)) {
        std::string console_appender_name = section_name.substr(console_appender_section_prefix.length());
        std::string stream_name = section_tree.get<std::string>("stream");

        // construct a default console appender config here
        // stdout/stderr will be taken from ini file, everything else hard-coded here
        fc::console_appender::config console_appender_config;
        console_appender_config.level_colors.emplace_back(fc::console_appender::level_color(fc::log_level::debug, fc::console_appender::color::green));
        console_appender_config.level_colors.emplace_back(fc::console_appender::level_color(fc::log_level::warn, fc::console_appender::color::brown));
        console_appender_config.level_colors.emplace_back(fc::console_appender::level_color(fc::log_level::error, fc::console_appender::color::cyan));
        console_appender_config.stream = fc::variant(stream_name).as<fc::console_appender::stream::type>();
        logging_config.appenders.push_back(fc::appender_config(console_appender_name, "console", fc::variant(console_appender_config)));
        found_logging_config = true;
      } else if (boost::starts_with(section_name, file_appender_section_prefix)) {
        std::string file_appender_name = section_name.substr(file_appender_section_prefix.length());
        fc::path file_name = section_tree.get<std::string>("filename");
        if (file_name.is_relative()) file_name = fc::absolute(config_ini_filename).parent_path() / file_name;

        // construct a default file appender config here
        // filename will be taken from ini file, everything else hard-coded here
        fc::file_appender::config file_appender_config;
        file_appender_config.filename = file_name;
        file_appender_config.flush = true;
        file_appender_config.rotate = true;
        file_appender_config.rotation_interval = fc::hours(1);
        file_appender_config.rotation_limit = fc::days(1);
        logging_config.appenders.push_back(fc::appender_config(file_appender_name, "file", fc::variant(file_appender_config)));
        found_logging_config = true;
      } else if (boost::starts_with(section_name, logger_section_prefix)) {
        std::string logger_name = section_name.substr(logger_section_prefix.length());
        std::string level_string = section_tree.get<std::string>("level");
        std::string appenders_string = section_tree.get<std::string>("appenders");
        fc::logger_config logger_config(logger_name);
        logger_config.level = fc::variant(level_string).as<fc::log_level>();
        boost::split(logger_config.appenders, appenders_string, boost::is_any_of(" ,"), boost::token_compress_on);
        logging_config.loggers.push_back(logger_config);
        found_logging_config = true;
      }
    }
    if (found_logging_config)
      return logging_config;
    else
      return fc::optional<fc::logging_config>();
  }
  FC_RETHROW_EXCEPTIONS(warn, "")
}
